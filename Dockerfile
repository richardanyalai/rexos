FROM archlinux:latest

RUN pacman -Syyu --noconfirm

WORKDIR	/opt/rexos

COPY . .

RUN chmod +x ./scripts/setup.sh
RUN ./scripts/setup.sh docker

CMD ["make", "build/rexos.iso"]

# RexOS! changelog

## v1.0 alpha

- Created bootloader
- Own libC
- Own types
- Integer to string
- VGA textmode works
- Added Snake
- Added Pong
- Added TicTacToe
- Added digital clock app
- Own GUI library
- Desktop
- Date widget
- Clock widget

---

## v1.0 beta

- Implemented GDT
- Implemented IDT
- Created interrut handler
- Proper keyboard driver
- Driver for textmode cursor
- Updated the games to use the keyboard driver
- Added a TTY interface
- Own random number generator
- Float to string
- Integer to string in hexadecimal form
- Proper shutdown for QEMU

---

## v1.0

- Implemented paging
- Implemented heap
- Implemented malloc(), free()
- Added initial ramdisk
- Added mouse driver
- Started working on multitasking
- Started working on usermode (Ring 3)
- Started working on libCC
- Implemented std::string
- Implemented std::cout and std::endl
- Implemented std::vector
- Virtual functions are now supported
- Implemented ordered array
- Implemented linked list
- Implemented stack datastructure
- Finished TTY (scrolling, backspace, ctrl+c, sleep, hascii)
- Colored printf
- Added ASCII arts
- Fixed reboot
- Added Game of Life game
- Sqrt, sin, cos, pow, fact math functions
- Oh finally fixed that nasty keyboard driver
- uptime, ls, cat and ogrep commands

---

## v1.1

- VGA 320x200 256-color graphics mode
- VGA 90x60 16-color textmode
- PCI devices
- ATA driver
- Network driver
- Network stack(ARP, IPv4, ICMP, UDP, TCP)
- ping command
- webpussy command (netcat clone)
- Multiplayer chess
- Serial port driver
- Parallel port driver
- Graphics mode font
- Drawing pixels, lines, rectangles, triangles, circles, also images
- Added graphics mode demos
- \_\_udivdi3 and \_\_umoddi3 support
- Improved Makefile
- Replaced own custom types with stdint.h and stddef.h
- Hangman game
- Cowsay clone
- Getting uptime
- Rexfetch (Neofetch / Ufetch clone)
- Getting size of physical memory
- Fixed paging

## v1.2

- ACPI shutdown
- BrainF\*ck interpreter
- sprintf
- scanf
- ctype.h
- Fixed clock and date widgets
- donut.c
- Never gonna give you up "gif" in VGA demos
- FAT32
- Fixed ATA
- Fixed clock
- Fixed all the warnings
- time.h
- Fixed global constructors

## v2.0

- Added support for VESA
- Spinning 3D cube
- TetrOS (Tetris clone)
- Perlin noise VGA graphics mode demo
- Fixed VGA textmode drawing (added back buffer)
- Meteors (Asteroids clone)
- Matrix rain
- Fixed a serious bug that caused page faults
- Mines
- Sudoku
- Pac-Man

# RexOS

<div align="center" style="text-align:center">
	<img src="docs/img/logo.svg" width="40%" style="max-width: 400px" />
</div>

A tiny 32-bit OS written in C, C++ and x86 Assembly.

## Introduction

I’ve always wanted to write my own operating system but I never had the time
nor the knowledge to get started. It was in the summer break after my second
year at the University of Szeged that I decided I have to deepen my C and C++
knowledge and I came up with the idea of writing an operating system. I named
the project after my second dog who died the day I started this project.

This is also
[my Bachelor's thesis](https://gitlab.com/richardanyalai/thesis_bsc)
work at the University of Szeged, so the documentation (in the thesis) is in
Hungarian.

## Specs

- 32-bit Intel CPU (tested on QEMU with the i386 model)
- VGA textmode and VGA graphics mode for displaying content, so it can not be
  booted on modern hardware
- Has an AMD Am79C973 network card driver
- The OS has all the programs built into the kernel

## Features

- [x] System
  - [x] GDT, IDT, TSS
  - [x] Interrupt handling
  - [x] RTC and CMOS
  - [x] Paging
  - [x] Heap
  - [x] Initrd and vfs
  - [ ] Multitasking
  - [ ] Usermode
  - [x] System calls
  - [x] PCI devices
  - [x] ACPI shutdown
  - [x] Small libC and libCXX
  - [x] Abstract data structures
- [x] Drivers
  - [x] VGA driver (both textmode and graphics mode)
  - [x] VESA driver
  - [x] PS/2 keyboard driver
  - [x] PS/2 mouse driver
  - [x] AMD Am79C973 network card driver
  - [x] ATA driver
  - [x] Serial port driver
  - [x] Parallel port driver
  - [x] PC speaker
- [x] Programs
  - [x] Core utils and shell
    - [x] echo, ls, cat, ogrep, sleep, uptime
  - [x] Tools
    - [x] webpussy (netcat clone), ping, lspci, hexdump
    - [x] cowjonas (cowsay clone)
    - [x] rexfetch
    - [x] BrainF\*ck interpreter
  - [x] Games and other programs
    - [x] Date and clock widgets
    - [x] Floating digital clock
    - [x] Snake, Pong, Breakout, Meteors, TetrOS, Mines, Pac-Man, Sudoku,
Tic Tac Toe, Hangman
    - [x] Local multiplayer chess
    - [x] VGA 320x200 video mode demos

## Some screenshots

<img src="docs/img/screenshots/boot.png" height="333px" width="600px" />
<img src="docs/img/screenshots/digiclock.png" height="333px" width="600px" />
<img src="docs/img/screenshots/breakout.png" height="333px" width="600px" />
<img src="docs/img/screenshots/chess.png" height="333px" width="600px" />
<img src="docs/img/screenshots/tetros.png" height="333px" width="600px" />
<img src="docs/img/screenshots/mines.png" height="333px" width="600px" />
<img src="docs/img/screenshots/pacman.png" height="333px" width="600px" />

More screenshots can be found in the [screenshots](./docs/img/screenshots)
folder.

## Getting started

### Setting up the environment

For Arch Linux users:

```bash
make setup
```

Other GNU+Linux distros / macOS:

Install the following packages: __qemu, docker, docker-compose__

### Building the bootable ISO

```bash
make
```

To use VESA graphics instead of VGA uncomment the following line in the file
`kernel/src/include/settings.h`:

```C
//#define VESA
```

### Running the ISO in a virtual machine

```bash
# for QEMU
make run

# for VirtualBox
make vbox

# for building in a Docker container
make docker
```

### Other make commads

```bash
# cleaning build directory
make clean

# verifying multiboot header and showing contents of kernel
make check

# showing SLOC statistics
make code

# listing TODOs
make todo

# listing referenced resources
make ref
```

### Debugging

```bash
# clean the build directory
make clean

# build the kernel with debug symbols
make DEBUG=1

# start debugging
make debug
```

If you use Visual Studio Code you can press the _F5_ button to start debugging
with GDB instead of the procedure described above. You can also set
breakpoints in the code editor.

For more details visit the following pages:
[osdev/GDB](https://wiki.osdev.org/GDB),
[kernel debugging](https://wiki.osdev.org/Kernel_Debugging),
[GDB](https://ftp.gnu.org/old-gnu/Manuals/gdb/html_node/gdb_28.html).

## Documentation

For the full, comprehensive documentation please read the markdown documents in
the [docs](./docs) folder.

## Useful resources

[osdev.org](https://wiki.osdev.org/Main_Page)  
[All of the osdev tutorials](https://wiki.osdev.org/Tutorials)  
[Write your own operating system - YouTube](https://www.youtube.com/channel/UCQdZltW7bh1ta-_nCH7LWYw)  
[wyoos.org](http://wyoos.org/)  
[lowlevel.eu Wiki](http://www.lowlevel.eu/wiki/)  
[Operating System Development Discord server](https://discord.com/invite/RnCtsqD)  
[brokenthorn.com](http://www.brokenthorn.com/Resources/OSDevIndex.html)  
[Poncho's OS tutorials](https://www.youtube.com/watch?v=7LTB4aLI7r0&list=PLxN4E629pPnKKqYsNVXpmCza8l0Jb6l8-)  
[jamesmolloy.co.uk](http://www.jamesmolloy.co.uk)  
[Known bugs in the James Molloy tutorial](https://wiki.osdev.org/James_Molloy%27s_Tutorial_Known_Bugs#Before_you_follow_the_tutorial)  
[Bran's kernel development tutorial](http://www.osdever.net/bkerndev/index.php)  
[gnu.org](https://www.gnu.org/software/grub/manual/multiboot/multiboot.html)  
[codeproject.com](https://www.codeproject.com/Articles/1225196/Create-Your-Own-Kernel-In-C-2)  
[arjunsreedharan.org](https://arjunsreedharan.org/post/82710718100/kernels-101-lets-write-a-kernel)  
[C libraries](https://wiki.osdev.org/C_Library)  
[VGA fonts](https://github.com/viler-int10h/vga-text-mode-fonts)

## Licensing

RexOS is free software: you can redistribute it and/or modify it under the
terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

RexOS is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
RexOS. If not, see <https://www.gnu.org/licenses/>.

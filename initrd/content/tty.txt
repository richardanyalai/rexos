${F10}help	 ${CLC}						- show help
${F10}echo	 ${F09}<text>${CLC}					- prints out text
${F10}ls		 ${F09}<dir>${CLC}					- lists all the files in a directory
${F10}cat		 ${F09}<file>${CLC}					- displays the content of a file
${F10}ogrep	 ${F09}<switch> <text> <file>${CLC}	- searches the given expression in file
${F10}sleep	 ${F09}<millis>${CLC}				- blocks the OS for the given time
${F10}exit	 ${CLC}						- exit terminal
${F10}shutdown ${CLC}/ ${F10}poweroff${CLC}				- shut down computer
${F10}reboot	 ${CLC}/ ${F10}restart${CLC}				- reboot computer
${F10}lspci	 ${F09}<switch>${CLC}				- lists PCI devices
${F10}uptime	 ${CLC}						- prints the elapsed time since boot
${F10}clock	 ${CLC}						- show clock
${F10}date	 ${CLC}						- show date
${F10}license	 ${CLC}						- show legal information

To read about game controls use ${F10}cat games.txt${CLC}.

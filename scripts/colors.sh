#!/bin/bash
#
# This file is part of RexOS!.
#
# RexOS! is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# RexOS! is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with RexOS!.  If not, see <https://www.gnu.org/licenses/>.

# Reset
export Color_Off='\033[0m'	# Text Reset

# Regular Colors
export Black='\033[0;30m'	# Black
export Red='\033[0;31m'		# Red
export Green='\033[0;32m'	# Green
export Yellow='\033[0;33m'	# Yellow
export Blue='\033[0;34m'	# Blue
export Purple='\033[0;35m'	# Purple
export Cyan='\033[0;36m'	# Cyan
export White='\033[0;37m'	# White

# Bold
export BBlack='\033[1;30m'	# Black
export BRed='\033[1;31m'	# Red
export BGreen='\033[1;32m'	# Green
export BYellow='\033[1;33m'	# Yellow
export BBlue='\033[1;34m'	# Blue
export BPurple='\033[1;35m'	# Purple
export BCyan='\033[1;36m'	# Cyan
export BWhite='\033[1;37m'	# White

# Underline
export UBlack='\033[4;30m'	# Black
export URed='\033[4;31m'	# Red
export UGreen='\033[4;32m'	# Green
export UYellow='\033[4;33m'	# Yellow
export UBlue='\033[4;34m'	# Blue
export UPurple='\033[4;35m'	# Purple
export UCyan='\033[4;36m'	# Cyan
export UWhite='\033[4;37m'	# White

#!/bin/bash
#
# This file is part of RexOS!.
#
# RexOS! is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# RexOS! is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with RexOS!.  If not, see <https://www.gnu.org/licenses/>.

source scripts/colors.sh

if [ -f "/etc/arch-release" ]
then
	echo "Installing necessary packages"
	if [ "$1" = "docker" ]
	then
		pacman -S --noconfirm $(cat scripts/packages) && success=1
	else
		sudo pacman -S --noconfirm $(cat scripts/packages) qemu qemu-emulators-full && success=1
	fi
else
	printf "${Red}Unsupported distro!${Color_Off}\n"
	printf "Please install Arch Linux or run ${Blue}make docker${Color_Off}\n"
fi

if [ "$success" = 1 ]
then
	printf "${Green}Successfully installed necessary packages${Color_Off}\n"
else
	printf "${Red}Failed to install necessary packages${Color_Off}\n"
fi

#!/bin/bash
#
# This file is part of RexOS!.
#
# RexOS! is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# RexOS! is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with RexOS!.  If not, see <https://www.gnu.org/licenses/>.

source scripts/colors.sh

files_count=0
lines_count=0

for ext in S c h cpp hpp
do
	files=$(											\
		find . -regextype sed -regex ".*/*\.${ext}$"	\
		-type f											\
		! -path "./.*"									\
		! -path "./build/*"								\
	)

	num=0
	count=0

	for file in $files
	do
		count=$((count + 1))
		num=$((num + $(egrep -v '^[[:space:]]*(;|\*|#|//|/\*|<!--)' $file | awk 'NF' | wc -l)))
	done

	files_count=$((files_count + count))
	lines_count=$((lines_count + num))

	if [ ! $count = 0 ]
	then
		[ "${ext}" = "S"	] && color="${Green}"	&& type="Assembly	"
		[ "${ext}" = "c"	] && color="${Yellow}"	&& type="C code		"
		[ "${ext}" = "cpp"	] && color="${Cyan}"	&& type="C++ code	"
		[ "${ext}" = "h"	] && color="${BYellow}"	&& type="C header	"
		[ "${ext}" = "hpp"	] && color="${BCyan}"	&& type="C++ header	"

		echo -e "${color}${type}${Color_Off}: ${Blue}${num}${Color_Off} lines of code in ${Green}${count}${Color_Off} files"
	fi
done

if [ $files_count -gt 0 ]
then
	echo "-----------------------------------------------"
	echo -e "Totally		: ${Blue}${lines_count}${Color_Off} lines of code in ${Green}${files_count}${Color_Off} files"
fi

#!/bin/bash
#
# This script lists all the TODO tasks and REF links in the project.
# Takes one optional argument, which is used to determine if we
# want to list all TODOs or all REFs.
#
# This file is part of RexOS!.
#
# RexOS! is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# RexOS! is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with RexOS!.  If not, see <https://www.gnu.org/licenses/>.

source scripts/colors.sh

arg="TODO: "
[ "$1" = "ref" ] && arg="REF: "

for file in $(find . -maxdepth 10 -mindepth 1 -type f -not -path "./.git*" -not -name "*todo.sh*")
do
	grep -n "${arg}" "$file" | while read -r line
	do
		line_number=$(echo "$line" | cut -d : -f 1)
		todo=$(echo $line | awk '{print $2}' FS="${arg}")
		[ ! -z "$todo" ] && echo -e "${file} ${Blue}(${line_number})${Color_Off}: ${Yellow}${todo}${Color_Off}"
	done
done

#!/bin/bash
#
# This file is part of RexOS!.
#
# RexOS! is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# RexOS! is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with RexOS!.  If not, see <https://www.gnu.org/licenses/>.

cp -r initrd/ build/
cd build/initrd/

gcc -o create_initrd create_initrd.c
mv create_initrd content/ && cd content/

files=$(ls -I "*initrd*")
content=""

for file in $files
do
	content="${content} ${file} ${file}"
done

./create_initrd ${content}
mv initrd.img ../../iso/boot/

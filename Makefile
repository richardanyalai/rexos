# This file is part of RexOS!.
#
# RexOS! is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# RexOS! is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with RexOS!.  If not, see <https://www.gnu.org/licenses/>.

######################
###    Programs    ###
######################

SHELL		:=	/bin/sh
QEMU		:=	qemu-system-i386

######################
###  Directories   ###
######################

KERNEL_DIR	:=	kernel
INITRD_DIR	:=	initrd

SCRIPTS		:=	scripts

BUILD_DIR	:=	build
ISO_DIR		:=	$(BUILD_DIR)/iso
BOOT_DIR	:=	$(ISO_DIR)/boot
GRUB_DIR	:=	$(BOOT_DIR)/grub

KERNEL		:=	$(BUILD_DIR)/kernel.elf
INITRD		:=	$(BOOT_DIR)/initrd.img
ISO			:=	$(BUILD_DIR)/rexos.iso
HDD			:=	$(BUILD_DIR)/hdd.qcow2

######################
###      Flags     ###
######################

WARNINGS	:=	$(WARNINGS) -Wall -Wextra -Werror

# Networking flags
# REF: https://gist.github.com/extremecoders-re/e8fd8a67a515fee0c873dcafc81d811c
NETWORKING	:=	-netdev user,id=nd0,ifname=tap0								\
				-device pcnet,netdev=nd0,mac=52:55:00:d1:55:01

QEMUFLAGS	:=	-m 18M														\
				-boot order=d,menu=off										\
				-serial stdio												\
				-drive id=disk,file=$(HDD),if=none,format=qcow2				\
				-device ide-hd,drive=disk,bus=ide.0,unit=0					\
				-rtc base=localtime											#\
				#-cpu host 													\
				#-enable-kvm													\
				#-audiodev sdl,id=sdl,out.frequency=48000,out.channels=2,out.format=s32	\
				#-machine pcspk-audiodev=sdl

######################
###    Sources     ###
######################

INITRD_CONT	:=	$(shell find initrd/content/*)

######################
###    Targets     ###
######################

TARGETS		:=	$(ISO) $(HDD)

all: run

# Creating a bootable iso image
$(ISO): $(KERNEL) $(INITRD) grub.cfg
	@mkdir -p $(GRUB_DIR)
	@cp grub.cfg $(GRUB_DIR)
	@cp $(KERNEL) $(BOOT_DIR)
	@echo "Creating a bootable ISO image"
	@rm -rf $(ISO)
	@grub-mkrescue -o $(ISO) $(ISO_DIR)
	@printf "Size of ISO image: " && ls -l --b=M $(ISO) | cut -d " " -f5 && echo

# Kernel image
$(KERNEL): FORCE
	@make -C kernel

# Creates an initrd image
$(INITRD): $(INITRD_CONT)
	@mkdir -p $(GRUB_DIR)
	@rm -rf $(BUILD_DIR)/$(INITRD_DIR)
	@bash $(SCRIPTS)/create_initrd.sh > /dev/null

# Creates a hard disk image
$(HDD):
	@mkdir -p $(BUILD_DIR)
	@echo "Creating hard disk image"
	@qemu-img create -f qcow2 $(HDD) 4M > /dev/null

.PHONY: all run vbox gdb setup todo ref check code clean

run: $(TARGETS)
	@printf "\nBooting RexOS\n\n"
	@$(QEMU) $(QEMUFLAGS) -cdrom $(ISO)

vbox: $(TARGETS)
	@pkill VirtualBoxVM && sleep 1 || true
	@VBoxManage startvm "RexOS"

docker: $(HDD)
	@docker compose up
	@$(QEMU) $(QEMUFLAGS) -cdrom $(ISO)

debug:
	@$(QEMU) -S -gdb tcp::1234 $(QEMUFLAGS) -cdrom $(ISO) &
	@gdb -ex "target remote localhost:1234" build/kernel.elf

setup:
	@bash $(SCRIPTS)/setup.sh

todo:
	@bash $(SCRIPTS)/todo.sh

ref:
	@bash $(SCRIPTS)/todo.sh ref

check:
	@bash $(SCRIPTS)/check_multiboot.sh

code:
	@bash $(SCRIPTS)/code.sh

clean:
	@rm -rf $(BUILD_DIR)

FORCE:

/* This file is part of RexOS!.
 *
 * RexOS! is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * RexOS! is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with RexOS!.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <gui/graphics_mode.h>
#include <stdint.h>

void vga_draw_image(
	const uint8_t *image,
	uint16_t start_x,
	uint16_t start_y,
	uint16_t width,
	uint16_t height
)
{
	for (uint16_t y = 0; y < height; ++y)
		for (uint16_t x = 0; x < width; ++x)
			vga_draw_pixel(
				x + start_x,
				y + start_y,
				image[x+y*width]
			);
}

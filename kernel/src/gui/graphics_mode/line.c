/* This file is part of RexOS!.
 *
 * RexOS! is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * RexOS! is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with RexOS!.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <gui/graphics_mode.h>
#include <stdint.h>
#include <stdlib.h>

void vga_draw_line(
	uint16_t x1, uint16_t y1, uint16_t x2, uint16_t y2, uint8_t color
)
{
	int16_t dx		= x2 - x1;
	int16_t dy		= y2 - y1;
	int16_t steps	= 0;

	if (abs(dx) > abs(dy))
		steps = abs(dx);
	else
		steps = abs(dy);

	double x = x1, y = y1, x_inc, y_inc;

	x_inc = dx / (float) steps;
	y_inc = dy / (float) steps;

	for (int16_t v = 0; v < steps; ++v)
	{
		x += x_inc;
		y += y_inc;

		vga_draw_pixel(x, y, color);
	}
}

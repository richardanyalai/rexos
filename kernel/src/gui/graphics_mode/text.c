/* This file is part of RexOS!.
 *
 * RexOS! is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * RexOS! is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with RexOS!.  If not, see <https://www.gnu.org/licenses/>.
 */

// REF: https://github.com/szhou42/osdev/blob/master/src/kernel/gui/font_parser.c

#include <drivers/VGA.h>
#include <gui/graphics_mode.h>
#include <stdint.h>
#include <string.h>

extern const uint8_t vga_graphics_mode_font[2048];

void vga_draw_string(
	uint16_t start_x, uint16_t start_y,
	const char *text,
	char bgcolor, uint8_t fgcolor
)
{
	uint16_t col = start_x, row = start_y, stop = 0, len = strlen(text);

	for (uint16_t i = 0; i < len; ++i)
	{
		switch (text[i])
		{
			case '\n':
				row++;
				col = 0;
				break;

			case '\r':
				break;

			case '\t':
				col += 4 - col % 4;
				break;

			case '\0':
				stop = 1;
				break;

			default:
				col++;
				break;
		}

		if (stop)
			break;

		for (uint8_t y = 0; y < 8; ++y)
			for (uint8_t x = 0; x < 8; ++x)
				if ((vga_graphics_mode_font[text[i] * 8 + y] >> (7 - x) & 1))
					vga_draw_pixel((col - 1) * 8 + x, y + row * 8, fgcolor);
				else if (bgcolor > -1)
					vga_draw_pixel((col - 1) * 8 + x, y + row * 8, bgcolor);
	}
}

/* This file is part of RexOS!.
 *
 * RexOS! is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * RexOS! is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with RexOS!.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <gui/graphics_mode.h>
#include <stdint.h>

void vga_draw_ellipse(
	uint16_t xc, uint16_t yc, uint16_t rx, uint16_t ry, uint8_t color
)
{
	float
		x	= 0,
		y	= ry,
		dx	= 2 * ry * ry * x,
		dy	= 2 * rx * rx * y,
		d1	= (ry * ry) - (rx * rx * ry) + (0.25 * rx * rx),
		d2	= 0;

	// For region 1
	while (dx < dy)
	{
		// Print points based on 4-way symmetry
		vga_draw_pixel(xc + x, yc + y, color);
		vga_draw_pixel(xc - x, yc + y, color);
		vga_draw_pixel(xc + x, yc - y, color);
		vga_draw_pixel(xc - x, yc - y, color);

		// Checking and updating value of
		// decision parameter based on algorithm
		if (d1 < 0)
		{
			x++;
			dx = dx + (2 * ry * ry);
			d1 = d1 + dx + (ry * ry);
		}
		else
		{
			x++;
			y--;
			dx = dx + (2 * ry * ry);
			dy = dy - (2 * rx * rx);
			d1 = d1 + dx - dy + (ry * ry);
		}
	}

	// Decision parameter of region 2
	d2 =	((ry * ry) * ((x + 0.5) * (x + 0.5))) +
			((rx * rx) * ((y - 1) * (y - 1))) -
			(rx * rx * ry * ry);

	// Plotting points of region 2
	while (y >= 0)
	{

		// Print points based on 4-way symmetry
		vga_draw_pixel(xc + x, yc + y, color);
		vga_draw_pixel(xc - x, yc + y, color);
		vga_draw_pixel(xc + x, yc - y, color);
		vga_draw_pixel(xc - x, yc - y, color);

		// Checking and updating parameter
		// value based on algorithm 
		if (d2 > 0)
		{
			dy = dy - (2 * rx * rx);
			d2 = d2 + (rx * rx) - dy;
		}
		else
		{
			x++;
			dx = dx + (2 * ry * ry);
			dy = dy - (2 * rx * rx);
			d2 = d2 + dx - dy + (rx * rx);
		}

		y--;
	}
}

/* This file is part of RexOS!.
 *
 * RexOS! is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * RexOS! is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with RexOS!.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <gui/graphics_mode.h>
#include <stdint.h>

void vga_draw_rect(
	uint16_t x1, uint16_t y1, uint16_t x2, uint16_t y2, uint8_t color
)
{
	vga_draw_line(x1, y1, x2, y1, color);
	vga_draw_line(x2, y1, x2, y2, color);
	vga_draw_line(x2, y2, x1, y2, color);
	vga_draw_line(x1, y2, x1, y1, color);
}

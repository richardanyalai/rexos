#include <drivers/VESA.h>

void vesa_draw_bg()
{
	const uint16_t aspect_ratio = VESA_WIDTH / VESA_HEIGHT;
	const uint16_t image_width  = VESA_WIDTH;
	const uint16_t image_height = (uint16_t) (image_width / aspect_ratio);

	for (int16_t j = image_height - 1; j >= 0; --j)
	{
		for (int16_t i = 0; i < image_width; ++i)
		{
			double r = ((double) i) / (image_width - 1);
			double g = ((double) j) / (image_height - 1);
			double b = 0.25;

			vesa_draw_pixel(
				i,
				j,
				vesa_color(
					0,
					(uint32_t) (255.999 * r),
					(uint32_t) (255.999 * g),
					(uint32_t) (255.999 * b)
				)
			);
		}
	}
}

/* This file is part of RexOS!.
 *
 * RexOS! is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * RexOS! is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with RexOS!.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <drivers/VGA.h>
#include <stdbool.h>
#include <stdio.h>

void textmode_draw_rect(
	uint8_t x1, uint8_t y1, uint8_t x2, uint8_t y2, uint8_t bg
)
{
	for (uint8_t i = x1; i < x2; ++i)
		for (uint8_t j = y1; j < y2; ++j)
			vga_draw_char_at(
				' ',
				vga_get_pos(i, j), bg, bg == BLACK ? WHITE : BLACK
			);
}

void draw_window(
	uint8_t x1, uint8_t y1, uint8_t x2, uint8_t y2, uint8_t bg, bool shadow
)
{
	textmode_draw_rect(x1, y1, x2, y2, bg);

	if (shadow)
	{
		for (uint8_t i = x1 + 1; i <= x2; ++i)
			vga_draw_char_at(' ', vga_get_pos(i, y2), BLACK, WHITE);

		for (uint8_t j = y1 + 1; j <= y2; ++j)
			vga_draw_char_at(' ', vga_get_pos(x2, j), BLACK, WHITE);
	}
}

void draw_window_with_border(
	uint8_t x1, uint8_t y1,
	uint8_t x2, uint8_t y2,
	uint8_t bg, uint8_t fg,
	bool shadow
)
{
	draw_window(x1, y1, x2, y2, bg, shadow);

	uint8_t k;

	for (uint8_t i = x1; i < x2; ++i)
		for (uint8_t j = y1; j < y2; ++j)
		{
			bool print = true;

			if (i == x1 && j == y1)
				k = 218;
			else if (i == x1 && j == y2 - 1)
				k = 192;
			else if (i == x2 - 1 && j == y1)
				k = 191;
			else if (i == x2 - 1 && j == y2 - 1)
				k = 217;
			else if (i == x1 || i == x2 - 1)
				k = 179;
			else if (j == y1 || j == y2 - 1)
				k = 196;
			else
				print = false;

			if (print)
				vga_draw_char_at(k, vga_get_pos(i, j), bg, fg);
		}
}

void draw_window_with_title(
	uint8_t x1, uint8_t y1,
	uint8_t x2, uint8_t y2,
	uint8_t title_x,
	uint8_t bg, uint8_t fg,
	char *title,
	bool border,
	bool shadow
)
{
	if (border)
		draw_window_with_border(x1, y1, x2, y2, bg, fg, shadow);
	else
		draw_window(x1, y1, x2, y2, bg, shadow);

	vga_set_index(vga_get_pos(title_x, y1));
	printf(title);
}

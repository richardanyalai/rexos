#include <net/HTTP.h>
#include <iostream.hpp>
#include <stdbool.h>
#include <stddef.h>
#include <unistd.h>

extern bool am79c973_ready;

extern "C" void test()
{
	if (am79c973_ready)
	{
		std::cout << "\n\nRunning HTTP tests:\n";

		std::cout << "GET\n";

		GET("http://localhost:8080/api/get/69");

		sleep(2);

		std::cout << "\n\nPOST\n";

		POST(
			"http://localhost:8080/api/post/",
			"{ \"name\": \"Heisenberg\" }"
		);

		sleep(2);

		std::cout << "\n\nDELETE\n";

		DELETE("http://localhost:8080/api/delete/420", "{}");
	}
}

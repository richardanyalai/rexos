/* This file is part of RexOS!.
 *
 * RexOS! is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * RexOS! is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with RexOS!.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <descriptors/idt.h>
#include <sys/isr.h>
#include <sys/pic.h>
#include <utils.h>
#include <stdint.h>
#include <string.h>

extern isr_t	interrupt_handlers[];
idt_entry_t		idt_entries[256];
idt_ptr_t		idt_pointer;

static void idt_set_gate(
	uint8_t interrupt, uint32_t base, uint16_t selector, uint8_t flags
)
{
	idt_entries[interrupt].base_lo  = base & 0xFFFF;
	idt_entries[interrupt].base_hi  = (base >> 16) & 0xFFFF;

	idt_entries[interrupt].selector = selector;
	idt_entries[interrupt].reserved = 0;
	idt_entries[interrupt].flags    = flags | 0x60;
}

static uint32_t sidt()
{
	idt_ptr_t idt;

	asm volatile ("sidtl (%0)" : : "r" (&idt));

#pragma GCC diagnostic ignored "-Wuninitialized"
	return idt.base;
#pragma GCC diagnostic pop
}

void init_idt()
{
	idt_pointer.limit = sizeof(idt_entry_t) * 256 - 1;
	idt_pointer.base = (uint32_t) &idt_entries;

	memset(&idt_entries, 0, sizeof(idt_entry_t) * 256);

	pic_remap();

	/* ISRs */

	idt_set_gate( 0,  (uint32_t) isr0,   0x08, 0x8E);
	idt_set_gate( 1,  (uint32_t) isr1,   0x08, 0x8E);
	idt_set_gate( 2,  (uint32_t) isr2,   0x08, 0x8E);
	idt_set_gate( 3,  (uint32_t) isr3,   0x08, 0x8E);
	idt_set_gate( 4,  (uint32_t) isr4,   0x08, 0x8E);
	idt_set_gate( 5,  (uint32_t) isr5,   0x08, 0x8E);
	idt_set_gate( 6,  (uint32_t) isr6,   0x08, 0x8E);
	idt_set_gate( 7,  (uint32_t) isr7,   0x08, 0x8E);
	idt_set_gate( 8,  (uint32_t) isr8,   0x08, 0x8E);
	idt_set_gate( 9,  (uint32_t) isr9,   0x08, 0x8E);
	idt_set_gate( 10, (uint32_t) isr10,  0x08, 0x8E);
	idt_set_gate( 11, (uint32_t) isr11,  0x08, 0x8E);
	idt_set_gate( 12, (uint32_t) isr12,  0x08, 0x8E);
	idt_set_gate( 13, (uint32_t) isr13,  0x08, 0x8E);
	idt_set_gate( 14, (uint32_t) isr14,  0x08, 0x8E);
	idt_set_gate( 15, (uint32_t) isr15,  0x08, 0x8E);
	idt_set_gate( 16, (uint32_t) isr16,  0x08, 0x8E);
	idt_set_gate( 17, (uint32_t) isr17,  0x08, 0x8E);
	idt_set_gate( 18, (uint32_t) isr18,  0x08, 0x8E);
	idt_set_gate( 19, (uint32_t) isr19,  0x08, 0x8E);
	idt_set_gate( 20, (uint32_t) isr20,  0x08, 0x8E);
	idt_set_gate( 21, (uint32_t) isr21,  0x08, 0x8E);
	idt_set_gate( 22, (uint32_t) isr22,  0x08, 0x8E);
	idt_set_gate( 23, (uint32_t) isr23,  0x08, 0x8E);
	idt_set_gate( 24, (uint32_t) isr24,  0x08, 0x8E);
	idt_set_gate( 25, (uint32_t) isr25,  0x08, 0x8E);
	idt_set_gate( 26, (uint32_t) isr26,  0x08, 0x8E);
	idt_set_gate( 27, (uint32_t) isr27,  0x08, 0x8E);
	idt_set_gate( 28, (uint32_t) isr28,  0x08, 0x8E);
	idt_set_gate( 29, (uint32_t) isr29,  0x08, 0x8E);
	idt_set_gate( 30, (uint32_t) isr30,  0x08, 0x8E);
	idt_set_gate( 31, (uint32_t) isr31,  0x08, 0x8E);
	idt_set_gate(128, (uint32_t) isr128, 0x08, 0x8E);

	/* IRQs */

	idt_set_gate( 32, (uint32_t) irq0,   0x08, 0x8E);
	idt_set_gate( 33, (uint32_t) irq1,   0x08, 0x8E);
	idt_set_gate( 34, (uint32_t) irq2,   0x08, 0x8E);
	idt_set_gate( 35, (uint32_t) irq3,   0x08, 0x8E);
	idt_set_gate( 36, (uint32_t) irq4,   0x08, 0x8E);
	idt_set_gate( 37, (uint32_t) irq5,   0x08, 0x8E);
	idt_set_gate( 38, (uint32_t) irq6,   0x08, 0x8E);
	idt_set_gate( 39, (uint32_t) irq7,   0x08, 0x8E);
	idt_set_gate( 40, (uint32_t) irq8,   0x08, 0x8E);
	idt_set_gate( 41, (uint32_t) irq9,   0x08, 0x8E);
	idt_set_gate( 42, (uint32_t) irq10,  0x08, 0x8E);
	idt_set_gate( 43, (uint32_t) irq11,  0x08, 0x8E);
	idt_set_gate( 44, (uint32_t) irq12,  0x08, 0x8E);
	idt_set_gate( 45, (uint32_t) irq13,  0x08, 0x8E);
	idt_set_gate( 46, (uint32_t) irq14,  0x08, 0x8E);
	idt_set_gate( 47, (uint32_t) irq15,  0x08, 0x8E);

	asm volatile ("lidtl (%0)" : : "r" (&idt_pointer));

	KOK("Loaded IDT at 0x%.8x", sidt());

	memset(&interrupt_handlers, 0, sizeof(isr_t) * 256);

	asm volatile ("sti");
}

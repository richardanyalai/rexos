/* This file is part of RexOS!.
 *
 * RexOS! is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * RexOS! is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with RexOS!.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <descriptors/gdt.h>
#include <descriptors/tss.h>
#include <utils.h>
#include <string.h>

gdt_entry_t	gdt_entries[6];
gdt_ptr_t	gdt_ptr;
tss_entry_t	tss_entry;

// Lets us access our ASM functions from our C code.
extern void gdt_flush(uint32_t);
extern void tss_flush(void);

// Set the value of one GDT entry.
static void set_gdt_entry(
	uint32_t offset,
	uint32_t base,
	uint32_t limit,
	uint8_t access,
	uint8_t gran
)
{
	gdt_entries[offset].base_lo		= (base & 0xFFFF);
	gdt_entries[offset].base_mid	= (base >> 16) & 0xFF;
	gdt_entries[offset].base_hi		= (base >> 24) & 0xFF;

	gdt_entries[offset].limit_lo	= (limit & 0xFFFF);
	gdt_entries[offset].granularity	= (limit >> 16) & 0x0F;

	gdt_entries[offset].granularity	|= gran & 0xF0;
	gdt_entries[offset].access		= access;

	char segment[11] = {0};

	switch (access)
	{
		case 0x9A: strcpy(segment, "Code");			break;
		case 0x92: strcpy(segment, "Data");			break;
		case 0xFA: strcpy(segment, "User Code");	break;
		case 0xF2: strcpy(segment, "User Data");	break;
		case 0xE9: strcpy(segment, "Task State");	break;
		default:   strcpy(segment, "Null");
	}


	KINF("Initializing %s segment", segment);
}

// Initialise our task state segment structure.
static void write_tss(int num, uint16_t ss0, uint32_t esp0)
{
	// Firstly, let's compute the base and limit of our entry into the GDT.
	uint32_t base	= (uint32_t) &tss_entry;
	uint32_t limit	= base + sizeof(tss_entry);

	// Now, add our TSS descriptor's address to the GDT.
	set_gdt_entry(num, base, limit, 0xE9, 0x00);

	// Ensure the descriptor is initially zero.
	memset(&tss_entry, 0, sizeof(tss_entry));

	tss_entry.ss0	= ss0;	// Set the kernel stack segment.
	tss_entry.esp0	= esp0;	// Set the kernel stack pointer.

	// Here we set the cs, ss, ds, es, fs and gs entries in the TSS. These
	// specify what segments should be loaded when the processor switches to
	// kernel mode. Therefore they are just our normal kernel code/data
	// segments - 0x08 and 0x10 respectively, but with the last two bits set,
	// making 0x0b and 0x13. The setting of these bits sets the RPL (requested
	// privilege level) to 3, meaning that this TSS can be used to switch to
	// kernel mode from ring 3.
	tss_entry.cs	= 0x0B;
	tss_entry.ss	= tss_entry.ds
					= tss_entry.es
					= tss_entry.fs
					= tss_entry.gs
					= 0x13;
}

void set_kernel_stack(uint32_t stack)
{ tss_entry.esp0 = stack; }

static uint32_t sgdt()
{
	gdt_ptr_t gdt;

	asm volatile ("sgdtl (%0)" : : "r" (&gdt));

#pragma GCC diagnostic ignored "-Wuninitialized"
	return gdt.base;
#pragma GCC diagnostic pop
}

// Initializes the Global Desciptor Table
void init_gdt()
{
	gdt_ptr.limit = (sizeof(gdt_entry_t) * 6) - 1;
	gdt_ptr.base  = (uint32_t) &gdt_entries;

	set_gdt_entry(0, 0, 0,			0,		0	);	// 0 segment
	set_gdt_entry(1, 0, 0xFFFFFFFF,	0x9A,	0xCF);	// Code segment
	set_gdt_entry(2, 0, 0xFFFFFFFF,	0x92,	0xCF);	// Data segment
	set_gdt_entry(3, 0, 0xFFFFFFFF,	0xFA,	0xCF);	// User mode code segment
	set_gdt_entry(4, 0, 0xFFFFFFFF,	0xF2,	0xCF);	// User mode data segment

	write_tss(5, 0x10, 0x0);

	gdt_flush((uint32_t) &gdt_ptr);
	tss_flush();

	KOK("Loaded GDT at 0x%.8x", sgdt());
}

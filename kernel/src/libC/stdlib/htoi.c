/* This file is part of RexOS!.
 *
 * RexOS! is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * RexOS! is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with RexOS!.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <stdint.h>

uint32_t htoi(char *hex)
{
	uint32_t val = 0;

	if (*(hex + 1) == 'x')
	{
		(void) *hex++;
		(void) *hex++;
	}

	while (*hex)
	{
		// Get current character then increment.
		uint8_t byte = *hex++;

		// Transform hex character to the 4bit equivalent
		// number, using the ascii table indexes.
		if (byte >= '0' && byte <= '9')
			byte = byte - '0';
		else if (byte >= 'a' && byte <='f')
			byte = byte - 'a' + 10;
		else if (byte >= 'A' && byte <='F')
			byte = byte - 'A' + 10;

		// Shift 4 to make space for new digit,
		// and add the 4 bits of the new digit.
		val = (val << 4) | (byte & 0xF);
	}

	return val;
}

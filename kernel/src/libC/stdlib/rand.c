/* This file is part of RexOS!.
 *
 * RexOS! is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * RexOS! is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with RexOS!.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <sys/pit.h>
#include <limits.h>
#include <stdint.h>
#include <stdlib.h>

static uint64_t next = 1;

void srand(uint32_t seed)
{ next = seed; }

// Generates a random integer.
int rand()
{
	next = next * 1103515245 + 12345;
	return (uint32_t)(next / 65536) % 32768;
}

// Generates a random unsigned short integer in an interval.
int randint(int start, int end)
{
	int r = rand();

	if (r < 0)
		r *= -1;

	int (*rand_in_interval)(int, int) = lambda(int, (int _start, int _end)
	{
		int rand = (r % (_end - _start + 1)) + _start;

		if (rand <= _start)
			rand = _start;
		else if (rand >= _end)
			rand = _end;

		return rand;
	});

	return rand_in_interval(start, end-1);
}

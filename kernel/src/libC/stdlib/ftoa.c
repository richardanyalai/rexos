/* This file is part of RexOS!.
 *
 * RexOS! is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * RexOS! is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with RexOS!.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <stdlib.h>
#include <stdint.h>

#define MAX_PRECISION 10

static const double rounders[MAX_PRECISION + 1] =
{
	0.5,				//  0
	0.05,				//  1
	0.005,				//  2
	0.0005,				//  3
	0.00005,			//  4
	0.000005,			//  5
	0.0000005,			//  6
	0.00000005,			//  7
	0.000000005,		//  8
	0.0000000005,		//  9
	0.00000000005		// 10
};

char *ftoa(double value, char *buffer, uint8_t precision)
{
	char *ptr = buffer;
	char *p = ptr;
	char *p1;
	uint8_t c;
	size_t intPart;

	if (precision > MAX_PRECISION)
		precision = MAX_PRECISION;

	if (value < 0)
	{
		value = -value;
		*ptr++ = '-';
	}

	if (precision <= 0)
	{
		if (value < 1.0)			precision = 6;
		else if (value < 10.0)		precision = 5;
		else if (value < 100.0)		precision = 4;
		else if (value < 1000.0)	precision = 3;
		else if (value < 10000.0)	precision = 2;
		else if (value < 100000.0)	precision = 1;
		else						precision = 2;
	}

	if (precision)
		value += rounders[precision];

	intPart = value;
	value -= intPart;

	if (!intPart)
		*ptr++ = '0';
	else
	{
		p = ptr;

		while (intPart)
		{
			*p++ = '0' + intPart % 10;
			intPart /= 10;
		}

		p1 = p;

		while (p > ptr)
		{
			c = *--p;
			*p = *ptr;
			*ptr++ = c;
		}

		ptr = p1;
	}

	if (precision)
	{
		*ptr++ = '.';
		while (precision--)
		{
			value *= 10.0;
			c = value;
			*ptr++ = '0' + c;
			value -= c;
		}
	}

	*ptr = 0;

	return buffer;
}

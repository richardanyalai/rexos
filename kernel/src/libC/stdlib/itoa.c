/* This file is part of RexOS!.
 *
 * RexOS! is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * RexOS! is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with RexOS!.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <stdlib.h>

// inline function to swap two numbers
void swap(char *x, char *y)
{
	uint8_t t = *x;
	*x = *y;
	*y = t;
}

// function to reverse buffer[i..j]
char *reverse(char *buffer, uint16_t i, uint16_t j)
{
	while (i < j)
		swap(&buffer[i++], &buffer[j--]);

	return buffer;
}

// Iterative function to implement itoa() function in C
char *itoa(int value, char *buffer, uint8_t base)
{
	// invalid input
	if (base < 2 || base > 32)
		return buffer;

	// consider absolute value of number
	int n = abs(value);

	uint16_t i = 0;

	while (n)
	{
		int r = n % base;

		if (r >= 10) 
			buffer[i++] = 65 + (r - 10);
		else
			buffer[i++] = 48 + r;

		n = n / base;
	}

	// if number is 0
	if (i == 0)
		buffer[i++] = '0';

	/*
	If base is 10 and value is negative, the resulting string
	is preceded with a minus sign (-)
	With any other base, value is always considered unsigned
	*/
	if (value < 0 && base == 10)
		buffer[i++] = '-';

	buffer[i] = '\0'; // 0 terminate string

	// reverse the string and return it
	return reverse(buffer, 0, i - 1);
}

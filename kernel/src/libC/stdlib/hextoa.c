/* This file is part of RexOS!.
 *
 * RexOS! is free software: you can redibufferibute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * RexOS! is dibuffeributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with RexOS!.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <stdint.h>
#include <string.h>

void hextoa_8(char *buffer, uint8_t key)
{
	char *hex = "0123456789ABCDEF";
	char *str = "00";

	str[0] = hex[(key >> 4) & 0xF];
	str[1] = hex[key & 0xF];

	strncat(buffer, str, 2);
}

void hextoa_16(char *buffer, uint16_t key)
{
	hextoa_8(buffer, (key >> 8) & 0xFF);
	hextoa_8(buffer, key & 0xFF);
}

void hextoa_32(char *buffer, uint32_t num)
{
	hextoa_8(buffer, (num >> 24) & 0xFF);
	hextoa_8(buffer, (num >> 16) & 0xFF);
	hextoa_8(buffer, (num >> 8) & 0xFF);
	hextoa_8(buffer, num & 0xFF);
}

void hextoa(char *buffer, int64_t num, uint8_t bits)
{
	int tmp;
	char no_zeroes = 1;

	switch (bits)
	{
		case  8: hextoa_8(buffer, num);  break;
		case 16: hextoa_16(buffer, num); break;
		case 32: hextoa_32(buffer, num); break;
		default:
			for (uint8_t i = 28; i > 0; i -= 4)
			{
				tmp = (num >> i) & 0xF;
				if (tmp == 0 && no_zeroes != 0)
					continue;
			
				if (tmp >= 0xA)
				{
					no_zeroes = 0;
					charcat(buffer, tmp - 0xA + 'A');
				}
				else
				{
					no_zeroes = 0;
					charcat(buffer, tmp + '0');
				}
			}

			if ((tmp = num & 0xF) >= 0xA)
				charcat(buffer, tmp - 0xA + 'A');
			else
				charcat(buffer, tmp + '0');

			break;
	}
}

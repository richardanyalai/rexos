/* This file is part of RexOS!.
 *
 * RexOS! is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * RexOS! is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with RexOS!.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <drivers/VGA.h>
#include <ctype.h>
#include <limits.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

static uint8_t bg_bak, fg_bak;

static bool print(const char *data, size_t length)
{
	bg_bak = vga_get_bgcolor();
	fg_bak = vga_get_fgcolor();

	const uint8_t *bytes = (const uint8_t *) data;
	bool is_color = false;

	for (uint64_t i = 0; i < length; ++i)
	{
		// Here come the colors
		if (bytes[i] == '$' && (bytes[i + 1]) == '{')
		{
			is_color = true;

			i += 2;

			char color[3] = { bytes[i + 1], bytes[i+2], '\0' };

			if (isdigit(bytes[i+1]) && isdigit(bytes[i+1]))
			{

				if (bytes[i] == 'F')
					vga_set_fgcolor(atoi(color));
				else if (bytes[i] == 'B')
					vga_set_bgcolor(atoi(color));
			}
			else if (bytes[i] == 'C')
			{
				vga_set_bgcolor(bg_bak);
				vga_set_fgcolor(fg_bak);
			}

			i += 3;
		}

		if (bytes[i] == '\\' && bytes[i + 1] == 'n')
		{
			i += 2;
			putchar('\n');
		}

		if (!(bytes[i] == '}' && is_color))
			if (putchar(bytes[i]) == EOF)
				return false;
	}

	vga_set_bgcolor(bg_bak);
	vga_set_fgcolor(fg_bak);

	return true;
}

int vsprintf(char *str, const char *__restrict format, va_list parameters)
{
	int written = 0, precision = 0;

	for (uint16_t i = 0; i < strlen(str); ++i)
		str[i] = '\0';

	while (*format != '\0')
	{
		uint64_t maxrem = INT_MAX - written;

		if (strstr(format, "\x1b[H"))
		{
			format += 3;
			written += 3;
			vga_set_next_line_index(1);
			vga_set_index(0);
			continue;
		}

		if (strstr(format, "\x1b[2J"))
		{
			format += 4;
			written += 4;
			vga_clear();
			continue;
		}

		if (format[0] != '%' || format[1] == '%')
		{
			if (format[0] == '%')
				format++;

			uint32_t amount = 1;

			while (format[amount] && format[amount] != '%')
				amount++;

			if (maxrem < amount)
				return -1;

			if (!strncat(str, format, amount))
				return -1;

			format += amount;
			written += amount;

			continue;
		}

		const char *format_begun_at = format++;

		precision = -1;

		if (*(format - 1) == '%' && *format == '.')
		{
			format++;

			if (*format != '0')
				precision = (int) *format - 48;

			format++;
		}

		if (*format == 'c')
		{
			format++;
			char c = (char) va_arg(parameters, int);

			if (!maxrem)
				return -1;

			if (!charcat(str, c))
				return -1;

			written++;
		}
		else if (*format == 's')
		{
			format++;
			const char *string = va_arg(parameters, const char *);
			uint64_t len = strlen(string);

			if (precision > 0 && precision < (int) len)
				len = precision;

			if (maxrem < len)
				return -1;

			if (!strncat(str, string, len))
				return -1;

			written += len;
		}
		else if (*format == 'd')
		{
			format++;

			int i = va_arg(parameters, int);

			if (!maxrem)
				return -1;

			char str_num[digits(i) + 1];

			itoa(i, (char *) str_num, 10);

			if (precision && precision > digits(i))
			{
				char padding[precision - digits(i)];

				memset(padding, '0', precision - digits(i));

				strcat(str, padding);
			}

			strncat(str, str_num, strlen(str_num));

			written++;
		}
		else if (*format == 'x')
		{
			format++;

			char bits = -1;

			if (precision == 2)
				bits = 8;

			if (precision == 4)
				bits = 16;

			if (precision == 8)
				bits = 32;

			int i = va_arg(parameters, int);

			if (!maxrem)
				return -1;

			char str_num[12];
			memset(str_num, '\0', 12);

			hextoa(str_num, i, bits);

			strncat(str, str_num, strlen(str_num));

			written++;
		}
		else if (*format == 'f')
		{
			format++;

			double i = va_arg(parameters, double);

			if (!maxrem)
				return -1;

			char str_num[digits(i) + precision + 2];

			ftoa(i, (char *) str_num, precision);

			strncat(str, str_num, strlen(str_num));

			written++;
		}
		else
		{
			format = format_begun_at;
			uint32_t len = strlen(format);

			if (maxrem < len)
				return -1;

			if (!strncat(str, format, len))
				return -1;

			written += len;
			format += len;
		}
	}

	return written;
}

int sprintf(char *str, const char *__restrict format, ...)
{
	va_list parameters;
	va_start(parameters, format);

	int written = vsprintf(str, format, parameters);

	va_end(parameters);

	return written;
}

int printf(const char *__restrict format, ...)
{
	va_list parameters;
	va_start(parameters, format);

	char str[10000];

	int written = vsprintf(str, format, parameters);

	va_end(parameters);

	print(str, strlen(str) - 1);

	return written;
}

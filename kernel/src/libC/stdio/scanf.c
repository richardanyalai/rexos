/* This file is part of RexOS!.
 *
 * RexOS! is free software: you can rediformatibute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * RexOS! is diformatibuted in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	ee the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with RexOS!.	f not, see <https://www.gnu.org/licenses/>.
 */

#include <drivers/keyboard.h>
#include <sys/isr.h>
#include <stdarg.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>

int scanf(const char *format, ...)
{
	va_list vl;
	bool scanf_reading = true;
	int i = 0, j = 0, ret = 0;
	char buf[100] = {0}, c;
	char *out_loc;

	void *curr_kbrd_handler = get_keyboard_handler();

	set_keyboard_handler(lambda(void, (active_keys_t *active_keys)
	{
		c			= active_keys->active_key.char_code;
		uint8_t k	= active_keys->active_key.key_code;

		if (active_keys->active_key.pressed)
		{
			switch (k)
			{
				case KEY_ENTER:
					goto EXIT_SCANF;

				case KEY_BKSPACE:
					buf[i] = '\0';
					i--;
					break;

				default:
					if (c != 0)
					{
						if (c == 'c' && active_keys->ctrl)
							goto EXIT_SCANF;

						buf[i] = c;
						i++;

						null_active();
					}
					break;

EXIT_SCANF:
					scanf_reading = false;
					set_keyboard_handler(curr_kbrd_handler);
					break;
			}
		}

		if (k != 0)
			null_active();
	}));

	while (scanf_reading)
		HALT;

	va_start(vl, format);
	i = 0;

	while (format && format[i])
	{
		if (format[i] == '%')
		{
			i++;
			switch (format[i])
			{
				case 'c':
				{
					*(char *) va_arg(vl, char *) = buf[j];
					j++;
					ret++;
					break;
				}
				case 'd':
				{
					*(int *) va_arg(vl, int *) = strtol(&buf[j], &out_loc, 10);
					j += out_loc -&buf[j];
					ret++;
					break;
				}
				case 'x':
				{
					*(int *) va_arg(vl, int *) = strtol(&buf[j], &out_loc, 16);
					j += out_loc -&buf[j];
					ret++;
					break;
				}
			}
		}
		else
		{
			buf[j] = format[i];
			j++;
		}
		i++;
	}

	va_end(vl);

	return ret;
}

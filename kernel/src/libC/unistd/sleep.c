/* This file is part of RexOS!.
 *
 * RexOS! is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * RexOS! is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with RexOS!.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <sys/isr.h>
#include <sys/pit.h>
#include <stdint.h>
#include <unistd.h>

uint32_t usleep(uint32_t timer_count)
{
	uint32_t end = pit_get_ticks() + timer_count;

	IRQ_RES;

	while (end > pit_get_ticks())
		HALT;

	IRQ_RES;

	return 0;
}

uint32_t sleep(uint32_t timer_count)
{
	usleep(1000 * timer_count);

	return timer_count;
}

/* This file is part of RexOS!.
 *
 * RexOS! is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * RexOS! is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with RexOS!.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <stdbool.h>
#include <stdio.h>
#include <sys/pit.h>
#include <sys/rtc.h>
#include <time.h>

const char *weekday_names[7] =
{ "Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun" };

const char *month_names[12] =
{
	"Jan", "Feb", "Mar",
	"Apr", "May", "Jun",
	"Jul", "Aug", "Sep",
	"Okt", "Nov", "Dec"
};

const uint8_t month_days[12] =
{ 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };

bool is_leap_year(uint32_t y)
{ return (!(y % 4) && (!(y % 400) || y % 100)); }

char *asctime(const struct tm *timeptr)
{
	static char result[26];

	sprintf(
		result,
		"%s %s %.2d %.2d:%.2d:%.2d %d\n",
		weekday_names[timeptr->tm_wday],
		month_names[timeptr->tm_mon-1],
		timeptr->tm_mday,
		timeptr->tm_hour,
		timeptr->tm_min,
		timeptr->tm_sec,
		timeptr->tm_year
	);

	return result;
}

clock_t clock(void)
{ return pit_get_ticks(); }

double difftime(time_t time1, time_t time2)
{ return time1 - time2; }

time_t mktime(struct tm *timeptr)
{
	uint64_t time        = 0;
	uint32_t day_seconds = 24 * 60 * 60;

	for (uint16_t y = 1970; y < timeptr->tm_year; ++y)
		time += (is_leap_year(y) ? 366 : 365) * day_seconds;

	if (timeptr->tm_mon)
		for (uint8_t m = 0; m < timeptr->tm_mon - 1; ++m)
			time += month_days[m] * day_seconds;

	if (timeptr->tm_mday)
		time += (timeptr->tm_mday - 1) * day_seconds;

	if (timeptr->tm_hour)
		time += (timeptr->tm_hour - 1) * 60 * 60;

	time += timeptr->tm_min * 60;
	time += timeptr->tm_sec;

	return time;
}

time_t time(time_t *timer)
{	
	update_clock();

	struct tm time =
	{
		.tm_year	= get_year(),
		.tm_mon		= get_month(),
		.tm_mday	= get_day(),
		.tm_hour	= get_hour(),
		.tm_min		= get_minutes(),
		.tm_sec		= get_seconds()
	};
	
	time_t result = mktime(&time);

	if (timer)
		*timer = result;

	return result;
}

/* This file is part of RexOS!.
 *
 * RexOS! is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * RexOS! is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with RexOS!.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef __TIME_H
#define __TIME_H

#include <stddef.h>
#include <stdint.h>

typedef long long int time_t;
typedef long long int clock_t;

#ifdef __cplusplus
extern "C" {
#endif

typedef struct tm
{
	uint32_t tm_sec;	// seconds,  range 0 to 59
	uint32_t tm_min;	// minutes, range 0 to 59
	uint32_t tm_hour;	// hours, range 0 to 23
	uint32_t tm_mday;	// day of the month, range 1 to 31
	uint32_t tm_mon;	// month, range 0 to 11
	uint32_t tm_year;	// The number of years since 1900
	uint32_t tm_wday;	// day of the week, range 0 to 6
	uint32_t tm_yday;	// day in the year, range 0 to 365
	uint32_t tm_isdst;	// daylight saving time
} timedate_t;

/*
 * Returns a pointer to a string which represents the day and time of the
 * structure timeptr.
 */
char *asctime(const struct tm *timeptr);

// Returns the processor clock time used since the beginning of the program.
clock_t clock(void);

// Returns a string representing the localtime based on the argument timer.
char *ctime(const time_t *timer);

// Returns the difference of seconds between time1 and time2. (time1 - time2)
double difftime(time_t time1, time_t time2);

/*
 * Converts the structure pointed to by timeptr into a time_t value according
 * to the local timezone.
 */
time_t mktime(struct tm *timeptr);

// Calculates the current calendar time and decodes it into time_t format.
time_t time(time_t *timer);

#ifdef __cplusplus
}
#endif

#endif

/* This file is part of RexOS!.
 *
 * RexOS! is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * RexOS! is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with RexOS!.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef __CTYPE_H
#define __CTYPE_H

#ifdef __cplusplus
extern "C" {
#endif

// This function checks whether the passed character is alphanumeric.
int isalnum(int c);

// This function checks whether the passed character is alphabetic.
int isalpha(int c);

// This function checks whether the passed character is decimal digit.
int isdigit(int c);

// This function checks whether the passed character is uppercase letter.
int isupper(int c);

// This function checks whether the passed character is lowercase letter.
int islower(int c);

// This function checks whether the passed character is whitespace.
int isspace(int c);

// This function checks whether a character is a printable character or not.
int isprint(int c);

// This function checks whether a character is a control character or not.
int iscntrl(int c);

#ifdef __cplusplus
}
#endif

#endif

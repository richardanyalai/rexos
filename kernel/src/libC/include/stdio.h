/* This file is part of RexOS!.
 *
 * RexOS! is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * RexOS! is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with RexOS!.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef __STDIO_H
#define __STDIO_H

#include <stdarg.h>

#define EOF -1

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief			Sends formatted output to stdout.
 * @param format	is the format string
 * @param ...		is the list of the arguments
 * @return			the number of characters that are printed
 */
int printf(const char *format, ...);

/**
 * @brief				Sends formatted output to a string pointed to, by str.
 * @param str			is the destination buffer
 * @param format		is the format string
 * @param parameters	is the list of the arguments
 * @return				the number of characters that are copied
 */
int vsprintf(char *str, const char *__restrict format, va_list parameters);

/**
 * @brief			Sends formatted output to a string pointed to, by str.
 * @param str		is the destination buffer
 * @param format	is the format string
 * @param ...		is the list of the arguments
 * @return			the number of characters that are copied
 */
int sprintf(char *str, const char *format, ...);

/**
 * @brief			Reads formatted input from stdin.
 * @param format	is the format string
 * @param ...		is the list of the arguments
 * @return			the number of items of the argument list successfully read
 */
int scanf(const char *format, ...);

/**
 * @brief		Writes a character (an unsigned char) specified by the
 * 				argument char to stdout.
 * @param chr	is the character
 * @return		the character code of the character
 */
int putchar(int chr);

// Gets a character (an unsigned char) from stdin.
int getchar(void);

/**
 * @brief		Writes a string to stdout up to but not including the null
 * 				character.
 				A newline character is appended to the output.
 * @param str	is the string
 * @return		if successful, non-negative value is returned. On error, the
 * 				function returns EOF.
 */
int puts(const char *str);

#ifdef __cplusplus
}
#endif

#endif

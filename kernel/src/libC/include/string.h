/* This file is part of RexOS!.
 *
 * RexOS! is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * RexOS! is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with RexOS!.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef __STRING_H
#define __STRING_H

#include <stddef.h>
#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief			Compares the first n bytes of memory
 *					area str1 and memory area str2.
 * @param str1		is the pointer to a block of memory
 * @param str2		is the pointer to a block of memory
 * @param n			is the number of bytes to be compared
 * @return			< 0 -> str1 < str2; > 0 -> str1 > str2; 0 -> str1 == str2
 */
int memcmp(const void *str1, const void *str2, size_t n);

/**
 * @brief			Copies n characters from src to dest.
 * @param dest		is a pointer to the destination array where the content
 *					is to be copied, type-casted to a pointer of type void *
 * @param src		is a pointer to the source of data to be copied,
 *					type-casted to a pointer of type void *
 * @param n			is the number of bytes to be copied
 */
void *memcpy(void *dest, const void *src, size_t n);

/**
 * @brief			Copies n characters from str2 to str1, but for overlapping
 *					memory blocks, is a safer approach than memcpy.
 * @see				memcpy
 * @param str1		is a pointer to the destination array where the content
 *					is to be copied, type-casted to a pointer of type void *
 * @param str2		is a pointer to the source of data to be copied,
 *					type-casted to a pointer of type void *
 * @param n			is the number of bytes to be copied
 * @return			a pointer to the destination, which is str1
 */
void *memmove(void *str1, const void *str2, size_t n);

/**
 * @brief			Copies the character c (an unsigned char)
 *					to the first n characters of the string pointed
 *					to, by the argument str.
 * @param str		is a pointer to the block of memory to fill
 * @param c			is the value to be set. The value is passed as an int,
 * 					but the function fills the block of memory using
 * 					the unsigned char conversion of this value
 * @param n			is the number of bytes to be set to the value
 */
void *memset(void *str, int c, size_t n);

/**
 * @brief			Concatenates a char to the end of a string.
 * @param str		is the string
 * @param ch		is the character
 * @return			a pointer to the concatenated string
 */
char *charcat(char *str, uint8_t ch);

/**
 * @brief			Concatenates two strings.
 * @param dest		is the first string
 * @param src		is the second string
 * @return			a pointer to the concatenated string
 */
char *strcat(char *dest, const char *src);

/**
 * @brief			Concatenates two strings.
 * @param dest		is the first string
 * @param str		is the second string
 * @param n			is the length of the source
 * @return			a pointer to the concatenated string
 */
char *strncat(char *dest, const char *src, size_t n);

/**
 * @brief			the string pointed to, by str1 to the string pointed to by str2.
 * @param str1		is the first string
 * @param str2		is the second string
 * @return			0 if th two strings match, otherwise str 1 -str2
 */
int strcmp(const char *str1, const char *str2);

/**
 * @brief			Copies the source to the destination buffer.
 * @param dest		is the destination
 * @param src		is the source
 * @return			a pointer to the destination
 */
char *strcpy(char *dest, const char *src);

/**
 * @brief			Copies the source to the destination buffer.
 * @param dest		is the destination
 * @param src		is the source
 * @param n			is the length of the source
 * @return			a pointer to the destination
 */
char *strncpy(char *dest, const char *src, size_t n);

/**
 * @brief			Calculates the length of a string.
 * @param str		is the string we want to get the length of
 * @return			the result
 */
size_t strlen(const char *str);

/**
 * @brief			Searches for the first occurence of a substring in a string.
 * @param haystack	is the string
 * @param needle	is the substring we are looking for
 * @return			a pointer to the first occurence of the substring
 */
char *strstr(const char *haystack, const char *needle);

/**
 * @brief			Converts a string to have only uppercase letters.
 * @param str		is the string we want convert
 * @return			the result
 */
char *to_upper(char *str);

/**
 * @brief			Converts a string to have only lowercase letters.
 * @param str		is the string we want convert
 * @return			the result
 */
char *to_lower(char *str);

#ifdef __cplusplus
}
#endif

#endif

/* This file is part of RexOS!.
 *
 * RexOS! is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * RexOS! is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with RexOS!.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef __LIMITS_H
#define __LIMITS_H

#define CHAR_BIT			   8
#define SCHAR_MIN		   - 128
#define SCHAR_MAX		   + 127
#define UCHAR_MAX			 255
#define CHAR_MIN		   - 128
#define CHAR_MAX		   + 127
#define MB_LEN_MAX			  16
#define SHRT_MIN		 - 32768
#define SHRT_MAX		 + 32767
#define USHRT_MAX		   65535
#define INT_MIN		- 2147483648
#define INT_MAX		+ 2147483647
#define UINT_MAX	  4294967295
#define LONG_MIN	- 2147483648
#define LONG_MAX	+ 2147483647
#define ULONG_MAX	  4294967295

#endif

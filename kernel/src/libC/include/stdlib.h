/* This file is part of RexOS!.
 *
 * RexOS! is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * RexOS! is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with RexOS!.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef __STDLIB_H
#define __STDLIB_H

#include <stddef.h>
#include <stdint.h>

/**
 * @brief 			Lambda function implementation for C.
 * REF: https://stackoverflow.com/a/3378514/5596516
 * @param type		is the return type of the lambda function
 * @param body		is the function body
 */
#define lambda(type, body)	\
	({						\
		type name body		\
		name;				\
	})

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief			Calculates the absolute value of an integer.
 * @param number	is the number we want to get the absolute value of
 * @return			the result
 */
int abs(int x);

/**
 * @brief			Converts string to integer.
 * @param str		is the string we want to convert
 * @return			the input as a string
 */
int atoi(const char *str);

/**
 * @brief			Converts hex string to integer.
 * @param str		is the hex string we want to convert
 * @return			the input as a string
 */
int htoi(const char *str);

/**
 * @brief			Converts an integer number to string.
 * @param value		is the integer number
 * @param buffer	is the character array we use to store the digits
 * @param base		is the base of the conversion
 * @return			the input as a string
 */
char *itoa(int value, char *buffer, uint8_t base);

/**
 * @brief			Converts a double number to string.
 * @param value		is the double floatng point number
 * @param buffer	is the character array we use to store the digits
 * @param precision	is the precision we want to use while rounding
 * @return			the input as a string
 */
char *ftoa(double value, char *buffer, uint8_t precision);

/**
 * @brief			Converts an integer number to hexadecimal string.
 * @param value		is the number
 * @param buffer	is the character array we use to store the digits
 * @return			the input number as a hexadecimal string
 */
void hextoa(char *buffer, uint64_t key, uint8_t bits);
void hextoa_8(char *buffer, uint8_t key);
void hextoa_16(char *buffer, uint16_t key);
void hextoa_32(char *buffer, uint32_t key);

/**
 * Generates a random integer.
 * @return a random integer 
 */
int rand(void);

/**
 * @brief			Creates a random integer in
 *					a given interval using rand.
 * @see				rand
 * @param start		is the start point of an interval
 * @param end		is the end point of an interval
 * @return			the result
 */
int randint(int start, int end);

/**
 * @brief			This function seeds the random number generator used by the
 *					function rand.
 * @param seed		is the seed
 */
void srand(uint32_t seed);

/**
 * @brief			Converts the initial part of the string in str to a long int
 *					value according to the given base, which must be between 2
 *					and 36 inclusive, or be the special value 0.
 * @param str		is the string containing the representation of an integral
 					number
 * @param endptr	is the reference to an object of type char*, whose value is
 					set by the function to the next character in str after the
 *					numerical value
 * @param base		is the base, which must be between 2 and 36 inclusive, or
 					be the special value 0
 * @return			the result
 */
long int strtol(const char *str, char **endptr, int base);

/**
 * @brief			Counts the digits of an integer number.
 * @param number	is the number we want to get the digits of
 * @return			the result
 */
uint8_t digits(int number);

/**
 * @brief			Deallocates the memory previously allocated by a call
 *					to calloc, malloc, or realloc.
 * @param ptr		is the pointer to the memory addres
 *					we want to make free
 */
void free(void *ptr);

/**
 * @brief			Allocates the requested memory
 *					and returns a pointer to it.
 * @param size		is the size of memory to be allocated
 * @return			pointer to the allocated memory chunk
 */
void *malloc(size_t size);

// Aborts the functions of the OS.
void abort(void) __attribute__((__noreturn__));

#ifdef __cplusplus
}
#endif

#endif

/* This file is part of RexOS!.
 *
 * RexOS! is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * RexOS! is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with RexOS!.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef __MATH_H
#define __MATH_H

#define PI					3.1415926535897932384650288

#define MAX(A, B)			((A) > (B) ? (A) : (B))
#define MIN(A, B)			((A) < (B) ? (A) : (B))
#define BETWEEN(X, A, B)	((A) <= (X) && (X) <= (B))

#define RAD(X)				(X * (PI / 180.0))

#define tan(X)				(sin(X) / cos(X))
#define cotan(X)			(cos(X) / sin(X))

#ifdef __cplusplus
extern "C" {
#endif

// Calculates the square root of x.
double sqrt(double x);

// Calculates the sine of x.
double sin(double x);

// Calculates the cosine of x.
double cos(double x);

// Calculates the nth power of x.
double pow(double x, double y);

// Absolute value of a double
double fabs(double x);

#ifdef __cplusplus
}
#endif

#endif

/* This file is part of RexOS!.
 *
 * RexOS! is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * RexOS! is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with RexOS!.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef __UNISTD_H
#define __UNISTD_H

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

// Makes the OS wait for the time given in timer_count (milliseconds).
uint32_t usleep(uint32_t timer_count);

// Makes the OS wait for the time given in timer_count (seconds).
uint32_t sleep(uint32_t timer_count);

#ifdef __cplusplus
}
#endif

#endif

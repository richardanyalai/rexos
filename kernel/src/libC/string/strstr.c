/* This file is part of RexOS!.
 *
 * RexOS! is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * RexOS! is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with RexOS!.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <stdint.h>
#include <math.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>

char *strstr(const char *haystack, const char *needle)
{
	if (haystack == 0 || needle == 0)
		return 0;

	for (; *haystack; ++haystack)
	{
		char *h, *n;
	
		for (
			h = (char *) haystack, n = (char *) needle;
			*h && *n && (*h == *n);
			++h, ++n
		);
	
		if (*n == '\0')
			return (char *) haystack;
	}
	return 0;
}

char *strstr2(const char *haystack, const char *needle)
{
	if (haystack == 0 || needle == 0)
		return 0;

	uint8_t (*small_letter)(uint8_t) = lambda(uint8_t, (uint8_t l)
	{
		if (BETWEEN(l, 'A', 'Z'))
			l += 32;

		return l;
	});

	bool (*comp)(uint8_t, uint8_t) = lambda(bool, (uint8_t a, uint8_t b)
	{ return small_letter(a) == small_letter(b); });

	for (; *haystack; ++haystack)
	{
		char *h, *n;
	
		for (
			h = (char *) haystack, n = (char *) needle;
			*h && *n && comp(*h, *n);
			++h, ++n
		);

		if (*n == '\0')
			return (char *) haystack;
	}
	
	return 0;
}

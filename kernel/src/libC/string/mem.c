/* This file is part of RexOS!.
 *
 * RexOS! is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * RexOS! is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with RexOS!.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <string.h>
#include <stdint.h>

int memcmp(const void *str1, const void *str2, size_t n)
{
	const uint8_t *a = (const uint8_t *) str1;
	const uint8_t *b = (const uint8_t *) str2;

	for (size_t i = 0; i < n; ++i)
		if (a[i] < b[i])
			return -1;
		else if (b[i] < a[i])
			return 1;

	return 0;
}

void *memcpy(void *dest, const void *src, size_t n)
{
	const uint8_t *sp = (const uint8_t *) src;
	uint8_t *dp = (uint8_t *) dest;

	for (; n != 0; n--) *dp++ = *sp++;

	return dest;
}

void *memset(void *str, int c, size_t n)
{
	uint8_t *temp = (uint8_t *) str;

	for (; n != 0; n--) *temp++ = c;

	return str;

}

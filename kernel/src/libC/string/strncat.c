/* This file is part of RexOS!.
 *
 * RexOS! is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * RexOS! is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with RexOS!.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <string.h>
#include <stdint.h>

char *strncat(char *dest, const char *src, size_t n)
{
	uint16_t i = 0, j = 0;

	while (dest[i] != '\0')
		i++;

	for (j = 0; j < n; ++j)
		dest[j+i] = src[j];

	dest[j+i] = '\0';

	return dest;
}

char *strcat(char *dest, const char *src)
{ return strncat(dest, src, strlen(src)); }

char *charcat(char *str, uint8_t ch)
{
	uint16_t i = 0;

	while (str[i] != '\0')
		i++;

	str[i] = ch;

	str[i+1] = '\0';

	return str;
}

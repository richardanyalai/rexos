/* This file is part of RexOS!.
 *
 * RexOS! is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * RexOS! is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with RexOS!.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <string.h>
#include <stdint.h>

void *memmove(void *str1, const void *str2, size_t n)
{
	char *dest = (char *) str1;
	const char *src = (const char *) str2;

	if (dest < src)
		for (uint64_t i = 0; i < n; dest[i] = src[i], ++i);
	else
		for (uint64_t i = n; i != 0; dest[i-1] = src[i-1], i--);

	return str1;
}

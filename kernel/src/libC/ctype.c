/* This file is part of RexOS!.
 *
 * RexOS! is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * RexOS! is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with RexOS!.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <ctype.h>
#include <math.h>

int isalnum(int c)
{ return isalpha(c) || isdigit(c); }

int isalpha(int c)
{ return islower(c) || isupper(c); }

int isdigit(int c)
{ return BETWEEN(c, 48, 57); }

int isupper(int c)
{ return BETWEEN(c, 65, 90); }

int islower(int c)
{ return BETWEEN(c, 97, 122); }

int isspace(int c)
{ return c == ' ' || c == '\t'; }

int isprint(int c)
{ return BETWEEN(c, 32, 126) || c == '\n' || c == '\t'; }

int iscntrl(int c)
{ return c < 32 || c > 126; }

/* This file is part of RexOS!.
 *
 * RexOS! is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * RexOS! is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with RexOS!.  If not, see <https://www.gnu.org/licenses/>.
 */

// REF: https://wiki.osdev.org/Time_And_Date

#include <io/ports.h>
#include <io/registers.h>
#include <sys/isr.h>
#include <sys/pit.h>
#include <sys/rtc.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

static uint16_t booted_at;
static uint8_t second, minute, hour, week_day, day, month, century;
static uint16_t year;
static volatile bool has_booted = false;

static char uptime_full[80] = {0}, uptime_tmp[2] = {0};

enum
{
	CMOS_ADDR = 0x70,
	CMOS_DATA = 0x71
};

void (*rtc_funcs[8])();

bool get_update_in_progress_flag()
{
	outb(CMOS_ADDR, 0x0A);
	return (inb(CMOS_DATA) & 0x80);
}

uint8_t get_RTC_register(uint8_t reg)
{
	outb(CMOS_ADDR, reg);
	return inb(CMOS_DATA);
}

void update_clock()
{
	while (get_update_in_progress_flag());

	second   = get_RTC_register(REGISTER_SECOND);
	minute   = get_RTC_register(REGISTER_MINUTE);
	hour     = get_RTC_register(REGISTER_HOUR);
	week_day = get_RTC_register(REGISTER_WEEKDAY);
	day      = get_RTC_register(REGISTER_DAY);
	month    = get_RTC_register(REGISTER_MONTH);
	year     = get_RTC_register(REGISTER_YEAR);
	century  = get_RTC_register(REGISTER_CENTURY);

	uint8_t registerB = get_RTC_register(0x0B);

	// Convert BCD to binary values if necessary
	if (!(registerB & 0x04))
	{
		second  = (second & 0x0F) + ((second / 16) * 10);
		minute  = (minute & 0x0F) + ((minute / 16) * 10);
		hour    = ((hour & 0x0F) + (((hour & 0x70) / 16) * 10)) | (hour & 0x80);
		day     = (day   & 0x0F) + ((day   / 16) * 10);
		month   = (month & 0x0F) + ((month / 16) * 10);
		year    = (year  & 0x0F) + ((year  / 16) * 10);
		century = (century & 0x0F) + ((century / 16) * 10);
	}

	// Convert 12 hour clock to 24 hour clock if necessary
	if (!(registerB & 0x02) && (hour & 0x80))
		hour = ((hour & 0x7F) + 12) % 24;

	// Calculate the 4 digit year
	year += century * 100;
}

uint16_t get_year()
{ return year; }

uint8_t get_month()
{ return month; }

uint8_t get_day()
{ return day; }

uint8_t get_week_day()
{
	return ((uint32_t) mktime(
		&((timedate_t)
		{
			.tm_year = year,
			.tm_mon  = month,
			.tm_mday = day,
			.tm_hour = hour,
			.tm_min  = minute,
			.tm_sec  = second 
		})
	) / (24 * 60 * 60) + 3) % 7;
}

uint8_t get_hour()
{ return hour; }

uint8_t get_minutes()
{ return minute; }

uint8_t get_seconds()
{ return second; }

timedate_t *get_timedate()
{
	timedate_t *timedate = malloc(sizeof(timedate_t));

	timedate->tm_year = get_year();
	timedate->tm_mon  = get_month();
	timedate->tm_mday = get_day();
	timedate->tm_hour = get_hour();
	timedate->tm_min  = get_minutes();
	timedate->tm_sec  = get_seconds();
	timedate->tm_wday = get_week_day();

	return timedate;

}

void get_text_clock(char *clock_buffer)
{
	update_clock();

	uint8_t hr = get_hour();

	if (hr > 23)
		hr -= 24;
	if (hr < 10)
	{
		clock_buffer[0] = '0';
		clock_buffer[1] = hr + 48;
	}
	else
		itoa(hr, clock_buffer, 10);

	strcat(clock_buffer, ":");

	if (minute < 10 || minute == 60)
	{
		clock_buffer[3] = '0';
		clock_buffer[4] = minute + 48;
	}
	else
		itoa(get_minutes(), clock_buffer + 3, 10);
}

void get_text_date(char *date_buffer)
{
	update_clock();

	const char days[7][4] =
	{ "Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun" };

	const char months[12][4] =
	{
		"Jan", "Feb", "Mar",
		"Apr", "May", "Jun",
		"Jul", "Aug", "Sep",
		"Okt", "Nov", "Dec"
	};

	strcat(date_buffer, days[get_week_day()]);
	strcat(date_buffer, ", ");
	strcat(date_buffer, months[month - 1]);
	strcat(date_buffer, " ");

	itoa(get_day(), date_buffer + 9, 10);
}

static void set_booted_at()
{
	if (!has_booted)
	{
		has_booted = true;

		update_clock();

		booted_at = hour * 3600 + minute * 60 + second;
	}
}

char *get_uptime()
{
	uint32_t millis  = pit_get_ticks();
	uint32_t seconds = millis  / 1000;
	uint16_t minutes = seconds / 60;
	uint16_t hours   = minutes / 60;

	millis  -= seconds * 1000;
	seconds -= minutes * 60;
	minutes -= hours   * 60;

	uint8_t res_length = 1;

	for (uint8_t i = 0; i < 80; uptime_full[i++] = '\0');

	if (hours > 0)
		res_length += 7 + digits(hours);

	if (minutes > 0)
		res_length += 10 + digits(minutes);

	if (seconds > 0)
		res_length += 10 + digits(seconds);

	if (millis > 0)
		res_length += 7 + digits(millis);

	if (hours > 0)
	{
		for (uint8_t i = 0; i < 2; uptime_tmp[i++] = '\0');

		itoa(hours, uptime_tmp, 10);

		strcat(uptime_full, uptime_tmp);
		strcat(uptime_full, hours > 1 ? " hours, " : " hour, ");
	}

	if (minutes > 0)
	{
		memset(uptime_tmp, strlen(uptime_tmp), '\0');

		itoa(minutes, uptime_tmp, 10);

		strcat(uptime_full, uptime_tmp);
		strcat(uptime_full, minutes > 1 ? " minutes, " : " minute, ");
	}

	if (seconds > 0)
	{
		for (uint8_t i = 0; i < 2; uptime_tmp[i++] = '\0');

		itoa(seconds, uptime_tmp, 10);

		strcat(uptime_full, uptime_tmp);
		strcat(uptime_full, seconds > 1 ? " seconds" : " second");
	}

	return uptime_full;
}

void rtc_handler()
{
	outb(CMOS_ADDR, 0x0C);
	inb(CMOS_DATA);

	for (uint8_t i = 0; i < 8; ++i)
		if (rtc_funcs[i])
			rtc_funcs[i]();
}

void register_rtc_func(uint8_t func_num, void *func)
{ rtc_funcs[func_num] = func; }

void init_rtc(uint8_t rate)
{
	IRQ_OFF;

	register_interrupt_handler(IRQ8, rtc_handler);

	outb(CMOS_ADDR, 0x8B);					// Select register B and disable NMI
	uint8_t prev = inb(CMOS_DATA);			// Read the current value of
											// register B
	outb(CMOS_ADDR, 0x8B);					// Set the index again (a read will
											// reset the index to register D)
	outb(CMOS_DATA, prev | 0x40);			// Write the previous value. This
											// turns on bit 6 of register B
	rate &= 0x0F;							// Rate must be above 2 and not
											// over 15
	outb(CMOS_ADDR, 0x8A);					// Set index to register A,
											// disable NMI
	prev = inb(CMOS_DATA);					// Get initial value of register A
	outb(CMOS_ADDR, 0x8A);					// Reset index to A
	outb(CMOS_DATA, (prev & 0xF0) | rate);	// Write only our rate to A. Note,
											// rate is the bottom 4 bits.

	IRQ_RES;

	set_booted_at();
}

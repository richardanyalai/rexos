/* This file is part of RexOS!.
 *
 * RexOS! is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * RexOS! is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with RexOS!.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <drivers/serial.h>
#include <drivers/VGA.h>
#include <sys/isr.h>
#include <stdarg.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>

void log(uint8_t type, const char *__restrict format, ...)
{
	va_list parameters;
	va_start(parameters, format);

	uint8_t fg_bak = vga_get_fgcolor();

	char str[80];

	switch (type)
	{
		case 0:
			printf("[ ${F03}INFO${CLC} ] ");
			serial_printf("\x1b[37;0m[ \x1b[34;1mINFO\x1b[37;0m ] ");
			vga_set_fgcolor(BRIGHT_GREY);
			break;

		case 1:
			printf("[ ${F02}OKAY${CLC} ] ");
			serial_printf("\x1b[37;0m[ \x1b[32;1mOKAY\x1b[37;0m ] ");
			break;

		case 2:
			printf("[ ${F04}FAIL${CLC} ] ");
			serial_printf("\x1b[37;0m[ \x1b[31;1mFAIL\x1b[37;0m ] ");
			break;
	}

	vsprintf(str, format, parameters);

	va_end(parameters);

	strcat(str, "\n");

	printf(str);

	vga_set_fgcolor(fg_bak);

	serial_printf(str);
}

void debug(
	const char *file, const char *function, uint32_t line, const char *message
)
{
	printf("%s/%s (%d): %s\n", file, function, line, message);
	serial_printf("%s/%s (%d): %s\n", file, function, line, message);
	STOP;
}

/* This file is part of RexOS!.
 *
 * RexOS! is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * RexOS! is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with RexOS!.  If not, see <https://www.gnu.org/licenses/>.
 */

 /**
  * Useful links:
  * REF: https://wiki.osdev.org/Shutdown#:~:text=The%20ACPI%20shutdown%20is%20technically,DSDT%20and%20therefore%20AML%20encoded.
  * REF: https://forum.osdev.org/viewtopic.php?t=16990
  */

#include <io/ports.h>
#include <sys/isr.h>

void reboot()
{
	uint8_t good = 0x02;

	while (good & 0x02)
		good = inb(0x64);

	outb(0x64, 0xFE);

	HALT;
}

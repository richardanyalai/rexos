/* This file is part of RexOS!.
 *
 * RexOS! is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * RexOS! is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with RexOS!.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <io/ports.h>
#include <math.h>

void pic_remap()
{
	outb(0x20, 0x11);	// Write ICW1 to PICM, we are gonna write
						// commands to PICM
	outb(0xA0, 0x11);	// Write ICW1 to PICS, we are gonna write
						// commands to PICS
	outb(0x21, 0x20);	// Remap PICM to 0x20 (32 decimal)
	outb(0xA1, 0x28);	// Remap PICS to 0x28 (40 decimal)
	outb(0x21, 0x04);	// IRQ2 -> connection to slave
	outb(0xA1, 0x02);
	outb(0x21, 0x01);	// Write ICW4 to PICM, we are gonna write
						// commands to PICM
	outb(0xA1, 0x01);	// Write ICW4 to PICS, we are gonna write
						// commands to PICS
	outb(0x21, 0x00);	// Enable all IRQs on PICM
	outb(0xA1, 0x00);	// Enable all IRQs on PICS
}

void send_eoi(uint8_t int_no)
{
	if (BETWEEN(int_no, 0x20, 0x30))
	{
		outb(PORT_MCP, 0x20);

		if (int_no >= 0x28)
			outb(PORT_SCP, 0x20);
	}
}

/* This file is part of RexOS!.
 *
 * RexOS! is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * RexOS! is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with RexOS!.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <drivers/pcspkr.h>
#include <drivers/serial.h>
#include <gui/graphics_mode.h>
#include <io/ports.h>
#include <io/registers.h>
#include <math.h>
#include <sys/isr.h>
#include <sys/pic.h>
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>

extern uint8_t panik[105*120];

isr_t interrupt_handlers[256];

char *exception_messages[] =
{
	"DIVISION BY ZERO",				/* 0 */
	"DEBUG",
	"NON-MASKABLE INTERUPT",
	"BREAKPOINT",
	"DETECTED OVERFLOW",
	"OUT-OF-BOUNDS",				/* 5 */
	"INVALID OPCODE",
	"NO COPROCESSOR",
	"DOUBLE FAULT",
	"COPROCESSOR SEGMENT OVERRUN",
	"BAD TSS",						/* 10 */
	"SEGMENT NOT PRESENT",
	"STACK FAULT",
	"GENERAL PROTECTION FAULT",
	"PAGE FAULT",
	"UNKNOWN INTERRUPT",			/* 15 */
	"COPROCESSOR FAULT",
	"ALIGNMENT CHECK",
	"MACHINE CHECK",
	"RESERVED",
	"RESERVED",
	"RESERVED",
	"RESERVED",
	"RESERVED",
	"RESERVED",
	"RESERVED",
	"RESERVED",
	"RESERVED",
	"RESERVED",
	"RESERVED",
	"RESERVED",
	"RESERVED"
};

void register_interrupt_handler(uint8_t n, isr_t handler)
{ interrupt_handlers[n] = handler; }

bool are_interrupts_enabled()
{
	size_t flags;

	asm volatile (
		"pushf\n\t"
		"pop %0"
		: "=g" (flags)
	);

	return flags & (1 << 9);
}

// This gets called from our ASM interrupt handler stub.
void isr_handler(registers_t regs)
{
	IRQ_OFF;

	// This line is important. When the processor extends the 8-bit interrupt
	// number to a 32bit value, it sign-extends, not zero extends. So if the
	// most significant bit (0x80) is set, regs.int_no will be very large
	// (about 0xffffff80).
	uint8_t int_no = regs.int_no & 0xFF;

	if (interrupt_handlers[int_no] != 0)
		interrupt_handlers[regs.int_no](&regs);
	else
	{
		BEEP;

		IRQ_OFF;

		init_vga(320, 200);

		vga_draw_image(panik, 10, 40, 105, 120);

		vga_draw_string(18, 10, "KERNEL PANIK", 0, 40);

		vga_draw_string(15, 15, exception_messages[regs.int_no], 0, 40);

		vga_set_index(vga_get_pos(22, 18));
		printf("esp  : 0x%.8x", regs.esp);
		vga_set_index(vga_get_pos(22, 19));
		printf("eip  : 0x%.8x", regs.eip);
		vga_set_index(vga_get_pos(22, 20));
		printf("flgs : 0x%.8x", regs.eflags);
		vga_set_index(vga_get_pos(22, 21));
		printf("err  : 0x%.8x", regs.err_code);

		serial_printf(
			"\nKERNEL PANIK\n\nreason: %s\n\n"
			"esp  : 0x%x\neip  : 0x%x\nflgs : 0x%x\nerr  : 0x%x\n",
			exception_messages[regs.int_no],
			regs.esp, regs.eip, regs.eflags, regs.err_code
		);

		STOP;
	}

	IRQ_RES;
}

// This gets called from our ASM interrupt handler stub.
void irq_handler(registers_t regs)
{
	IRQ_OFF;

	if (interrupt_handlers[regs.int_no] != 0)
		interrupt_handlers[regs.int_no](&regs);
	else if (regs.int_no != 0x20 && regs.int_no != 0x2C)
		printf("UNH_INT 0x%.2x\n", regs.int_no);

	send_eoi(regs.int_no);

	IRQ_RES;
}

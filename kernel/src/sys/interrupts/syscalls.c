/* This file is part of RexOS!.
 *
 * RexOS! is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * RexOS! is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with RexOS!.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <io/registers.h>
#include <utils.h>
#include <sys/isr.h>
#include <sys/syscalls.h>
#include <stdint.h>
#include <stdio.h>

#define SYSCALL_COUNT 3

DEFN_SYSCALL1(putchar, 0, const char);
DEFN_SYSCALL1(printf, 1, const char *);

static void *syscalls[SYSCALL_COUNT] =
{
	&putchar,
	&printf
};

void syscall_handler(registers_t *regs)
{
	// Firstly, check if the requested syscall number is valid.
	// The syscall number is found in EAX.
	if (regs->eax >= SYSCALL_COUNT)
		return;

	// Get the required syscall location.
	void *location = syscalls[regs->eax];

	// We don't know how many parameters the function wants, so we just
	// push them all onto the stack in the correct order. The function will
	// use all the parameters it wants, and we can pop them all back off afterwards.
	int ret;

	asm volatile ("	\
		push %1;	\
		push %2;	\
		push %3;	\
		push %4;	\
		push %5;	\
		call *%6;	\
		pop %%ebx;	\
		pop %%ebx;	\
		pop %%ebx;	\
		pop %%ebx;	\
		pop %%ebx;	\
	" : "=a" (ret) : "r" (regs->edi),
	"r" (regs->esi),
	"r" (regs->edx),
	"r" (regs->ecx),
	"r" (regs->ebx),
	"r" (location));
	regs->eax = ret;
}

void init_syscalls()
{
	register_interrupt_handler(ISR128, &syscall_handler);

	KOK("Initialized system call handler");
}

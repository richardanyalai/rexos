/*
 * Copyright (c) 2006-2007 -  http://brynet.biz.tm - <brynet@gmail.com>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
 * AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL
 * THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

// REF: https://forum.osdev.org/viewtopic.php?t=11998

#include <sys/cpudet.h>
#include <stdlib.h>
#include <string.h>

#define cpuid(in, a, b, c, d) \
asm volatile ("cpuid" : "=a"(a), "=b"(b), "=c"(c), "=d"(d) : "a"(in));

cpu_data_t cpu_data;

// Intel Specific brand list.
char *Intel[] =
{
	"Brand ID Not Supported.",
	"Intel(R) Celeron(R) processor",
	"Intel(R) Pentium(R) III processor",
	"Intel(R) Pentium(R) III Xeon(R) processor",
	"Intel(R) Pentium(R) III processor",
	"Reserved",
	"Mobile Intel(R) Pentium(R) III processor-M",
	"Mobile Intel(R) Celeron(R) processor",
	"Intel(R) Pentium(R) 4 processor",
	"Intel(R) Pentium(R) 4 processor",
	"Intel(R) Celeron(R) processor",
	"Intel(R) Xeon(R) Processor",
	"Intel(R) Xeon(R) processor MP",
	"Reserved",
	"Mobile Intel(R) Pentium(R) 4 processor-M",
	"Mobile Intel(R) Pentium(R) Celeron(R) processor",
	"Reserved",
	"Mobile Genuine Intel(R) processor",
	"Intel(R) Celeron(R) M processor",
	"Mobile Intel(R) Celeron(R) processor",
	"Intel(R) Celeron(R) processor",
	"Mobile Geniune Intel(R) processor",
	"Intel(R) Pentium(R) M processor",
	"Mobile Intel(R) Celeron(R) processor"
};

/* This table is for those brand strings that have two values
 *depending on the processor signature. It should have the
 *same number of entries as the above table.
 */
char *Intel_Other[] =
{
	"Reserved",
	"Reserved",
	"Reserved",
	"Intel(R) Celeron(R) processor",
	"Reserved",
	"Reserved",
	"Reserved",
	"Reserved",
	"Reserved",
	"Reserved",
	"Reserved",
	"Intel(R) Xeon(R) processor MP",
	"Reserved",
	"Reserved",
	"Intel(R) Xeon(R) processor",
	"Reserved",
	"Reserved",
	"Reserved",
	"Reserved",
	"Reserved",
	"Reserved",
	"Reserved",
	"Reserved",
	"Reserved"
};

void put_cpu_data(char *dest, char *data)
{ strcat(dest, data); }

/* Print Registers */
void printregs(int eax, int ebx, int ecx, int edx)
{
	char string[100] = { 0 };

	for (uint8_t j = 0; j < 4; ++j)
	{
		string[j] = eax >> (8 * j);
		string[j + 4] = ebx >> (8 * j);
		string[j + 8] = ecx >> (8 * j);
		string[j + 12] = edx >> (8 * j);
	}

	put_cpu_data(cpu_data.brand, string);
}

// Intel-specific information.
void do_intel()
{
	uint32_t eax, ebx, ecx, edx, max_eax, signature, unused;
	int model, family, type, brand;

	cpuid(1, eax, ebx, unused, unused);

	model		= (eax >> 4) & 0xf;
	family		= (eax >> 8) & 0xf;
	type		= (eax >> 12) & 0x3;
	brand		= ebx & 0xff;
	signature	= eax;

	switch (type)
	{
		case 0:  put_cpu_data(cpu_data.type, "Original OEM");	break;
		case 1:  put_cpu_data(cpu_data.type, "Overdrive");		break;
		case 2:  put_cpu_data(cpu_data.type, "Dual-capable");	break;
		case 3:  put_cpu_data(cpu_data.type, "Reserved");
	}
	
	switch (family)
	{
		case 3:  put_cpu_data(cpu_data.family, "i386");			break;
		case 4:  put_cpu_data(cpu_data.family, "i486");			break;
		case 5:  put_cpu_data(cpu_data.family, "Pentium");		break;
		case 6:  put_cpu_data(cpu_data.family, "Pentium Pro");	break;
		case 15: put_cpu_data(cpu_data.family, "Pentium 4");
	}

	switch (family)
	{
		case 3:
			break;

		case 4:
			switch (model)
			{
				case 0:
				case 1: put_cpu_data(cpu_data.model, "DX");
					break;
				case 2: put_cpu_data(cpu_data.model, "SX");
					break;
				case 3: put_cpu_data(cpu_data.model, "487/DX2");
					break;
				case 4: put_cpu_data(cpu_data.model, "SL");
					break;
				case 5: put_cpu_data(cpu_data.model, "SX2");
					break;
				case 7: put_cpu_data(cpu_data.model, "Write-back enhanced DX2");
					break;
				case 8: put_cpu_data(cpu_data.model, "DX4");
			}
			break;

		case 5:
			switch (model)
			{
				case 1: put_cpu_data(cpu_data.model, "60/66");			break;
				case 2: put_cpu_data(cpu_data.model, "75-200");			break;
				case 3: put_cpu_data(cpu_data.model, "for 486 system");	break;
				case 4: put_cpu_data(cpu_data.model, "MMX");
			}
			break;

		case 6:
			switch (model)
			{
				case 1:
					put_cpu_data(cpu_data.model, "Pentium Pro");
					break;

				case 3:
					put_cpu_data(cpu_data.model, "Pentium II Model 3");
					break;

				case 5:
					put_cpu_data(
						cpu_data.model,
						"Pentium II Model 5/Xeon/Celeron"
					);
					break;

				case 6:
					put_cpu_data(cpu_data.model, "Celeron");
					break;

				case 7:
					put_cpu_data(
						cpu_data.model,
						"Pentium III/Pentium III Xeon - external L2 cache"
					);
					break;

				case 8:
					put_cpu_data(
						cpu_data.model,
						"Pentium III/Pentium III Xeon - internal L2 cache"
					);
			}
			break;

		case 15: break;
	}

	cpuid(0x80000000, max_eax, unused, unused, unused);

	/* Quok said: If the max extended eax value is high enough to support the
	 * processor brand string (values 0x80000002 to 0x80000004), then we'll use
	 * that information to return the brand information. Otherwise, we'll refer
	 * back to the brand tables above for backwards compatibility with older
	 * processors. 
	 * According to the Sept. 2006 Intel Arch Software Developer's Guide,
	 * if extended eax values are supported, then all 3 values for the
	 * processor brand string are supported, but we'll test just to make sure
	 * and be safe.
	 */
	if (max_eax >= 0x80000002)
	{
		for (uint32_t j = 0x80000002; j <= 0x80000004; ++j)
		{
			cpuid(j, eax, ebx, ecx, edx);
			printregs(eax, ebx, ecx, edx);
		}

		for (uint8_t i = 99; i > 0; --i)
			if (cpu_data.brand[i] == ' ' || cpu_data.brand[i] == '\0')
				cpu_data.brand[i] = '\0';
			else
				break;
	}
	else if (brand > 0)
	{
		if (brand < 0x18)
		{
			if (signature == 0x000006B1 || signature == 0x00000F13)
				put_cpu_data(cpu_data.brand, Intel_Other[brand]);
			else
				put_cpu_data(cpu_data.brand, Intel[brand]);
		}
		else
			put_cpu_data(cpu_data.brand, "Reserved");
	}
}

/* AMD-specific information */
void do_amd()
{
	uint32_t	extended, eax, ebx, ecx, edx, unused;
	int			family, model;

	cpuid(1, eax, unused, unused, unused);

	model	= (eax >> 4) & 0xf;
	family	= (eax >> 8) & 0xf;

	char familystr[3] = {0};

	itoa(family, familystr, 10);

	put_cpu_data(cpu_data.family, familystr);

	switch (family)
	{
		case 4:
			put_cpu_data(cpu_data.model, "486 Model 4");
			break;

		case 5:
			switch (model)
			{
				case 0:
				case 1:
				case 2:
				case 3:
				case 6:
				case 7: put_cpu_data(cpu_data.model, "K6 Model 7");		break;
				case 8: put_cpu_data(cpu_data.model, "K6-2 Model 8");	break;
				case 9: put_cpu_data(cpu_data.model, "K6-III Model 9");	break;

				default:
					put_cpu_data(cpu_data.model, "K5/K6 Model ");

					char modelstr[3];

					modelstr[2] = '\0';

					itoa(model, modelstr, 10);

					put_cpu_data(cpu_data.model, modelstr);
			}
			break;

		case 6:
			switch (model)
			{
				case 1:
				case 2:
				case 3:
					put_cpu_data(cpu_data.model, "Duron Model 3");
					break;

				case 4:
					put_cpu_data(cpu_data.model, "Athlon Model 4");
					break;

				case 6:
					put_cpu_data(
						cpu_data.model, "Athlon MP/Mobile Athlon Model 6"
					);
					break;

				case 7:
					put_cpu_data(cpu_data.model, "Mobile Duron Model 7");
					break;

				default:
					put_cpu_data(cpu_data.model, "Duron/Athlon Model ");

					char modelstr[3] = {0};

					itoa(model, modelstr, 10);

					put_cpu_data(cpu_data.model, modelstr);
			}
			break;
	}

	cpuid(0x80000000, extended, unused, unused, unused);

	if (extended == 0)
		return;

	if (extended >= 0x80000002)
	{
		for (uint32_t j = 0x80000002; j <= 0x80000004; ++j)
		{
			cpuid(j, eax, ebx, ecx, edx);
			printregs(eax, ebx, ecx, edx);
		}

		for (uint8_t i = 99; i > 0; --i)
			if (cpu_data.brand[i] == ' ' || cpu_data.brand[i] == '\0')
				cpu_data.brand[i] = '\0';
			else
				break;
	}
}

void detect_cpu()
{
	uint32_t ebx, unused;
	cpuid(0, unused, ebx, unused, unused);

	switch (ebx)
	{
		case 0x756e6547: // Intel Magic Code
			do_intel();
			break;

		case 0x68747541: // AMD Magic Code
			do_amd();
			break;
	}
}

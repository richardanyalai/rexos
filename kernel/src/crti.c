/* This file is part of RexOS!.
 *
 * RexOS! is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * RexOS! is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with RexOS!.  If not, see <https://www.gnu.org/licenses/>.
 */

typedef void (*func_t)(void);

extern func_t __init_array_start[];
extern func_t __init_array_end[];
extern func_t __fini_array_start[];
extern func_t __fini_array_end[];

// Calls constructors of global objects.
void _init(void)
{
	for (func_t *p = __init_array_start; p != __init_array_end; ++p)
		(*p)();
}

// Calls destructors of global objects.
void _fini(void)
{
	for (func_t *p = __fini_array_start; p != __fini_array_end; ++p)
		(*p)();
}

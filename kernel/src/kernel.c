/* This file is part of RexOS!.
 *
 * RexOS! is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * RexOS! is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with RexOS!.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <descriptors/gdt.h>
#include <descriptors/idt.h>
#include <drivers/ACPI.h>
#include <drivers/fs/initrd.h>
#include <drivers/keyboard.h>
#include <drivers/mouse.h>
#include <drivers/parallel.h>
#include <drivers/PCI.h>
#include <drivers/serial.h>
#include <drivers/VGA.h>
#include <memory/heap.h>
#include <memory/paging.h>
#include <multiboot.h>
#include <programs/common.h>
#include <settings.h>
#include <sys/cpudet.h>
#include <sys/isr.h>
#include <sys/pit.h>
#include <sys/rtc.h>
#include <sys/syscalls.h>
#include <utils.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>

extern uint32_t placement_address, available_memory;
extern void test();

#ifdef VESA
extern multiboot_info_t *mbi;
#endif

void kernel_main(
	multiboot_info_t *multiboot_info, 	// Pointer to the multiboot struct
	uint32_t magic,						// Multiboot magic number
	uint32_t initial_stack				// Stack pointer
)
{
#ifdef VESA
	mbi = multiboot_info;
	void vesa_draw_bg();
	vesa_draw_bg();
#endif
	init_vga(80, 25);

#ifdef VESA
	KINF(
		"Using VESA VBE, resolution: %dx%d, %dbpp",
		multiboot_info->framebuffer_width,
		multiboot_info->framebuffer_height,
		multiboot_info->framebuffer_bpp
	);
#endif
	if (magic != MULTIBOOT_BOOTLOADER_MAGIC)
	{
		KERR("Invalid magic number: 0x%.8x\n", magic);
		goto FOREVER;
	}
	else if (multiboot_info->mods_count == 0)
	{
		KERR("No Multiboot modules available!\n");
		goto FOREVER;
	}

	KINF("Stack pointer is at: 0x%.8x", initial_stack);

	ACPI_init();
	ACPI_enable();
	enable_fpu();
	detect_cpu();
	init_gdt();
	init_idt();

	uint32_t initrd_location =
		*((uint32_t *) multiboot_info->mods_addr);		// 8 * i     = 0

	placement_address =
		*(uint32_t *) (multiboot_info->mods_addr + 4);	// 8 * i + 4 = 0

	multiboot_memory_map_t *mmap =
		(multiboot_memory_map_t *) multiboot_info->mmap_addr;

	available_memory = 0;

	while (
		mmap < (multiboot_memory_map_t *) (
			multiboot_info->mmap_addr + multiboot_info->mmap_length
		)
	)
	{
		if (mmap->type == MULTIBOOT_MEMORY_AVAILABLE)
		{
			printf(
				"${F07}Mmap offset: 0x%.8x, size: %d bytes\n${F15}",
				mmap,
				mmap->len
			);
			available_memory += mmap->len;
		}

		mmap = (multiboot_memory_map_t *) (
			(uint32_t) mmap + mmap->size + sizeof(uint32_t)
		);
	}

	KINF(
		"Available Physical Memory = %d kB",
		available_memory / 1024
	);

	fs_root = initialize_initrd(initrd_location);
	init_paging(available_memory);

	init_syscalls();
	init_rtc(13);
	init_pit(FREQUENCY);
	srand(time(NULL));
	init_serial_ports();
	parallel_write("Hello friend");
	init_keyboard();
	init_mouse();
	init_pci();

	printf(
		"%s"
		"Welcome to ${F12}R${F10}e${F11}x${F14}O${F13}S${F15}! v%s\n\n"
		"Press ${F10}'d'${F15} for desktop or ${F10}'t'${F15} for terminal.",
		asctime(get_timedate()),
		VER
	);

	set_keyboard_handler(lambda(void, (active_keys_t * active_keys)
	{
		uint8_t ch = active_keys->active_key.char_code;

		if (active_keys->active_key.pressed)
			switch (ch)
			{
				case 'd':
					show_desktop(0);
					break;

				case 't':
					null_active();
					start_terminal_session();
					IRQ_OFF;
					show_desktop(0);
					break;
			}

		if (ch != 0)
			null_active();
	}));

	set_left_click_handler(0);
	set_right_click_handler(0);

	test();

FOREVER:
	while (true)
		HALT;
}

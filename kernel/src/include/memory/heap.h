/* This file is part of RexOS!.
 *
 * RexOS! is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * RexOS! is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with RexOS!.  If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * Interface for heap.
 * Written for JamesM's kernel development tutorials,
 * rewritten by Richard Anyalai for the RexOS! project.
 * REF: http://www.jamesmolloy.co.uk/tutorial_html/7.-The%20Heap.html
 * REF: https://en.wikipedia.org/wiki/C_dynamic_memory_allocation
 */

#ifndef __HEAP_H
#define __HEAP_H

#include <ds/ordered_array.h>
#include <stdbool.h>
#include <stdint.h>

#define KHEAP_START			0xC0000000
#define KHEAP_INITIAL_SIZE	0x100000

#define HEAP_INDEX_SIZE		0x20000
#define HEAP_MAGIC			0x123890AB
#define HEAP_MIN_SIZE		0x70000

#define PAGE_SIZE			0x1000

#ifdef __cplusplus
extern "C" {
#endif

// Size information for a hole / block
typedef struct
{
	uint32_t		magic;			// Magic number, used for error checking
									// and identification.
	uint8_t			is_hole;		// 1 if this is a hole. 0 if this is a
									// block.
	uint32_t		size;			// Size of the block, including the end
									//footer.
} header_t;

// Block footer data structure
typedef struct
{
	uint32_t		magic;			// Magic number, same as in header_t.
	header_t		*header;		// Pointer to the block header.
} footer_t;

// Data structure to keep information about heap
typedef struct
{
	ordered_array_t	index;			// Is the index of the ordered array.
	uint32_t		start_address;	// The start of our allocated space.
	uint32_t		end_address;	// The end of our allocated space. May be
									// expanded up to max_address.
	uint32_t		max_address;	// The maximum address the heap can be
									// expanded to.
	bool			supervisor;		// Should extra pages requested by us be
									// mapped as supervisor-only?
	bool			readonly;		// Should extra pages requested by us be
									// mapped as read-only?
} heap_t;

/**
 * @brief				Creates heap with given paramaters.
 * @param start			is the start address
 * @param end			is the end address
 * @param max			is maximum address the heap can be expanded to
 * @param supervisor	indicates whether extra pages should be
 *						requested by us be mapped as supervisor-only
 * @param readonly		indicates whether extra pages should be
 *						requested by us be mapped as read-only
 */
heap_t *create_heap(
	uint32_t start, uint32_t end, uint32_t max, bool supervisor, bool readonly
);

/**
 * @brief Allocates a memory block of given size.
 * @param size			is the size of the memory we want to allocate
 * @param page_align	indicates wheter we want it to be page aligned
 * @param heap			is the data structure for heap
 */
void *alloc(uint32_t size, bool page_align, heap_t *heap);

/**
 * @brief				Releases a block allocated with 'alloc'.
 * @param p				is the pointer to the address of the memory chunk
 *						we want to make free
 * @param heap			is the data structure for heap
 */
void freeup(void *p, heap_t *heap);

/**
 * Allocate a chunk of memory, sz in size. If align == 1,
 * the chunk must be page-aligned. If phys != 0, the physical
 * location of the allocated chunk will be stored into phys.
 *
 * This is the internal version of kmalloc. More user-friendly
 * parameter representations are available in kmalloc, kmalloc_a,
 * kmalloc_ap, kmalloc_p.
 * @param sz			is the size
 * @param phys			is the physical address
 * @return				a pointer to the allocated memory
 */
uint32_t kmalloc_int(uint32_t sz, int align, uint32_t *phys);

/* Allocate a chunk of memory, sz in size.
 * The chunk must be page aligned.
 */
uint32_t kmalloc_a(uint32_t sz);

/**
 * @brief				Allocate a chunk of memory, sz in size.
 *						The physical address is returned in phys.
 *						Phys MUST be a valid pointer to uint32_t!
 * @param sz			is the size
 * @param phys			is the physical address
 * @return				a pointer to the allocated memory
 */
uint32_t kmalloc_p(uint32_t sz, uint32_t *phys);

/**
 * @brief				Allocate a chunk of memory, sz in size.
 *						The physical address is returned in phys.
 *						It must be page-aligned.
 * @param sz			is the size
 * @param phys			is the physical address
 * @return				a pointer to the allocated memory
 */
uint32_t kmalloc_ap(uint32_t sz, uint32_t *phys);

// Allocates memory chunk with given size.
uint32_t kmalloc(uint32_t sz);

// Makes an allocated memory chunk free.
void kfree(void *p);

#ifdef __cplusplus
}
#endif

#endif

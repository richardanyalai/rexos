/* This file is part of RexOS!.
 *
 * RexOS! is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * RexOS! is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with RexOS!.  If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * Interface for and structures related to paging.
 * Written for JamesM's kernel development tutorials, rewritten
 * by Richard Anyalai for the RexOS! project.
 * REF: http://www.jamesmolloy.co.uk/tutorial_html/6.-Paging.html
 * REF: https://wiki.osdev.org/Paging
 */

#ifndef __PAGING_H
#define __PAGING_H

#include <io/registers.h>
#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

// A data structure to represent a page.
typedef struct
{
	uint32_t present	: 1;	// Page present in memory
	uint32_t rw			: 1;	// Read-only if clear, readwrite if set
	uint32_t user		: 1;	// Supervisor level only if clear
	uint32_t accessed	: 1;	// Page has  been accessed since last refresh
	uint32_t dirty		: 1;	// Page has been written to since last refresh
	uint32_t unused		: 7;	// Amalgamation of unused and reserved bits
	uint32_t frame		: 20;	// Frame address (shifted right 12 bits)
} page_t;

// Data structure to hold all the pages.
typedef struct
{ page_t pages[1024]; } page_table_t;

typedef struct
{
	// Array of pointers to pagetables.
	page_table_t *tables[1024];

	/* Array of pointers to the pagetables above, but gives their *physical*
	 * location, for loading into the CR3 register.
	 */
	uint32_t tablesPhysical[1024];

	/* The physical address of tablesPhysical. This comes into play
	 * when we get our kernel heap allocated and the directory
	 * may be in a different location in virtual memory.
	 */
	uint32_t physicalAddr;
} page_directory_t;

// Sets up the environment, page directories etc and enables paging.
void init_paging(uint32_t mem_end_page);

// Causes the specified page directory to be loaded into the CR3 register.
void switch_page_directory(page_directory_t *new);

/**
 * @brief			Retrieves a pointer to the page.
 * @param address	is the address of the page
 * @param make		indicates whether we want to create a new page
 * @param dir		is the page directory
 * @return			a pointer to the needed page
 */
page_t *get_page(uint32_t address, int make, page_directory_t *dir);

// Handler for page faults.
void page_fault(registers_t *regs);

//Makes a copy of a page directory.
page_directory_t *clone_directory(page_directory_t *src);

#ifdef __cplusplus
}
#endif

#endif

/* This file is part of RexOS!.
 *
 * RexOS! is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * RexOS! is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with RexOS!.  If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * Interface for PIT (Programmable Interval Timer).
 * Written for JamesM's kernel development tutorials,
 * rewritten by Richard Anyalai for the RexOS! project.
 * REF: http://www.jamesmolloy.co.uk/tutorial_html/5.-IRQs%20and%20the%20PIT.html
 * REF: https://en.wikipedia.org/wiki/Programmable_interval_timer
 * REF: https://wiki.osdev.org/Programmable_Interval_Timer
 * REF: https://littleosbook.github.io/#programmable-interval-timer
 */

#ifndef __PIT_H
#define __PIT_H

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

// Initializes the timer with the given frequency.
void init_pit(uint16_t frequency);

// Gives current timer ticks count.
uint64_t pit_get_ticks(void);

#ifdef __cplusplus
}
#endif

#endif

/* This file is part of RexOS!.
 *
 * RexOS! is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * RexOS! is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with RexOS!.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef __RTC_H
#define __RTC_H

#include <stdint.h>
#include <time.h>

#define UNIX_YEAR			1970
#define UNIX_MONTH			1
#define UNIX_DAY			1

/**
 * Registers can be found here:
 * REF: https://wiki.osdev.org/CMOS
 */
#define REGISTER_SECOND		0x00
#define REGISTER_MINUTE		0x02
#define REGISTER_HOUR		0x04
#define REGISTER_WEEKDAY	0x06
#define REGISTER_DAY		0x07
#define REGISTER_MONTH		0x08
#define REGISTER_YEAR		0x09
#define REGISTER_CENTURY	0x32

#ifdef __cplusplus
extern "C" {
#endif

enum
{
	RTC_CLOCK_WIDGET,
	RTC_DATE_WIDGET
};

// Updates the clock.
void update_clock(void);

// Returns current year.
uint16_t get_year(void);

// Returns current month.
uint8_t get_month(void);

// Returns current day.
uint8_t get_day(void);

// Returns current day of week.
uint8_t get_week_day(void);

// Returns hours.
uint8_t get_hour(void);

// Returns minutes.
uint8_t get_minutes(void);

// Returns seconds.
uint8_t get_seconds(void);

// Writes current time into buffer.
void get_text_clock(char *clock_buffer);

// Writes current date into buffer.
void get_text_date(char *date_buffer);

// Returns the time at which the computer was booted.
char *get_uptime(void);

// Returns a pointer to a timedate struct which contains current time and date.
timedate_t *get_timedate();

// Registers a rtc callback.
void register_rtc_func(uint8_t func_num, void *func);

// Initializes the Real Time Clock.
void init_rtc(uint8_t rate);

#ifdef __cplusplus
}
#endif

#endif

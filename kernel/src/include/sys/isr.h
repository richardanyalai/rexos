/* This file is part of RexOS!.
 *
 * RexOS! is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * RexOS! is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with RexOS!.  If not, see <https://www.gnu.org/licenses/>.
 */

 /**
	* Interface for interrupts.
	* Written for JamesM's kernel development tutorials,
	* rewritten by Richard Anyalai for the RexOS! project.
	* REF: http://www.jamesmolloy.co.uk/tutorial_html/5.-IRQs%20and%20the%20PIT.html
	*/

#ifndef __IRS_H
#define __IRS_H

#include <io/registers.h>
#include <stdbool.h>
#include <stdint.h>

#define IRQ_OFF	{ asm volatile ("cli"); }
#define IRQ_RES	{ asm volatile ("sti"); }
#define HALT	{ asm volatile ("hlt"); }
#define IRQS_ON_AND_PAUSE { asm volatile ("sti\nhlt\ncli"); }

#define STOP while (true) { HALT; }

#define IRQ0	32
#define IRQ1	33
#define IRQ2	34
#define IRQ3	35
#define IRQ4	36
#define IRQ5	37
#define IRQ6	38
#define IRQ7	39
#define IRQ8	40
#define IRQ9	41
#define IRQ10	42
#define IRQ11	43
#define IRQ12	44
#define IRQ13	45
#define IRQ14	46
#define IRQ15	47
#define ISR128	128

// A simple type definition to create handlers
typedef void (*isr_t)(registers_t *);

#ifdef __cplusplus
extern "C" {
#endif

// Returns a true boolean value if irq are enabled for the CPU.
bool are_interrupts_enabled();

/**
 * @brief			Sets callbacks for interrupts or IRQs.
 * @param n			is the d of an interrupt
 * @param handler	is a handler
 */
void register_interrupt_handler(uint8_t n, isr_t handler);

#ifdef __cplusplus
}
#endif

#endif

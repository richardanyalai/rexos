/* This file is part of RexOS!.
 *
 * RexOS! is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * RexOS! is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with RexOS!.  If not, see <https://www.gnu.org/licenses/>.
 */

 /**
  * Interface for interrupts.
  * Written for JamesM's kernel development tutorials,
  * rewritten by Richard Anyalai for the RexOS! project.
  * REF: http://www.jamesmolloy.co.uk/tutorial_html/5.-IRQs%20and%20the%20PIT.html
  */

#ifndef __PIC_H
#define __PIC_H

#ifdef __cplusplus
extern "C" {
#endif

// Remap the PIC.
void pic_remap(void);

// Send an EOI (end of interrupt) signal to the PICs.
void send_eoi(uint8_t int_no);

#ifdef __cplusplus
}
#endif

#endif

/* This file is part of RexOS!.
 *
 * RexOS! is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * RexOS! is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with RexOS!.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef __PCI_H
#define __PCI_H

#include <stdbool.h>
#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

#define PCI_COMMAND_PORT	0xCF8
#define PCI_DATA_PORT		0xCFC

struct __pci_driver;

typedef struct {
	uint32_t
		port_base,
		interrupt;
	uint16_t
		bus,
		device_num,
		function,
		vendor_id,
		device_id;
	uint8_t
		class_id,
		subclass_id,
		interface,
		revision;
	struct __pci_driver *driver;
} pci_device_t;

typedef struct {
	uint32_t
		vendor,
		device,
		function;
} pci_device_id_t;

typedef struct __pci_driver {
	pci_device_id_t *table;
	char            *name;
	uint8_t         (*init_one)(pci_device_t *);
	uint8_t         (*init_driver)(void);
	uint8_t         (*exit_driver)(void);
} pci_driver_t;

typedef enum bar_type
{
	MEM = 0,
	IO  = 1
} bar_type_t;

typedef struct base_address_register
{
	bool       prefetchable;
	uint8_t    *address;
	uint32_t   size;
	bar_type_t type;
} bar_t;

// Lists pci devices.
void pci_list(void);

// Dumps process data.
void pci_proc_dump(void);

// Initializes PCI driver.
void init_pci(void);

#ifdef __cplusplus
}
#endif

#endif

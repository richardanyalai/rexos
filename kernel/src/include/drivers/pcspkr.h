/* This file is part of RexOS!.
 *
 * RexOS! is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * RexOS! is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with RexOS!.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef __PCSPKR_H
#define __PCSPKR_H

#include <stdint.h>

// Macro for beeping.
#define BEEP beep(1000, 50)

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief		Uses the PC speaker to make a "beep" sound.
 * @param freq	is the frequency
 * @param time	is the time between each "beep"
 */
void beep(uint32_t freq, uint8_t time);

#ifdef __cplusplus
}
#endif

#endif

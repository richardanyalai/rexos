/* This file is part of RexOS!.
 *
 * RexOS! is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * RexOS! is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with RexOS!.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef __VESA_H
#define __VESA_H

#include <multiboot.h>
#include <stdint.h>
#include <string.h>

#ifdef __cplusplus
extern "C" {
#endif

uint32_t vesa_get_address(void);

#define VIDEO_ADDRESS	vesa_get_address()

uint16_t
	vesa_get_width(void),
	vesa_get_height(void),
	vesa_get_pitch(void),
	vesa_get_bpp(void);

#define VESA_WIDTH		vesa_get_width()
#define VESA_HEIGHT		vesa_get_height()
#define VESA_PITCH		vesa_get_pitch()
#define VESA_BPP		vesa_get_bpp()

// Calculates color from color channels.
#define vesa_color(a, r, g, b) ((((uint32_t) a) << 24)	\
							  + (((uint32_t) r) << 16)	\
							  + (((uint32_t) g) <<  8)	\
							  + (((uint32_t) b) <<  0))

// Clears the screen.
void vesa_clear(void);

// Draws a pixel at the given coordinates.
void vesa_draw_pixel(uint16_t x, uint16_t y, uint32_t color);

// Convert VGA color to VESA color.
uint32_t color_vga_to_vesa(uint8_t color);

// Draw VGA pixels.
void vesa_draw_vga_pixel(uint16_t x, uint16_t y, uint8_t color);

// Draw black background behind terminal.
void vesa_draw_tty_bg();

#ifdef __cplusplus
}
#endif

#endif

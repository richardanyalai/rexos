/* This file is part of RexOS!.
 *
 * RexOS! is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * RexOS! is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with RexOS!.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef __ATA_H
#define __ATA_H

#include <stdbool.h>
#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

// ATA port numbers
#define ATA_PORT_BASE_PRI	0x1F0
#define ATA_PORT_BASE_SEC	0x170
#define ATA_PORT_BASE_TER	0x1E8
#define ATA_PORT_BASE_QUA	0x168

// Misc
#define BYTES_PER_SECTOR	512

// Represents an ATA device.
typedef struct
{
	uint8_t number;
	bool    master;
	uint16_t
		port_data,
		port_error,
		port_sector_count,
		port_lba_low,
		port_lba_mid,
		port_lba_hi,
		port_device,
		port_command,
		port_control;
} ata_device_t;

// Initializes ATA driver.
void init_ata(void);

// Reads from hard drive.
void ata_read28(
	ata_device_t *device, uint32_t sector, uint8_t *data, uint32_t c
);

#ifdef __cplusplus
}
#endif

#endif

/* This file is part of RexOS!.
 *
 * RexOS! is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * RexOS! is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with RexOS!.  If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * Interface for a simple file system.
 * Written for JamesM's kernel development tutorials,
 * rewritten by Richard Anyalai for the RexOS! project.
 * REF: http://www.jamesmolloy.co.uk/tutorial_html/8.-The%20VFS%20and%20the%20initrd.html
 */

#ifndef __FS_H
#define __FS_H

#include <stdbool.h>
#include <stdint.h>

#define FS_FILE			0x01
#define FS_DIRECTORY	0x02
#define FS_CHARDEVICE	0x03
#define FS_BLOCKDEVICE	0x04
#define FS_PIPE			0x05
#define FS_SYMLINK		0x06
#define FS_MOUNTPOINT	0x08 // Is the file an active mountpoint?

#ifdef __cplusplus
extern "C" {
#endif

struct fs_node;

typedef uint32_t (*read_type_t)(struct fs_node *, uint32_t, uint32_t, char *);
typedef uint32_t (*write_type_t)(struct fs_node *, uint32_t, uint32_t, char *);
typedef void (*open_type_t)(struct fs_node *);
typedef void (*close_type_t)(struct fs_node *);
typedef struct dirent *(*readdir_type_t)(struct fs_node *, uint32_t);
typedef struct fs_node *(*finddir_type_t)(struct fs_node *, char *);

typedef struct fs_node
{
	char name[128];			// The filename
	uint32_t mask;			// The permissions mask
	uint32_t uid;			// The owning user
	uint32_t gid;			// The owning group
	uint32_t flags;			// Includes the node type. See #defines above.
	uint32_t inode;			// This is device-specific - provides a way for a
							// filesystem to identify files.
	uint32_t length;		// Size of the file, in bytes
	uint32_t impl;			// An implementation-defined number
	read_type_t read;
	write_type_t write;
	open_type_t open;
	close_type_t close;
	readdir_type_t readdir;
	finddir_type_t finddir;
	struct fs_node *ptr;	// Used by mountpoints and symlinks.
} fs_node_t;

struct dirent
{
	char name[128];			// Filename
	uint32_t ino;			// Inode number, required by POSIX
};

extern fs_node_t *fs_root;	// The root of the filesystem

/* Standard read/write/open/close functions. Note that these are all
 * suffixed with _fs to distinguish them from the read/write/open/close
 * which deal with file descriptors, not file nodes.
 */

uint32_t read_fs(
	fs_node_t *node, uint32_t offset, uint32_t size, char *buffer
);

uint32_t write_fs(
	fs_node_t *node, uint32_t offset, uint32_t size, char *buffer
);

void open_fs(fs_node_t *node, bool read, bool write);

void close_fs(fs_node_t *node);

bool is_dir(fs_node_t *node);

struct dirent *readdir_fs(fs_node_t *node, uint32_t index);

fs_node_t *finddir_fs(fs_node_t *node, char *name);

#ifdef __cplusplus
}
#endif

#endif

/* This file is part of RexOS!.
 *
 * RexOS! is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * RexOS! is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with RexOS!.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef __MSDOS_H
#define __MSDOS_H

#include <drivers/ATA.h>

#ifdef __cplusplus
extern "C" {
#endif

typedef struct
{
	uint8_t  bootable;
	uint8_t  start_head;
	uint8_t  start_sector	:  6;
	uint16_t start_cylinder	: 10;
	uint8_t  partition_id;
	uint8_t  end_head;
	uint8_t  end_sector		:  6;
	uint16_t end_cylinder	: 10;
	uint32_t start_lba;
	uint32_t length;

} __attribute__((packed)) partition_table_entry_t;

typedef struct
{
	uint8_t  bootloader[440];
	uint32_t signature;
	uint16_t unused;
	partition_table_entry_t primary_partition[4];
	uint16_t magic_number;
} __attribute__((packed)) MBR_t;

void MSDOS_read_partitions(ata_device_t *hd);

#ifdef __cplusplus
}
#endif

#endif

/* This file is part of RexOS!.
 *
 * RexOS! is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * RexOS! is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with RexOS!.  If not, see <https://www.gnu.org/licenses/>.
 */

/* Interface for initial ramdisk.
 * Written for JamesM's kernel development tutorials,
 * rewritten by Richard Anyalai for the RexOS! project.
 */

#ifndef __INITRD_H
#define __INITRD_H

#include <drivers/fs/fs.h>
#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

typedef struct
{
	uint32_t nfiles;	// The number of files in the ramdisk.
} initrd_header_t;

typedef struct
{
	uint8_t magic;		// Magic number, for error checking.
	uint8_t name[64];	// Filename.
	uint32_t offset;	// Offset in the initrd that the file starts.
	uint32_t length;	// Length of the file.
} initrd_file_header_t;

/* initializes the initial ramdisk. It gets
 * passed the address of the multiboot module,
 * and returns a completed filesystem node.
 */
fs_node_t *initialize_initrd(uint32_t location);

#ifdef __cplusplus
}
#endif

#endif

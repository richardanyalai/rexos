/* This file is part of RexOS!.
 *
 * RexOS! is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * RexOS! is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with RexOS!.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef __FAT32_H
#define __FAT32_H

#include <drivers/ATA.h>

#ifdef __cplusplus
extern "C" {
#endif

typedef struct
{
	uint8_t  jump[3];
	uint8_t  softName[8];
	uint16_t bytes_per_sector;
	uint8_t  sectors_per_cluster;
	uint16_t reserved_sectors;
	uint8_t  fat_copies;
	uint16_t root_dir_entries;
	uint16_t total_sectors;
	uint8_t  media_type;
	uint16_t fat_sector_count;
	uint16_t sectors_per_track;
	uint16_t head_count;
	uint32_t hidden_sectors;
	uint32_t total_sector_count;
	
	uint32_t table_size;
	uint16_t ext_flags;
	uint16_t fat_version;
	uint32_t root_cluster;
	uint16_t fat_info;
	uint16_t backup_sector;
	uint8_t  reserved_0[12];
	uint8_t  drive_number;
	uint8_t  reserved;
	uint8_t  boot_signature;
	uint32_t volume_id;
	uint8_t  volume_label[11];
	uint8_t  fat_type_label[8];
} __attribute__((packed)) bios_parameter_block_32_t;

typedef struct
{
	uint8_t  name[8];
	uint8_t  ext[3];
	uint8_t  attributes;
	uint8_t  reserved;
	uint8_t  c_time_tenth;
	uint16_t c_time;
	uint16_t c_date;
	uint16_t a_time;
	uint16_t first_cluster_hi;
	uint16_t w_time;
	uint16_t w_date;
	uint16_t first_cluster_lo;
	uint32_t size;
} __attribute__((packed)) dirent_FAT32_t;

void FAT32_read_bios_block(ata_device_t *hd, uint32_t partition_offset);

#ifdef __cplusplus
}
#endif

#endif

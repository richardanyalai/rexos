/* This file is part of RexOS!.
 *
 * RexOS! is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * RexOS! is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with RexOS!.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef __KEYBOARD_H
#define __KEYBOARD_H

#include <stdbool.h>
#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

typedef enum keycodes
{
	KEY_A		= 0x1E,
	KEY_B		= 0X30,
	KEY_C		= 0X2E,
	KEY_D		= 0x20,
	KEY_E		= 0X12,
	KEY_F		= 0X21,
	KEY_G		= 0X22,
	KEY_H		= 0X23,
	KEY_I		= 0X17,
	KEY_J		= 0X24,
	KEY_K		= 0X25,
	KEY_L		= 0X26,
	KEY_M		= 0X32,
	KEY_N		= 0X31,
	KEY_O		= 0x18,
	KEY_P		= 0x19,
	KEY_Q		= 0X10,
	KEY_R		= 0X13,
	KEY_S		= 0X1F,
	KEY_T		= 0x14,
	KEY_U		= 0x16,
	KEY_V		= 0x2F,
	KEY_W		= 0x11,
	KEY_X		= 0x2D,
	KEY_Y		= 0x2c,
	KEY_Z		= 0x15,

	KEY_0		= 0x0B,
	KEY_1		= 0x02,
	KEY_2		= 0x03,
	KEY_3		= 0x04,
	KEY_4		= 0x05,
	KEY_5		= 0x06,
	KEY_6		= 0x07,
	KEY_7		= 0x08,
	KEY_8		= 0x09,
	KEY_9		= 0x0A,

	KEY_NUM_0	= 0x52,
	KEY_NUM_1	= 0x4F,
	KEY_NUM_2	= 0x50,
	KEY_NUM_3	= 0x51,
	KEY_NUM_4	= 0x4B,
	KEY_NUM_5	= 0x4C,
	KEY_NUM_6	= 0x4D,
	KEY_NUM_7	= 0x47,
	KEY_NUM_8	= 0x48,
	KEY_NUM_9	= 0x49,

	KEY_DOT		= 0x34,
	KEY_COMMA	= 0x33,
	KEY_SEMICOL	= 0x29,
	KEY_PARENTH	= 0x27,

	KEY_MINUS_1	= 0x35,
	KEY_MINUS_2	= 0x4A,
	KEY_PLUS	= 0x4E,
	KEY_DIV		= 0x36,
	KEY_MUL	 	= 0x37,
	KEY_EQUALS	= 0x0C,

	KEY_SPACE	= 0x39,

	KEY_SUPER	= 0x5B,

	KEY_CTRL	= 0x1D,
	KEY_ALT		= 0x38,

	KEY_LSHIFT	= 0x2A,
	KEY_RSHIFT	= 0x36,

	KEY_BKSPACE	= 0x0E,

	KEY_NUML	= 0x45,

	KEY_INSERT	= 0x52,
	KEY_DELETE	= 0x53,
	KEY_HOME	= 0x47,
	KEY_END		= 0x4F,
	KEY_PGUP	= 0x49,
	KEY_PGDOWN	= 0x51,

	KEY_ENTER	= 0x1C,
	KEY_TAB		= 0x0F,

	KEY_CAPS_L	= 0x3A,

	KEY_ESC		= 0x01,

	MOD_2		= 0x60,

	KEY_LEFT	= 0x4B,
	KEY_RIGHT	= 0x4D,
	KEY_UP		= 0x48,
	KEY_DOWN	= 0x50
} keycode;

// Stores data about a specific key.
typedef struct
{
	uint8_t key_code;				// Key code
	uint8_t char_code;				// Character code
	uint8_t shift_char_code;		// Shift+<key> character code
	uint8_t alt_char_code;			// Alt+<key> character code
	uint8_t alt_shift_char_code;	// Alt+Shift+<key> character code
	bool    pressed;				// The key is pressed or released
} key_t;

// Stores data about modifier keys.
typedef struct
{
	uint8_t key_code;	// Key code
	bool    pressed;	// The key is pressed or released
} modifier_t;

// A struct to keep information about the active keys
typedef struct
{
	bool super;			// Is the super key pressd?
	bool ctrl;			// Is the ctrl key pressed?
	bool alt;			// Is the Alt key pressed?
	bool shift;			// Is the Shift key pressed?
	bool numlk;			// Is numpad enabled by numlock?
	key_t active_key;	// Active key
} active_keys_t;

/**
 * @brief			Starts keyboard handler for a specific keycode.
 * @param key_code	is the keycode of the key
 * @param pressed	indicates whether the key was
 *					pressed or released
 * @return			true if the keycode matches an
 *					existing, registered key
 */
bool handle_keyboard(uint8_t key_code, bool pressed);

// A getter function for the active key and modifiers.
active_keys_t *get_active_keys(void);

// 0s the active keys.
void null_active();

// Sets the keyboard handler.
void set_keyboard_handler(void *handler);

// Get current keyboard handler.
void *get_keyboard_handler();

// Initializes the keyboard driver.
void init_keyboard(void);

#ifdef __cplusplus
}
#endif

#endif

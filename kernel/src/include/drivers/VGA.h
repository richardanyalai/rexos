/* This file is part of RexOS!.
 *
 * RexOS! is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * RexOS! is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with RexOS!.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef __VGA_H
#define __VGA_H

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

#define TEXTMODE_SEGMENT		0xB800
#define GRAPHICS_MODE_SEGMENT	0xA000

#define TEXTMODE_ADDRESS		0xB8000
#define GRAPHICS_MODE_ADDRESS	0xA0000

#define VGA_AC_INDEX			0x3C0
#define VGA_AC_WRITE			0x3C0
#define VGA_AC_READ				0x3C1
#define VGA_MISC_WRITE			0x3C2
#define VGA_SEQ_INDEX			0x3C4
#define VGA_SEQ_DATA			0x3C5
#define VGA_DAC_READ_INDEX		0x3C7
#define VGA_DAC_WRITE_INDEX		0x3C8
#define VGA_DAC_DATA			0x3C9
#define VGA_MISC_READ			0x3CC
#define VGA_GC_INDEX 			0x3CE
#define VGA_GC_DATA 			0x3CF

#define VGA_CRTC_INDEX			0x3D4 // 0x3B4
#define VGA_CRTC_DATA			0x3D5 // 0x3B5
#define	VGA_INSTAT_READ			0x3DA

#define VGA_NUM_SEQ_REGS		5
#define VGA_NUM_CRTC_REGS		25
#define VGA_NUM_GC_REGS			9
#define VGA_NUM_AC_REGS			21
#define VGA_NUM_REGS			(				\
	1 + VGA_NUM_SEQ_REGS + VGA_NUM_CRTC_REGS	\
	+ VGA_NUM_GC_REGS + VGA_NUM_AC_REGS			\
)

#define peekb(o)			(*(uint8_t  *)(16uL * vga_get_fb_segment() + o))
#define pokeb(se, o, v)		(*(uint8_t  *)(16uL * se + o) = v)
#define pokew(se, o, v)		(*(uint16_t *)(16uL * se + o) = v)
#define vmemwr(se, d, s, c)	memcpy((char *) (se * 16 + d), s, c)

uint16_t vga_get_width(void), vga_get_height(void);

#define VGA_WIDTH			vga_get_width()
#define VGA_HEIGHT			vga_get_height()

#define vga_get_pos(x, y)	(x + y * 80)
#define vga_get_y(pos)		(pos / 80)
#define vga_get_x(pos)		(pos - vga_get_y(pos) * 80)

// Color constants for background and foreground colors
enum vga_textmode_colors
{
	BLACK,			// 00
	BLUE,
	GREEN,
	CYAN,
	RED,
	MAGENTA,		// 05
	BROWN,
	BRIGHT_GREY,
	GREY,
	BRIGHT_BLUE,
	BRIGHT_GREEN,	// 10
	BRIGHT_CYAN,
	BRIGHT_RED,
	BRIGHT_MAGENTA,
	YELLOW,
	WHITE,			// 15
};

/* Getters */

// Gets the current VGA index.
uint16_t vga_get_index(void);

// Gets the index of the next line.
uint16_t vga_get_next_line_index(void);

// Gets the x coordinate of the cursor.
uint8_t vga_get_cursor_x(void);

// Gets the y coordinate of the cursor.
uint8_t vga_get_cursor_y(void);

// Gets the background color.
uint8_t vga_get_bgcolor(void);

// Gets the foreground color.
uint8_t vga_get_fgcolor(void);

// Returns the entry at the given position.
uint16_t vga_get_entry_at(uint16_t pos);

// Inverts the given entry from the buffer.
uint16_t vga_invert_entry(uint16_t pos);

/* Setters */

// Sets the VGA index.
uint16_t vga_set_index(uint16_t index);

// Sets the index of the new line.
uint16_t vga_set_next_line_index(uint16_t index);

// Sets the x coordinate of the cursor.
uint8_t vga_set_cursor_x(uint8_t x);

// Sets the y coordinate of the cursor.
uint8_t vga_set_cursor_y(uint8_t y);

// Sets the background color.
uint8_t vga_set_bgcolor(uint8_t bgcolor);

// Sets the foreground color.
uint8_t vga_set_fgcolor(uint8_t fgcolor);

/* Textmode cursor */

// Turns the textmode cursor on with the given width.
void vga_cursor_on(uint8_t cursor_start, uint8_t cursor_end);

// Turns the textmode cursor off.
void vga_cursor_off(void);

// Moves the textmode cursor to the given coordinates.
void vga_cursor_update(uint8_t x, uint8_t y);

/* Drawing */

// Draws a filled rectangle.
void vga_draw_fill_rect(
	uint16_t x1, uint16_t y1, uint16_t x2, uint16_t y2, uint8_t color
);

// Creates a VGA entry from the given character and colors.
uint16_t vga_entry(uint8_t ch, uint8_t bgcolor, uint8_t fgcolor);

// Sets an entry in the VGA textmode buffer.
void vga_set_entry(uint16_t pos, uint16_t entry);

// Draw character with given color at the specified position.
void vga_draw_char_at(
	uint8_t ch, uint16_t pos, uint8_t bgcolor, uint8_t fgcolor
);

// Draws a character.
void vga_draw_char(uint8_t ch);

// Draws a string at the give coordinates with the given radius and color.
void vga_draw_string(
	uint16_t   start_x,
	uint16_t   start_y,
	const char *text,
	char       bgcolor,
	uint8_t    fgcolor
);

/* Other */

// Clears the VGA buffer.
void vga_clear(void);

// Initializes the VGA driver
void init_vga(uint16_t width, uint16_t height);

#ifdef __cplusplus
}
#endif

#endif

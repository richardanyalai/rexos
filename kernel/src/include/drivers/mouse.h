/* This file is part of RexOS!.
 *
 * RexOS! is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * RexOS! is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with RexOS!.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef __MOUSE_H
#define __MOUSE_H

#ifdef __cplusplus
extern "C" {
#endif

// Initialize mouse driver.
void init_mouse(void);

// Sets a handler for the left mouse button.
void set_left_click_handler(void *handler);

// Sets a handler for the right mouse button.
void set_right_click_handler(void *handler);

#ifdef __cplusplus
}
#endif

#endif

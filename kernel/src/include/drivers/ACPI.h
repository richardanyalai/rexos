/* This file is part of RexOS!.
 *
 * RexOS! is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * RexOS! is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with RexOS!.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef __ACPI_H
#define __ACPI_H

#ifdef __cplusplus
extern "C" {
#endif

// Initializes ACPI.
int ACPI_init(void);

// Enables ACPI.
int ACPI_enable(void);

// Powers off the computer
void ACPI_power_off(void);

#ifdef __cplusplus
}
#endif

#endif

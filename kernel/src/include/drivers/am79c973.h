/* This file is part of RexOS!.
 *
 * RexOS! is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * RexOS! is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with RexOS!.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef __am79c973_H
#define __am79c973_H

#include <drivers/PCI.h>
#include <stdint.h>

#define MAC_ADDR_0_PORT			(am79c973->port_base + 0x00)
#define MAC_ADDR_2_PORT			(am79c973->port_base + 0x02)
#define MAC_ADDR_4_PORT			(am79c973->port_base + 0x04)

#define REG_DATA_PORT			(am79c973->port_base + 0x10)
#define REG_ADDR_PORT			(am79c973->port_base + 0x12)
#define RESET_PORT				(am79c973->port_base + 0x14)
#define BUS_CTRL_REG_DATA_PORT	(am79c973->port_base + 0x16)

#ifdef __cplusplus
extern "C" {
#endif

typedef struct
{
	uint16_t mode;
	uint32_t reserved1			: 4;
	uint32_t num_send_buffers	: 4;
	uint32_t reserved2			: 4;
	uint32_t num_recv_buffers	: 4;
	uint64_t physical_address	: 48;
	uint16_t reserved3;
	uint64_t logical_address;
	uint32_t recv_buffer_desc_addr;
	uint32_t send_buffer_desc_addr;

} __attribute__((packed)) init_block_t;

typedef struct
{
	uint32_t address;
	uint32_t flags;
	uint32_t flags2;
	uint32_t avail;
} __attribute__((packed)) buffer_descriptor_t;

// Returns MAC address.
uint64_t am79c973_get_MAC_address(void);

// Returns IP address.
uint32_t am79c973_get_IP_address(void);

// Sets IP address.
void am79c973_set_IP_address(uint32_t ip);

// Sends raw data.
void am79c973_send(uint8_t *buffer, uint32_t size);

// Initializes the network card driver.
void am79c973_init(pci_device_t *dev);

#ifdef __cplusplus
}
#endif

#endif
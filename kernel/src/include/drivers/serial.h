/* This file is part of RexOS!.
 *
 * RexOS! is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * RexOS! is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with RexOS!.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef __SERIAL_H
#define __SERIAL_H

#include <stdarg.h>
#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

#define COM1		0x3F8
#define COM2		0x2F8
#define COM3		0x3E8
#define COM4		0x2E8

#define SERIAL_IRQ	0x24

// Prints the given string to the serial device.
void serial_print(uint32_t device, char *str);

// Prints formatted stringd
int serial_printf(const char *__restrict format, ...);

void init_serial_ports();

#ifdef __cplusplus
}
#endif

#endif

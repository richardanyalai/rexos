/* This file is part of RexOS!.
 *
 * RexOS! is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * RexOS! is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with RexOS!.  If not, see <https://www.gnu.org/licenses/>.
 */

/* Interface for Interrupt Descriptor Tables.
 * Written for JamesM's kernel development tutorials,
 * rewritten by Richard Anyalai for the RexOS! project.
 */

#ifndef __IDT_H
#define __IDT_H

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

// A struct describing an interrupt gate.
typedef struct
{
	uint16_t	base_lo;	// The lower 16 bits of the address to jump to when
							// this interrupt fires
	uint16_t	selector;	// Kernel segment selector
	uint8_t		reserved;	// This must always be zero
	uint8_t		flags;		// More flags. See documentation
	uint16_t	base_hi;	// The upper 16 bits of the address to jump
} __attribute__((packed)) idt_entry_t;

// Stores idt pointer information.
typedef struct
{
	uint16_t	limit;
	uint32_t	base;		// is the address of the first element in our
							// idt_entry_t array
} __attribute__((packed)) idt_ptr_t;

// Here come the interrupts:
void isr0 ();
void isr1 ();
void isr2 ();
void isr3 ();
void isr4 ();
void isr5 ();
void isr6 ();
void isr7 ();
void isr8 ();
void isr9 ();
void isr10();
void isr11();
void isr12();
void isr13();
void isr14();
void isr15();
void isr16();
void isr17();
void isr18();
void isr19();
void isr20();
void isr21();
void isr22();
void isr23();
void isr24();
void isr25();
void isr26();
void isr27();
void isr28();
void isr29();
void isr30();
void isr31();
void irq0 ();
void irq1 ();
void irq2 ();
void irq3 ();
void irq4 ();
void irq5 ();
void irq6 ();
void irq7 ();
void irq8 ();
void irq9 ();
void irq10();
void irq11();
void irq12();
void irq13();
void irq14();
void irq15();
void isr128();

// Initializes the Interrupt Descriptor Table.
void init_idt(void);

#ifdef __cplusplus
}
#endif

#endif

/* This file is part of RexOS!.
 *
 * RexOS! is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * RexOS! is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with RexOS!.  If not, see <https://www.gnu.org/licenses/>.
 */

/* Interface for Global Descriptor Tables.
 * Written for JamesM's kernel development tutorials,
 * rewritten by Richard Anyalai for the RexOS! project.
 * Some useful links:
 * REF: https://wiki.osdev.org/GDT_Tutorial
 * REF: https://wiki.osdev.org/Global_Descriptor_Table
 * REF: https://en.wikipedia.org/wiki/Global_Descriptor_Table
 */

#ifndef __GDT_H
#define __GDT_H

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

// Stores gdt entry information.
typedef struct
{
	uint16_t limit_lo;		// The lower 16 bits of the limit
	uint16_t base_lo;		// The lower 16 bits of the base
	uint8_t base_mid;		// The next 8 bits of the base
	uint8_t access;			// Determine what ring this segment can be used in
	uint8_t granularity;
	uint8_t base_hi;		// The lower 16 bits of the limit  
} __attribute__((packed)) gdt_entry_t;

// Stores gdt pointer informations.
typedef struct
{
	uint16_t limit;			// The upper 16 bits of all selector limits
	uint32_t base;			// The address of the first gdt_entry_t
} __attribute__((packed)) gdt_ptr_t;

// Allows the kernel stack in the TSS to be changed.
void set_kernel_stack(uint32_t stack);

// Initializes the Global Descriptor Table.
void init_gdt(void);

#ifdef __cplusplus
}
#endif

#endif

/* This file is part of RexOS!.
 *
 * RexOS! is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * RexOS! is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with RexOS!.  If not, see <https://www.gnu.org/licenses/>.
 */

/* Interface for Task State Segments.
 * Written for JamesM's kernel development tutorials,
 * rewritten by Richard Anyalai for the RexOS! project.
 */

#ifndef __TSS_H
#define __TSS_H

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

typedef struct
{
	uint32_t prev_tss;	// Previous TSS - if we used hardware task switching
						// this would form a linked list.
	uint32_t esp0;		// Stack pointer to load when we change to kernel mode.
	uint32_t ss0;		// Stack segment to load when we change to kernel mode.
	uint32_t esp1;		// Unused...
	uint32_t ss1;
	uint32_t esp2;
	uint32_t ss2;
	uint32_t cr3;
	uint32_t eip;
	uint32_t eflags;
	uint32_t eax;
	uint32_t ecx;
	uint32_t edx;
	uint32_t ebx;
	uint32_t esp;
	uint32_t ebp;
	uint32_t esi;
	uint32_t edi;
	uint32_t es;		// Load into ES when we change to kernel mode.
	uint32_t cs;		// Load into CS when we change to kernel mode.
	uint32_t ss;		// Load into SS when we change to kernel mode.
	uint32_t ds;		// Load into DS when we change to kernel mode.
	uint32_t fs;		// Load into FS when we change to kernel mode.
	uint32_t gs;		// Load into GS when we change to kernel mode.
	uint32_t ldt;		// Unused...
	uint16_t trap;
	uint16_t iomap_base;

} __attribute__((packed)) tss_entry_t;

#ifdef __cplusplus
}
#endif

#endif

/* This file is part of RexOS!.
 *
 * RexOS! is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * RexOS! is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with RexOS!.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef __REGISTERS_H
#define __REGISTERS_H

#include <stdint.h>

// Stores information about the registers.
typedef struct
{	
	uint32_t
	// Data segment selector
	ds,
	// Pushed by pusha
	edi, esi, ebp, useless_esp, ebx, edx, ecx, eax,
	// Interrupt number and error code (if applicable)
	int_no, err_code,
	// Pushed by the processor automatically
	eip, cs, eflags, esp, ss;
} registers_t;

#endif

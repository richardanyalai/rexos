/* This file is part of RexOS!.
 *
 * RexOS! is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * RexOS! is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with RexOS!.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef __PORT_H
#define __PORT_H

#include <stdint.h>

#define PORT_MCP	0x20
#define PORT_MDP	0x21
#define PORT_SCP	0xA0
#define PORT_SDP	0xA1

#define inb			read_8_bit
#define outb		write_8_bit
#define inw			read_16_bit
#define outw		write_16_bit
#define inl			read_32_bit
#define outl		write_32_bit

#ifdef __cplusplus
extern "C" {
#endif

// Read from 8/16/32 bit ports.
uint8_t  read_8_bit(uint16_t port_number);
uint16_t read_16_bit(uint16_t port_number);
uint32_t read_32_bit(uint16_t port_number);

// Write to 8/16/32 bit ports.
void write_8_bit(uint16_t port_number, uint8_t data);
void write_8_bit_slow(uint16_t port_number, uint8_t data);
void write_16_bit(uint16_t port_number, uint16_t data);
void write_32_bit(uint16_t port_number, uint32_t data);

#ifdef __cplusplus
}
#endif

#endif

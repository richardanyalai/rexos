/* This file is part of RexOS!.
 *
 * RexOS! is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * RexOS! is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with RexOS!.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef __ARP_H
#define __ARP_H

#include <net/etherframe.h>
#include <stdint.h>

#define ETHER_FRAME_TYPE_ARP BIGENDIAN(0x806)

#ifdef __cplusplus
extern "C" {
#endif

typedef struct
{
	uint16_t hardware_type;
	uint16_t protocol;
	uint8_t  hardware_addr_size;
	uint8_t  protocol_addr_size;
	uint16_t command;
	uint64_t src_MAC : 48;
	uint32_t src_IP;
	uint64_t dst_MAC : 48;
	uint32_t dst_IP;
} __attribute__((packed)) arp_msg_t;

// Broadcasts our MAC address.
void arp_broadcast_MAC_address(uint32_t IP_BE);

// Receiving ARP message.
bool arp_on_ether_frame_received(uint8_t *payload, uint32_t size);

// Returns the MAC address.
uint64_t arp_resolve(uint32_t IP_BE);

#ifdef __cplusplus
}
#endif

#endif

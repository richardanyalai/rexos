/* This file is part of RexOS!.
 *
 * RexOS! is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * RexOS! is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with RexOS!.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef __ICMP_H
#define __ICMP_H

#include <net/etherframe.h>
#include <stdbool.h>
#include <stdint.h>

#define PROTOCOL_TYPE_ICMP 0x01

#ifdef __cplusplus
extern "C" {
#endif

typedef struct
{
	uint8_t  type;
	uint8_t  code;
	uint16_t checksum;
	uint32_t data;
} icmp_msg_t;

// This function handles the incoming IPv4 messages.
bool ICMP_on_IP_received(
	uint32_t src_IP_BE, uint32_t dst_IP_BE, uint8_t *payload, uint32_t size
);

// Send ping message.
void request_echo_reply(uint32_t IP_BE);

// Handler for ping responses;
void ICMP_set_ping_handler(void *handler);

#ifdef __cplusplus
}
#endif

#endif

/* This file is part of RexOS!.
 *
 * RexOS! is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * RexOS! is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with RexOS!.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef __TCP_HPP
#define __TCP_HPP

#include <stdint.h>

#define PROTOCOL_TYPE_TCP 0x06

#ifdef __cplusplus

enum TCP_socket_state
{
	CLOSED,
	LISTEN,
	SYN_SENT,
	SYN_RECEIVED,

	ESTABLISHED,

	FIN_WAIT_1,
	FIN_WAIT_2,
	CLOSING,
	TIME_WAIT,

	CLOSE_WAIT
};

enum TCP_flag
{
	FIN = 1,
	SYN = 2,
	RST = 4,
	PSH = 8,
	ACK = 16,
	URG = 32,
	ECE = 64,
	CWR = 128,
	NS  = 256
};

typedef struct
{
	uint16_t src_port;
	uint16_t dst_port;
	uint32_t seq_num;
	uint32_t ack_num;

	uint8_t  reserved      : 4;
	uint8_t  header_size_32: 4;
	uint8_t  flags;

	uint16_t window_size;
	uint16_t checksum;
	uint16_t urgent_ptr;

	uint32_t options;
} __attribute__((packed)) TCP_header_t;

typedef struct
{
	uint32_t src_IP;
	uint32_t dst_IP;
	uint16_t protocol;
	uint16_t total_length;
} __attribute__((packed)) TCP_pseudo_header_t;

class TCP_socket;
class TCP_provider;

class TCP_handler
{
	public:
		bool handle_TCP_message(
			TCP_socket *socket, uint8_t *data, uint16_t size
		);

		TCP_handler();
		~TCP_handler();
};

class TCP_socket
{
	friend class TCP_provider;

	protected:
		uint16_t remote_port;
		uint32_t remote_IP;
		uint16_t local_port;
		uint32_t local_IP;
		uint32_t seq_num;
		uint32_t ack_num;

		TCP_provider     *backend;
		TCP_handler      *handler;
		TCP_socket_state state;

	public:
		virtual bool handle_TCP_message(uint8_t *data, uint16_t size);
		virtual void send(uint8_t *data, uint16_t size);
		virtual void disconnect();

		TCP_socket(TCP_provider *backend);
		~TCP_socket();
};

class TCP_provider
{
	protected:
		TCP_socket *sockets[65535];
		uint16_t   num_sockets;
		uint16_t   free_port;

	public:
		virtual bool on_IP_received(
			uint32_t src_IP_BE,
			uint32_t dst_IP_BE,
			uint8_t  *payload,
			uint32_t size
		);

		virtual TCP_socket *connect(uint32_t ip, uint16_t port);
		virtual void disconnect(TCP_socket *socket);
		virtual void send(
			TCP_socket *socket,
			uint8_t    *data,
			uint16_t   size,
			uint16_t   flags = 0
		);

		virtual TCP_socket *listen(uint16_t port);
		virtual void bind(TCP_socket *socket, TCP_handler *handler);

		TCP_provider();
		~TCP_provider();
};

#endif

#ifdef __cplusplus
extern "C" {
#endif

bool TCP_on_IP_received(
	uint32_t src_IP_BE, uint32_t dst_IP_BE, uint8_t *payload, uint32_t size
);
void TCP_connect(uint32_t ip, uint16_t port);
void TCP_listen(uint16_t port);
void TCP_send(uint8_t *message, uint16_t size);
void TCP_disconnect(void);

#ifdef __cplusplus
}
#endif

#endif

/* This file is part of RexOS!.
 *
 * RexOS! is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * RexOS! is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with RexOS!.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef __ETHERNET_FRAME_H
#define __ETHERNET_FRAME_H

#include <stdbool.h>
#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

typedef struct
{
	uint64_t dst_MAC_BE : 48;
	uint64_t src_MAC_BE : 48;
	uint16_t ether_type_BE;
} __attribute__((packed)) ether_frame_header_t;

// Initializes the handlers.
void init_ether_frame_handlers(void);

// Handles raw data.
bool efp_on_raw_data_received(uint8_t *buffer, uint32_t size);

// Send data from ethernet frame provider.
void efp_send(
	uint64_t dst_MAC_BE, uint16_t ether_type_BE, uint8_t *buffer, uint32_t size
);

#ifdef __cplusplus
}
#endif

#endif

/* This file is part of RexOS!.
 *
 * RexOS! is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * RexOS! is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with RexOS!.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef __IPV4_H
#define __IPV4_H

#include <stdbool.h>
#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

#define ETHER_FRAME_TYPE_IPv4	BIGENDIAN(0x800)

#define ARRAY_TO_IPv4(X)	(							 \
								((uint32_t) X[3] << 24) |\
								((uint32_t) X[2] << 16) |\
								((uint32_t) X[1] <<  8) |\
								((uint32_t) X[0])		 \
							);

typedef struct
{
	uint8_t  header_length		: 4;
	uint8_t  version			: 4;
	uint8_t  tos;
	uint16_t total_length;

	uint16_t ident;
	uint16_t flags_and_offset;

	uint8_t  time_to_live;
	uint8_t  protocol;
	uint16_t checksum;

	uint32_t src_IP;
	uint32_t dst_IP;
} IPv4_msg_t;

// Initializes the IPv4 handlers.
void init_IPv4_handlers(void);

// Calculates the checksum of an IPv4 message.
uint16_t ip_checksum(uint16_t *data, uint32_t length_in_bytes);

// Sends IPv4 message to the given address.
void ip_send(
	uint32_t dst_IP_BE, uint8_t protocol, uint8_t *data, uint32_t size
);

// Handles incoming ethernet frames.
bool ip_on_ether_frame_received(uint8_t *payload, uint32_t size);

#ifdef __cplusplus
}
#endif

#endif

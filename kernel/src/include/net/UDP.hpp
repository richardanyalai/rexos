/* This file is part of RexOS!.
 *
 * RexOS! is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * RexOS! is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with RexOS!.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef __UDP_HPP
#define __UDP_HPP

#include <stdbool.h>
#include <stdint.h>

#define PROTOCOL_TYPE_UDP 0x11

#ifdef __cplusplus

typedef struct
{
	uint16_t src_port;
	uint16_t dst_port;
	uint16_t length;
	uint16_t checksum;
} __attribute__((packed)) UDP_header_t;

class UDP_socket;

class UDP_handler
{
	public:
		void handle_UDP_message(uint8_t *data, uint16_t size);
	
		UDP_handler();
		~UDP_handler();
};

class UDP_provider;

class UDP_socket
{
	friend class UDP_provider;

	protected:
		uint16_t remote_port;
		uint32_t remote_IP;
		uint16_t local_port;
		uint32_t local_IP;

		UDP_provider *backend;
		UDP_handler  *handler;

		bool listening;

	public:
		virtual void handle_UDP_message(uint8_t *data, uint16_t size);
		virtual void send(uint8_t *data, uint16_t size);
		virtual void disconnect();

		UDP_socket(UDP_provider *backend);
		~UDP_socket();
};

class UDP_provider
{
	protected:
		UDP_socket *sockets[65535];
		uint16_t   num_sockets;
		uint16_t   free_port;

	public:
		virtual bool on_IP_received(
			uint32_t src_IP_BE,
			uint32_t dst_IP_BE,
			uint8_t  *payload,
			uint32_t size
		);
		virtual UDP_socket *connect(uint32_t IP, uint16_t port);
		virtual UDP_socket *listen(uint16_t port);
		virtual UDP_socket *new_socket(uint32_t ip, uint16_t port);
		virtual void disconnect(UDP_socket *socket);
		virtual void send(UDP_socket *socket, uint8_t *data, uint16_t size);
		virtual void bind(UDP_socket *socket, UDP_handler *handler);

		UDP_provider();
		~UDP_provider();
};

#endif

#ifdef __cplusplus
extern "C" {
#endif

bool UDP_on_IP_received(
	uint32_t src_IP_BE, uint32_t dst_IP_BE, uint8_t *payload, uint32_t size
);
void UDP_connect(uint32_t ip, uint16_t port);
void UDP_listen(uint16_t port);
void UDP_send(uint8_t *message, uint16_t size);
void UDP_disconnect(void);
void UDP_set_handler(void *h);

#ifdef __cplusplus
}
#endif

#endif

/* This file is part of RexOS!.
 *
 * RexOS! is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * RexOS! is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with RexOS!.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef __LINKED_LISTS_HPP
#define __LINKED_LISTS_HPP

#include <stdint.h>
#include <string.hpp>

namespace std
{
	template<typename T>
	class Node
	{
	public:
		// The value of a node
		T value;

		// Pointer to the next node
		struct Node<T> *next;

		// Pointer to the previous node
		struct Node<T> *prev;

		// Constructor
		Node<T>(T value);

		// Parameterless constructor
		Node<T>();

		// Destructor
		~Node<T>();
	};

	template<typename T>
	class LinkedList
	{
	private:
		// The head (first node) of the list
		Node<T> *head;

		// The tail (last node) of the list
		Node<T> *tail;

		// The size of the list
		uint32_t size;

	public:
		//returns the size of the list.
		uint32_t get_size();

		// Return the head of the list.
		Node<T> *get_head();

		// Return the tail of the list.
		Node<T> *get_tail();

		/**
		 * @brief			Inserts a node after a given node.
		 * @param after		is the node we are inserting after
		 * @param insert	is the node we want to insert
		 * @return			pointer to the inserted node
		 */
		Node<T> *insert_node_after(Node<T> *after, Node<T> *insert);

		/**
		 * @brief			Inserts a node after a given node.
		 * @param before	is the node we are inserting before
		 * @param insert	is the node we want to insert
		 * @return			pointer to the inserted node
		 */
		Node<T> *insert_node_before(Node<T> *before, Node<T> *insert);
			
		/**
		 * @brief			Finds a node.
		 * @param value		is the value of the node
		 * @return			the first node which has the given value
		 */
		Node<T> *find(T value);

		/**
		 * @brief			Removes a node.
		 * @param node		is the node we want to delete
		 * @return			true if the operation succeeded
		 */
		bool remove(Node<T> *node);

		/**
		 * @brief			Initializes the list with a header.
		 * @param header	is the new header
		 */
		void init(Node<T> *head);

		// Constructor
		LinkedList<T>();

		// Destructor
		~LinkedList<T>();
	};

	template class Node<uint8_t>;
	template class Node<uint16_t>;
	template class Node<uint32_t>;
	template class Node<int>;
	template class Node<float>;
	template class Node<std::string>;

	template class LinkedList<uint8_t>;
	template class LinkedList<uint16_t>;
	template class LinkedList<uint32_t>;
	template class LinkedList<int>;
	template class LinkedList<float>;
	template class LinkedList<std::string>;
}

#endif

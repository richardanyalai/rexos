/* This file is part of RexOS!.
 *
 * RexOS! is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * RexOS! is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with RexOS!.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef __STACK_HPP
#define __STACK_HPP

#include <ds/linked_list.hpp>
#include <stdbool.h>
#include <stdint.h>

namespace std
{
	template <typename T>
	class Stack : public LinkedList<T>
	{
	public:
		// Returns the size of the stack.
		uint32_t get_size();

		/**
		 * @brief 		Pushes the given node to the top of the stack.
		 * @param node	is the node we want to push
		 */
		void push(T item);

		// Returns the element on top and removes it from the stack.
		Node<T> *pop();

		// Constructor
		Stack<T>();

		// Destructor
		~Stack<T>();
	};

	template class Stack<int>;
}

#endif

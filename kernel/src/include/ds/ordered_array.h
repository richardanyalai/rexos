/* This file is part of RexOS!.
 *
 * RexOS! is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * RexOS! is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with RexOS!.  If not, see <https://www.gnu.org/licenses/>.
 */

/* Interface for creating, inserting and deleting from ordered arrays.
 * Written for JamesM's kernel development tutorials,
 * rewritten by Richard Anyalai for the RexOS! project.
 */

#ifndef __ORDERED_ARRAY_H
#define __ORDERED_ARRAY_H

#include <stdbool.h>
#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

/* This array is insertion sorted - it always remains in a sorted state
 * (between calls).
 * It can store anything that can be cast to a void * -- so a uint32_t, or any
 * pointer.
 */
typedef void *type_t;

/* A predicate should return nonzero if the first argument is less than the
 *second. Else it should return zero.
 */
typedef bool (*lessthan_predicate_t)(type_t, type_t);
typedef struct
{
	type_t *array;
	uint32_t size;
	uint32_t max_size;
	lessthan_predicate_t less_than;
} ordered_array_t;

/**
 * @brief		A standard less than predicate.
 * @param a		is the first item we want to compare
 * @param b		is the second item we want to
 *				compare with the first item
 */
bool standard_lessthan_predicate(type_t a, type_t b);

/**
 * @brief		Destroys an array.
 * @param array	is the array we want to destroy
 */
ordered_array_t create_ordered_array(
	uint32_t max_size, lessthan_predicate_t less_than
);

/**
 * @brief		Destroys an array.
 * @param array	is the array we want to destroy
 */
ordered_array_t place_ordered_array(
	void *addr, uint32_t max_size, lessthan_predicate_t less_than
);

/**
 * @brief		Destroys an array.
 * @param array	is the array we want to destroy
 */
void destroy_ordered_array(ordered_array_t *array);

/**
 * @brief		Adds an item to an array.
 * @param item	is the item
 * @param array	is the array we want our item to store in
 */
void insert_ordered_array(type_t item, ordered_array_t *array);

/**
 * @brief		Lookup the item at index i.
 * @param i		is the index
 * @param array	is the array we want to lookup the item in
 * @return		the item we are searchng for
 */
type_t lookup_ordered_array(uint32_t i, ordered_array_t *array);

/**
 * @brief		Deletes item at a specific location from an array.
 * @param i		is the location
 * @param array	is the array we want to delete an item from
 */
void remove_ordered_array(uint32_t i, ordered_array_t *array);

#ifdef __cplusplus
}
#endif

#endif

/* This file is part of RexOS!.
 *
 * RexOS! is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * RexOS! is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with RexOS!.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef __UTILS_H
#define __UTILS_H

#include <drivers/ACPI.h>
#include <stdarg.h>
#include <stdint.h>

#define UNUSED(var)		((void) var);

#define BIGENDIAN(X)	(((X & 0x00FF) << 8) | ((X & 0xFF00) >> 8))

#define ASSERT(MESSAGE)	((MESSAGE) ? 0 : PANIC("ASSERT ERROR"))
#define LOG(MESSAGE)	(serial_print(0x3F8, MESSAGE))

void log(uint8_t type, const char *__restrict format, ...);

#define KINF(args...)	log(0, args)
#define KOK(args...)	log(1, args)
#define KERR(args...)	log(2, args)

/**
 * @brief			Simple macro to use the debug function.
 * @see				debug
 * @param MESSAGE	is the message that gets printed out
 */
#define PANIC(MESSAGE) debug( \
	__FILE__, (__func__ != 0) ? __func__ : "none", __LINE__, MESSAGE \
)

#ifdef __cplusplus
extern "C" {
#endif

/**
 * Prints a debug message on standard output.
 * @param file		is the name of the source file
 * @param function	is the name of the caller function
 * @param line		is the line number at which the function
 *					has been invoked
 * @param message	is the debug message
 */
void debug(
	const char *file, const char *function, uint32_t line, const char *message
);

#define shutdown ACPI_power_off

// Reboots the OS.
void reboot(void);

// Enables floating-point arithmetic unit.
void enable_fpu(void);

#ifdef __cplusplus
}
#endif

#endif

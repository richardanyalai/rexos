/* This file is part of RexOS!.
 *
 * RexOS! is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * RexOS! is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with RexOS!.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef __GRAPHICS_MODE_H
#define __GRAPHICS_MODE_H

#include <drivers/VGA.h>
#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

// Draws a pixel at the give coordinates with the given color.
void vga_draw_pixel(uint16_t x, uint16_t y, uint8_t color);

// Draws a line connecting the give coordinates with the given color.
void vga_draw_line(
	uint16_t x1, uint16_t y1, uint16_t x2, uint16_t y2, uint8_t color
);

// Draws a rectangle at the give coordinates with the given color.
void vga_draw_rect(
	uint16_t x1, uint16_t y1, uint16_t x2, uint16_t y2, uint8_t color
);

// Draws a filled rectangle at the give coordinates with the given color.
void vga_draw_fill_rect(
	uint16_t x1, uint16_t y1, uint16_t x2, uint16_t y2, uint8_t color
);

// Draws a triangle at the give coordinates with the given color.
void vga_draw_triangle(
	uint16_t x1, uint16_t y1,
	uint16_t x2, uint16_t y2,
	uint16_t x3, uint16_t y3,
	uint8_t color
);

// Draws an ellipse at the give coordinates with the given radius and color.
void vga_draw_ellipse(
	uint16_t xc, uint16_t yc, uint16_t rx, uint16_t ry, uint8_t color
);

// Draws a string at the give coordinates with the given radius and color.
void vga_draw_string(
	uint16_t start_x,
	uint16_t start_y,
	const char *text,
	char bgcolor,
	uint8_t fgcolor
);

// Draws an image starting at the given offsets
void vga_draw_image(
	const uint8_t *image,
	uint16_t start_x,
	uint16_t start_y,
	uint16_t width,
	uint16_t height
);

#ifdef __cplusplus
}
#endif

#endif

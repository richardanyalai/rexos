/* This file is part of RexOS!.
 *
 * RexOS! is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * RexOS! is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with RexOS!.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef __TEXT_MODE_H
#define __TEXT_MODE_H

#include <stdbool.h>
#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief			Draws a rectangle on the screen
 * @param x1		is the x coordinate start position
 * @param y1		is the y coordinate start position
 * @param x2		is the x coordinate end position
 * @param y2		is the y coordinate end position
 * @param bg		is the background color
 */
void textmode_draw_rect(
	uint8_t x1,
	uint8_t y1,
	uint8_t x2,
	uint8_t y2,
	uint8_t bg
);

/**
 * @brief			Draws a basic window.
 * @param x1		top left X coordinate
 * @param y1		top left Y coordinate
 * @param x2		bottom right X coordinate
 * @param y2		bottom right Y coordinate
 * @param bg		is the background color
 * @param shadow	is true if we want to display shadow
 */
void draw_window(
	uint8_t x1, uint8_t y1, uint8_t x2, uint8_t y2, uint8_t bg, bool shadow
);

/**
 * @brief			Draws a basic window.
 * @param x1		top left X coordinate
 * @param y1		top left Y coordinate
 * @param x2		bottom right X coordinate
 * @param y2		bottom right Y coordinate
 * @param bg		is the background color
 * @param fg		is the foreground color
 * @param shadow	is true if we want to display shadow
 */
void draw_window_with_border(
	uint8_t x1,
	uint8_t y1,
	uint8_t x2,
	uint8_t y2,
	uint8_t bg,
	uint8_t fg,
	bool shadow
);

/**
 * @brief			Draws a basic window.
 * @param x1		top left X coordinate
 * @param y1		top left Y coordinate
 * @param x2		bottom right X coordinate
 * @param y2		bottom right Y coordinate
 * @param title_x	is where the title starts
 * @param bg		is the background color
 * @param fg		is the foreground color
 * @param title		is the window title
 * @param shadow	is true if we want to display borrder
 * @param shadow	is true if we want to display shadow
 */
void draw_window_with_title(
	uint8_t x1,
	uint8_t y1,
	uint8_t x2,
	uint8_t y2,
	uint8_t title_x,
	uint8_t bg,
	uint8_t fg,
	char *title,
	bool border,
	bool shadow
);

#ifdef __cplusplus
}
#endif

#endif

/* This file is part of RexOS!.
 *
 * RexOS! is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * RexOS! is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with RexOS!.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <io/ports.h>
#include <stdint.h>

uint8_t read_8_bit(uint16_t port_num)
{
	uint8_t data;
	asm volatile ("inb %1, %0" : "=a"(data) : "d"(port_num));
	return data;
}

uint16_t read_16_bit(uint16_t port_num)
{
	uint16_t data;
	asm volatile ("inw %1, %0" : "=a"(data) : "d"(port_num));
	return data;
}

uint32_t read_32_bit(uint16_t port_num)
{
	uint32_t data;
	asm volatile ("inl %1, %0" : "=a"(data) : "d"(port_num));
	return data;
}

void write_8_bit(uint16_t port_num, uint8_t data)
{ asm volatile ("outb %0, %1" : : "a"(data), "Nd"(port_num)); }

void write_8_bit_slow(uint16_t port_num, uint8_t data)
{
	asm volatile (
		"outb %0, %1\njmp 1f\n1: jmp 1f\n1:" : : "a"(data),
		"Nd"(port_num)
	);
}

void write_16_bit(uint16_t port_num, uint16_t data)
{ asm volatile ("outw %0, %1" : : "a"(data), "Nd"(port_num)); }

void write_32_bit(uint16_t port_num, uint32_t data)
{ asm volatile ("outl %0, %1" : : "a"(data), "Nd"(port_num)); }

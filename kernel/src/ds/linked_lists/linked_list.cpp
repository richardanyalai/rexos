/* This file is part of RexOS!.
 *
 * RexOS! is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * RexOS! is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with RexOS!.  If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * Some useful material:
 * REF: https://en.wikipedia.org/wiki/Linked_list
 * REF: https://www.youtube.com/watch?v=VOpjAHCee7c&t=910s
 * REF: https://www.youtube.com/watch?v=KFbm6lkMhgw
 */

#include <ds/linked_list.hpp>
#include <stdint.h>

namespace std
{
	template <typename T>
	LinkedList<T>::LinkedList()
	{ LinkedList<T>::init(0); }

	template <typename T>
	LinkedList<T>::~LinkedList() {}

	template <typename T>
	Node<T> *LinkedList<T>::insert_node_after(Node<T> *after, Node<T> *insert)
	{
		insert->next = after->next;

		if (insert->next != 0)
			insert->next->prev = insert;

		if (after == tail)
			tail = insert;

		insert->prev = after;
		after->next = insert;
				
		size++;

		return insert;
	}

	template <typename T>
	Node<T> *LinkedList<T>::insert_node_before(Node<T> *before, Node<T>* insert)
	{
		insert->prev = before->prev;

		if (insert->prev != 0)
			insert->prev->next = insert;

		if (before == head)
			head = insert;

		insert->next = before;
		before->prev = insert;
				
		size++;

		return insert;
	}

	template <typename T>
	Node<T> *LinkedList<T>::find(T value)
	{
		Node<T> *tmp = head;

		while (tmp->next != 0 && tmp->value != value)
			tmp = tmp->next;

		if (tmp->value == value)
			return tmp;
		else
			return 0;
	}

	template <typename T>
	bool LinkedList<T>::remove(Node<T>* node)
	{
		if (node == 0)
			return false;

		node->prev->next = node->next;

		if (node->next != 0)
			node->next->prev = node->prev;

		if (node == head && node->next != 0)
			head = node->next;

		if (node == tail && node->prev != 0)
			tail = node->prev;

		node->next = 0;
		node->prev = 0;

		delete node;

		size--;

		return true;
	}

	template <typename T>
	Node<T> *LinkedList<T>::get_head()
	{ return head; }

	template <typename T>
	Node<T> *LinkedList<T>::get_tail()
	{ return tail; }

	template <typename T>
	uint32_t LinkedList<T>::get_size()
	{ return this->size; }

	template <typename T>
	void LinkedList<T>::init(Node<T> *head)
	{
		size = 1;
		this->head = head;
		this->tail = head;
	}
}

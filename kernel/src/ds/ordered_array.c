/* This file is part of RexOS!.
 *
 * RexOS! is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * RexOS! is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with RexOS!.  If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * Implementation for creating, inserting and deleting from ordered arrays.
 * Written for JamesM's kernel development tutorials,
 * rewritten by Richard Anyalai for the RexOS! project.
 * 
 * REF: https://en.wikipedia.org/wiki/Sorted_array
 */

#include <ds/ordered_array.h>
#include <memory/heap.h>
#include <utils.h>
#include <string.h>

bool standard_lessthan_predicate(type_t a, type_t b)
{ return a < b; }

ordered_array_t create_ordered_array(
	uint32_t max_size, lessthan_predicate_t less_than
)
{
	ordered_array_t to_ret;

	to_ret.array		= (void *) kmalloc(max_size * sizeof(type_t));

	memset(to_ret.array, 0, max_size * sizeof(type_t));

	to_ret.size			= 0;
	to_ret.max_size		= max_size;
	to_ret.less_than	= less_than;

	return to_ret;
}

ordered_array_t place_ordered_array(
	void *addr, uint32_t max_size, lessthan_predicate_t less_than
)
{
	ordered_array_t to_ret;

	to_ret.array		= (type_t *) addr;

	memset(to_ret.array, 0, max_size * sizeof(type_t));

	to_ret.size			= 0;
	to_ret.max_size		= max_size;
	to_ret.less_than	= less_than;

	return to_ret;
}

void destroy_ordered_array(ordered_array_t *array)
{ kfree(array->array); }

void insert_ordered_array(type_t item, ordered_array_t *array)
{
	ASSERT(array->less_than);
	uint32_t iterator = 0;

	while (
		iterator < array->size &&
		array->less_than(array->array[iterator++], item)
	);

	if (iterator == array->size)
		array->array[array->size++] = item;	// Just add at the end of the array.
	else
	{
		type_t tmp = array->array[iterator];

		array->array[iterator] = item;

		while (iterator < array->size)
		{
			type_t tmp2				= array->array[++iterator];
			array->array[iterator]	= tmp;
			tmp						= tmp2;
		}
		array->size++;
	}
}

type_t lookup_ordered_array(uint32_t i, ordered_array_t* array)
{
	ASSERT(i < array->size);
	return array->array[i];
}

void remove_ordered_array(uint32_t i, ordered_array_t* array)
{
	while (i < array->size)
	{
		array->array[i] = array->array[i+1];
		i++;
	}

	array->size--;
}

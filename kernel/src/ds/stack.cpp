/* This file is part of RexOS!.
 *
 * RexOS! is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * RexOS! is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with RexOS!.  If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * Some useful material:
 * REF: https://en.wikipedia.org/wiki/Stack_(abstract_data_type)
 * REF: https://www.youtube.com/watch?v=A4sRhuGkRb0&t=643s
 */

#include <ds/stack.hpp>

namespace std
{
	template <typename T>
	Stack<T>::Stack() : LinkedList<T>()
	{}

	template <typename T>
	Stack<T>::~Stack()
	{}

	template <typename T>
	uint32_t Stack<T>::get_size()
	{ return LinkedList<T>::get_size(); }

	template <typename T>
	void Stack<T>::push(T item)
	{
		Node<T> *node = new Node<T>(item);

		if (LinkedList<T>::get_head() == 0)
			LinkedList<T>::init(node);
		else
			LinkedList<T>::insert_node_after(LinkedList<T>::get_tail(), node);
	}

	template <typename T>
	Node<T> *Stack<T>::pop()
	{
		Node<T> *tail = LinkedList<T>::get_tail();
		LinkedList<T>::remove(tail);
		return tail;
	}
}

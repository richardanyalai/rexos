/* This file is part of RexOS!.
 *
 * RexOS! is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * RexOS! is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with RexOS!.  If not, see <https://www.gnu.org/licenses/>.
 */

/* Implementation for paging.
 * Written for JamesM's kernel development tutorials,
 * rewritten by Richard Anyalai for the RexOS! project.
 */

#include <drivers/VESA.h>
#include <drivers/VGA.h>
#include <io/registers.h>
#include <memory/heap.h>
#include <memory/paging.h>
#include <sys/isr.h>
#include <settings.h>
#include <utils.h>
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>

page_directory_t
	*kernel_directory  = 0,	// The kernel's page directory.
	*current_directory = 0;	// The current page directory.

// A bitset of frames - used or free.
uint32_t *frames;
uint32_t nframes;

// Defined in heap.c
extern uint32_t placement_address;
extern void copy_page_physical(int, int);
extern heap_t *kheap;

// Macros used in the bitset algorithms.
#define INDEX_FROM_BIT(a)  (a / (8 * 4))
#define OFFSET_FROM_BIT(a) (a % (8 * 4))

// Sets a bit in the frames bitset.
static void set_frame(uint32_t frame_addr)
{
	uint32_t frame	= frame_addr / PAGE_SIZE;
	uint32_t idx	= INDEX_FROM_BIT(frame);
	uint32_t off	= OFFSET_FROM_BIT(frame);
	frames[idx]		|= (0x1 << off);
}

// Clears a bit in the frames bitset.
static void clear_frame(uint32_t frame_addr)
{
	uint32_t frame	= frame_addr / PAGE_SIZE;
	uint32_t idx	= INDEX_FROM_BIT(frame);
	uint32_t off	= OFFSET_FROM_BIT(frame);
	frames[idx]		&= ~(0x1 << off);
}

// Finds the first frame.
static uint32_t first_frame()
{
	for (uint32_t i = 0; i < INDEX_FROM_BIT(nframes); ++i)
		if (frames[i] != 0xFFFFFFFF)			// nothing free, exit early.
			for (uint32_t j = 0; j < 32; ++j)	// at least one bit is free.
				if (!(frames[i] & (0x1 << j)))
					return i * 4 * 8 + j;
	
	return 0;
}

// Function to allocate a frame.
void alloc_frame(page_t *page, bool is_kernel, bool is_writeable)
{
	if (page->frame != 0)
		return;
	else
	{
		uint32_t idx = first_frame();

		if (idx == (uint32_t) -1)
			PANIC("NO FREE FRAMES");

		set_frame(idx * PAGE_SIZE);

		page->present	= 1;
		page->rw		= is_writeable;
		page->user		= !is_kernel;
		page->frame		= idx;
	}
}

// Function to deallocate a frame.
void free_frame(page_t *page)
{
	uint32_t frame;

	if (!(frame = page->frame))
		return;

	clear_frame(frame);
	page->frame = 0x0;
}

// REF: https://forum.osdev.org/viewtopic.php?p=144866&sid=8787b485336ef7de980f58526ed2419b#p144866
void force_frame(page_t *page, bool is_kernel, bool is_writeable, uint32_t addr)
{
	page->present = 1;
	page->rw      = (is_writeable) ? 1 : 0;
	page->user    = (is_kernel) ? 0 : 1;
	page->frame   = addr >> 12;

	set_frame(addr);
}

void switch_page_directory(page_directory_t *dir)
{
	current_directory = dir;
	asm volatile ("mov %0, %%cr3":: "r"(dir->physicalAddr));
	uint32_t cr0;
	asm volatile ("mov %%cr0, %0": "=r"(cr0));
	cr0 |= 0x80000000;	// Enable paging!
	asm volatile ("mov %0, %%cr0":: "r"(cr0));
}

page_t *get_page(uint32_t address, int make, page_directory_t *dir)
{
	// Turn the address into an index.
	address /= PAGE_SIZE;
	// Find the page table containing this address.
	uint32_t table_idx = address / 1024;

	if (dir->tables[table_idx]) // If this table is already assigned
		return &dir->tables[table_idx]->pages[address % 1024];
	else if (make)
	{
		uint32_t tmp;
		dir->tables[table_idx] =
			(page_table_t *) kmalloc_ap(sizeof(page_table_t), &tmp);
		memset(dir->tables[table_idx], 0, PAGE_SIZE);
		dir->tablesPhysical[table_idx] = tmp | 0x7; // PRESENT, RW, US.

		return &dir->tables[table_idx]->pages[address % 1024];
	}
	else
		return 0;
}

void page_fault(registers_t *regs)
{
	// The faulting address is stored in the CR2 register.
	size_t faulting_address;
	asm volatile ("mov %%cr2, %0" : "=r" (faulting_address));

	// The error code gives us details of what happened.
	int present  = !(regs->err_code & 0x1);	// Page not present
	int rw       = regs->err_code & 0x2;	// Write operation?
	int us       = regs->err_code & 0x4;	// Processor was in user-mode?
	int reserved = regs->err_code & 0x8;	// Overwritten CPU-reserved
											// bits of page entry?

	vga_set_index(0);

	// Output an error message.
	KERR(
		"Page fault! (%s%s%s) at 0x%.8x",
		present		? "present "	: "",
		rw			? "read-only "	: "",
		us			? "usermode "	: "",
		reserved	? "reserved"	: ""
	);

	while (true)
		HALT;
}

void init_paging(uint32_t mem_end_page)
{	
	nframes = mem_end_page / PAGE_SIZE;
	frames  = (uint32_t *) kmalloc(INDEX_FROM_BIT(nframes));
	memset(frames, 0, INDEX_FROM_BIT(nframes));
	
	// Let's make a page directory.
	kernel_directory = (page_directory_t *) kmalloc_a(sizeof(page_directory_t));
	memset(kernel_directory, 0, sizeof(page_directory_t));
	kernel_directory->physicalAddr =
	(uint32_t) kernel_directory->tablesPhysical;

	// Map some pages in the kernel heap area.
	// Here we call get_page but not alloc_frame. This causes page_table_t's 
	// to be created where necessary. We can't allocate frames yet because they
	// they need to be identity mapped first below, and yet we can't increase
	// placement_address between identity mapping and enabling the heap!
	uint32_t i;
	for (i = KHEAP_START; i < KHEAP_START + KHEAP_INITIAL_SIZE; i += PAGE_SIZE)
		get_page(i, 1, kernel_directory);

#ifdef VESA
	// Workaround to get VBE working again
	// REF: https://forum.osdev.org/viewtopic.php?p=144868&sid=8787b485336ef7de980f58526ed2419b#p144868
	for (
		i = VIDEO_ADDRESS;
		i < VIDEO_ADDRESS + VESA_WIDTH * VESA_HEIGHT * VESA_BPP;
		i += PAGE_SIZE
	)
	{
		page_t *tmp = get_page(i, true, kernel_directory);
		force_frame(tmp, false, true, i);
	}
#endif

	// We need to identity map (phys addr = virt addr) from
	// 0x0 to the end of used memory, so we can access this
	// transparently, as if paging wasn't enabled.
	// NOTE that we use a while loop here deliberately.
	// inside the loop body we actually change placement_address
	// by calling kmalloc(). A while loop causes this to be
	// computed on-the-fly rather than once at the start.
	// Allocate a lil' bit extra so the kernel heap can be
	// initialized properly.
	i = 0;
	while (i < placement_address + PAGE_SIZE)
	{
		// Kernel code is readable but not writeable from userspace.
		alloc_frame(get_page(i, 1, kernel_directory), 0, 0);
		i += PAGE_SIZE;
	}

	// Now allocate those pages we mapped earlier.
	for (i = KHEAP_START; i < KHEAP_START + KHEAP_INITIAL_SIZE; i += PAGE_SIZE)
		alloc_frame(get_page(i, 1, kernel_directory), 0, 0);

	// Before we enable paging, we must register our page fault handler.
	register_interrupt_handler(14, page_fault);

	// Now, enable paging!
	switch_page_directory(kernel_directory);

	KOK("Enabled paging");

	// Initialize the kernel heap.
	kheap = create_heap(
		KHEAP_START, KHEAP_START + KHEAP_INITIAL_SIZE, 0xCFFFF000, 0, 0
	);

	KOK("Initialized heap");
}

/* This file is part of RexOS!.
 *
 * RexOS! is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * RexOS! is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with RexOS!.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <drivers/VGA.h>
#include <sys/isr.h>
#include <sys/rtc.h>
#include <stdio.h>

void update_clock_widget()
{
	IRQ_OFF;

	uint16_t index = vga_get_index();

	vga_set_index(VGA_WIDTH - 6);

	char clock[6] = {0};

	get_text_clock(clock);

	printf("${B00}${F15}%s${CLC}", clock);

	vga_set_index(index);

	IRQ_RES;
}

void update_date_widget()
{
	IRQ_OFF;

	uint16_t index = vga_get_index();

	vga_set_index(VGA_WIDTH / 2 - 5);

	char date[12] = { 0 };

	get_text_date(date);

	printf("${B00}${F15}%s${CLC}", date);

	vga_set_index(index);

	IRQ_RES;
}

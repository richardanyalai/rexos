/* This file is part of RexOS!.
 *
 * RexOS! is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * RexOS! is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with RexOS!.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef __PROGRAMS_H
#define __PROGRAMS_H

#include <stdbool.h>
#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

// Shows desktop.
void show_desktop(uint8_t option_id);

// Opens the terminal.
void start_terminal_session(void);

/**
 * Displays an ascii art.
 * @see				print_ascii
 * @param option	is the name of the option
 */
void display_ascii(const char *option);

// Prints OS information in a really cool way.
void rexfetch(void);

// Makes the lil' cow say it out loud.
void cowsay(char *str);

// Shows digital clock app.
void start_digiclock(void);

// Updates the clock widget.
void update_clock_widget(void);

// Updates the date widget.
void update_date_widget(void);

// Starts Game of Life.
void gol_start(void);

// Draws a Sierpinski triangle.
void sierpinski_draw(void);

// Draws Pythagoras's tree.
void tree_draw(void);

// Visualizes Perlin noise.
void perlin_start(void);

// Displays a rotating cube.
void cube_start(void);

// Displays Matrix rain.
void matrix_start(void);

// Displays Never Gonna Give You Up animation.
void start_nggyu(void);

#ifdef __cplusplus
}
#endif

#endif

/* This file is part of RexOS!.
 *
 * RexOS! is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * RexOS! is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with RexOS!.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef __REXSHELL_h
#define __REXSHELL_h

#include <stdbool.h>
#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

// Prins the file names in a directory
void ls(char *dir);

// Prins the content of a file
void cat(char *file);

/**
 * @brief		Searches for an expression in a file.
 * @param exp	is the expression
 * @param file	is the file
 * @param color	indicates whether the expression should be highlighted
 * @param ins	if set, the search is insensitive for upper/lower letters
 * @return		the line in which the expression can be found
 */
char *ogrep(char *exp, char *file, bool color, bool ins);

// Prints hexdump of a buffer.
void hexdump(char *input, bool addr);

// Prints calendar.
void cal();

// Enters the shell.
void enter_shell(void);

// Leaves the shell.
void exit_shell(void);

#ifdef __cplusplus
}
#endif

#endif

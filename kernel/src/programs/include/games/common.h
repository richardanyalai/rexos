/* This file is part of RexOS!.
 *
 * RexOS! is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * RexOS! is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with RexOS!.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef __GAMES_H
#define __GAMES_H

#include <stdbool.h>
#include <stdint.h>

#define PLAYGROUND_SIZE 1716
#define BORDER_SIZE 206

extern uint16_t border[BORDER_SIZE];	// Stores the coordinates of border
extern uint16_t borderTop[80];			// Top wall
extern uint16_t borderLeft[24];			// Left wall

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief			Draws the borders around game window
 * @param bgcolor	is the background color
 * @param fgcolor	is the foreground color
 */
void draw_borders(uint8_t bgcolor, uint8_t fgcolor);

// Starts TicTacToe.
void tictac_play(void);

// Starts Snake.
void snake_play(void);

// Starts Pong.
void pong_play(void);

// Starts Breakout.
void brkt_play(void);

// Starts Meteors.
void meteors_play(void);

// Starts TetrOS.
void tetros_play(void);

// Starts Mines.
void mines_play(void);

// Starts Pac-Man.
void pacman_play(void);

// Starts Chess.
void chess_play(void);

// Starts Sudoku.
void sudoku_play(void);

// Starts HangMan.
void hangman_play(void);

// Makes the lil' cow say it out loud.
void cowsay(char *str);

// Shows pause popup window.
void show_pause();

#ifdef __cplusplus
}
#endif

#endif

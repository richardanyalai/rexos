/* This file is part of RexOS!.
 *
 * RexOS! is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * RexOS! is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with RexOS!.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <drivers/keyboard.h>
#include <drivers/VGA.h>
#include <games/common.h>
#include <gui/text_mode.h>
#include <programs/common.h>
#include <sys/isr.h>
#include <sys/rtc.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>

uint16_t
	pong_left_player[6],	// Blocks on the left player's side
	pong_right_player[6],	// Blocks on the right player's side
	pong_left_score,		// The left player's score
	pong_right_score,		// The right player's score
	pong_ball_pos,			// The position of the ball
	pong_prev_ball_pos,		// The previous position of the ball
	pong_timer;				// Simple counter

int16_t
	pong_left_dir,			// The left player's direction, 80 (down) -80 (up)
	pong_right_dir,			// The right player's direction, ...
	pong_speed_x,			// The horizontal speed of the ball
	pong_speed_y;			// The vertical speed of the ball

uint8_t
	pong_old_c;				// Stores old key character

bool
	pong_running,			// Indicates whether the game is running
	pong_paused;			// Indicates whether the game is paused

// Updates score on screen
void pong_write_score()
{
	vga_set_index(38 - digits(pong_left_score));

	textmode_draw_rect(35, 0, 50, 1, vga_get_bgcolor());

	printf(
		"${B00}${F15}%d : %d${CLC}", pong_left_score, pong_right_score
	);
}

// Updates score and resets ball position and speed
void scored()
{
	pong_write_score();
	pong_ball_pos = vga_get_pos(39, 13);
	pong_speed_x *= -1;
}

// Stops the game
void pong_pause()
{
	if ((pong_paused = !pong_paused))
		show_pause();
	else
	{
		textmode_draw_rect(23, 8, 59, 16, vga_get_bgcolor());
		vga_draw_char_at(254, pong_ball_pos, vga_get_bgcolor(), BRIGHT_RED);
	}
}

// Handle keyboard events
void pong_handle_keyboard(active_keys_t *active_keys)
{
	key_t active_key = active_keys->active_key;

	uint8_t (*key_to_char)(uint8_t) = lambda(
		uint8_t, (uint8_t key_code) {
			switch (key_code)
			{
				case KEY_W:		return 'w';
				case KEY_S:		return 's';
				case KEY_K:		return 'w';
				case KEY_J:		return 's';
				case KEY_UP:	return 'w';
				case KEY_DOWN:	return 's';
				case KEY_SPACE:	return ' ';
				case KEY_Q:		return 'q';
				case KEY_E:		return 'e';
				default:		return  0 ;
			}
		});

	uint8_t c = key_to_char(active_key.key_code);

	if (pong_old_c == 'Z' && active_key.char_code == 'Q')
	{
		pong_running = false;
		return;
	}

	if (!pong_paused && (c == 'w' || c == 's'))
	{
		if (active_key.pressed)
			pong_left_dir = ((c == 'w') ? -80 : 80);
		else
			pong_left_dir = 0;
	}
	else if (pong_paused && (c == 'q' || c == 'e'))
		pong_running = false;

	if (c == ' ' && active_key.pressed)
		pong_pause();

	if (active_key.char_code)
	{
		if (active_key.pressed)
			pong_old_c = active_key.char_code;

		null_active();
	}
}

// Initializes the game
void pong_init()
{
	set_keyboard_handler(pong_handle_keyboard);

	srand(time(NULL));

	vga_set_bgcolor(BLACK);
	vga_set_fgcolor(WHITE);

	pong_left_dir = pong_right_dir = 0;
	pong_speed_x = pong_speed_y = -1;

	pong_left_score = pong_right_score = 0;

	pong_paused = false;
	pong_running = true;

	int16_t i = -1;
	while (++i < 6)
	{
		uint8_t y = 10 + i;
		pong_left_player[i] = vga_get_pos(2, y);
		pong_right_player[i] = vga_get_pos(77, y);
	}

	pong_ball_pos = vga_get_pos(39, 13);
	pong_prev_ball_pos = pong_ball_pos;

	draw_borders(vga_get_bgcolor(), vga_get_fgcolor());

	pong_write_score();
}

// Update the game status
void pong_update()
{
	if (pong_left_dir != 0)
	{
		vga_draw_char_at(
			' ', pong_left_player[0], vga_get_bgcolor(), vga_get_bgcolor()
		);
		vga_draw_char_at(
			' ', pong_left_player[5], vga_get_bgcolor(), vga_get_bgcolor()
		);
	}

	if (pong_right_dir != 0)
	{
		if (pong_right_player[0] > vga_get_pos(77, 2))
			vga_draw_char_at(
				' ',
				pong_right_player[0] - 80,
				vga_get_bgcolor(),
				vga_get_bgcolor()
			);

		vga_draw_char_at(
			' ', pong_right_player[0], vga_get_bgcolor(), vga_get_bgcolor()
		);

		if (pong_right_player[5] < vga_get_pos(77, 23))
			vga_draw_char_at(
				' ',
				pong_right_player[5] + 80,
				vga_get_bgcolor(),
				vga_get_bgcolor()
			);

		vga_draw_char_at(
			' ', pong_right_player[5], vga_get_bgcolor(), vga_get_bgcolor()
		);
	}

	uint16_t i = -1;
	if (
		pong_left_player[0] + pong_left_dir >= 160 &&
		pong_left_player[5] + pong_left_dir <= 1920
	)
		while (++i < 6)
			pong_left_player[i] += pong_left_dir;

	uint16_t pong_right_player_y = vga_get_y(pong_right_player[0]);
	uint16_t ballX = vga_get_x(pong_ball_pos);
	uint16_t ballY = vga_get_y(pong_ball_pos);

	if (pong_speed_x == 1 && ballX >= VGA_WIDTH / 2)
	{
		if (ballY + (int) clock() % 2 < pong_right_player_y)
			pong_right_dir = -80;

		if (ballY - (int) clock() % 3 > vga_get_y(pong_right_player[5]))
			pong_right_dir = 80;
	}
	else
	{
		if (10 < pong_right_player_y)
			pong_right_dir = -80;
		else if (15 > vga_get_y(pong_right_player[5]))
			pong_right_dir = 80;
		else
			pong_right_dir = 0;
	}

	i = -1;
	if (
		pong_right_player[0] + pong_right_dir >= 160 &&
		pong_right_player[5] + pong_right_dir <= 1920
	)
		while (++i < 6)
			pong_right_player[i] += pong_right_dir;

	if (pong_timer % 2 == 0)
	{
		pong_prev_ball_pos			= pong_ball_pos;

		uint16_t pong_new_ball_pos	= pong_ball_pos + pong_speed_x
									+ pong_speed_y * 80;
		uint16_t pong_new_ball_y	= vga_get_y(pong_new_ball_pos);

		if (pong_new_ball_y <= 1 || pong_new_ball_y >= VGA_HEIGHT - 1)
			pong_new_ball_pos	= pong_ball_pos + pong_speed_x
								+ (pong_speed_y *= -1) * 80;

		i = -1;
		while (++i < 6)
		{
			uint16_t pong_newest_ball_pos =
			(pong_new_ball_pos - pong_speed_y * 80);

			if (
				pong_left_player[i] == pong_newest_ball_pos ||
				pong_right_player[i] == pong_newest_ball_pos
			)
				pong_new_ball_pos	= pong_ball_pos + (pong_speed_x *= -1)
									+ pong_speed_y * 80;
		}

		pong_ball_pos = pong_new_ball_pos;

		uint16_t newBallX = vga_get_x(pong_ball_pos);

		if (newBallX <= 0 || newBallX >= 79)
		{
			if (newBallX <= 0)
				pong_right_score++;
			else
				pong_left_score++;

			scored();
		}
	}
}

// Render the game
void pong_render()
{
	vga_draw_char_at(254, pong_prev_ball_pos, vga_get_bgcolor(), BLACK);

	uint16_t i = -1;
	while (++i < 6)
	{
		vga_draw_char_at(
			' ', pong_left_player[i], vga_get_fgcolor(), vga_get_bgcolor()
		);
		vga_draw_char_at(
			' ', pong_right_player[i], vga_get_fgcolor(), vga_get_bgcolor()
		);
	}

	i = 1;
	while (++i < 24)
		vga_draw_char_at(
			'|', vga_get_pos(39, i), vga_get_bgcolor(), vga_get_fgcolor()
		);

	vga_draw_char_at(254, pong_ball_pos, vga_get_bgcolor(), BRIGHT_RED);
}

void pong_play()
{
	pong_init();				// Initialize the game

	register_rtc_func(RTC_CLOCK_WIDGET, update_clock_widget);

	// Game loop
	while (pong_running)
	{
		if (!pong_paused)
		{
			pong_timer++;		// Increment counter
			pong_update();		// Update the coordinates
			pong_render();		// Render the players and the ball

			usleep(30);				// Sleep so the screen wont be cleared
		}

		HALT;
	}
}

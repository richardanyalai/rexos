/* This file is part of RexOS!.
 *
 * RexOS! is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * RexOS! is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERsnake_charANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with RexOS!.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <drivers/keyboard.h>
#include <drivers/serial.h>
#include <drivers/VGA.h>
#include <gui/graphics_mode.h>
#include <sys/isr.h>
#include <sys/rtc.h>
#include <math.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#define WIDTH			35
#define HEIGHT			22

#define TILE_SIZE		8

#define X_OFFSET		20
#define Y_OFFSET		20

#define OFFSET_GHOST	208
#define OFFSET_PACMAN	224

uint8_t *pacman_map;
const char *pacman_map_orig =
"QGGGGGGGGGGGGGGGGNGGGGGGGGGGGGGGGGR"
"HYYYYYYYYYYYYYYYYHYYYYYYYYYYYYYYYYH"
"HYCAAADYCAAAAAADYHYCAAAAAADYCAAADYH"
"HZEAAAFYEAAAAAAFYIYEAAAAAAFYEAAAFZH"
"HYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYH"
"HYKGGGJYCADYKGGGGNGGGGJYCADYKGGGJYH"
"HYYYYYYYB BYYYYYYHYYYYYYB BYYYYYYYH"
"SGGGGGRYB LGGGGJ I KGGGGM BYQGGGGGT"
"      HYB B             B BYH      "
"KGGGGGTYEAF QGGGJXKGGGR EAFYSGGGGGJ"
"            H         H            "
"KGGGGGRYCAD SGGGGGGGGGT CADYQGGGGGJ"
"      HYB B             B BYH      "
"QGGGGGTYEAF KGGGGNGGGGJ EAFYSGGGGGR"
"HYYYYYYYYYYYYYYYYHYYYYYYYYYYYYYYYYH"
"HYKGGGRYKGGGGGGJYIYKGGGGGGJYQGGGJYH"
"HZYYYYHYYYYYYYYYY YYYYYYYYYYHYYYYZH"
"OGGGJYIYCADYKGGGGNGGGGJYCADYIYKGGGP"
"HYYYYYYYB BYYYYYYHYYYYYYB BYYYYYYYH"
"HYKGGGGGVUWGGGGJYIYKGGGGVUWGGGGGJYH"
"HYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYH"
"SGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGT";

const uint8_t pacman_sprites[304] =
{
	0x00, 0x00, 0x00, 0xff, 0xff, 0x00, 0x00, 0x00, 0x18, 0x18, 0x18, 0x18,
	0x18, 0x18, 0x18, 0x18, 0x00, 0x00, 0x00, 0x0f, 0x1f, 0x18, 0x18, 0x18,
	0x00, 0x00, 0x00, 0xf0, 0xf8, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x1f,
	0x0f, 0x00, 0x00, 0x00, 0x18, 0x18, 0x18, 0xf8, 0xf0, 0x00, 0x00, 0x00,
	0x00, 0xff, 0xff, 0x00, 0x00, 0xff, 0xff, 0x00, 0x66, 0x66, 0x66, 0x66,
	0x66, 0x66, 0x66, 0x66, 0x66, 0x66, 0x66, 0x66, 0x66, 0x66, 0x3c, 0x00,
	0x00, 0xfc, 0xfe, 0x06, 0x06, 0xfe, 0xfc, 0x00, 0x00, 0x3f, 0x7f, 0x60,
	0x60, 0x7f, 0x3f, 0x00, 0x18, 0x1f, 0x0f, 0x00, 0x00, 0x0f, 0x1f, 0x18,
	0x18, 0xf8, 0xf0, 0x00, 0x00, 0xf0, 0xf8, 0x18, 0x00, 0xff, 0xff, 0x00,
	0x00, 0xc3, 0xe7, 0x66, 0x66, 0x67, 0x63, 0x60, 0x60, 0x63, 0x67, 0x66,
	0x66, 0xe6, 0xc6, 0x06, 0x06, 0xc6, 0xe6, 0x66, 0x00, 0x3f, 0x7f, 0x60,
	0x60, 0x63, 0x67, 0x66, 0x00, 0xfc, 0xfe, 0x06, 0x06, 0xc6, 0xe6, 0x66,
	0x66, 0x67, 0x63, 0x60, 0x60, 0x7f, 0x3f, 0x00, 0x66, 0xe6, 0xc6, 0x06,
	0x06, 0xfe, 0xfc, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xff, 0xff, 0x00,
	0x18, 0xf8, 0xf0, 0x00, 0x00, 0xff, 0xff, 0x00, 0x18, 0x1f, 0x0f, 0x00,
	0x00, 0xff, 0xff, 0x00, 0x00, 0x00, 0x00, 0xff, 0xff, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x3c, 0x7e, 0x7e,
	0x7e, 0x3c, 0x00, 0x00, 0x18, 0x3c, 0x56, 0x7e, 0x7e, 0x7e, 0x7e, 0x6c,
	0x18, 0x3c, 0x56, 0x7e, 0x7e, 0x7e, 0x7e, 0x5a, 0x00, 0x18, 0x3c, 0x7e,
	0x7e, 0x7e, 0x3c, 0x18, 0x00, 0x18, 0x3c, 0x7e, 0x7e, 0x7e, 0x3c, 0x18,
	0x00, 0x18, 0x3c, 0x7e, 0x7e, 0x7e, 0x3c, 0x18, 0x00, 0x00, 0x42, 0x66,
	0x66, 0x66, 0x3c, 0x18, 0x00, 0x18, 0x3c, 0x7e, 0x7e, 0x7e, 0x3c, 0x18,
	0x00, 0x18, 0x3c, 0x66, 0x66, 0x66, 0x42, 0x00, 0x00, 0x18, 0x3c, 0x7e,
	0x7e, 0x7e, 0x3c, 0x18, 0x00, 0x18, 0x0c, 0x06, 0x06, 0x06, 0x0c, 0x18,
	0x00, 0x18, 0x3c, 0x7e, 0x7e, 0x7e, 0x3c, 0x18, 0x00, 0x18, 0x30, 0x60,
	0x40, 0x60, 0x30, 0x18,
};

enum pacman_dirs
{
	UP    = 1,
	DOWN  = 2,
	LEFT  = 3,
	RIGHT = 4,
};

enum pacman_state
{
	SCATTER,
	ATTACK,
	FRIGHTENED
};

typedef struct
{
	uint16_t x;
	uint8_t y;
} coords;

bool pacman_playing, pacman_paused, pacman_pickup, pacman_win;
uint8_t
	pacman_ghost_spr_state,
	pacman_status,
	pacman_dir,
	ghost_dirs[4],
	ghost_path_idx[4],
	ghost_states[4],
	pacman_lives,
	pacman_state_timeout,
	pacman_coin_count,
	pacman_powerup_count,
	pacman_state;
uint16_t pacman_frame_counter;
uint32_t pacman_score;
coords pacman_pman, pacman_ghosts[4];
coords pacman_ghost_paths[4][8] =
{
	{{ 27,  4 }, { 27,  1 }, { 30,  1 }, { 33,  1 }, { 33,  4 }, { 30,  4 }},
	{{ 27, 16 }, { 33, 18 }, { 33, 20 }, { 18, 20 }, { 18, 18 }, { 23, 16 }},
	{{  7,  4 }, {  7,  1 }, {  4,  1 }, {  1,  1 }, {  1,  4 }, {  4,  4 }},
	{{ 11, 16 }, {  7, 16 }, {  1, 18 }, {  1, 20 }, { 16, 20 }, { 16, 18 }},
};
coords pacman_ghost_orig_pos[4] =
{{ 17,  8 }, { 15, 10 }, { 17, 10 }, { 19, 10 }};

bool pacman_is_wall(uint8_t ch)
{ return BETWEEN(ch, 'A', 'X'); }

void pacman_handle_keyboard(active_keys_t *active_keys)
{
	uint8_t x = pacman_pman.x, y = pacman_pman.y;

	if (active_keys->active_key.pressed)
		switch (active_keys->active_key.key_code)
		{
			case KEY_LEFT:
			case KEY_A:
			case KEY_H:
				if (
					!pacman_is_wall(pacman_map[y * WIDTH + x - 1]) &&
					!pacman_paused &&
					!pacman_win
				)
					pacman_dir = LEFT;
				break;

			case KEY_RIGHT:
			case KEY_D:
			case KEY_L:
				if (
					!pacman_is_wall(pacman_map[y * WIDTH + x + 1]) &&
					!pacman_paused &&
					!pacman_win
				)
					pacman_dir = RIGHT;
				break;

			case KEY_UP:
			case KEY_W:
			case KEY_K:
				if (
					!pacman_is_wall(pacman_map[(y - 1) * WIDTH + x]) &&
					!pacman_paused &&
					!pacman_win
				)
					pacman_dir = UP;
				break;

			case KEY_DOWN:
			case KEY_S:
			case KEY_J:
				if (
					!pacman_is_wall(pacman_map[(y + 1) * WIDTH + x]) &&
					!pacman_paused &&
					!pacman_win
				)
					pacman_dir = DOWN;
				break;

			case KEY_P:
				if (pacman_dir)
					pacman_paused = !pacman_paused;
				break;

			case KEY_Q:
				pacman_playing = false;
				break;
		}

	null_active();
}

void pacman_draw_sprite(
	uint16_t x, uint8_t y, const uint8_t sprite[8], uint8_t color
)
{
	uint16_t real_x = x * TILE_SIZE + X_OFFSET;
	uint16_t real_y = y * TILE_SIZE + Y_OFFSET;

	for (uint8_t y = 0; y < 8; ++y)
		for (uint8_t x = 0; x < 8; ++x)
			if ((sprite[y] >> (7 - x) & 1))
				vga_draw_pixel(real_x + x, real_y + y, color);
			else
				vga_draw_pixel(real_x + x, real_y + y, BLACK);
}

void pacman_update_score()
{
	vga_draw_fill_rect(0, 0, 320, 25, BLACK);

	vga_set_index(vga_get_pos(3, 1));

	vga_set_bgcolor(BLACK);
	vga_set_fgcolor(WHITE);

	for (uint8_t i = 0; i < 6 - digits(pacman_score); ++i)
		putchar('0');

	printf("%d", pacman_score);

	for (uint8_t i = 0; i < pacman_lives; ++i)
		for (uint8_t y = 0; y < 8; ++y)
			for (uint8_t x = 0; x < 8; ++x)
				if (
					((pacman_sprites + OFFSET_PACMAN + 72)[y] >>
						(7 - x) & 1)
				)
					vga_draw_pixel(270 + i * 10 + x, 5 + y, YELLOW);

}

void pacman_move_ghost(uint8_t ghost)
{
	uint8_t x   = pacman_ghosts[ghost].x;
	uint8_t y   = pacman_ghosts[ghost].y;
	uint8_t dir = ghost_dirs[ghost];

	if (pacman_state == ATTACK && (x == 34) && (y == 10) && dir == RIGHT)
	{
		x = 0;
		goto ghost_end;
	}
	else if (pacman_state == ATTACK && (x == 0) && (y == 10) && dir == LEFT)
	{
		x = 34;
		goto ghost_end;
	}

	bool (*cango)(uint8_t, uint8_t) = lambda(bool, (uint8_t _x, uint8_t _y)
	{
		return
			(
				BETWEEN(_x, 1, WIDTH - 1)
				&& BETWEEN(_y, 1, HEIGHT - 1)
				&& !pacman_is_wall(pacman_map[_y * WIDTH + _x])
				&& !(pacman_state != ATTACK && (_x == 27) && (_y == 10))
				&& !(pacman_state != ATTACK && (_x == 6) && (_y == 10))
			) || (
				(pacman_state == SCATTER || pacman_state == FRIGHTENED) &&
				pacman_map[_y * WIDTH + _x] == 'X'
			);
	});

	bool (*cango_dir)(uint8_t) = lambda(bool, (uint8_t dir)
	{
		switch (dir)
		{
			case UP:    return cango(x, y - 1);
			case DOWN:  return cango(x, y + 1);
			case LEFT:  return cango(x - 1, y);
			case RIGHT: return cango(x + 1, y);
		}

		return false;
	});

	coords target_coords;

	if (pacman_state == ATTACK)
		target_coords = pacman_pman;
	else if (ghost_states[ghost])
		target_coords = pacman_ghost_orig_pos[ghost];
	else
	{
		uint8_t path_idx = ghost_path_idx[ghost];
		target_coords = pacman_ghost_paths[ghost][path_idx];

		if (target_coords.x == x && target_coords.y == y)
		{
			if (path_idx < 5)
				path_idx++;
			else
				path_idx = 0;
		}

		ghost_path_idx[ghost] = path_idx;

		target_coords = pacman_ghost_paths[ghost][path_idx];
	}

	if (target_coords.y < y && cango_dir(UP) && dir != DOWN)
	{
		y--;
		dir = UP;
	}
	else if (target_coords.y > y && cango_dir(DOWN) && dir != UP)
	{
		y++;
		dir = DOWN;
	}
	else if (target_coords.x < x && cango_dir(LEFT) && dir != RIGHT)
	{
		x--;
		dir = LEFT;
	}
	else if (target_coords.x > x && cango_dir(RIGHT) && dir != LEFT)
	{
		x++;
		dir = RIGHT;
	}
	else
	{
		if (x == 17 && y == 10)
		{
			y--;
			dir = UP;
		}
		else
			switch (dir)
			{
				case UP:
					if (cango_dir(UP))
						y--;
					else if (cango_dir(RIGHT))
					{
						x++;
						dir = RIGHT;
					}
					else if (cango_dir(LEFT))
					{
						x--;
						dir = LEFT;
					}
					else if (cango_dir(DOWN))
					{
						y++;
						dir = DOWN;
					}
					break;
				case DOWN:
					if (cango_dir(DOWN))
						y++;
					else if (cango_dir(RIGHT))
					{
						x++;
						dir = RIGHT;
					}
					else if (cango_dir(LEFT))
					{
						x--;
						dir = LEFT;
					}
					else if (cango_dir(UP))
					{
						y--;
						dir = UP;
					}
					break;
				case LEFT:
					if (cango_dir(LEFT))
						x--;
					else if (cango_dir(UP))
					{
						y--;
						dir = UP;
					}
					else if (cango_dir(DOWN))
					{
						y++;
						dir = DOWN;
					}
					else if (cango_dir(RIGHT))
					{
						x++;
						dir = RIGHT;
					}
					break;
				case RIGHT:
					if (cango_dir(RIGHT))
						x++;
					else if (cango_dir(UP))
					{
						y--;
						dir = UP;
					}
					else if (cango_dir(DOWN))
					{
						y++;
						dir = DOWN;
					}
					else if (cango_dir(LEFT))
					{
						x--;
						dir = LEFT;
					}
					break;
			}
	}

ghost_end:
	ghost_dirs[ghost] = dir;
	pacman_ghosts[ghost] = (coords){ x, y };
}

void pacman_update()
{
	if (pacman_ghost_spr_state < 1)
		pacman_ghost_spr_state++;
	else
		pacman_ghost_spr_state = 0;

	if (pacman_status < 1)
		pacman_status++;
	else
		pacman_status = 0;

	switch (pacman_dir)
	{
		case UP:
			if (
				!pacman_is_wall(
					pacman_map[(pacman_pman.y - 1) * WIDTH + pacman_pman.x]
				)
			)
				pacman_pman.y -= 1;
			break;

		case DOWN:
			if (
				!pacman_is_wall(
					pacman_map[(pacman_pman.y + 1) * WIDTH + pacman_pman.x]
				)
			)
				pacman_pman.y += 1;
			break;

		case LEFT:
			if (
				!pacman_is_wall(
					pacman_map[pacman_pman.y * WIDTH + pacman_pman.x - 1]
				)
			)
				pacman_pman.x -= 1;
			break;

		case RIGHT:
			if (
				!pacman_is_wall(
					pacman_map[pacman_pman.y * WIDTH + pacman_pman.x + 1]
				)
			)
				pacman_pman.x += 1;
			break;
	}

	uint16_t pos = pacman_pman.y * WIDTH + pacman_pman.x;

	if (pacman_map[pos] == 'Y')
	{
		pacman_score += 100;
		pacman_map[pos] = ' ';
		pacman_coin_count--;
		pacman_update_score();
	}
	else if (pacman_map[pos] == 'Z')
	{
		pacman_score += 1000;
		pacman_map[pos] = ' ';
		pacman_powerup_count--;
		pacman_update_score();
		pacman_pickup = true;

		pacman_state = FRIGHTENED;
		pacman_state_timeout = 75;
	}

	if (pacman_dir == LEFT && pacman_pman.x == 0 && pacman_pman.y == 10)
		pacman_pman = ((coords) { 34, 10 });
	else if (pacman_dir == RIGHT && pacman_pman.x == 34 && pacman_pman.y == 10)
		pacman_pman = ((coords) { 0, 10 });

	if (pacman_dir)
		for (uint8_t i = 0; i < 4; ++i)
			pacman_move_ghost(i);

	for (uint8_t i = 0; i < 4; ++i)
	{
		if (
			pacman_pman.x == pacman_ghosts[i].x &&
			pacman_pman.y == pacman_ghosts[i].y
		)
		{
			if (pacman_pickup)
			{
				ghost_states[i] = 1;

				pacman_score += 500;
				pacman_update_score();
			}
			else
			{
				pacman_dir = 0;
				pacman_pman = (coords){ 17, 16 };
				pacman_lives--;

				pacman_update_score();

				for (uint8_t i = 0; i < 4; ++i)
				{
					pacman_ghosts [i] = pacman_ghost_orig_pos[i];
					ghost_dirs    [i] = LEFT;
					ghost_path_idx[i] = 0;
					ghost_states  [i] = 0;
				}

				if (pacman_lives == 0)
					pacman_playing = false;
			}
		}
	}

	if (!pacman_win && !pacman_coin_count && !pacman_powerup_count)
	{
		pacman_dir = 0;
		pacman_lives--;

		pacman_update_score();

		pacman_win = true;
		pacman_dir = 0;
	} else if (pacman_lives == 0)
		pacman_playing = false;
}

void pacman_draw_string(
	uint16_t _x, uint16_t _y, const char *text, uint8_t fgcolor
)
{
	extern const uint8_t vga_graphics_mode_font[2048];

	uint16_t col = 0, len = strlen(text);

	for (uint16_t i = 0; i < len; ++i)
	{
		if (text[i])
			col++;
		else
			break;

		for (uint8_t y = 0; y < 8; ++y)
			for (uint8_t x = 0; x < 8; ++x)
				if ((vga_graphics_mode_font[text[i] * 8 + y] >> (7 - x) & 1))
					vga_draw_pixel(x + _x + col * 8, y + _y, fgcolor);
	}
}

void pacman_render()
{
	for (uint8_t j = 0; j < HEIGHT; ++j)
		for (uint8_t i = 0; i < WIDTH; ++i)
		{
			uint16_t x = i * TILE_SIZE + X_OFFSET;
			uint16_t y = j * TILE_SIZE + Y_OFFSET;

			vga_draw_fill_rect(
				x, y, x + TILE_SIZE, y + TILE_SIZE, BLACK
			);

			uint8_t ch = pacman_map[j * WIDTH + i];
			uint8_t color;

			switch (ch)
			{
				case 'X':			color = pacman_win ?  0 : 29;	break;
				case 'Y': case 'Z':	color = 66;						break;
				default:			color = pacman_win ? 29 : 104;
			}

			if (BETWEEN(ch, 'A', 'Z'))
				pacman_draw_sprite(
					i, j, pacman_sprites + (ch - 'A') * 8, color
				);
		}

	pacman_draw_sprite(
		pacman_pman.x,
		pacman_pman.y,
		pacman_sprites + OFFSET_PACMAN + pacman_dir * 2 * 8 + pacman_status * 8,
		YELLOW
	);

	for (uint8_t i = 0; i < 4; ++i)
		pacman_draw_sprite(
			pacman_ghosts[i].x,
			pacman_ghosts[i].y,
			pacman_sprites + OFFSET_GHOST + pacman_ghost_spr_state * 8,
			pacman_win ? 0 :
			(
				pacman_pickup
				? 32
				: ((uint8_t[]) { 40, 77, 61, 42 })[i]
			)
		);

	if (pacman_dir == 0)
		pacman_draw_string(128, 116, pacman_win ? "YOU WON" : "READY!", YELLOW);
}

void pacman_rtc()
{
	if (!pacman_paused && pacman_dir)
		switch (pacman_state)
		{
			case SCATTER:
				if (pacman_state_timeout)
					pacman_state_timeout--;
				else
				{
					pacman_state_timeout = 30;
					pacman_state = ATTACK;
				}
				break;

			case ATTACK:
			case FRIGHTENED:
				if (pacman_state_timeout)
					pacman_state_timeout--;
				else
				{
					pacman_state_timeout = 90;
					pacman_state = SCATTER;
					for (uint8_t i = 0; i < 4; ghost_states[i++] = 0);
					pacman_pickup = false;
				}
				break;
		}
}

void pacman_init()
{
	init_vga(320, 200);

	pacman_playing = true;
	pacman_paused  = false;
	pacman_pickup  = false;
	pacman_win     = false;
	
	pacman_state         = SCATTER;
	pacman_state_timeout = 90;

	pacman_ghost_spr_state = 0;
	pacman_status          = 0;

	pacman_dir = 0;

	pacman_frame_counter = 0;

	pacman_score = 0;
	pacman_lives = 3;

	pacman_coin_count    = 254;
	pacman_powerup_count = 4;

	pacman_pman = (coords){ 17, 16 };

	for (uint8_t i = 0; i < 4; ++i)
	{
		pacman_ghosts [i] = pacman_ghost_orig_pos[i];
		ghost_dirs    [i] = LEFT;
		ghost_path_idx[i] = 0;
		ghost_states  [i] = 0;
	}

	for (uint16_t i = 0; i < WIDTH * HEIGHT; ++i)
		pacman_map[i] = pacman_map_orig[i];

	srand(time(NULL));

	pacman_update_score();
	pacman_render();

	set_keyboard_handler(pacman_handle_keyboard);

	register_rtc_func(4, pacman_rtc);
}

void pacman_play()
{
	pacman_map = malloc(WIDTH * HEIGHT * sizeof(uint8_t));

	pacman_init();

	while (pacman_playing)
	{
		HALT;

		if (pacman_frame_counter == 160 && !pacman_paused)
		{
			pacman_update();
			pacman_render();

			pacman_frame_counter = 0;
		}
		else if (!pacman_paused)
			pacman_frame_counter++;
		else
			pacman_draw_string(128, 116, "PAUSED", WHITE);
	}

	free(pacman_map);

	init_vga(80, 25);
}

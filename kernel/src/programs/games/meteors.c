/* This file is part of RexOS!.
 *
 * RexOS! is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * RexOS! is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERsnake_charANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with RexOS!.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <drivers/keyboard.h>
#include <drivers/pcspkr.h>
#include <drivers/VGA.h>
#include <gui/graphics_mode.h>
#include <sys/isr.h>
#include <sys/pit.h>
#include <math.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>

#define METEORS_MAX 69

typedef struct
{
	int16_t x;
	int16_t y;
} point_t;

typedef struct
{
	bool active;
	uint8_t radius;
	point_t coords;
	point_t vel;
	float r_offsets[20];
} meteor_gameobj_t;

static bool
	meteors_playing,
	meteors_paused,
	meteors_invincible,
	meteors_left_turn,
	meteors_right_turn,
	meteors_boost,
	meteors_firing;
static uint8_t meteors_lives, meteors_level;
static const char
	meteors_mx[3] = {   0, - 7, 7 },
	meteors_my[3] = { -10,   7, 7 },
	meteors_tx[3] = {   0, - 5, 5 },
	meteors_ty[3] = {  17,   8, 8 };
static uint16_t meteors_score;
static int16_t
	meteors_sx[3],
	meteors_sy[3],
	meteors_tsx[3],
	meteors_tsy[3],
	meteors_player_x,
	meteors_player_y;
static float meteors_player_angle, meteors_ship_angle;
static uint32_t meteors_invincible_timespamp, meteors_frame_counter;
static meteor_gameobj_t *meteors, *meteors_missiles;

void meteors_update(), meteors_render();

void meteors_handle_keyboard(active_keys_t *active_keys)
{
	switch (active_keys->active_key.key_code)
	{
		case KEY_Q:
			meteors_playing = false;
			break;

		case KEY_P:
			if (active_keys->active_key.pressed)
				meteors_paused = !meteors_paused;
			break;

		case KEY_LEFT:
		case KEY_A:
		case KEY_H:
			meteors_left_turn = active_keys->active_key.pressed;
			break;

		case KEY_RIGHT:
		case KEY_D:
		case KEY_L:
			meteors_right_turn = active_keys->active_key.pressed;
			break;

		case KEY_UP:
		case KEY_W:
		case KEY_K:
			meteors_boost = active_keys->active_key.pressed;
			break;

		case KEY_SPACE:
			meteors_firing = active_keys->active_key.pressed;
			break;

		default:
			break;
	}

	null_active();
}

void meteors_update_score()
{
	vga_set_bgcolor(-1);
	vga_set_fgcolor(WHITE);

	for (uint8_t i = 0; i < meteors_lives; ++i)
	{
		vga_draw_triangle(
			1 + (i * 10), 5,
			4 + (i * 10), 1,
			7 + (i * 10), 5,
			WHITE
		);
	}

	vga_set_index(vga_get_pos(32 - digits(meteors_score), 0));
	printf("score: %d", meteors_score);
}

void meteors_new_level()
{
	meteors_level++;

	beep(9, 25);

	for (uint8_t i = 0; i < meteors_level + 4; ++i)
	{
		meteor_gameobj_t *meteor = &meteors[i];

		int8_t vel[2] = { -1, 1 };

		*meteor =
			(meteor_gameobj_t){
				.active = true,
				.radius = 25,
				.coords = (point_t){
					.x = rand() % 320,
					.y = rand() % 200,
				},
				.vel = (point_t){
					.x = (rand() % 2) * vel[rand() % 2] + vel[rand() % 2],
					.y = (rand() % 2) * vel[rand() % 2] + vel[rand() % 2]
				},
				.r_offsets = { 0 }
		};

		for (uint8_t i = 0; i < 20; ++i)
			meteor->r_offsets[i] = 0.5 + (rand() % 3) / 10.0;
	}
}

void meteors_new_ship()
{
	meteors_player_x             = 160;
	meteors_player_y             = 100;
	meteors_ship_angle           = 0;
	meteors_player_angle         = 0;
	meteors_invincible           = true;
	meteors_invincible_timespamp = pit_get_ticks() + 2300;
}

void meteors_init()
{
	init_vga(320, 200);

	meteors_playing    = true;
	meteors_paused     = false;
	meteors_left_turn  = false;
	meteors_right_turn = false;
	meteors_boost      = false;
	meteors_firing     = false;

	meteors_lives = 3;
	meteors_score = 0;
	meteors_level = 0;

	meteors_frame_counter = 0;

	meteors_new_ship();

	srand(time(NULL));

	meteors_new_level();

	meteors_update_score();

	set_keyboard_handler(meteors_handle_keyboard);
}

uint16_t wrap_x_coord(int16_t), wrap_y_coord(int16_t);

void meteors_update()
{
	if (meteors_left_turn)
		meteors_ship_angle -= 0.25f;

	if (meteors_right_turn)
		meteors_ship_angle += 0.25f;

	if (meteors_invincible_timespamp < pit_get_ticks())
		meteors_invincible = false;

	if (meteors_boost)
		meteors_player_angle = meteors_ship_angle;

	if (meteors_firing)
		for (uint8_t i = 0; i < METEORS_MAX; ++i)
		{
			meteor_gameobj_t *missile = &meteors_missiles[i];

			if (!missile->active)
			{
				*missile = (meteor_gameobj_t){
					.active = true,
					.coords = (point_t){
						.x = meteors_sx[0],
						.y = meteors_sy[0],
					},
					.vel = (point_t){
						.x = sin(meteors_ship_angle) * 2.0f,
						.y = -cos(meteors_ship_angle) * 2.0f,
					},
					.r_offsets = { 0 }
				};

				break;
			}
		}

	for (uint8_t i = 0; i < METEORS_MAX; ++i)
	{
		meteor_gameobj_t *meteor = &meteors[i];

		if (meteor->active)
		{
			meteor->coords.x += meteor->vel.x;
			meteor->coords.y += meteor->vel.y;

			if (meteor->coords.x > 320 - meteor->radius)
				meteor->coords.x -= 320;

			if (meteor->coords.x < 0 - meteor->radius)
				meteor->coords.x += 320;

			if (meteor->coords.y > 200 - meteor->radius)
				meteor->coords.y -= 200;

			if (meteor->coords.y < 0 - meteor->radius)
				meteor->coords.y += 200;
		}

		meteor_gameobj_t *missile = &meteors_missiles[i];

		if (missile->active)
		{
			missile->coords.x += (6 + meteors_level) * missile->vel.x;
			missile->coords.y += (6 + meteors_level) * missile->vel.y;

			if (missile->coords.x >= 320)
				missile->active = false;

			if (missile->coords.x <= 0)
				missile->active = false;

			if (missile->coords.y >= 200)
				missile->active = false;

			if (missile->coords.y <= 0)
				missile->active = false;
		}
	}

	for (uint8_t i = 0; i < METEORS_MAX; ++i)
	{
		meteor_gameobj_t *meteor = &meteors[i];

		if (meteor->active)
		{
			// Check for missile collision
			for (uint8_t j = 0; j < METEORS_MAX; ++j)
			{
				meteor_gameobj_t *missile = &meteors_missiles[j];

				if (
					missile->active &&
					abs(
						wrap_x_coord(meteor->coords.x) -
						wrap_x_coord(missile->coords.x)
					) <= meteor->radius - 7 &&
					abs(
						wrap_y_coord(meteor->coords.y) -
						wrap_y_coord(missile->coords.y)
					) <= meteor->radius - 7
				)
				{
					meteors_score += meteor->radius == 15 ? 1000 : 500;

					if (meteor->radius == 15)
						meteor->active = false;
					else
					{
						meteor->radius = 15;

						for (uint8_t k = 0; i < METEORS_MAX; ++k)
							if (!meteors[k].active)
							{
								meteors[k] = (meteor_gameobj_t){
									.radius = 15,
									.active = true,
									.coords = meteor->coords,
									.vel = (point_t) {
										.x = -meteor->vel.x,
										.y = -meteor->vel.y
									},
									.r_offsets = { 0 }
								};

								for (uint8_t j = 0; j < 20; ++j)
									meteors[k].r_offsets[j] =
										meteor->r_offsets[j];

								break;
							}
					}

					beep(3, 25);

					missile->active = false;
				}
			}

			// Check for spaceship collision
			if (!meteors_invincible)
				for (uint8_t i = 0; i < 4; ++i)
				{
					uint8_t j = i + 1;

					int16_t x1 = meteors_sx[i % 3];
					int16_t y1 = meteors_sy[i % 3];
					int16_t x2 = meteors_sx[j % 3];
					int16_t y2 = meteors_sy[j % 3];

					int16_t dx = x2 - x1;
					int16_t dy = y2 - y1;
					int16_t steps = 0;

					if (abs(dx) > abs(dy))
						steps = abs(dx);
					else
						steps = abs(dy);

					double x = x1, y = y1, x_inc, y_inc;

					x_inc = dx / (float) steps;
					y_inc = dy / (float) steps;

					for (int16_t v = 0; v < steps; ++v)
					{
						x += x_inc;
						y += y_inc;

						if (
							meteor->active &&
							abs(
								wrap_x_coord(meteor->coords.x) -
								wrap_x_coord(x)
							) <= meteor->radius - 10 &&
							abs(
								wrap_y_coord(meteor->coords.y) -
								wrap_y_coord(y)
							) <= meteor->radius - 10
						)
						{
							meteors_lives--;

							meteors_new_ship();

							meteor->active = false;

							beep(2, 25);

							if (meteors_lives == 0)
								meteors_playing = false;
						}
					}
				}
		}
	}

	for (uint8_t i = 0; i < 3; ++i)
	{
		meteors_sx[i] =
			meteors_mx[i] * cos(meteors_ship_angle) -
			meteors_my[i] * sin(meteors_ship_angle);

		meteors_sy[i] =
			meteors_mx[i] * sin(meteors_ship_angle) +
			meteors_my[i] * cos(meteors_ship_angle);

		meteors_tsx[i] =
			meteors_tx[i] * cos(meteors_ship_angle) -
			meteors_ty[i] * sin(meteors_ship_angle);

		meteors_tsy[i] =
			meteors_tx[i] * sin(meteors_ship_angle) +
			meteors_ty[i] * cos(meteors_ship_angle);
	}

	for (uint8_t i = 0; i < 3; ++i)
	{
		meteors_sx[i] = meteors_sx[i] + meteors_player_x;
		meteors_sy[i] = meteors_sy[i] + meteors_player_y;
		meteors_tsx[i] = meteors_tsx[i] + meteors_player_x;
		meteors_tsy[i] = meteors_tsy[i] + meteors_player_y;
	}

	uint8_t booster = meteors_boost ? 3 : 1;

	meteors_player_x += booster * sin(meteors_player_angle) * 2.0f;
	meteors_player_y += booster * -cos(meteors_player_angle) * 2.0f;

	if (meteors_player_x > 320)
		meteors_player_x -= 320;

	if (meteors_player_x < 0)
		meteors_player_x += 320;

	if (meteors_player_y > 200)
		meteors_player_y -= 200;

	if (meteors_player_y < 0)
		meteors_player_y += 200;

	uint8_t meteors_count = 0;

	for (uint8_t i = 0; i < METEORS_MAX; ++i)
		if (meteors[i].active)
			meteors_count++;

	if (!meteors_count)
	{
		meteors_lives++;
		meteors_new_level();
	}
}

uint16_t wrap_x_coord(int16_t x)
{
	if (x < 0) return 319 + x;
	if (x >= 320) return 0 + x;
	return x;
}

uint16_t wrap_y_coord(int16_t y)
{
	if (y < 0) return 199 + y;
	if (y >= 200) return 0 + y;
	return y;
}

void meteors_draw_line(
	uint16_t x1, uint16_t y1, uint16_t x2, uint16_t y2, uint8_t color
)
{
	int16_t dx = x2 - x1;
	int16_t dy = y2 - y1;
	int16_t steps = 0;

	if (abs(dx) > abs(dy))
		steps = abs(dx);
	else
		steps = abs(dy);

	double x = x1, y = y1, x_inc, y_inc;

	x_inc = dx / (float) steps;
	y_inc = dy / (float) steps;

	for (int16_t v = 0; v < steps; ++v)
	{
		x += x_inc;
		y += y_inc;

		vga_draw_pixel(wrap_x_coord(x), wrap_y_coord(y), color);
	}
}

void meteors_render()
{
	vga_clear();

	meteors_update_score();

	// Drawing meteors
	for (uint8_t i = 0; i < METEORS_MAX; ++i)
	{
		meteor_gameobj_t meteor = meteors[i];

		if (meteor.active)
		{
			int16_t x = meteor.coords.x;
			int16_t y = meteor.coords.y;
			uint8_t r = meteor.radius;

			for (uint8_t j = 0; j < 20; ++j)
			{
				uint8_t idx = (j == 19 ? 0 : j + 1);

				meteors_draw_line(
					x + r * meteor.r_offsets[j] * cos(j * PI * 2 / 20),
					y + r * meteor.r_offsets[j] * sin(j * PI * 2 / 20),
					x + r * meteor.r_offsets[idx] * cos(idx * PI * 2 / 20),
					y + r * meteor.r_offsets[idx] * sin(idx * PI * 2 / 20),
					WHITE
				);
			}
		}

		meteor_gameobj_t missile = meteors_missiles[i];

		if (missile.active)
			vga_draw_ellipse(
				missile.coords.x,
				missile.coords.y,
				1,
				1,
				WHITE
			);
	}

	if (
		!meteors_invincible ||
		(meteors_invincible && pit_get_ticks() % 3 == 0)
	)
		for (uint8_t i = 0; i < 4; ++i)
		{
			uint8_t j = i + 1;

			meteors_draw_line(
				meteors_sx[i % 3], meteors_sy[i % 3],
				meteors_sx[j % 3], meteors_sy[j % 3],
				WHITE
			);

			if (meteors_boost)
				meteors_draw_line(
					meteors_tsx[i % 3], meteors_tsy[i % 3],
					meteors_tsx[j % 3], meteors_tsy[j % 3],
					WHITE
				);
		}
}

void meteors_play()
{
	size_t size = METEORS_MAX * sizeof(meteor_gameobj_t);

	meteors = malloc(size);
	meteors_missiles = malloc(size);

	memset(meteors, 0, size);
	memset(meteors_missiles, 0, size);

	meteors_init();

	while (meteors_playing)
	{
		if (
			!meteors_paused &&
			(int32_t) meteors_frame_counter++ >= 60 - meteors_level
		)
		{
			meteors_render();
			meteors_update();

			meteors_frame_counter = 0;
		}

		HALT;
	}

	free(meteors);
	free(meteors_missiles);

	init_vga(80, 25);
}

/* This file is part of RexOS!.
 *
 * RexOS! is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * RexOS! is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with RexOS!.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <games/common.h>
#include <drivers/VGA.h>
#include <gui/text_mode.h>
#include <stdbool.h>
#include <stdio.h>
#include <unistd.h>

uint16_t border[BORDER_SIZE];	// Stores the coordinates of border blocks
uint16_t borderTop[80];			// Top wall
uint16_t borderLeft[24];		// Left wall

void draw_borders(uint8_t bgcolor, uint8_t fgcolor)
{
	textmode_draw_rect(1, 2, 79, 24, bgcolor);	// CLC the screen partially

	uint16_t i = 0, pos = 0;
	uint8_t ch = 0;

	for (uint8_t y = 1; y < VGA_HEIGHT; ++y)
		for (uint8_t x = 0; x < VGA_WIDTH; ++x)
		{
			if (x == 0 || x == VGA_WIDTH - 1 || y == 1 || y == VGA_HEIGHT - 1)
			{
				pos = vga_get_pos(x, y);
				border[i++] = pos;

				if (x == 0 || x == VGA_WIDTH - 1)
					ch = 179;

				if (y == 1 || y == VGA_HEIGHT - 1)
					ch = 196;
				
				if (x == 0 && y == 1)
					ch = 218;

				if (x == VGA_WIDTH - 1 && y == 1)
					ch = 191;

				if (x == 0 && y == VGA_HEIGHT - 1)
					ch = 192;

				if (x == VGA_WIDTH - 1 && y == VGA_HEIGHT - 1)
					ch = 217;

				vga_draw_char_at(ch, pos, bgcolor, fgcolor);
			}
		}
}

void show_pause()
{
	draw_window_with_border(23, 10, 58, 15, BRIGHT_GREY, BLACK, true);

	vga_set_index(vga_get_pos(35, 11));
	printf("${B07}${F00}Game paused${CLC}");

	vga_set_index(vga_get_pos(26, 13));
	printf(
		"${B07}${F04}Space ${F00}- continue, ${F04}Q ${F00}/ "
		"${F04}E ${F00}- exit${CLC}"
	);
}

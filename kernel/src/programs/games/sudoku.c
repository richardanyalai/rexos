/* This file is part of RexOS!.
 *
 * RexOS! is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * RexOS! is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERsnake_charANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with RexOS!.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <drivers/keyboard.h>
#include <drivers/VGA.h>
#include <gui/graphics_mode.h>
#include <sys/isr.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>

extern uint8_t vga_graphics_mode_font[2048];

uint8_t *sudoku_board, *sudoku_values;
char sudoku_x, sudoku_y;
bool sudoku_playing;

void sudoku_draw_number(uint8_t _x, uint8_t _y, uint8_t num, uint8_t color)
{
	uint8_t x = sudoku_x * 20 + 75;
	uint8_t y = sudoku_y * 20 + 14;

	vga_draw_fill_rect(x, y, x + 11, y + 12, WHITE);

	for (uint8_t y = 0; y < 8; ++y)
		for (uint8_t x = 0; x < 8; ++x)
			if ((vga_graphics_mode_font[(num + '0') * 8 + y] >> (7 - x) & 1))
				vga_draw_pixel(_x * 20 + 77 + x, _y * 20 + 17 + y, color);
}

void sudoku_draw_sel(char _x, char _y)
{
	if (_x < 0)
		_x = 8;
	else if (_x > 8)
		_x = 0;

	if (_y < 0)
		_y = 8;
	else if (_y > 8)
		_y = 0;

	uint8_t x = sudoku_x * 20 + 74;
	uint8_t y = sudoku_y * 20 + 13;

	vga_draw_rect(x, y, x + 13, y + 14, WHITE);

	x = _x * 20 + 74;
	y = _y * 20 + 13;

	vga_draw_rect(x, y, x + 13, y + 14, RED);

	sudoku_x = _x;
	sudoku_y = _y;
}

void sudoku_init(void);

void sudoku_handle_keyboard(active_keys_t *active_keys)
{
	if (active_keys->active_key.pressed)
		switch (active_keys->active_key.key_code)
		{
			case KEY_LEFT:
			case KEY_A:
			case KEY_H:
				sudoku_draw_sel(sudoku_x - 1, sudoku_y);
				break;

			case KEY_RIGHT:
			case KEY_D:
			case KEY_L:
				sudoku_draw_sel(sudoku_x + 1, sudoku_y);
				break;

			case KEY_UP:
			case KEY_W:
			case KEY_K:
				sudoku_draw_sel(sudoku_x, sudoku_y - 1);
				break;

			case KEY_DOWN:
			case KEY_S:
			case KEY_J:
				sudoku_draw_sel(sudoku_x, sudoku_y + 1);
				break;

			case KEY_1:
			case KEY_2:
			case KEY_3:
			case KEY_4:
			case KEY_5:
			case KEY_6:
			case KEY_7:
			case KEY_8:
			case KEY_9:
				if (
					!sudoku_board[sudoku_y * 9 + sudoku_x]   &&
					active_keys->active_key.char_code  > '0' &&
					active_keys->active_key.char_code <= '9'
				)
				{
					sudoku_draw_number(
						sudoku_x,
						sudoku_y,
						active_keys->active_key.char_code - '0',
						BLACK
					);
				}
				break;

			case KEY_R:
				sudoku_init();
				break;

			case KEY_Q:
				sudoku_playing = false;
				break;
		}

	null_active();
}

void sudoku_init()
{
	init_vga(320, 200);

	for (uint8_t i = 0; i < 81; ++i)
		sudoku_board[i] = sudoku_values[i] = 0;

	sudoku_x = sudoku_y = 4;

	sudoku_playing = true;

	vga_draw_fill_rect(0, 0, 320, 200, WHITE);

	uint8_t x_offset = 70, y_offset = 10;

	for (uint8_t x = 0; x < 10; ++x)
	{
		vga_draw_line(
			x_offset + x * 20,
			y_offset + 0,
			x_offset + x * 20,
			y_offset + 180,
			BLACK
		);

		if (x == 0 || x % 3 == 0)
		{
			vga_draw_line(
				x_offset + x * 20 - 1,
				y_offset + 0,
				x_offset + x * 20 - 1,
				y_offset + 180,
				BLACK
			);

			vga_draw_line(
				x_offset + x * 20 + 1,
				y_offset + 0,
				x_offset + x * 20 + 1,
				y_offset + 180,
				BLACK
			);
		}
	}

	for (uint8_t y = 0; y < 10; ++y)
	{
		vga_draw_line(
			x_offset - 2,
			y_offset + y * 20,
			x_offset + 180 + 1,
			y_offset + y * 20,
			BLACK
		);

		if (y == 0 || y % 3 == 0)
		{
			vga_draw_line(
				x_offset - 2,
				y_offset + y * 20 - 1,
				x_offset + 180 + 1,
				y_offset + y * 20 - 1,
				BLACK
			);

			vga_draw_line(
				x_offset - 2,
				y_offset + y * 20 + 1,
				x_offset + 180 + 1,
				y_offset + y * 20 + 1,
				BLACK
			);
		}
	}

	for (uint8_t i = 0; i < 20; ++i)
	{
		uint8_t x = rand() % 9;
		uint8_t y = rand() % 9;

		bool nums[9] = { false };

		for (uint8_t _x = 0; _x < 9; ++_x)
		{
			uint8_t pos = y * 9 + _x;

			if (sudoku_board[pos])
				nums[sudoku_values[pos] - 1] = true;
		}

		for (uint8_t _y = 0; _y < 9; ++_y)
		{
			uint8_t pos = _y * 9 + x;

			if (sudoku_board[pos])
				nums[sudoku_values[pos]-1] = true;
		}

		uint8_t
			start_x = x - x % 3,
			start_y = y - y % 3;

		for (uint8_t _x = start_x; _x < start_x + 3; ++_x)
			for (uint8_t _y = start_y; _y < start_y + 3; ++_y)
			{
				uint8_t pos = _y * 9 + _x;

				if (sudoku_board[pos])
					nums[sudoku_values[pos]-1] = true;
			}

		uint8_t count = 0;

		for (uint8_t j = 0; j < 9; ++j)
			if (!nums[j])
				count++;

		uint8_t *good_nums = malloc(count * sizeof(uint8_t));
		uint8_t idx = 0;

		for (uint8_t j = 0; j < 9; ++j)
			if (!nums[j])
				good_nums[idx++] = j + 1;

		uint8_t pos = y * 9 + x;

		sudoku_board[pos]  = 1;
		sudoku_values[pos] = good_nums[rand() % count];

		free(good_nums);
	}

	for (uint8_t y = 0; y < 9; ++y)
		for (uint8_t x = 0; x < 9; ++x)
		{
			uint8_t pos = y * 9 + x;

			if (sudoku_board[pos] == 1)
				sudoku_draw_number(x, y, sudoku_values[pos], BRIGHT_GREY);
		}

	sudoku_draw_sel(sudoku_x, sudoku_y);

	set_keyboard_handler(sudoku_handle_keyboard);
}

void sudoku_play()
{
	sudoku_board  = malloc(81 * sizeof(uint8_t));
	sudoku_values = malloc(81 * sizeof(uint8_t));

	sudoku_init();

	while(sudoku_playing)
		HALT;

	free(sudoku_board);
	free(sudoku_values);

	init_vga(80, 25);
}

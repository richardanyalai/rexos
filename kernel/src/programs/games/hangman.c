/* This file is part of RexOS!.
 *
 * RexOS! is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * RexOS! is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with RexOS!.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <drivers/keyboard.h>
#include <drivers/VGA.h>
#include <sys/isr.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

static uint8_t
	hangman_old_vga_y,
	hangman_block,
	hangman_char,
	hangman_tries,
	hangman_length;

static bool hangman_playing;

static char *word, *guess;

const char *first_rows =
"                \n"
"|--------       \n"
"|       |       \n";

const char *last_rows =
"|               \n"
"|_______________\n";


const char *hangman[] =
{
	"|               \n"
	"|               \n"
	"|               \n"
	"|               \n"
	"|               \n",
	"|       O       \n"
	"|               \n"
	"|               \n"
	"|               \n"
	"|               \n",
	"|       O       \n"
	"|       |       \n"
	"|               \n"
	"|               \n"
	"|               \n",
	"|       O       \n"
	"|     --|       \n"
	"|               \n"
	"|               \n"
	"|               \n",
	"|       O       \n"
	"|     --|--     \n"
	"|               \n"
	"|               \n"
	"|               \n",
	"|       O       \n"
	"|     --|--     \n"
	"|               \n"
	"|               \n"
	"|               \n",
	"|       O       \n"
	"|     --|--     \n"
	"|       |       \n"
	"|               \n"
	"|               \n",
	"|       O       \n"
	"|     --|--     \n"
	"|       |       \n"
	"|      /        \n"
	"|               \n",
	"|       O       \n"
	"|     --|--     \n"
	"|       |       \n"
	"|      / \\     \n"
	"|               \n",
	"|       O       \n"
	"|     --|--     \n"
	"|       |       \n"
	"|      / \\     \n"
	"|     /         \n",
	"|       O       \n"
	"|     --|--     \n"
	"|       |       \n"
	"|      / \\     \n"
	"|     /   \\    \n"
};

const char *words[] =
{
	"rexos",
	"osdev",
	"linux",
	"shrek",
	"kernel",
	"hangman",
	"makefile",
	"stallman",
	"heisenberg",
	"programming",
	"operating system",
	"task state segment",
	"global descriptor table",
};

#define WORDS_COUNT			13
#define MAX_HANGMAN_TRIES	10

// Handle keyboard events
static void hangman_handle_keyboard(active_keys_t *active_keys)
{
	hangman_char	= 0;
	hangman_block	= 0;

	key_t active_key = active_keys->active_key;
	hangman_char = active_key.char_code;

	if (
		active_key.pressed			&&
		active_key.char_code != 0	&&
		hangman_char != '\n'		&&
		hangman_char != ' '
	)
	{
		if (active_keys->ctrl && hangman_char == 'c')
		{
			hangman_playing = false;
			return;
		}

		bool match = false;
		hangman_playing = false;
		
		for (uint8_t i = 0; i < hangman_length - 1; ++i)
		{
			if (word[i] == hangman_char && guess[i] != hangman_char)
			{
				match = true;
				guess[i] = hangman_char;
				printf("${F12}%c${F15}", guess[i]);
			}
			else
				putchar(guess[i]);

			if (guess[i] == '_')
				hangman_playing = true;
		}

		if (!match)
		{
			printf("\n%s", first_rows);
			printf(hangman[++hangman_tries]);
			puts(last_rows);

			printf("\n%d tries left\n%s\r", 10 - hangman_tries, guess);

			if (hangman_tries == MAX_HANGMAN_TRIES)
			{
				hangman_playing = false;
				set_keyboard_handler(0);
				printf("\nThe word was: %s", word);
				return;
			}
		}
		else
			putchar('\r');
	}

	if (active_key.char_code != 0)
		null_active();
}

// Initializes the game
static void hangman_init()
{
	set_keyboard_handler(hangman_handle_keyboard);

	null_active();
	hangman_old_vga_y	= vga_get_cursor_y();
	hangman_tries		= 0;
	hangman_playing		= true;

	uint8_t word_index	= randint(0, WORDS_COUNT);
	hangman_length		= strlen(words[word_index]);

	word				= malloc(sizeof(int) * hangman_length);
	guess				= malloc(sizeof(int) * hangman_length);

	for (uint8_t i = 0; i < hangman_length; word[i] = guess[i] = '\0', ++i);

	strcpy(word, words[word_index]);

	printf(first_rows);
	printf(hangman[0]);
	printf(last_rows);

	for (uint8_t i = 0; i < hangman_length - 1; ++i)
		guess[i] = (word[i] == ' ') ? ' ' : '_';

	printf("%s\r", guess);
}

void hangman_play()
{
	hangman_init();

	while (hangman_playing)
		HALT;

	free(word);
	free(guess);
}

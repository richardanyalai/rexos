/* This file is part of RexOS!.
 *
 * RexOS! is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * RexOS! is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERsnake_charANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with RexOS!.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <drivers/keyboard.h>
#include <drivers/VGA.h>
#include <gui/graphics_mode.h>
#include <sys/isr.h>
#include <sys/rtc.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include <time.h>

#define MINE_COUNT	65
#define MINE_X		30
#define MINE_Y		16

extern uint8_t vga_graphics_mode_font[2048];

bool mines_playing, mines_game_over;
char mines_x, mines_y;
uint8_t
	mines_flags,
	mines_counter,
	mines_all_mine_count,
	mines_mine_count,
	mines_x_old,
	mines_y_old,
	mines_map[MINE_Y][MINE_X],
	mines_map_render[MINE_Y][MINE_X],
	mines_digits[10][7] =
	{
		{ 1, 1, 1, 0, 1, 1, 1 },
		{ 0, 0, 1, 0, 0, 1, 0 },
		{ 1, 0, 1, 1, 1, 0, 1 },
		{ 1, 0, 1, 1, 0, 1, 1 },
		{ 0, 1, 1, 1, 0, 1, 0 },
		{ 1, 1, 0, 1, 0, 1, 1 },
		{ 1, 1, 0, 1, 1, 1, 1 },
		{ 1, 0, 1, 0, 0, 1, 0 },
		{ 1, 1, 1, 1, 1, 1, 1 },
		{ 1, 1, 1, 1, 0, 1, 0 },
	},
	mines_flag[8][8] =
	{
		{ 1, 1, 1, 1, 4, 1, 1, 1 },
		{ 1, 1, 1, 4, 4, 1, 1, 1 },
		{ 1, 1, 4, 4, 4, 1, 1, 1 },
		{ 1, 4, 4, 4, 4, 1, 1, 1 },
		{ 1, 1, 1, 0, 0, 1, 1, 1 },
		{ 1, 1, 1, 0, 0, 1, 1, 1 },
		{ 1, 1, 0, 0, 0, 0, 1, 1 },
		{ 1, 0, 0, 0, 0, 0, 0, 1 },
	},
	mines_mine[8][8] =
	{
		{ 0, 0, 0, 1, 1, 0, 0, 0 },
		{ 0, 1, 0, 1, 1, 0, 1, 0 },
		{ 0, 0, 1, 1, 1, 1, 0, 0 },
		{ 1, 1, 1, 1, 1, 1, 1, 1 },
		{ 1, 1, 1, 1, 1, 1, 1, 1 },
		{ 0, 0, 1, 1, 1, 1, 0, 0 },
		{ 0, 1, 0, 1, 1, 0, 1, 0 },
		{ 0, 0, 0, 1, 1, 0, 0, 0 },
	},
	mines_smiley_happy[17*17] =
	{
		15,15,15,15,15,15,00,00,00,00,00,15,15,15,15,15,15,
		15,15,15,15,00,00,14,14,14,14,14,00,00,15,15,15,15,
		15,15,15,00,14,14,14,14,14,14,14,14,14,00,15,15,15,
		15,15,00,14,14,14,14,14,14,14,14,14,14,14,00,15,15,
		15,00,14,14,14,14,14,14,14,14,14,14,14,14,14,00,15,
		15,00,14,14,14,14,14,14,14,14,14,14,14,14,14,00,15,
		00,14,14,14,14,00,00,14,14,14,00,00,14,14,14,14,00,
		00,14,14,14,14,00,00,14,14,14,00,00,14,14,14,14,00,
		00,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,00,
		00,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,00,
		00,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,00,
		15,00,14,14,14,00,00,14,14,14,00,00,14,14,14,00,15,
		15,00,14,14,14,14,00,00,00,00,00,14,14,14,14,00,15,
		15,15,00,14,14,14,14,00,00,00,14,14,14,14,00,15,15,
		15,15,15,00,14,14,14,14,14,14,14,14,14,00,15,15,15,
		15,15,15,15,00,00,14,14,14,14,14,00,00,15,15,15,15,
		15,15,15,15,15,15,00,00,00,00,00,15,15,15,15,15,15,
	},
	mines_smiley_sad[17*17] =
	{
		15,15,15,15,15,15,00,00,00,00,00,15,15,15,15,15,15,
		15,15,15,15,00,00,14,14,14,14,14,00,00,15,15,15,15,
		15,15,15,00,14,14,14,14,14,14,14,14,14,00,15,15,15,
		15,15,00,14,14,14,14,14,14,14,14,14,14,14,00,15,15,
		15,00,14,14,14,14,14,14,14,14,14,14,14,14,14,00,15,
		15,00,14,14,14,14,14,14,14,14,14,14,14,14,14,00,15,
		00,14,14,14,14,00,00,14,14,14,00,00,14,14,14,14,00,
		00,14,14,14,14,00,00,14,14,14,00,00,14,14,14,14,00,
		00,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,00,
		00,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,00,
		00,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,00,
		15,00,14,14,14,14,14,00,00,00,14,14,14,14,14,00,15,
		15,00,14,14,14,14,00,00,00,00,00,14,14,14,14,00,15,
		15,15,00,14,14,00,00,14,14,14,00,00,14,14,00,15,15,
		15,15,15,00,14,14,14,14,14,14,14,14,14,00,15,15,15,
		15,15,15,15,00,00,14,14,14,14,14,00,00,15,15,15,15,
		15,15,15,15,15,15,00,00,00,00,00,15,15,15,15,15,15,
	},
	mines_smiley_chad[17*17] =
	{
		15,15,15,15,15,15,00,00,00,00,00,15,15,15,15,15,15,
		15,15,15,15,00,00,14,14,14,14,14,00,00,15,15,15,15,
		15,15,15,00,14,14,14,14,14,14,14,14,14,00,15,15,15,
		15,15,00,14,14,14,14,14,14,14,14,14,14,14,00,15,15,
		15,00,14,14,14,14,14,14,14,14,14,14,14,14,14,00,15,
		15,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,15,
		00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,
		00,14,14,00,00,00,00,00,14,00,00,00,00,00,14,14,00,
		00,14,14,00,00,00,00,14,14,14,00,00,00,00,14,14,00,
		00,14,14,14,00,00,14,14,14,14,14,00,00,14,14,14,00,
		00,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,00,
		15,00,14,14,14,00,00,14,14,14,00,00,14,14,14,00,15,
		15,00,14,14,14,14,00,00,00,00,00,14,14,14,14,00,15,
		15,15,00,14,14,14,14,00,00,00,14,14,14,14,00,15,15,
		15,15,15,00,14,14,14,14,14,14,14,14,14,00,15,15,15,
		15,15,15,15,00,00,14,14,14,14,14,00,00,15,15,15,15,
		15,15,15,15,15,15,00,00,00,00,00,15,15,15,15,15,15,
	};

uint16_t mines_time, mines_free_fields;

void
	mines_init(void),
	mines_update_flags(void),
	mines_move(void),
	mines_draw_mine(uint8_t, uint8_t);

void mines_draw_count(uint8_t _x, uint8_t _y)
{
	uint8_t count = mines_map[_y][_x];

	uint8_t color;

	switch (count)
	{
		case 2:  color = GREEN;   break;
		case 3:  color = RED;     break;
		case 4:  color = MAGENTA; break;
		case 5:  color = BROWN;   break;
		case 6:  color = CYAN;    break;
		case 7:  color = BLACK;   break;
		case 8:  color = GREY;    break;
		default: color = BLUE;
	}

	for (uint8_t y = 0; y < 8; ++y)
		for (uint8_t x = 0; x < 8; ++x)
			if ((vga_graphics_mode_font[(count + '0') * 8 + y] >> (7 - x) & 1))
				vga_draw_pixel(_x * 10 + 12 + x, _y * 10 + 37 + y, color);
}

void mines_pop(uint8_t, uint8_t);

void mines_show_game_over()
{
	mines_game_over = true;

	register_rtc_func(3, 0);

	for (uint8_t x = 0; x < 17; ++x)
		for (uint8_t y = 0; y < 17; ++y)
			if (mines_smiley_sad[x + 17 * y] != 15)
				vga_draw_pixel(
					152 + x, 10 + y, mines_smiley_sad[x + 17 * y]
				);

	for (uint8_t i = 0; i < MINE_X; ++i)
		for (uint8_t j = 0; j < MINE_Y; ++j)
			if (mines_map[j][i] == 10 && mines_map_render[j][i] != 1)
			{
				mines_map_render[j][i] = 0;
				mines_pop(i, j);
			}
}

void mines_check_win()
{
	if (
		!mines_mine_count  &&
		!mines_free_fields &&
		99 - mines_flags == mines_all_mine_count
	)
	{
		mines_game_over = true;

		register_rtc_func(3, 0);

		for (uint8_t x = 0; x < 17; ++x)
			for (uint8_t y = 0; y < 17; ++y)
				if (mines_smiley_chad[x + 17 * y] != 15)
					vga_draw_pixel(
						152 + x, 10 + y, mines_smiley_chad[x + 17 * y]
					);
	}
}

void mines_pop(uint8_t _x, uint8_t _y)
{
	if (mines_map_render[_y][_x] == 2)
		return;

	if (!mines_map_render[_y][_x])
		mines_free_fields--;

	uint16_t x = (_x + 1) * 10;
	uint8_t  y = _y * 10 + 35;

	vga_draw_fill_rect(x, y, x + 10, y + 10, 29);

	vga_draw_line(x - 1, y, x + 9, y, 28);
	vga_draw_line(x, y, x, y + 9, 28);

	mines_map_render[_y][_x] = 1;

	if (mines_map[_y][_x] == 10)
	{
		if (!mines_game_over)
			mines_show_game_over();

		mines_draw_mine(_x, _y);
	}
	else if (mines_map[_y][_x] > 0)
		mines_draw_count(_x, _y);
	else
	{
		for (int8_t i = -1; i < 2; ++i)
			for (int8_t j = -1; j < 2; ++j)
				if (
					_x + i > -1     &&
					_x + i < MINE_X &&
					_y + j > -1     &&
					_y + j < MINE_Y &&
					mines_map[_y + j][_x + i] != 10       &&
					mines_map_render[_y + j][_x + i] != 1 &&
					mines_map_render[_y + j][_x + i] != 2
				)
					mines_pop(_x + i, _y + j);
	}

	if (mines_map[_y][_x] != 10)
		mines_check_win();
}

void mines_draw_tile(uint8_t _x, uint8_t _y)
{
	uint16_t x = (_x + 1) * 10;
	uint8_t  y = _y * 10 + 35;

	vga_draw_fill_rect(x, y, x + 10, y + 10, BRIGHT_GREY);

	vga_draw_line(x, y, x + 8, y, WHITE);
	vga_draw_line(x, y, x, y + 9, WHITE);

	vga_draw_line(x + 9, y, x + 9, y + 9, GREY);
	vga_draw_line(x, y + 9, x + 9, y + 9, GREY);
}

void mines_draw_flag(uint8_t _x, uint8_t _y)
{
	mines_draw_tile(_x, _y);

	for (uint8_t y = 0; y < 8; ++y)
		for (uint8_t x = 0; x < 8; ++x)
			if (mines_flag[y][x] != 1)
				vga_draw_pixel(
					_x * 10 + 11 + x, _y * 10 + 36 + y, mines_flag[y][x]
				);
}

void mines_draw_mine(uint8_t _x, uint8_t _y)
{
	uint16_t x = (_x + 1) * 10;
	uint8_t  y = _y * 10 + 35;

	for (uint8_t j = 0; j < 8; ++j)
		for (uint8_t i = 0; i < 8; ++i)
			if (mines_mine[j][i] == 1)
				vga_draw_pixel(x + i + 1, y + j + 1, BLACK);
}

void mines_mark_flag()
{
	uint8_t x = mines_x;
	uint8_t y = mines_y;

	if (mines_map_render[y][x] != 1)
	{
		bool flagged = mines_map_render[y][x] != 2;

		if (flagged && mines_flags > 0)
		{
			mines_draw_flag(x, y);
			mines_map_render[y][x] = 2;
			mines_flags--;
			mines_free_fields--;

			if (mines_map[y][x] == 10)
			{
				mines_mine_count--;
				mines_check_win();
			}
		}
		else
		{
			mines_draw_tile(x, y);
			mines_map_render[y][x] = 0;
			mines_flags++;
			mines_free_fields++;

			if (mines_map[y][x] == 10)
				mines_mine_count++;
		}

		mines_update_flags();
	}

	mines_move();
}

void mines_move()
{
	switch (mines_map_render[mines_y_old][mines_x_old])
	{
		case 0:
			mines_draw_tile(mines_x_old, mines_y_old);
			break;

		case 1:
			mines_pop(mines_x_old, mines_y_old);
			break;

		case 2:
			mines_draw_tile(mines_x_old, mines_y_old);
			mines_draw_flag(mines_x_old, mines_y_old);
			break;
	};

	vga_draw_rect(
		(mines_x + 1) * 10,
		mines_y * 10 + 35,
		(mines_x + 2) * 10 - 1,
		mines_y * 10 + 44,
		RED
	);

	mines_x_old = mines_x;
	mines_y_old = mines_y;
}

void mines_handle_keyboard(active_keys_t *active_keys)
{
	if (active_keys->active_key.pressed)
		switch (active_keys->active_key.key_code)
		{
			case KEY_LEFT:
			case KEY_A:
			case KEY_H:
				if (mines_game_over)
					break;

				if (mines_x - 1 >= 0)
					mines_x--;
				else
					mines_x = MINE_X - 1;
				break;

			case KEY_RIGHT:
			case KEY_D:
			case KEY_L:
				if (mines_game_over)
					break;

				if (mines_x + 1 < MINE_X)
					mines_x++;
				else
					mines_x = 0;
				break;

			case KEY_UP:
			case KEY_W:
			case KEY_K:
				if (mines_game_over)
					break;

				if (mines_y - 1 >= 0)
					mines_y--;
				else
					mines_y = MINE_Y - 1;
				break;

			case KEY_DOWN:
			case KEY_S:
			case KEY_J:
				if (mines_game_over)
					break;

				if (mines_y + 1 < MINE_Y)
					mines_y++;
				else
					mines_y = 0;
				break;

			case KEY_F:
				if (mines_game_over)
					break;

				mines_mark_flag();
				break;

			case KEY_R:
				mines_init();
				break;

			case KEY_Q:
				mines_playing = false;
				break;

			case KEY_SPACE:
				if (mines_game_over)
					break;

				mines_pop(mines_x, mines_y);
				break;
		}

	mines_move();

	null_active();
}

void mines_draw_digit(uint16_t x, uint8_t y, uint8_t digit)
{
	uint8_t l = 5;

	for (uint8_t i = 0; i < 7; ++i)
		if (mines_digits[digit][i])
			switch (i)
			{
				// Top
				case 0:
					vga_draw_line(x + 1, y, x + l, y, RED);
					break;

				// Top left
				case 1:
					vga_draw_line(x, y, x, y + l, RED);
					break;

				// Top right
				case 2:
					vga_draw_line(x + l + 2, y, x + l + 2, y + l, RED);
					break;

				// Centre
				case 3:
					vga_draw_line(x + 1, y + l + 1, x + l, y + l + 1, RED);
					break;

				// Bottom left
				case 4:
					vga_draw_line(x, y + l + 2, x, y + 2 * l + 2, RED);
					break;

				// Bottom right
				case 5:
					vga_draw_line(
						x + l + 2, y + l + 2, x + l + 2, y + 2 * l + 2, RED
					);
					break;

				// Bottom
				case 6:
					vga_draw_line(
						x + 1, y + 2 * l + 3, x + l, y + 2 * l + 3, RED
					);
					break;
			}
}

void mines_draw_number(uint16_t x, uint16_t number)
{
	uint8_t _digits[3] = {0};

	_digits[0] = number / 100;
	_digits[1] = number / 10 - _digits[0] * 10;
	_digits[2] = number - _digits[0] * 100 - _digits[1] * 10;

	mines_draw_digit(x +  0, 11, _digits[0]);
	mines_draw_digit(x + 12, 11, _digits[1]);
	mines_draw_digit(x + 24, 11, _digits[2]);
}

void mines_update_flags()
{
	vga_draw_fill_rect(14, 9, 50, 27, BLACK);
	mines_draw_number(16, mines_flags);
}

void mines_update_time()
{
	if (mines_counter++ % 2 == 0)
	{
		vga_draw_fill_rect(270, 9, 306, 27, BLACK);
		mines_draw_number(272, --mines_time);

		if (mines_time == 0)
			mines_show_game_over();
	}
}

void mines_init()
{
	init_vga(320, 200);

	init_rtc(15);

	mines_playing   = true;
	mines_game_over = false;

	mines_mine_count  = 0;
	mines_flags       = 99;
	mines_free_fields = MINE_X * MINE_Y; 
	mines_time        = 1000;
	mines_counter     = 2;

	mines_x       = mines_x_old = 0;
	mines_y       = mines_y_old = 0;

	// Background
	vga_draw_fill_rect(0, 0, 320, 200, BRIGHT_GREY);

	// Top frame
	vga_draw_line(8, 5, 310, 5, GREY);
	vga_draw_line(9, 5, 9, MINE_X, GREY);
	vga_draw_line(9, MINE_X, 310, MINE_X, WHITE);
	vga_draw_line(310, 5, 310, MINE_X, WHITE);

	// Reset button
	vga_draw_line(150, 9, 170, 9, WHITE);
	vga_draw_line(150, 9, 150, 27, WHITE);
	vga_draw_line(150, 27, 170, 27, GREY);
	vga_draw_line(170, 9, 170, 27, GREY);

	for (uint8_t x = 0; x < 17; ++x)
		for (uint8_t y = 0; y < 17; ++y)
			if (mines_smiley_happy[x + 17 * y] != 15)
				vga_draw_pixel(152 + x, 10 + y, mines_smiley_happy[x + 17 * y]);

	// Minesweeper frame
	vga_draw_line(8, 34, 310, 34, GREY);
	vga_draw_line(9, 34, 9, 195, GREY);
	vga_draw_line(9, 195, 310, 195, WHITE);
	vga_draw_line(310, 34, 310, 195, WHITE);

	for (uint8_t x = 0; x < MINE_X; ++x)
		for (uint8_t y = 0; y < MINE_Y; ++y)
		{
			mines_map[y][x] = mines_map_render[y][x] = 0;
			mines_draw_tile(x, y);
		}

	srand(time(NULL));

	for (uint8_t i = 0; i < MINE_COUNT; ++i)
	{
		uint8_t x = rand() % MINE_X;
		uint8_t y = rand() % MINE_Y;

		if (mines_map[y][x] != 10)
		{
			mines_map[y][x] = 10;
			mines_mine_count++;
		}
	}

	mines_all_mine_count = mines_mine_count;

	for (uint8_t x = 0; x < MINE_X; ++x)
		for (uint8_t y = 0; y < MINE_Y; ++y)
		{
			if (!mines_map[y][x])
				for (int8_t _x = -1; _x < 2; ++_x)
					for (int8_t _y = -1; _y < 2; ++_y)
						if (
							x + _x > -1     &&
							x + _x < MINE_X &&
							y + _y > -1     &&
							y + _y < MINE_Y &&
							mines_map[y+_y][x+_x] == 10
						)
							mines_map[y][x]++;
		}

	mines_update_flags();
	mines_update_time();

	mines_move();

	register_rtc_func(3, mines_update_time);

	set_keyboard_handler(mines_handle_keyboard);
}

void mines_play()
{
	mines_init();

	while (mines_playing)
		HALT;

	register_rtc_func(3, 0);

	init_vga(80, 25);
}

/* This file is part of RexOS!.
 *
 * RexOS! is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * RexOS! is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERsnake_charANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with RexOS!.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <drivers/keyboard.h>
#include <drivers/VGA.h>
#include <games/common.h>
#include <gui/text_mode.h>
#include <programs/common.h>
#include <sys/isr.h>
#include <sys/rtc.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>

int16_t *snake;

uint16_t
	apple_pos,		// The position of the apple
	old_apple_pos,	// Previous position of the apple
	snake_score,	// Current score
	snake_size,		// The snake's actual length
	prev_tail,		// The previous position of the snake's tail
	snake_timer;	// Simple counter

uint8_t
	snake_char,		// The uint8_t code of the pressed key
	snake_dir,		// The snake's direction
	snake_old_c;	// Previously pressed key

bool
	snake_running,	// Indicates whether the game is still running
	snake_paused;	// Indicates whether the game is paused or not

// Generates a new apple on the map
void new_apple()
{
	old_apple_pos = apple_pos;

	const uint16_t free_size = PLAYGROUND_SIZE - snake_size;
	uint16_t free_counter = 0;
	uint16_t free_places[free_size];
	uint8_t x = 1, y = 2;

	for (uint8_t i = y; i < VGA_HEIGHT - 1; ++i)
	{
		x = 1;
		for (uint8_t j = x; j < VGA_WIDTH - 1; ++j)
		{
			bool canAdd = true;
			for (uint8_t k = 0; k < snake_size; ++k)
				if (snake[k] == vga_get_pos(x, y))
					canAdd = false;

			if (canAdd)
				free_places[free_counter++] = vga_get_pos(x, y);

			x++;
		}
		y++;
	}

	apple_pos = free_places[randint(0, free_size)];

	vga_draw_char_at(254, apple_pos, BLUE, BRIGHT_RED);
}

// Updates the snake_score in the top right corner
void snake_write_score()
{
	textmode_draw_rect(35, 0, 46, 1, BLACK);
	vga_set_index(38 - digits(snake_score));
	printf("${B00}Score: %d${B01}", snake_score);
}

// Stops the game
void snake_pause()
{
	if ((snake_paused = !snake_paused))
		show_pause();
	else
	{
		textmode_draw_rect(23, 8, 59, 16, BLUE);
		vga_draw_char_at(254, apple_pos, BLUE, BRIGHT_RED);
	}
}

// Gets keyboard input for movement
void snake_handle_keyboard(active_keys_t *active_keys)
{
	key_t active_key = active_keys->active_key;

	uint8_t (*key_to_char)(uint8_t) = lambda (uint8_t, (uint8_t key_code)
	{
		switch (key_code)
		{
			case KEY_W:		return 'w';
			case KEY_A:		return 'a';
			case KEY_S:		return 's';
			case KEY_D:		return 'd';
			case KEY_K:		return 'w';
			case KEY_H:		return 'a';
			case KEY_J:		return 's';
			case KEY_L:		return 'd';
			case KEY_UP:	return 'w';
			case KEY_LEFT:	return 'a';
			case KEY_DOWN:	return 's';
			case KEY_RIGHT:	return 'd';
			case KEY_SPACE:	return ' ';
			case KEY_Q:		return 'q';
			case KEY_E:		return 'e';
		}

		return 0;
	});

	uint8_t c = key_to_char(active_key.key_code);
	
	if (snake_old_c == 'Z' && active_key.char_code == 'Q')
	{
		snake_running = false;
		return;
	}

	if (active_key.pressed)
	{
		if (
			!snake_paused &&
			(
				(c == 'w' && snake_dir != 's') ||
				(c == 'a' && snake_dir != 'd') ||
				(c == 's' && snake_dir != 'w') ||
				(c == 'd' && snake_dir != 'a')
			)
		)
			snake_char = snake_dir = c;
		else if (snake_paused && (c == 'q' || c == 'e'))
			snake_running = false;

		if (c == ' ')
			snake_pause();
	}

	if (active_key.char_code)
	{
		if (active_key.pressed)
			snake_old_c = active_key.char_code;

		null_active();
	}
}

void snake_init(void);

// Updates the game data
void snake_update()
{
	if (snake[0] == apple_pos)
	{
		snake_score++;
		snake_write_score();
		snake_size++;
		new_apple();
	}

	int16_t i = snake_size;

	while (--i > 0)
		snake[i] = snake[i-1];

	switch (snake_char)
	{
		case 'w':
			snake[0] -= 80;
			break;
				
		case 's':
			snake[0] += 80;
			break;

		case 'a':
			snake[0] -= 1;
			break;

		case 'd':
			snake[0] += 1;
			break;

		default:
			break;
	}

	i = 0;
	while (i++ < BORDER_SIZE - 1)
		if (snake[0] == border[i])
			return snake_init();

	i = 0;
	while (++i < PLAYGROUND_SIZE)
		if (snake[0] == snake[i])
			return snake_init();
}

// Renders the game according to the updated data
void snake_render()
{
	vga_draw_char_at(254, apple_pos, BLUE, BRIGHT_RED);

	vga_draw_char_at(0, prev_tail, BLUE, vga_get_bgcolor());

	// Now we can draw the parts of the snake

	uint16_t i = 0;
	while (snake[i] > -99)
	{
		if (i == 0)
			// The snake's head
			vga_draw_char_at(254, snake[i], YELLOW, BLACK);
		else
			// Other body parts
			vga_draw_char_at(' ', snake[i], i % 2 ? BRIGHT_GREEN : GREEN, BLACK);

		i++;
	}

	prev_tail = snake[snake_size-1];
}

// Initializes the game
void snake_init()
{
	set_keyboard_handler(snake_handle_keyboard);

	srand(time(NULL));

	snake_char = snake_dir = 'd';
	snake_size = 3;
	snake_paused = false;
	snake_running = true;
	snake_timer = 0;

	snake = malloc(PLAYGROUND_SIZE * sizeof(int16_t));

	snake[0] = vga_get_pos(10, 8);
	snake[1] = vga_get_pos(9, 8);
	snake[2] = vga_get_pos(8, 8);

	prev_tail = snake[2];

	for (uint16_t i = snake_size; i < PLAYGROUND_SIZE; snake[i++] = -99);

	textmode_draw_rect(65, 0, 85, 0, BLACK);

	snake_score = 0;

	vga_set_bgcolor(BLUE);
	vga_set_fgcolor(WHITE);

	draw_borders(vga_get_bgcolor(), vga_get_fgcolor());
	new_apple();
	snake_write_score();
}

void snake_play()
{
	snake_init();

	register_rtc_func(RTC_CLOCK_WIDGET, update_clock_widget);

	// Game loop
	while (snake_running)
	{
		if (!snake_paused)
		{
			if (snake_timer++ % 2 == 0)	// Increment the counter
				snake_update();			// Update the snake's coordinates
			
			snake_render();				// Render the body parts of the snake

			if (snake_char == 'w' || snake_char == 's')
				usleep(45);
			else
				usleep(40);
		}

		HALT;
	}

	free(snake);

	null_active();
}

/* This file is part of RexOS!.
 *
 * RexOS! is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * RexOS! is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with RexOS!.  If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * Contains some elements of the code from Matthew Steel's project
 * REF: https://gist.github.com/MatthewSteel/3158579
 */

#include <drivers/keyboard.h>
#include <drivers/VGA.h>
#include <gui/text_mode.h>
#include <programs/common.h>
#include <sys/isr.h>
#include <sys/rtc.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

static uint8_t tictac_block, tictac_char;
static bool tictac_running;

// Handle keyboard events
void tictac_handle_keyboard(active_keys_t *active_keys)
{
	tictac_char = tictac_block = 0;

	key_t active_key = active_keys->active_key;

	if (active_key.pressed)
	{
		tictac_char = active_key.char_code;

		if (tictac_char >= '1' && tictac_char <= '9')
			tictac_block = tictac_char - 48;

		if (tictac_char == 'q' || tictac_char == 'e')
			tictac_running = false;
	}

	if (active_key.char_code != 0)
		null_active();
}

// Initializes the game
void tictac_init()
{
	set_keyboard_handler(tictac_handle_keyboard);

	tictac_block = 0;
	tictac_running = true;

	vga_set_bgcolor(YELLOW);
	vga_set_fgcolor(BLUE);

	draw_window_with_title(
		3, 3, 28, 13, 10, YELLOW, BLUE, "Tic Tac Toe", true, true
	);

	vga_set_index(vga_get_pos(25, 3));
	printf("[${F12}Q${CLC}]");
}

/**
 * @brief			Determines if a player has won
 * @param board		is the game board
 * @return			-1 if the player won, 1 if the computer won, 0 otherwise
 */
char win(const char board[9])
{
	uint8_t wins[8][3] =
	{{0,1,2},{3,4,5},{6,7,8},{0,3,6},{1,4,7},{2,5,8},{0,4,8},{2,4,6}};
 
	for (uint8_t i = 0; i < 8; ++i)
		if (
			board[wins[i][0]] != 0					&&
			board[wins[i][0]] == board[wins[i][1]]	&&
			board[wins[i][0]] == board[wins[i][2]]
		)
			return board[wins[i][2]];

	return 0;
}

/**
 * @brief			Let the opponent make a move
 * @param board		is the game board
 * @param player	is the current player's value
 */
char minimax(char board[9], char player)
{
	// How is the position like for player (their turn) on board?
	char winner = win(board);
	if (winner != 0)
		return winner * player;

	char move = -1;
	char score = -2;				//Losing moves are preferred to no move
	
	for (uint8_t i = 0; i < 9; ++i)	//For all moves,
	{
		if (board[i] == 0)			// If legal,
		{
			board[i] = player;		// Try the move

			char thisScore = -minimax(board, -player);

			if (thisScore > score)
			{
				score = thisScore;
				move = i;
			}						// Pick the one that's worst for opponent

			board[i] = 0;			// Reset board after try
		}
	}

	if (move == -1)
		return 0;

	return score;
}

/**
 * @brief			Let the opponent make a move
 * @param board		is the game board
 */
void computer(char board[9])
{
	char move = -1;
	char score = -2;

	for (uint8_t i = 0; i < 9; ++i)
		if (board[i] == 0)
		{
			board[i] = 1;
			char temp_score = -minimax(board, -1);
			board[i] = 0;

			if (temp_score > score)
			{
				score = temp_score;
				move = i;
			}
		}

	// Returns a score based on minimax tree at a given node.
	board[(uint8_t) move] = 1;
}

// Renders the game
void tictac_draw(const char board[9])
{
	/**
	 * @brief			Converts the value of a field to an 'x', 'o' or space
	 * @param i			is the value of a field
	 * @return			'x', 'o', or space
	 */
	char *(*pos_to_char) (uint8_t) = lambda(char *, (uint8_t field)
	{
		char value = board[field];
		char *empty_field = " \0";
		empty_field[0] = field + 49;

		switch (value)
		{
			case -1:	return "${F09}x${CLC}";
			case 0:		return " ";
			case 1:		return "${F04}o${CLC}";
		}

		return 0;
	});

	vga_set_index(vga_get_pos(10, 5));
	printf(
		"${B14}${F01} %s %c %s %c %s${CLC}",
		pos_to_char(0), 179,
		pos_to_char(1), 179,
		pos_to_char(2)
	); // Prints the first row

	vga_set_index(vga_get_pos(10, 6));
	printf(
		"${B14}${F01}%c%c%c%c%c%c%c%c%c%c%c${CLC}",
		196, 196, 196, 197, 196, 196, 196, 197, 196, 196, 196
	); // Prints the first separator

	vga_set_index(vga_get_pos(10, 7));
	printf(
		"${B14}${F01} %s %c %s %c %s${CLC}",
		pos_to_char(3), 179,
		pos_to_char(4), 179,
		pos_to_char(5)
	); // Prints the second row

	vga_set_index(vga_get_pos(10, 8));
	printf(
		"${B14}${F01}%c%c%c%c%c%c%c%c%c%c%c${CLC}",
		196, 196, 196, 197, 196, 196, 196, 197, 196, 196, 196
	); // Prints the second separator

	vga_set_index(vga_get_pos(10, 9));
	printf(
		"${B14}${F01} %s %c %s %c %s${CLC}",
		pos_to_char(6), 179,
		pos_to_char(7), 179,
		pos_to_char(8)
	); // Prints the third row
}

/**
 * @brief			Let the us make a move
 * @param board		is the game board
 */
void player(char board[9])
{
	do
	{
		textmode_draw_rect(6, 11, 27, 12, vga_get_bgcolor());
		vga_set_index(vga_get_pos(9, 11));
		printf("${B14}${F01}Make a move: %d${CLC}", tictac_block);

		while (tictac_char == 0)
			HALT;

		vga_set_index(vga_get_pos(9, 11));
		printf("${B14}${F01}Make a move: %d${CLC}", tictac_block);
	}
	while (
		(tictac_block == 0 || board[tictac_block - 1] != 0) && tictac_running
	);

	board[tictac_block - 1] = -1;
}

void tictac_play()
{
	tictac_init();

	register_rtc_func(RTC_CLOCK_WIDGET, update_clock_widget);

	char board[9] =
	{
		0, 0, 0,
		0, 0, 0,
		0, 0, 0
	};

	for (
		uint8_t turn = 0; turn < 9 && win(board) == 0 && tictac_running; ++turn
	)
	{
		tictac_draw(board);

		if ((turn) % 2 == 0)
			player(board);
		else
			computer(board);

		tictac_draw(board);
	}

	textmode_draw_rect(6, 11, 27, 12, vga_get_bgcolor());
	vga_set_index(vga_get_pos(11, 11));

	switch (win(board))
	{
		case 0:
			vga_set_index(vga_get_pos(13, 11));
			printf("${B14}${F01}Draw.${CLC}");
			break;

		case 1:
			printf("${B14}${F01}You lost.${CLC}");
			break;

		case -1:
			printf("${B14}${F01}You won!${CLC}");
			break;
	}

	while (tictac_running)
		HALT;
}

/* This file is part of RexOS!.
 *
 * RexOS! is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * RexOS! is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERsnake_charANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with RexOS!.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <drivers/keyboard.h>
#include <drivers/VGA.h>
#include <gui/graphics_mode.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>

/**
 * I have written this Tetris clone fully by myself, however I found jdh's
 * music system so cool that I copied the sound headers and functions from his
 * project:
 * REF: https://www.youtube.com/watch?v=FaILnmUYS_U
 * REF: https://github.com/jdah/tetris-os
 * Improved PCSPKR by zment4:
 * REF: https://www.youtube.com/watch?v=BJS0j2B2EiY
 * REF: https://github.com/zment4/tetris-os
 */
#include "jdh/sound.h"
#include "jdh/music.h"

extern void speaker_pause();

#define TETROS_PIECES 7

typedef struct
{
	uint8_t light;
	uint8_t normal;
	uint8_t dark;
} TETROS_COLOR;

enum tetros_color_types
{
	TRED,
	TGREEN,
	TBLUE,
	TYELLOW,
	TORANGE,
	TCYAN,
	TMAGENTA,
	TGREY
};

const TETROS_COLOR tetros_colors[] =
{
	{ BRIGHT_RED,     RED,     111 },
	{ BRIGHT_GREEN,   GREEN,   120 },
	{ BRIGHT_BLUE,    BLUE,    104 },
	{ 92,             YELLOW,  172 },
	{ 43,             42,      114 },
	{ BRIGHT_CYAN,    CYAN,    104 },
	{ BRIGHT_MAGENTA, MAGENTA, 108 },
	{ BRIGHT_GREY,    GREY,     18 }
};

typedef struct
{
	uint8_t blocks[4][4];
	char color;
} TETROS_PIECE;

enum tetros_piece_types
{
	IBLOCK,
	JBLOCK,
	LBLOCK,
	OBLOCK,
	SBLOCK,
	TBLOCK,
	ZBLOCK
};

const TETROS_PIECE tetros_pieces[] =
{
	// I-block
	{
		.blocks =
		{
			{ 0, 0, 0, 0 },
			{ 0, 0, 0, 0 },
			{ 1, 1, 1, 1 },
			{ 0, 0, 0, 0 }
		},
		.color = TCYAN
	},
	// J-block
	{
		.blocks =
		{
			{ 0, 0, 0, 0 },
			{ 0, 0, 0, 0 },
			{ 1, 0, 0, 0 },
			{ 1, 1, 1, 0 }
		},
		.color = TBLUE
	},
	// L-block
	{
		.blocks =
		{
			{ 0, 0, 0, 0 },
			{ 0, 0, 0, 0 },
			{ 0, 0, 1, 0 },
			{ 1, 1, 1, 0 }
		},
		.color = TORANGE
	},
	// O-block
	{
		.blocks =
		{
			{ 0, 0, 0, 0 },
			{ 0, 0, 0, 0 },
			{ 1, 1, 0, 0 },
			{ 1, 1, 0, 0 }
		},
		.color = TYELLOW
	},
	// S-block
	{
		.blocks =
		{
			{ 0, 0, 0, 0 },
			{ 0, 0, 0, 0 },
			{ 0, 1, 1, 0 },
			{ 1, 1, 0, 0 }
		},
		.color = TGREEN
	},
	// T-block
	{
		.blocks =
		{
			{ 0, 0, 0, 0 },
			{ 0, 0, 0, 0 },
			{ 0, 1, 0, 0 },
			{ 1, 1, 1, 0 }
		},
		.color = TMAGENTA
	},
	// Z-block
	{
		.blocks =
		{
			{ 0, 0, 0, 0 },
			{ 0, 0, 0, 0 },
			{ 1, 1, 0, 0 },
			{ 0, 1, 1, 0 }
		},
		.color = TRED
	},
};

typedef enum
{
	LEFT,
	RIGHT,
	UP,
	DOWN,
	BOTTOM
} tetros_movement;

bool
	tetros_playing,
	tetros_paused,
	tetros_game_over,
	touching_bottom,
	music_playing = true;
char
	tetros_curr_x,
	tetros_curr_y,
	tetros_orientation,
	tetros_map[21][18],
	tetros_level;
uint16_t tetros_frame_counter;
uint32_t tetros_score;
TETROS_PIECE tetros_curr_piece, tetros_next_piece;

void tetros_draw_piece(char _x, uint8_t _y, TETROS_PIECE piece, bool clear);

uint8_t tetros_get_piece_left()
{
	uint8_t _x = 3;

	for (uint8_t y = 0; y < 4; ++y)
		for (uint8_t x = 0; x < _x; ++x)
			if (tetros_curr_piece.blocks[y][x] && x < _x)
				_x = x;

	return _x;
}

uint8_t tetros_get_piece_right()
{
	uint8_t _x = 0;

	for (uint8_t y = 0; y < 4; ++y)
		for (uint8_t x = 3; x > _x; --x)
			if (tetros_curr_piece.blocks[y][x] && x > _x)
				_x = x;

	return _x;
}

uint8_t tetros_get_piece_top(TETROS_PIECE piece)
{
	bool inc_j = true;
	uint8_t j = 0;

	for (; j < 4 && inc_j;)
	{
		for (uint8_t x = 0; x < 4; ++x)
			if (piece.blocks[j][x])
			{
				inc_j = false;
				break;
			}

		if (inc_j)
			++j;
	}

	return j;
}

bool tetros_collides(tetros_movement dir)
{
	char offset_x = 0;
	uint8_t offset_y = 0;

	switch (dir)
	{
		case LEFT:
			offset_x = -1;
			break;

		case RIGHT:
			offset_x = 1;
			break;

		default: // DOWN
			offset_y = 1;
	}

	bool (*tetros_is_masked)(uint8_t, uint8_t, char, uint8_t) =
	lambda(bool, (uint8_t _x, uint8_t _y, char offset_x, uint8_t offset_y) {
		uint8_t j = tetros_get_piece_top(tetros_curr_piece);

		for (uint8_t x = 0; x < 4; ++x)
			for (uint8_t y = 0; y < 4 - j; ++y)
				if (
					tetros_curr_piece.blocks[y + j][x] &&
					tetros_curr_x + offset_x + x == _x &&
					tetros_curr_y + offset_y + y == _y
				)
					return true;

		return false;
	});

	for (uint8_t x = 0; x < 18; ++x)
		for (uint8_t y = 0; y < 21; ++y)
			if (
				(
					tetros_map[y][x] != -1 &&
					!tetros_is_masked(x, y, 0, 0) &&
					tetros_is_masked(x, y, offset_x, offset_y)
				)
			)
				return true;

	return false;
}

void tetros_render(), tetros_update_next();

void tetros_rotate_3x3(uint8_t a[3][3])
{
	for (uint8_t y = 0; y < 1; ++y)
		for (uint8_t x = y; x < 2 - y; ++x)
		{
			uint8_t temp = a[y][x];
			a[y][x] = a[2 - x][y];
			a[2 - x][y] = a[2 - y][2 - x];
			a[2 - y][2 - x] = a[x][2 - y];
			a[x][2 - y] = temp;
		}
}

void tetros_rotate(TETROS_PIECE *piece)
{
	if (!tetros_collides(DOWN))
	{
		if (piece->color == TCYAN)
			for (uint8_t y = 0; y < 2; ++y)
				for (uint8_t x = y; x < 3 - y; ++x)
				{
					uint8_t temp = piece->blocks[y][x];
					piece->blocks[y][x] = piece->blocks[3 - x][y];
					piece->blocks[3 - x][y] = piece->blocks[3 - y][3 - x];
					piece->blocks[3 - y][3 - x] = piece->blocks[x][3 - y];
					piece->blocks[x][3 - y] = temp;
				}
		else if (piece->color != TYELLOW)
		{
			uint8_t matrix[3][3];

			for (uint8_t y = 0; y < 3; ++y)
				for (uint8_t x = 0; x < 3; ++x)
					matrix[y][x] = piece->blocks[y + 1][x];

			tetros_rotate_3x3(matrix);

			for (uint8_t y = 0; y < 3; ++y)
				for (uint8_t x = 0; x < 3; ++x)
					piece->blocks[y + 1][x] = matrix[y][x];
		}
	}
}

void tetros_update(), tetros_update_score();

void tetros_move_piece(tetros_movement dir)
{
	if (!tetros_paused && !tetros_game_over)
	{
		uint8_t old_x = tetros_curr_x, old_y = tetros_curr_y;

		switch (dir)
		{
			case LEFT:
				if (!tetros_collides(LEFT))
					--tetros_curr_x;
				break;

			case RIGHT:
				if (!tetros_collides(RIGHT))
					++tetros_curr_x;
				break;

			case UP:
				tetros_draw_piece(
					tetros_curr_x,
					tetros_curr_y,
					tetros_curr_piece,
					true
				);

				TETROS_PIECE piece = tetros_curr_piece;

				tetros_rotate(&tetros_curr_piece);

				if (tetros_collides(DOWN))
					tetros_curr_piece = piece;
				break;

			case DOWN:
				if (!tetros_collides(DOWN))
				{
					if (tetros_curr_y < 0)
						tetros_curr_y += 4;
					else
						++tetros_curr_y;
				}
				break;

			case BOTTOM:
				while (!tetros_collides(DOWN))
				{
					++tetros_curr_y;
					tetros_draw_piece(old_x, old_y, tetros_curr_piece, true);
					tetros_draw_piece(
						tetros_curr_x,
						tetros_curr_y,
						tetros_curr_piece,
						false
					);
					old_y = tetros_curr_y;
					tetros_render();
				}
				return;
		}

		tetros_draw_piece(old_x, old_y, tetros_curr_piece, true);
		tetros_draw_piece(
			tetros_curr_x,
			tetros_curr_y,
			tetros_curr_piece,
			false
		);

		if (tetros_collides(DOWN))
		{
			tetros_render();

			if (tetros_curr_y <= 2)
			{
				tetros_game_over = true;
				set_keyboard_handler(0);
				return;
			}
			
			tetros_score += 10;
			tetros_update_score();
			tetros_update_next();

			tetros_draw_piece(
				tetros_curr_x,
				tetros_curr_y + 1,
				tetros_curr_piece,
				true
			);

			touching_bottom = true;

			tetros_update();

			tetros_draw_piece(
				tetros_curr_x,
				tetros_curr_y,
				tetros_curr_piece,
				false
			);
		}

		tetros_render();
	}
}

void tetros_keyboard_handler(active_keys_t *active_keys)
{
	if (active_keys->active_key.pressed)
		switch (active_keys->active_key.key_code)
		{
			case KEY_Q:
				tetros_playing = false;
				break;

			case KEY_LEFT:
			case KEY_A:
			case KEY_H:
				tetros_move_piece(LEFT);
				break;

			case KEY_RIGHT:
			case KEY_D:
			case KEY_L:
				tetros_move_piece(RIGHT);
				break;

			case KEY_UP:
			case KEY_W:
			case KEY_K:
				tetros_move_piece(UP);
				break;

			case KEY_DOWN:
			case KEY_S:
			case KEY_J:
				tetros_move_piece(DOWN);
				break;

			case KEY_P:
				tetros_paused = !tetros_paused;
				break;

			case KEY_SPACE:
				tetros_move_piece(BOTTOM);
				break;

			case KEY_M:
				music_playing = !music_playing;
				sound_set_enabled(music_playing);
				if (!music_playing)
					speaker_pause();
				break;

			default:
				break;
		}

	null_active();
}

void tetros_draw_block(char _x, char _y, char _color);
void tetros_update_next();

void tetros_init()
{
	init_vga(320, 200);

	vga_clear();

	sound_init();

	if (sound_enabled())
	{
		music_init();
		sound_master(255);
	}

	tetros_playing   = true;
	tetros_paused    = false;
	tetros_game_over = false;
	touching_bottom  = false;

	tetros_score       = 0;
	tetros_level       = 0;
	tetros_orientation = 0;

	tetros_frame_counter = 1000;

	set_keyboard_handler(&tetros_keyboard_handler);

	for (uint8_t i = 0; i < 20; ++i)
	{
		for (uint8_t j = 1; j < 17; ++j)
			tetros_map[i][j] = -1;

		tetros_map[i][0] = TGREY;
		tetros_map[i][17] = TGREY;
	}

	tetros_update_score();

	srand(time(NULL));

	tetros_next_piece = tetros_pieces[rand() % TETROS_PIECES];

	tetros_update_next();

	tetros_draw_piece(tetros_curr_x, tetros_curr_y, tetros_curr_piece, false);
}

void tetros_update_score()
{
	vga_set_bgcolor(-1);
	vga_set_fgcolor(WHITE);
	vga_set_index(vga_get_pos(32, 1));
	printf("score:");

	vga_draw_fill_rect(255, 20, 320, 35, BLACK);

	vga_set_index(vga_get_pos(32, 3));
	printf("%d", tetros_score);

	vga_draw_line(255, 40, 312, 40, WHITE);

	vga_set_index(vga_get_pos(32, 6));
	printf("level:");

	vga_draw_fill_rect(255, 63, 320, 72, BLACK);

	vga_set_index(vga_get_pos(32, 8));
	printf("%d", tetros_level);
}

void tetros_update_next()
{
	vga_set_bgcolor(-1);
	vga_set_fgcolor(WHITE);
	vga_set_index(vga_get_pos(2, 1));
	printf("next:");

	tetros_curr_piece = tetros_next_piece;
	tetros_next_piece = tetros_pieces[rand() % TETROS_PIECES];

	tetros_curr_x = 6;
	tetros_curr_y = -4;

	uint8_t rotation = rand() % 4;

	for (uint8_t i = 0; i < rotation; ++i)
		tetros_rotate(&tetros_next_piece);

	tetros_draw_piece(-2, 3, tetros_curr_piece, true);
	tetros_draw_piece(-2, 3, tetros_next_piece, false);

	tetros_draw_piece(tetros_curr_x, tetros_curr_y, tetros_curr_piece, false);

	tetros_render();
}

void tetros_update()
{
	if (touching_bottom)
	{
		uint8_t last_row = 0;
		uint8_t num_rows = 0;

		for (uint8_t y = 0; y < 20; ++y)
		{
			uint8_t counter = 0;

			for (uint8_t x = 1; x < 17; ++x)
				if (tetros_map[y][x] != -1)
					counter++;

			if (counter == 16)
			{
				for (uint8_t x = 1; x < 17; ++x)
					tetros_map[y][x] = -1;

				num_rows++;

				if (y > last_row)
					last_row = y;

				tetros_score += 100;
				tetros_level++;
			}
		}

		for (uint8_t y = last_row; y > 1 + num_rows; --y)
			for (uint8_t x = 1; x < 17; ++x)
			{
				tetros_map[y][x] = tetros_map[y - num_rows][x];
				tetros_map[y - num_rows][x] = -1;
			}

		for (uint8_t y = 1 + num_rows; y > 0; --y)
			for (uint8_t x = 1; x < 17; ++x)
				tetros_map[y][x] = -1;

		touching_bottom = false;
	}

	tetros_move_piece(DOWN);
}

void tetros_draw_block(char _x, char _y, char _color)
{
	TETROS_COLOR color = _color < 0
					   ? (TETROS_COLOR) { 0, 0, 0 }
					   : tetros_colors[(uint8_t) _color];
	uint16_t x = (_x >= 0 ? _x + 7 : - _x) * 10;
	uint16_t y = _y * 10;

	vga_draw_fill_rect(x + 1, y + 1, x + 8, y + 8, color.normal);

	vga_draw_line(x, y, x + 8, y, color.light);
	vga_draw_line(x, y, x, y + 8, color.light);

	vga_draw_line(x + 9, y, x + 9, y + 9, color.dark);
	vga_draw_line(x, y + 9, x + 9, y + 9, color.dark);
}

void tetros_draw_piece(char _x, uint8_t _y, TETROS_PIECE piece, bool clear)
{
	uint8_t j = tetros_get_piece_top(piece);

	for (uint8_t x = 0; x < 4; ++x)
		for (uint8_t y = 0; y < 4 - j; ++y)
			if (piece.blocks[y + j][x])
			{
				if (_x < -1)
					tetros_draw_block(
						_x - x,
						_y + y,
						clear ? -1 : piece.color
					);
				else
					tetros_map[_y + y][_x + x] = clear ? -1 : piece.color;
			}
}

void tetros_render()
{
	for (uint8_t i = 0; i < 20; ++i)
		for (uint8_t j = 0; j < 18; ++j)
			tetros_draw_block(j, i, tetros_map[i][j]);
}

void tetros_play()
{
	tetros_init();

	while (tetros_playing)
	{
		asm volatile("hlt");

		if (sound_enabled() && music_playing)
		{
			music_tick();
			sound_tick();
		}

		if (
			!tetros_paused &&
			!tetros_game_over &&
			tetros_frame_counter++ >= 1000 - tetros_level * 10
		)
		{
			tetros_update();
			tetros_render();

			tetros_frame_counter = 0;
		}
		else if (tetros_game_over)
		{
			sound_set_enabled(false);
			speaker_pause();

			char game_over_string[11][15] =
			{
				{ -1, 07, 07, -1, -1, 07, -1, -1, 07, -1, 07, -1, 07, 07, 07 },
				{ 07, -1, -1, -1, 07, -1, 07, -1, 07, 07, 07, -1, 06, -1, -1 },
				{ 07, -1, -1, -1, 07, 02, 02, -1, 07, -1, 07, -1, 06, 06, 07 },
				{ 07, -1, 07, -1, 07, -1, 02, -1, 07, -1, 07, -1, 06, -1, -1 },
				{ -1, 07, 07, -1, 07, -1, 02, -1, 07, -1, 07, -1, 07, 07, 07 },
				{ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 },
				{ -1, 07, -1, -1, 05, -1, 07, -1, 07, 07, 07, -1, 07, 07, -1 },
				{ 07, -1, 07, -1, 05, -1, 07, -1, 07, -1, -1, -1, 07, -1, 07 },
				{ 07, -1, 07, -1, 05, -1, 07, -1, 07, 07, 07, -1, 07, 07, -1 },
				{ 07, -1, 07, -1, 05, -1, 07, -1, 04, -1, -1, -1, 07, -1, 07 },
				{ -1, 07, -1, -1, -1, 07, -1, -1, 04, 04, 04, -1, 07, -1, 07 },
			};

			vga_clear();

			for (uint8_t y = 0; y < 21; ++y)
				for (uint8_t x = 0; x < 18; ++x)
					tetros_map[y][x] = -1;

			for (uint8_t y = 0; y < 11; ++y)
				for (uint8_t x = 0; x < 15; ++x)
					tetros_map[y + 2][x + 1] = game_over_string[y][x];

			tetros_render();

			vga_set_fgcolor(WHITE);

			vga_set_index(vga_get_pos(15 - digits(tetros_score) / 2, 18));
			printf("SCORE: %d", tetros_score);

			usleep(1500);

			tetros_playing = false;

			break;
		}
	}

	sound_set_enabled(false);
	speaker_pause();

	init_vga(80, 25);
}

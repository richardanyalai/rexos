#include "sound.h"
#include <string.h>

static bool sound_is_enabled = false;

static uint8_t volume_master;
static uint8_t volumes[NUM_NOTES];
static uint8_t notes[NUM_NOTES];
static uint8_t waves[NUM_NOTES];

bool sound_enabled()
{ return sound_is_enabled; }

void sound_set_enabled(bool enabled)
{ sound_is_enabled = enabled; }

void sound_note(uint8_t index, uint8_t octave, uint8_t note)
{ notes[index] = (octave << 4) | note; }

uint8_t sound_get_note(uint8_t index)
{ return notes[index]; }

void sound_volume(uint8_t index, uint8_t v)
{ volumes[index] = v; }

uint8_t sound_get_volume(uint8_t index)
{ return volumes[index]; }

void sound_master(uint8_t v)
{ volume_master = v; }

uint8_t sound_get_master()
{ return volume_master; }

void sound_wave(uint8_t index, uint8_t wave)
{ waves[index] = wave; }

uint8_t sound_get_wave(uint8_t index)
{ return waves[index]; }

void sound_init()
{
	extern void sound_init_device();
	sound_init_device();

	memset(&notes, NOTE_NONE, sizeof(notes));
	memset(&waves, WAVE_SIN, sizeof(waves));
}

void sound_tick()
{
	extern void sound_tick_device();
	sound_tick_device();
}

/* This file is part of RexOS!.
 *
 * RexOS! is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * RexOS! is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with RexOS!.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <drivers/keyboard.h>
#include <drivers/VGA.h>
#include <games/common.h>
#include <gui/text_mode.h>
#include <programs/common.h>
#include <sys/isr.h>
#include <sys/rtc.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

uint16_t
	brkt_score,
	brkt_prev_ball_pos,
	brkt_ball_pos,
	brkt_timer,
	brkt_blocks[1050];

char
	brkt_dir,
	brkt_speed_x,
	brkt_speed_y;

uint8_t
	brkt_lives,
	brkt_old_char,
	brkt_racket[16];

bool
	brkt_running,
	brkt_paused,
	brkt_is_ball_moving;

// Updates score on screen
void brkt_write_score()
{
	vga_set_index(32 - digits(brkt_score));

	printf("Lives: ");

	for (uint8_t i = 0; i < brkt_lives; ++i)
		printf("${B00}${F04}%c${F15}", 3);

	for (uint8_t i = 0; i < 3 - brkt_lives; ++i)
		putchar(' ');

	printf(" Score: %d", brkt_score);
}

// Draw the brkt_blocks
void brkt_draw_brkt_blocks()
{
	uint8_t x = 5, y = 3;

	for (uint16_t i = 0; i < 1050; ++i)
	{
		vga_draw_char_at(' ', vga_get_pos(x, y), brkt_blocks[i], BLACK);

		if (x++ == 74)
		{
			x = 5;
			y++;
		}
	}
}

// Stops the game
void brkt_pause()
{
	if ((brkt_paused = !brkt_paused))
		show_pause();
	else
		brkt_draw_brkt_blocks();
}

// Handle keyboard events
void brkt_handle_keyboard(active_keys_t *active_keys)
{
	uint8_t (*key_to_char)(uint8_t) = lambda (uint8_t, (uint8_t key_code)
	{
		switch (key_code)
		{
			case KEY_W:		return 'w';
			case KEY_A:		return 'a';
			case KEY_D:		return 'd';
			case KEY_K:		return 'w';
			case KEY_H:		return 'a';
			case KEY_L:		return 'd';
			case KEY_UP:	return 'w';
			case KEY_LEFT:	return 'a';
			case KEY_RIGHT:	return 'd';
			case KEY_SPACE:	return ' ';
			case KEY_Q:		return 'q';
			case KEY_E:		return 'e';
			default:		return  0 ;
		}
	});

	key_t active_key = active_keys->active_key;
	bool pressed = active_key.pressed;
	uint8_t char_code = active_key.char_code;
	uint8_t c = key_to_char(active_key.key_code);

	if (brkt_old_char == 'Z' && char_code == 'Q')
	{
		brkt_running = false;
		return;
	}

	if (!brkt_paused)
	{
		if (c == 'a' || c == 'd')
		{
			if (pressed)
				brkt_dir = ((c == 'a') ? -1 : 1);
			else
				brkt_dir = 0;
		}
		else if (c == 'w' && pressed)
			brkt_is_ball_moving = true;
	}
	else if (brkt_paused && (c == 'q' || c == 'e'))
		brkt_running = false;

	if (c == ' ' && pressed)
		brkt_pause();

	if (char_code)
	{
		if (pressed)
			brkt_old_char = char_code;

		null_active();
	}
}

// Initializes the game
void brkt_init()
{
	set_keyboard_handler(brkt_handle_keyboard);

	vga_set_bgcolor(BLACK);
	vga_set_fgcolor(WHITE);

	brkt_running = true;
	brkt_paused = false;

	brkt_timer = brkt_score = brkt_dir = brkt_old_char = 0;
	brkt_lives = 3;

	uint8_t j = 31, i = 0;

	while (i++ < 16)
		brkt_racket[i-1] = j + i;

	uint8_t colors[5] = { BLUE, BRIGHT_BLUE, CYAN, BRIGHT_CYAN, BRIGHT_GREEN };

	uint8_t y = 0;

	for (i = 0; i < 5; ++i)
		for (j = 0; j < 3; ++j, ++y)
			for (uint8_t x = 0; x < 70; ++x)
				brkt_blocks[y * 70 + x] = colors[i];

	brkt_prev_ball_pos = brkt_ball_pos = vga_get_pos(brkt_racket[8], 22);
	
	textmode_draw_rect(20, 0, 55, 1, vga_get_bgcolor());
	draw_borders(vga_get_bgcolor(), vga_get_fgcolor());
	brkt_draw_brkt_blocks();
	brkt_write_score();

	brkt_speed_x = -1;
	brkt_speed_y = -1;

	brkt_is_ball_moving = false;
}

/**
 * @brief			Checks if the ball collides with a brick
 * @param newBallX	is the new X coordinate of the ball
 * @param newBallY	is the new Y coordinate of the ball
 * @return			true (1) when the ball collides with a brick,
 *					false (0) otherwise
 */
bool check_collision(uint8_t newBallX, uint8_t newBallY)
{
	uint8_t x = 5, y = 3;

	for (uint16_t i = 0; i < 1050; ++i)
	{
		uint16_t pos = y * 80 + x;

		if (brkt_blocks[i] != BLACK)
		{
			/**
			 * @brief			Checks if the virtual ball collides with a brick
			 * @param ballX		is the X coordinate of the virtual ball
			 * @param ballY		is the Y coordinate of the virtual ball
			 * @return			true (1) when the ball collides with a brick,
			 *					false (0) otherwise
			 */
			bool (*collision)(uint16_t, uint16_t) =
			lambda (bool, (uint16_t ballX, uint16_t ballY) {
				bool collides = false;
				if (vga_get_pos(ballX, ballY) == pos)
				{
					if (ballX == newBallX)
						brkt_speed_x *= -1;

					if (ballY == newBallY)
						brkt_speed_y *= -1;
					
					collides = true;

					brkt_blocks[i] = BLACK;
					vga_draw_char_at(' ', pos, brkt_blocks[i],  brkt_blocks[i]);
					
					if (x < 74)
					{
						brkt_blocks[i+1] = BLACK;
						vga_draw_char_at(
							' ', pos+1, brkt_blocks[i],  brkt_blocks[i]
						);
					}
					
					if (x > 5)
					{
						brkt_blocks[i-1] = BLACK;
						vga_draw_char_at(
							' ', pos-1, brkt_blocks[i],  brkt_blocks[i]
						);
					}

					brkt_score += 18 - y;
					brkt_write_score();
				}
				return collides;
			});

			if (
				collision(
					newBallX,
					vga_get_y(brkt_ball_pos)
				) || collision(vga_get_x(brkt_ball_pos), newBallY))
				return true;
		}

		if (x == 74)
		{
			x = 5;
			y++;
		}
		else
			x++;
	}

	return false;
}

// Updates game variables
void brkt_update()
{
	brkt_prev_ball_pos = brkt_ball_pos;

	if (brkt_is_ball_moving)
	{
		if (brkt_timer % 2 == 0)
		{
			uint8_t newBallX = vga_get_x(brkt_ball_pos) + brkt_speed_x;
			uint8_t newBallY = vga_get_y(brkt_ball_pos) + brkt_speed_y;

			if (!check_collision(newBallX, newBallY))
			{
				bool collidesWithPlayer = false;

				for (uint8_t i = 0; i < 16; ++i)
					if (
						vga_get_pos(newBallX, newBallY) ==
						vga_get_pos(brkt_racket[i], 23)
					)
					{
						collidesWithPlayer = true;
						brkt_speed_y *= -1;
					}

				if (newBallX <= 0 || newBallX >= 79)
					brkt_speed_x *= -1;

				if (newBallY <= 1)
					brkt_speed_y *= -1;

				if (!collidesWithPlayer && newBallY >= 23)
				{
					brkt_is_ball_moving = false;
					brkt_speed_x = -1;
					brkt_speed_y = -1;
					brkt_ball_pos = vga_get_pos(brkt_racket[8], 22);
					brkt_lives--;

					brkt_write_score();

					if (brkt_lives == 0)
						brkt_init();
				}
			}

			brkt_ball_pos = vga_get_pos(
				(vga_get_x(brkt_ball_pos) + brkt_speed_x),
				(vga_get_y(brkt_ball_pos) + brkt_speed_y)
			);
		}
	}
	else
		brkt_ball_pos = vga_get_pos(brkt_racket[8], 22);

	if (
		(brkt_racket[0] + brkt_dir > 0) && (brkt_racket[15] + brkt_dir < 79)
	)
		for (uint8_t i = 0; i < 16; ++i)
			brkt_racket[i] += brkt_dir;
}

// Renders the game screen
void brkt_render()
{
	vga_draw_char_at(
		' ', brkt_prev_ball_pos, vga_get_bgcolor(), vga_get_bgcolor()
	);

	vga_draw_char_at(254, brkt_ball_pos, vga_get_bgcolor(), BRIGHT_RED);
	
	if (brkt_racket[0] > 1)
		vga_draw_char_at(
			' ',
			vga_get_pos(brkt_racket[0]-1, 23),
			vga_get_bgcolor(),
			vga_get_bgcolor()
		);
	if (brkt_racket[15] < 78)
		vga_draw_char_at(
			' ',
			vga_get_pos(brkt_racket[15] + 1, 23),
			vga_get_bgcolor(),
			vga_get_bgcolor()
		);

	for (uint8_t i = 0; i < 16; ++i)
		vga_draw_char_at(
			223,
			vga_get_pos(brkt_racket[i], 23),
			vga_get_bgcolor(),
			vga_get_fgcolor()
		);
}

void brkt_play()
{
	brkt_init();

	register_rtc_func(RTC_CLOCK_WIDGET, update_clock_widget);

	while (brkt_running)
	{
		if (!brkt_paused)
		{
			brkt_timer++;
			brkt_update();
			brkt_render();
			usleep(30);
		}

		HALT;
	}
}

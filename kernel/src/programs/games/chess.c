/* This file is part of RexOS!.
 *
 * RexOS! is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * RexOS! is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with RexOS!.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <drivers/keyboard.h>
#include <drivers/VGA.h>
#include <gui/graphics_mode.h>
#include <math.h>
#include <net/IPv4.h>
#include <net/UDP.hpp>
#include <sys/isr.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#define forx		for (uint8_t x = 0; x < 8; ++x)
#define fory		for (uint8_t y = 0; y < 8; ++y)
#define foryx		fory forx

#define TILE_SIZE	23

#define OFFSET_X	16

#define BLACK_TILE	24
#define WHITE_TILE	27

extern uint16_t gateway_IP;
extern bool am79c973_ready;

typedef struct { uint8_t color, type; } piece_t;

static bool chess_running, players_turn;
static char board[8][8] =
{
	{-2, -3, -4, -6, -5, -4, -3, -2 },
	{-1, -1, -1, -1, -1, -1, -1, -1 },
	{ 0,  0,  0,  0,  0,  0,  0,  0 },
	{ 0,  0,  0,  0,  0,  0,  0,  0 },
	{ 0,  0,  0,  0,  0,  0,  0,  0 },
	{ 0,  0,  0,  0,  0,  0,  0,  0 },
	{ 1,  1,  1,  1,  1,  1,  1,  1 },
	{ 2,  3,  4,  6,  5,  4,  3,  2 }
};
static const uint16_t chess_pieces[] =
{
	// Pawn
	0b000000000000,
	0b000000000000,
	0b000000000000,
	0b000001100000,
	0b000111111000,
	0b001111111100,
	0b001111111100,
	0b000111111000,
	0b000111111000,
	0b001111111100,
	0b001111111100,
	0b000111111000,
	0b001111111100,
	0b011111111110,
	0b111111111111,
	0b000000000000,

	// Rook
	0b000000000000,
	0b000000000000,
	0b000000000000,
	0b011001100110,
	0b011001100110,
	0b001111111100,
	0b000111111000,
	0b000111111000,
	0b000111111000,
	0b000111111000,
	0b000111111000,
	0b000111111000,
	0b001111111100,
	0b011111111110,
	0b111111111111,
	0b000000000000,

	// Knight
	0b000000000000,
	0b000000000000,
	0b000000011000,
	0b000011111110,
	0b011111001110,
	0b011111111111,
	0b000000111111,
	0b000001111111,
	0b000011111111,
	0b000111111111,
	0b000111111100,
	0b000111111000,
	0b001111111100,
	0b011111111110,
	0b111111111111,
	0b000000000000,

	// Bishop
	0b000001100000,
	0b000111110000,
	0b001111101100,
	0b001111011100,
	0b000111111000,
	0b000111111000,
	0b000111111000,
	0b001111111100,
	0b011111111110,
	0b011111111110,
	0b001111111100,
	0b000111111000,
	0b000111111000,
	0b001111111100,
	0b011111111110,
	0b111111111111,

	// Queen
	0b001101101100,
	0b000111111000,
	0b001111111100,
	0b001111111100,
	0b000111111000,
	0b000111111000,
	0b000111111000,
	0b001111111100,
	0b011111111110,
	0b011111111110,
	0b001111111100,
	0b000111111000,
	0b000111111000,
	0b001111111100,
	0b011111111110,
	0b111111111111,

	// King
	0b001101101100,
	0b001111111100,
	0b001111111100,
	0b001111111100,
	0b000111111000,
	0b000111111000,
	0b000111111000,
	0b001111111100,
	0b011111111110,
	0b001111111100,
	0b000111111000,
	0b000111111000,
	0b000111111000,
	0b001111111100,
	0b011111111110,
	0b111111111111,
};
static uint8_t chess_player1, chess_player2, chess_move_str[5] = {0};
static piece_t pieces[8][8];

void chess_draw_piece(
	uint8_t board_x, uint8_t board_y, const uint8_t piece, uint8_t color
)
{
	for (uint8_t y = 0; y < 16; ++y)
		for (uint8_t x = 0; x < 16; ++x)
			if ((chess_pieces[(piece * 16) + y] >> (15 - x) & 1))
				vga_draw_pixel(
					OFFSET_X + board_x * TILE_SIZE + x + 2,
					board_y * TILE_SIZE + y + 4,
					color
				);
}

void chess_draw_tile(uint8_t x, uint8_t y, uint8_t color)
{
	vga_draw_fill_rect(
		OFFSET_X + x * TILE_SIZE,
		y * TILE_SIZE,
		OFFSET_X +  (x + 1) * TILE_SIZE,
		(y + 1) * TILE_SIZE,
		color
	);
}

bool chess_rook_move(
	uint8_t from_x, uint8_t from_y, uint8_t to_x, uint8_t to_y
)
{
	if ((to_x != from_x && to_y != from_y))
		return false;

	uint8_t	dist_h = abs(to_x - from_x),
			dist_v = abs(to_y - from_y);

	if (dist_h)
	{
		char step = (from_x < to_x) ? 1 : -1;

		for (uint8_t i = 1; i < dist_h; ++i)
			if (pieces[to_y][from_x + i * step].type)
				return false;
	}
	else
	{
		char step = (from_y < to_y) ? 1 : -1;

		for (uint8_t i = 1; i < dist_v; ++i)
			if (pieces[from_y + i * step][to_x].type)
				return false;
	}

	return true;
}

bool chess_bishop_move(
	uint8_t from_x, uint8_t from_y, uint8_t to_x, uint8_t to_y
)
{
	uint8_t	dist_h	= abs(to_x - from_x);
	char	step_x	= (from_x < to_x) ? 1 : -1,
			step_y	= (from_y < to_y) ? 1 : -1;

	if (dist_h != abs(to_y - from_y))
		return false;

	for (uint8_t i = 1; i < dist_h; ++i)
		if (pieces[from_y + i * step_y][from_x + i * step_x].type)
			return false;

	return true;
}

bool chess_move(uint8_t _from_x, uint8_t _from_y, uint8_t _to_x, uint8_t _to_y)
{
	uint8_t from_x		= _from_x - 1;
	uint8_t from_y		= 7 - (_from_y - 1);
	uint8_t to_x		= _to_x - 1;
	uint8_t to_y		= 7 - (_to_y - 1);

	uint8_t type_from	= pieces[from_y][from_x].type;
	uint8_t type_to		= pieces[to_y][to_x].type;
	uint8_t color_from	= pieces[from_y][from_x].color;
	uint8_t color_to	= pieces[to_y][to_x].color;

	bool valid_move = false;

	if (type_from)
	{
		if (
			(
				type_to		>	0			&&
				color_to	==	color_from
			)								||
			players_turn	!=	color_from	||
			type_to			==	6
		)
			return false;

		switch (type_from)
		{
			// Pawn
			case 1:
				if (abs(to_y - from_y) <= 2)
				{
					uint8_t y1 = from_y, y2 = to_y;

					if (color_from)
					{
						y1 = 7 - from_y;
						y2 = 7 - to_y;
					}

					if (from_x == to_x)
					{
						if (
							(
								y1 == 1						&&
								!pieces[y2-1][to_x].type	&&
								abs(to_y - from_y) == 2
							)								||
								abs(to_y - from_y) == 1
						)
							valid_move = true;
					}
					else if (
						abs(to_x - from_x) == 1	&&
						abs(to_y - from_y) == 1	&&
						type_to > 0				&&
						type_to < 6
					)
						valid_move = true;
				}
				break;

			// Rook
			case 2:
				valid_move = chess_rook_move(from_x, from_y, to_x, to_y);
				break;

			// Knight
			case 3:
				valid_move =
				(abs(to_x - from_x) == 1 && abs(to_y - from_y) == 2) ||
				(abs(to_y - from_y) == 1 && abs(to_x - from_x) == 2);
				break;

			// Bishop
			case 4:
				valid_move = chess_bishop_move(from_x, from_y, to_x, to_y);
				break;

			// Queen
			case 5:
				valid_move =	chess_rook_move(from_x, from_y, to_x, to_y) ||
								chess_bishop_move(from_x, from_y, to_x, to_y);
				break;

			// King
			case 6:
				valid_move = abs(to_x - from_x) <= 1 && abs(to_y - from_y) <= 1;
				break;
			
			default: return false;
		}

		if (!valid_move)
			return false;

		if (type_to)
		{
			if (players_turn)
				chess_player1 += type_to;
			else
				chess_player2 += type_to;
		}

		chess_draw_tile(
			from_x,
			from_y,
			(
				(from_x % 2 == 0 && from_y % 2 == 0) ||
				(from_x % 2 != 0 && from_y % 2 != 0)
			) ? WHITE_TILE : BLACK_TILE
		);
		chess_draw_tile(
			to_x,
			to_y,
			(
				(to_x % 2 == 0 && to_y % 2 == 0) ||
				(to_x % 2 != 0 && to_y % 2 != 0)
			) ? WHITE_TILE : BLACK_TILE
		);

		chess_draw_piece(to_x, to_y, type_from - 1, color_from ? 15 : 0);

		pieces[to_y][to_x] = pieces[from_y][from_x];
		pieces[from_y][from_x].type = 0;

		vga_draw_string(
			26,
			15,
			(players_turn = !players_turn) ? "Player1s turn" : "Player2s turn",
			WHITE,
			BLACK
		);

		vga_set_index(vga_get_pos(30, 10));
		printf("%d : %d", chess_player1, chess_player2);
	}

	return valid_move;
}

void chess_init();

bool chess_handler(char *move)
{
	uint8_t x1 = 0, from_y, x2 = 0, to_y;

	if (BETWEEN(move[0], 'A', 'H'))
		x1 = move[0] - 64;
	else if (BETWEEN(move[0], 'a', 'h'))
		x1 = move[0] - 96;

	if (BETWEEN(move[2], 'A', 'H'))
		x2 = move[2] - 64;
	else if (BETWEEN(move[2], 'a', 'h'))
		x2 = move[2] - 96;

	from_y	= move[1] - 48;
	to_y	= move[3] - 48;

	return chess_move(x1, from_y, x2, to_y);
}

void chess_keyboard_handler(active_keys_t *active_keys)
{
	uint8_t k = active_keys->active_key.key_code;

	if (k != 0 && active_keys->active_key.pressed)
	{
		uint8_t ch = active_keys->active_key.char_code;
		uint8_t move_length = strlen((char *) chess_move_str) - 1;

		switch (k)
		{
			case KEY_Q:
				chess_running = false;
				break;

			case KEY_R:
				chess_init();
				break;

			case KEY_A:
			case KEY_B:
			case KEY_C:
			case KEY_D:
			case KEY_E:
			case KEY_F:
			case KEY_G:
			case KEY_H:
				if (move_length % 2 == 0 && move_length < 4)
					chess_move_str[move_length] = ch;
				break;

			case KEY_NUM_1:
			case KEY_NUM_2:
			case KEY_NUM_3:
			case KEY_NUM_4:
			case KEY_NUM_5:
			case KEY_NUM_6:
			case KEY_NUM_7:
			case KEY_NUM_8:
			case KEY_1:
			case KEY_2:
			case KEY_3:
			case KEY_4:
			case KEY_5:
			case KEY_6:
			case KEY_7:
			case KEY_8:
				if (move_length % 2 == 1 && move_length < 4)
					chess_move_str[move_length] = ch;
				break;

			case KEY_ENTER:
					if (
						move_length == 4				&&
						chess_handler((char *) chess_move_str)	&&
						am79c973_ready
					)
					{
						chess_move_str[4] = '\n';

						UDP_send((uint8_t *) chess_move_str, 5);
					}

					chess_move_str[0] = '\0';
					chess_move_str[1] = '\0';
					chess_move_str[2] = '\0';
					chess_move_str[3] = '\0';
					chess_move_str[4] = '\0';
				break;

			case KEY_BKSPACE:
				if (move_length > 0)
					chess_move_str[move_length-1] = '\0';
				break;
			
			default:
				break;
		}

		vga_set_index(vga_get_pos(30, 21));
		printf("%s    ", chess_move_str);

		null_active();
	}
}

void chess_init()
{
	init_vga(320, 200);

	vga_draw_fill_rect(0, 0, VGA_WIDTH, VGA_HEIGHT, WHITE);

	set_keyboard_handler(&chess_keyboard_handler);

	uint8_t IP[4]	= { 127, 0, 0, 1 };
	uint32_t IP_BE	= ARRAY_TO_IPv4(IP);

	if (am79c973_ready)
	{
		UDP_connect(IP_BE, 1234);
		UDP_set_handler(&chess_handler);
		UDP_send((uint8_t *) "Welcome to RexOS Chess!\n", 25);
	}

	chess_running	= true;
	players_turn	= true;
	chess_player1	= chess_player2 = 0;

	uint8_t c		= 1;

	foryx
	{
		c++;

		chess_draw_tile(
			x,
			y,
			(
				(x % 2 == 0 && y % 2 == 0) ||
				(x % 2 != 0 && y % 2 != 0)
			) ? WHITE_TILE : BLACK_TILE
		);

		int8_t piece = board[y][x];

		pieces[y][x] = (piece_t) {
			.color	 = (piece > 0) ? 1 : 0,
			.type	 = abs(piece)
		};
		
		if (abs(piece))
			chess_draw_piece(x, y, abs(piece) - 1, (piece > 0) ? 15 : 0);
	}

	void (*draw_char)(uint8_t, uint8_t, uint8_t, bool) = lambda(void, (
		uint8_t _x, uint8_t _y, uint8_t ch, bool is_x
	)
	{
		extern uint8_t vga_graphics_mode_font[2048];

		for (uint8_t y = 0; y < 8; ++y)
			for (uint8_t x = 0; x < 8; ++x)
				if ((vga_graphics_mode_font[ch * 8 + y] >> (7 - x) & 1))
					vga_draw_pixel(
						_x * TILE_SIZE + x + (is_x ? 0 : 4),
						_y * TILE_SIZE + y + (is_x ? 4 : 8),
						GREY
					);
	});

	forx
		draw_char(x + 1, 8, 'A' + x, true);

	fory
		draw_char(0, y, '8' - y, false);

	vga_set_bgcolor(WHITE);
	vga_set_fgcolor(BLACK);

	vga_draw_string(27, 1, "RexOS Chess", WHITE, BLACK);
	vga_draw_string(30, 10, "0 : 0", WHITE, BLACK);
	vga_draw_string(26, 15, "Player1s turn", WHITE, BLACK);

	vga_draw_line(OFFSET_X + 8 * TILE_SIZE + 4, 180, VGA_WIDTH - 5, 180, 0);
}

void chess_play()
{
	chess_init();

	while (chess_running)
		HALT;

	if (am79c973_ready)
		UDP_disconnect();

	init_vga(80, 25);
}

/* This file is part of RexOS!.
 *
 * RexOS! is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * RexOS! is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with RexOS!.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <drivers/fs/fs.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char *strstr2(const char *, const char *);

char *ogrep(char *exp, char *file, bool color, bool ins)
{
	uint32_t i = 0;
	struct dirent *node = 0;
	bool file_found = false;
	char *ret = 0;

	while ((node = readdir_fs(fs_root, i)) != 0)
	{
		if (strcmp(node->name, file) == 0)
		{
			file_found = true;
			fs_node_t *fsnode = finddir_fs(fs_root, node->name);
			char *buf = malloc(sizeof(uint8_t) * 5000);
			uint32_t sz = read_fs(fsnode, 0, 5000, buf);
			buf[sz] = '\0';
			
			ret = malloc(sizeof(uint8_t) * 3000);

			bool not_first = false;

			uint16_t j = 0;
			
			while (j < sz)
			{
				uint16_t k = 0;

				char *line = malloc(sizeof(uint8_t) * 1000);

				for (uint16_t m = 0; m < 1000; line[m++] = '\0');
				
				while (buf[j] != '\n' && buf[j] != '\0')
					line[k++] = buf[j++];

				j++;

				char *(*search)(char *, char *) =
				lambda(char *, (char *str1, char *str2)
				{
					if (ins)
						return (char *) strstr2(str1, str2);
					else
						return (char *) strstr(str1, str2);
				});

				if (search(line, exp))
				{
					if (not_first)
						charcat(ret, '\n');

					char * new_line = malloc(sizeof(uint8_t) * 1000);
					for (uint16_t m = 0; m < 1000; new_line[m++] = '\0');

					uint16_t pos = 0, prev_pos = 0;
					char *p = line;

					for (; (p = search(p, exp)) != 0; ++p)
					{
						pos = p - line;

						for (uint16_t m = prev_pos; m < pos; ++m)
							charcat(new_line, line[m]);

						if (color)
							strcat(new_line, "${F12}");
						
						for (uint16_t m = pos; m < pos + strlen(exp) - 1; ++m)
							charcat(new_line, line[m]);
						
						if (color)
							strcat(new_line, "${F15}");

						prev_pos = pos + strlen(exp) - 1;

						if (*p == '\0')
							break;
					}

					for (uint16_t m = prev_pos; line[m] != 0; ++m)
						charcat(new_line, line[m]);

					if (not_first)
						strcat(ret, new_line);
					else
						strcpy(ret, new_line);

					free(new_line);

					not_first = true;
				}

				free(line);
			}

			break;
		}
		i++;
	}

	if (!file_found)
		printf("ogrep: \"%s\": No such file or directory", file);

	return ret;
}

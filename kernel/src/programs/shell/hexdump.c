/* This file is part of RexOS!.
 *
 * RexOS! is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * RexOS! is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with RexOS!.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <drivers/keyboard.h>
#include <drivers/VGA.h>
#include <limits.h>
#include <sys/isr.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define BUF_LEN 16

void hexdump(char *input, bool addr)
{
	uint8_t bg_bak = vga_get_bgcolor();
	uint8_t fg_bak = vga_get_fgcolor();

	vga_set_bgcolor(BLACK);
	vga_set_fgcolor(GREEN);

	uint32_t input_len = addr? INT_MAX : strlen(input);

	uint32_t address = addr ? (uint32_t) input : 0;

	bool running = true;

	for (uint32_t l = 0; l < input_len && running; l += BUF_LEN)
	{
		bool not_empty = false;
		char buf[BUF_LEN];

		for (uint8_t i = 0; i < BUF_LEN; buf[i++] = '\0');

		uint8_t step = BUF_LEN;

		if (l + BUF_LEN > input_len)
		{
			if (input_len < BUF_LEN)
			{
				l = 0;
				step = input_len;
			}
			else
				step = abs(input_len - l);
		}

		for (
			uint8_t i = 0;
			i < step;
			buf[i] = input[l+i],
			not_empty = input[l+i] ? true : not_empty,
			++i
		);

		if (not_empty)
		{
			if (not_empty)
				vga_set_fgcolor(YELLOW);

			printf("\n%.8x", address);

			vga_set_fgcolor(GREEN);

			for (uint8_t i = 0; i < BUF_LEN; ++i)
			{
				if (i % 8 == 0 )
					putchar(' ');

				if (buf[i] != 0)
					vga_set_fgcolor(BRIGHT_CYAN);

				if (i < BUF_LEN )
					printf(" %.2x", (uint8_t) buf[i]);
				else
					printf("   ");

				vga_set_fgcolor(GREEN);
			}

			printf("  |");

			for (uint8_t i = 0; i < BUF_LEN; ++i)
			{
				if (buf[i] != 0)
					vga_set_fgcolor(WHITE);

				if (buf[i] < 32)
					putchar('.');
				else
					putchar(buf[i]);

				vga_set_fgcolor(GREEN);

				if (addr)
				{
					bool next = false;

					set_keyboard_handler(lambda(void,
					(active_keys_t *active_keys) {
						key_t active_key = active_keys->active_key;

						if (
							active_key.pressed &&
							active_key.key_code == KEY_SPACE
						)
								next = true;

						if (active_key.key_code == KEY_C && active_keys->ctrl)
						{
							running = false;
							next = true;
						}

						null_active();
					}));

					if (!buf[i] && buf[i-1])
						while (!next)
							HALT;

					if (!running)
					{
						vga_set_bgcolor(bg_bak);
						vga_set_fgcolor(fg_bak);

						return;
					}
				}
			}

			putchar('|');
		}

		address += BUF_LEN;
	}

	vga_set_bgcolor(bg_bak);
	vga_set_fgcolor(fg_bak);
}

/* This file is part of RexOS!.
 *
 * RexOS! is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * RexOS! is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with RexOS!.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <drivers/VGA.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>

char *cow =
"    \\   ^__^            \n"
"     \\  (${F03}oo${CLC})\\_______    \n"
"        (__)\\       )\\/\\\n"
"            ||----${F13}w${CLC} |   \n"
"            ||     ||   ";

char *sodomized =
"      \\                _   \n"
"       \\              (_)  \n"
"        \\   ^__^       /\\ \n"
"         \\  (${F03}oo${CLC})\\_____/_\\ \\\n"
"            (__)\\       ) /\n"
"                ||----${F13}w${CLC} (( \n"
"                ||     ||>>";

void cowsay(char *str)
{
	puts("");

	uint8_t length = strlen(str);

	putchar(218);
	for (uint8_t i = 1; i++ < length + 2; putchar(196));
	putchar(191);

	printf("\n%c %s %c\n", 179, str, 179);

	putchar(192);
	for (uint8_t i = 1; i++ < length + 2; putchar(196));
	putchar(217);

	puts("");
	printf(cow);
}

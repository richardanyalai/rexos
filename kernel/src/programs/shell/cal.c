/* This file is part of RexOS!.
 *
 * RexOS! is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * RexOS! is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with RexOS!.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <drivers/VGA.h>
#include <sys/rtc.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>

extern const uint8_t month_days[12];

void cal() {
	char *month_names[12] = {
		"January",
		"February",
		"March",
		"April",
		"May",
		"June",
		"July",
		"August",
		"September",
		"Oktober",
		"November",
		"December"
	};

	uint8_t month = get_month();
	uint8_t day = get_day();
	char *month_name = month_names[month - 1];
	uint8_t offset = (20 - (strlen(month_name) + 5)) / 2 + 1;

	putchar('\n');

	for (uint8_t i = 0; i < offset; ++i)
		putchar(' ');

	printf("%s %d\nMo Tu We Th Fr Sa Su\n", month_name, get_year());

	uint8_t weekday_idx = day % (get_week_day() + 1);
	uint8_t dm = month_days[month - 1];

	for (uint8_t i = 0; i < weekday_idx; ++i)
		printf("   ");

	for (uint8_t i = 1; i < dm + 1; ++i)
	{
		printf(
			"%c%s%d${CLC}%c",
			i < 11 ? ' ' : 0,
			i == day ? "${B15}${F00}" : "",
			i,
			(i < dm + 1 && !(i < 10 && (i + 1) >= 10)) ? ' ' : 0
		);

		if (weekday_idx == 6)
		{
			weekday_idx = 0;

			putchar('\n');
		} else
			++weekday_idx;
	}
}

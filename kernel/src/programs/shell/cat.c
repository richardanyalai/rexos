/* This file is part of RexOS!.
 *
 * RexOS! is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * RexOS! is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with RexOS!.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <drivers/fs/fs.h>
#include <drivers/VGA.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void cat(char *file)
{
	uint8_t bg_bak = vga_get_bgcolor();
	uint8_t fg_bak = vga_get_fgcolor();

	uint32_t i = 0;
	struct dirent *node = 0;
	bool file_found = false;

	while ((node = readdir_fs(fs_root, i)) != 0)
	{
		if (strcmp(node->name, file) == 0)
		{
			file_found = true;
			fs_node_t *fsnode = finddir_fs(fs_root, node->name);

			char *buf = malloc(sizeof(char) * 2500);
			uint32_t sz = read_fs(fsnode, 0, 2500, buf);
			buf[sz] = 0;
				
			printf(buf);
			free(buf);

			break;
		}
		i++;
	}

	if (!file_found)
		printf("cat: \"%s\": No such file or directory", file);
	
	vga_set_bgcolor(bg_bak);
	vga_set_fgcolor(fg_bak);
}

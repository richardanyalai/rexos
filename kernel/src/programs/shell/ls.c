/* This file is part of RexOS!.
 *
 * RexOS! is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * RexOS! is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with RexOS!.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <drivers/fs/fs.h>
#include <stdio.h>
#include <string.h>

void ls(char *dir)
{
	uint8_t x = 0, longest = 0;
	uint32_t i = 0;
	struct dirent *node = 0;
	bool file_found = true;

	while ((node = readdir_fs(fs_root, i++)) != 0)
		longest = (strlen(node->name) > longest)
				? strlen(node->name) : longest;

	i = 0;

	while ((node = readdir_fs(fs_root, i++)) != 0)
	{
		x += strlen(node->name);

		if (x > 79)
		{
			puts("");
			x = 0;
		}

		if (x == 0)
			x += strlen(node->name);

		fs_node_t *fsnode = finddir_fs(fs_root, node->name);
		printf(
			"%s%s ",
			is_dir(fsnode) ? "${F09}" : "${F15}",
			node->name
		);
		
		for (
			uint8_t j = strlen(node->name);
			j < longest + 1 && x <= 79;
			++j, ++x
		)
			printf(" ");
	}

	if (!file_found)
		printf("ls: \"%s\": No such file or directory", dir);
}

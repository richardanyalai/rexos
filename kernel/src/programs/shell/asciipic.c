/* This file is part of RexOS!.
 *
 * RexOS! is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * RexOS! is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with RexOS!.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <drivers/fs/fs.h>
#include <drivers/VGA.h>
#include <programs/rexshell.h>
#include <settings.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/compile_time.h>
#include <sys/cpudet.h>
#include <sys/rtc.h>

extern uint32_t available_memory;

void display_ascii(const char *option)
{
	char *filename = malloc(sizeof(uint8_t) * strlen(option) + 5);
	for (uint8_t i = 0; i < strlen(filename); filename[i++] = '\0');

	strcpy(filename, option);
	strcat(filename, ".raa");

	cat(filename);

	free(filename);
}

void rexfetch()
{
	uint8_t fg_bak = vga_get_fgcolor();

	vga_set_fgcolor(YELLOW);

	puts("");
	display_ascii("keksz");

	vga_set_fgcolor(fg_bak);

	uint16_t index = vga_get_index();

	vga_set_index(vga_get_pos(19, (vga_get_y(vga_get_index()) - 8)));
	printf(
		"${F09}OS\t\t\t\t${F03}>${F15} ${F12}R$"
		"{F10}e${F11}x${F14}O${F13}S${F15}!"
	);
	vga_set_index(vga_get_pos(19, (vga_get_y(vga_get_index()) + 1)));
	printf(
		"${F09}Kernel version\t${F03}>${F15} %s.%d%d%d",
		VER,
		__TIME_MONTH__,
		__TIME_DAYS__,
		__TIME_HOURS__
	);
	vga_set_index(vga_get_pos(19, (vga_get_y(vga_get_index()) + 1)));
	printf(
		"${F09}Build time\t\t${F03}>${F15} %s, %s",
		__DATE__,
		__TIME__
	);
	vga_set_index(vga_get_pos(19, (vga_get_y(vga_get_index()) + 1)));
	printf(
		"${F09}Uptime\t\t\t${F03}>${F15} %s", get_uptime()
	);
	vga_set_index(vga_get_pos(19, (vga_get_y(vga_get_index()) + 1)));

	uint32_t ram = available_memory / (1024 * 1024) + 1;
	printf("${F09}Ram\t\t\t\t${F03}>${F15} %d MB", ram);

	if (strcmp(cpu_data.type, ""))
	{
		vga_set_index(vga_get_pos(19, (vga_get_y(vga_get_index()) + 1)));

		printf(
			"${F09}CPU type\t\t\t${F03}>${F15} %s",
			cpu_data.type
		);
	}

	if (strcmp(cpu_data.brand, ""))
	{
		vga_set_index(vga_get_pos(19, (vga_get_y(vga_get_index()) + 1)));

		printf(
			"${F09}Brand\t\t\t${F03}>${F15} %s",
			cpu_data.brand
		);
	}

	if (strcmp(cpu_data.family, ""))
	{
		vga_set_index(vga_get_pos(19, (vga_get_y(vga_get_index()) + 1)));

		printf(
			"${F09}Family\t\t\t${F03}>${F15} %s",
			cpu_data.family
		);
	}

	if (strcmp(cpu_data.model, ""))
	{
		vga_set_index(vga_get_pos(19, (vga_get_y(vga_get_index()) + 1)));

		printf(
			"${F09}Model\t\t\t${F03}>${F15} %s",
			cpu_data.model
		);
	}

	vga_set_index(index);
}

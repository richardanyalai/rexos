/* This file is part of RexOS!.
 *
 * RexOS! is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * RexOS! is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with RexOS!.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <drivers/keyboard.h>
#include <drivers/fs/fs.h>
#include <drivers/PCI.h>
#include <drivers/pcspkr.h>
#include <drivers/VGA.h>
#include <games/common.h>
#include <net/ICMP.h>
#include <net/IPv4.h>
#include <net/TCP.hpp>
#include <net/UDP.hpp>
#include <programs/common.h>
#include <programs/rexshell.h>
#include <sys/isr.h>
#include <sys/rtc.h>
#include <utils.h>
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>

extern uint32_t gateway_IP;
extern bool am79c973_ready, terminal_running;
extern int16_t terminal_index, command_index;
extern char *terminal_buffer, *command, *line;
extern void
	clear_screen(),
	new_line(uint8_t),
	move_cursor(uint16_t),
	tty_kbrd_handler(active_keys_t *active_keys),
	interpret(char *input);

// Converts a string to an IP address.
uint32_t IP_from_string(char *str)
{
	uint8_t counter = 0, IP[4];
	char *buf = malloc(4 * sizeof(char));

	while (*str != '\0' && counter < 4)
	{
		for (uint8_t i = 0; i < 4; buf[i++] = '\0');

		uint8_t i = 0;

		while (*str != '.' && *str != '\0')
		{
			buf[i++] = *str;
			(void) *str++;
		}

		(void) *str++;

		IP[counter++] = atoi(buf);
	}

	free(buf);

	return ARRAY_TO_IPv4(IP);
}

// Clears the command buffer.
void clear_command()
{
	for (uint16_t i = 0; i < 5400; terminal_buffer[i++] = 0);
	for (uint16_t i = 0; i < 5400;         command[i++] = 0);
	for (uint16_t i = 0; i <   90;            line[i++] = 0);

	command_index = 0;
}

void help()
{
	puts("\n\nHere are some commands you can use in the terminal:\n");
	cat("tty.txt");
}

// Processes the command
uint8_t process_command()
{
	uint8_t prompt = 1;
	char *start;

	// Clears the terminal screen
	if (!strcmp("clear", command))
	{
		clear_screen();
		prompt = 2;
	}

	// Prints instructions
	else if (!strcmp("help", command))
		help();

	// Prints given string
	else if ((start = strstr(command, "echo ")))
	{
		new_line(false);
		printf("%s", start + 5);
	}

	// Prints the name of all files in a directory
	else if (!strcmp("ls", command))
	{
		new_line(false);
		ls(start + 3);
	}

	// Prints content of given file
	else if ((start = strstr(command, "cat")))
	{
		new_line(false);
		cat(start + 4);
	}

	// Searches for an expression in a file
	else if (strstr(command, "ogrep "))
	{
		char *exp = malloc(sizeof(uint8_t) * 100);
		char *file = malloc(sizeof(uint8_t) * 100);

		for (uint8_t i = 0; i < 100; exp[i] = file[i] = '\0', ++i);

		uint16_t i = 6;
		uint8_t j = 0, k = 0;
		bool exp_started = true,
			 exp_dquotes = false,
			 colored = false,
			 insensitive = false;

		while (command[i] != '\0')
		{
			for (uint8_t m = 0; m < 2; ++m)
				if (!exp_dquotes && command[i] == '-')
				{
					if (command[i + 1] == 'c')
						colored = true;
					else if (command[i + 1] == 'i')
						insensitive = true;

					i += 3;
				}

			if (command[i] == '"')
			{
				if (exp_dquotes)
				{
					exp_dquotes = false;
					exp_started = false;
					i++;
				}
				else
					exp_dquotes = true;

				i++;
			}
			else
			{
				if (exp_started)
				{
					exp[j++] = command[i++];
					if (!exp_dquotes && command[i] == ' ')
					{
						exp_started = false;
						i++;
					}
				}
				else
					file[k++] = command[i++];
			}
		}

		new_line(false);

		char *ret = ogrep(exp, file, colored, insensitive);

		if (ret != 0)
		{
			printf("%s", ret);
			free(ret);
		}

		free(exp);
		free(file);
	}

	// Waits for X milliseconds
	else if ((start = strstr(command, "sleep ")))
		usleep(atoi(start + 6));

	// Closes the terminal
	else if (!strcmp("exit", command))
	{
		vga_cursor_off();
		terminal_running = false;
	}

	// List peripheral devices
	else if (!strcmp("lspci", command))
	{
		new_line(false);
		pci_list();
	}

	else if (!strcmp("lspci -n", command))
	{
		new_line(false);
		pci_proc_dump();
	}

	// Hexdump the contents of a file
	else if ((start = strstr(command, "hexdump ")))
	{
		char *file = start + 8;

		if (file[0] == '0' && file[1] == 'x')
			hexdump((char *) htoi(file), true);
		else
		{
			uint32_t i = 0;
			struct dirent *node = 0;
			bool file_found = false;

			while ((node = readdir_fs(fs_root, i)) != 0)
			{
				if (strcmp(node->name, file) == 0)
				{
					file_found = true;
					fs_node_t *fsnode = finddir_fs(fs_root, node->name);

					char *buf = malloc(sizeof(char) * 10000);
					uint32_t sz = read_fs(fsnode, 0, 10000, buf);
					buf[sz] = 0;
						
					hexdump(buf, false);
					free(buf);

					break;
				}
				i++;
			}

			if (!file_found)
				printf("\nhexdump: \"%s\": No such file or directory", file);
		}

		set_keyboard_handler(tty_kbrd_handler);
	}

	// Simple UDP chat
	else if (am79c973_ready && (start = strstr(command, "webpussy")))
	{
		new_line(false);
		move_cursor(vga_get_index());

		uint32_t IP = IP_from_string("127.0.0.1");
		uint16_t port = 1234;
		uint8_t UDP_msg[256], PORT[4], UDP_msg_index = 0, port_length = 0;
		bool webpussy_running = true, udp = true;

		if ((start = strstr(command, "webpussy ")))
		{
			start += 9;

			if (strstr(start, "-t"))
			{
				udp = false;
				start += 3;
			}
			else if (strstr(start, "-u"))
				start += 3;

			if (strstr(start, "-p "))
			{
				for (
					start += 3;
					*start != '\0' && *start != ' ';
					(void) *start++
				)
					PORT[port_length++] = *start;

				port = atoi((char *) PORT);
				IP = IP_from_string(start + 1);
			}
		}

		if (udp)
		{
			UDP_connect(IP, port);

			UDP_set_handler(lambda(void *, (char *data) {
				printf(data);
				move_cursor(vga_get_index());

				return 0;
			}));
		}
		else
			TCP_connect(IP, port);

		set_keyboard_handler(lambda(void, (active_keys_t *active_keys) {
			key_t key = active_keys->active_key;
			uint8_t ch = key.char_code;
			uint8_t k = key.key_code;

			if (key.pressed)
			{
				if (!active_keys->ctrl && ch)
					putchar(UDP_msg[UDP_msg_index++] = ch);

				switch (k)
				{
					case KEY_ENTER:
						if (udp)
							UDP_send(UDP_msg, UDP_msg_index);
						else
							TCP_send(UDP_msg, UDP_msg_index);

						for (uint8_t i = 0; i < UDP_msg_index; ++i)
							UDP_msg[i] = '\0';
						break;

					case KEY_BKSPACE:
						if (UDP_msg_index > 0)
						{
							UDP_msg[--UDP_msg_index] = '\0';
							vga_set_index(vga_get_index() - 1);
							putchar(' ');
							vga_set_index(vga_get_index() - 1);
							move_cursor(vga_get_index());
						}
						break;

					case KEY_C:
						if (active_keys->ctrl)
						{
							if (udp)
								UDP_disconnect();
							else
								TCP_disconnect();

							webpussy_running = false;
						}
						break;
				
					default:
						break;
				}
			}

			move_cursor(vga_get_index());
		}));

		while (webpussy_running)
			HALT;

		null_active();

		set_keyboard_handler(tty_kbrd_handler);

		vga_set_index(vga_get_index() - 80);
	}

	// Send ping message
	else if (am79c973_ready && (start = strstr(command, "ping ")))
	{
		uint64_t current_time;

		uint32_t IP_BE = IP_from_string(start + 5);

		bool ping_running = true;

		ICMP_set_ping_handler(lambda(void *, (uint8_t IP[4]) {
			printf(
				"\nping response from %d.%d.%d.%d time=%dms",
				IP[0], IP[1], IP[2], IP[3],
				abs(current_time - clock())
			);

			return 0;
		}));

		set_keyboard_handler(0);

		while (ping_running)
		{
			current_time = clock();
			request_echo_reply(IP_BE);

			if (get_active_keys()->active_key.char_code == 'c')
				break;

			usleep(700);

			if (get_active_keys()->active_key.char_code == 'c')
				break;
		}

		null_active();

		ICMP_set_ping_handler(0);

		set_keyboard_handler(tty_kbrd_handler);
	}

	else if (!strcmp("webhost", command))
	{
		new_line(false);

		bool server_running = true;

		set_keyboard_handler(lambda(void, (active_keys_t * active_keys) {
			uint8_t k = active_keys->active_key.key_code;

			if (active_keys->active_key.pressed)
			{
				switch (k)
				{
					case KEY_C:
						if (active_keys->ctrl)
						{
							TCP_disconnect();
							server_running = false;
						}
						break;
				
					default:
						break;
				}
			}

			move_cursor(vga_get_index());
		}));

		TCP_listen(1234);

		while (server_running)
			HALT;

		null_active();

		set_keyboard_handler(tty_kbrd_handler);
	}

	// Start BrainFuck interpreter
	else if ((start = strstr(command, "bf")))
	{
		bool file_found	= false;
		char *input		= command + 3;
		uint32_t len	= strlen(input) - 1;

		new_line(0);

		if (len > 3)
		{
			if (
				input[len-1] == 'f' &&
				input[len-2] == 'b' &&
				input[len-3] == '.'
			)
			{
					uint32_t i = 0;
					struct dirent *node = 0;

					while ((node = readdir_fs(fs_root, i)) != 0)
					{
						if (strcmp(node->name, input) == 0)
						{
							file_found = true;
							fs_node_t *fsnode = finddir_fs(fs_root, node->name);

							char *buf = malloc(sizeof(char) * 10000);
							uint32_t sz = read_fs(fsnode, 0, 10000, buf);
							buf[sz] = 0;

							interpret(buf);
							free(buf);

							break;
						}
						i++;
					}

					if (!file_found)
						printf("bf: \"%s\": No such file or directory", input);
			}
		}

		if (len > 0 && !file_found)
			interpret(input);
	}

	// Prints the elapsed time since boot
	else if (!strcmp("uptime", command))
		printf("\n%s", get_uptime());

	// Makes a beep sound
	else if (!strcmp("beep", command))
		BEEP;

	// Shuts the computer down
	else if (!strcmp("shutdown", command) || !strcmp("poweroff", command))
		shutdown();

	// Reboots the computer
	else if (!strcmp("reboot", command) || !strcmp("restart", command))
		reboot();

	// Prints current tick count
	else if (!strcmp("tick", command))
		printf("\n%d", clock());

	// Prints a random number
	else if (!strcmp("random", command))
		printf("\n%d", rand());

	// Prints a random number from an interval
	else if (strstr(command, "randint "))
	{
		start = strstr(command, "randint ") + 8;

		uint8_t counter = 0;
		char *buf = malloc(8 * sizeof(char));
		uint32_t intv[2];

		while (*start != '\0')
		{
			for (uint8_t i = 0; i < 8; buf[i++] = '\0');

			uint8_t i = 0;

			while (*start != ' ' && *start != '\0')
			{
				buf[i++] = *start;
				(void) *start++;
			}

			(void) *start++;

			intv[counter++] = atoi(buf);
		}

		free(buf);

		printf("\n%d", randint(intv[0], intv[1]));
	}

	// Prints current date
	else if (!strcmp("date", command))
	{
		char date[12] = {0};

		get_text_date(date);

		printf("\n%s", date);
	}

	// Prints current time
	else if (!strcmp("clock", command))
	{
		char clock[6] = {0};

		get_text_clock(clock);

		printf("\n%s", clock);
	}

	// Prints calendar
	else if(!strcmp("cal", command))
		cal();

	// Prints legal information
	else if (!strcmp("license", command))
		cat("LICENSE.txt");

	// Starts the hangman game
	else if (!strcmp("hangman", command))
	{
		hangman_play();
		set_keyboard_handler(tty_kbrd_handler);
	}

	// Makes the cow say the text out loud
	else if ((start = strstr(command, "cowjonas ")))
		cowsay(start + 9);

	// Ascii art
	else if ((start = strstr(command, "hascii ")))
	{
		new_line(false);
		display_ascii(start + 7);
	}

	// Display OS information
	else if (!strcmp("rexfetch", command))
		rexfetch();

	// Display spinning donut
	else if (!strcmp("donut", command))
	{
		extern bool donut_spinning;

		set_keyboard_handler(lambda(void, (active_keys_t *active_keys) {
			uint8_t k = active_keys->active_key.key_code;

			if (active_keys->active_key.pressed)
			{
				switch (k)
				{
					case KEY_C:
						if (active_keys->ctrl)
						{
							donut_spinning = false;
							set_keyboard_handler(tty_kbrd_handler);
						}
						break;
				
					default:
						break;
				}
			}

			move_cursor(vga_get_index());
		}));
		extern void dain();
		dain();
	}

	// Prints the Mandelbrot set
	else if (!strcmp("brot", command))
	{ int k=100;float i,j,r,x,y=-16;while(puts(""),y++<12)for(x=0;x++<80;putchar(" .:-;!/>)|&IH%*#"[k&15]))for(i=k=r=0;j=r*r-i*i-2+x/25,i=2*r*i+y/10,j*j+i*i<11&&k++<111;r=j); }

	// No command
	else if (strcmp("", command))
		printf("\nrsh: command not found: \"%s\"", command);

	return prompt;
}

void enter_shell()
{
	terminal_buffer	= malloc(sizeof(uint8_t) * 5400);
	command			= malloc(sizeof(uint8_t) * 5400);
	line			= malloc(sizeof(uint8_t) * 90);

	srand(time(NULL));
}

void exit_shell()
{
	free(terminal_buffer);
	free(command);
	free(line);
}

/* This file is part of RexOS!.
 *
 * RexOS! is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * RexOS! is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with RexOS!.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <drivers/keyboard.h>
#include <drivers/VGA.h>
#include <games/common.h>
#include <gui/text_mode.h>
#include <programs/common.h>
#include <sys/isr.h>
#include <sys/rtc.h>
#include <utils.h>
#include <math.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#define OPTION_MAX 14

extern void show_demo_chooser(void);

static uint16_t option = 0;
volatile bool chosen = false;

static uint8_t mario_x;
static bool mario_running, mario_started = true;
static uint16_t mario_fbkp, mario_sbkp;

void (*option_func_ptr[])() =
{
	start_terminal_session,
	tictac_play,
	snake_play,
	pong_play,
	brkt_play,
	meteors_play,
	tetros_play,
	mines_play,
	pacman_play,
	chess_play,
	sudoku_play,
	start_digiclock,
	show_demo_chooser,
	reboot,
	shutdown
};

char *desktop_options[] =
{
	"Terminal",
	"Play TicTacToe",
	"Play Snake",
	"Play Pong",
	"Play Breakout",
	"Play Meteors",
	"Play TetrOS",
	"Play Mines",
	"Play Pac-Man",
	"Play Chess",
	"Play Sudoku",
	"DigiClock",
	"VGA Graphics demos",
	"Restart",
	"Shutdown"
};

void show_mouse_cursor();

void show_options()
{
	/**
	 * @brief			Shows / updates an option in the program chooser
	 * @param option_id	is the identifier of the option (0 - 6)
	 * @param string	is the label of the option
	 * @param y			is the vertical position of the option in the menu
	 */
	void (*show_option)(uint8_t, char *) =
	lambda(void, (uint8_t option_id, char *str) {
		vga_set_index(vga_get_pos(17, (option_id + 5)));

		bool is_selected = option == option_id;

		if (is_selected)
		{
			vga_set_bgcolor(RED);
			vga_set_fgcolor(WHITE);
		}
		else
		{
			vga_set_bgcolor(BRIGHT_GREY);
			vga_set_fgcolor(BLACK);
		}

		printf(
			"%c %s %c",
			is_selected ? '>' : ' ',
			str,
			is_selected ? '<' : ' '
		);
	});

	uint8_t pos = 0;

	for (uint8_t i = 0; i <= OPTION_MAX; ++i)
		show_option(pos++, desktop_options[i]);
}

void desktop_handle_keyboard(active_keys_t *active_keys)
{
	IRQ_OFF;
	
	uint8_t k = active_keys->active_key.key_code;

	if (active_keys->active_key.pressed)
		switch (k)
		{
			case KEY_UP:
			case KEY_W:
			case KEY_K:
				if (option > 0)
					option--;
				else
					option = OPTION_MAX;
				break;

			case KEY_DOWN:
			case KEY_S:
			case KEY_J:
				if (option < OPTION_MAX)
					option++;
				else
					option = 0;
				break;

			case KEY_ENTER:
			case KEY_RIGHT:
			case KEY_D:
			case KEY_L:
				chosen = true;
				vga_set_index(80);
				break;
		}

	show_options();

	if (k != 0)
		null_active();
}

static void mario_render()
{
	uint8_t
		fx = mario_x,
		sx = mario_x < 79 ? mario_x + 1 : 0;

	if (!mario_started)
	{
		vga_set_entry(fx ? fx - 1 : 79, mario_fbkp);
		vga_set_entry(fx, mario_sbkp);
	}
	else
		mario_started = false;

	mario_fbkp = vga_get_entry_at(fx);
	mario_sbkp = vga_get_entry_at(sx);

	uint16_t mario_fh = vga_entry(mario_running ? 19 : 17, BLACK, WHITE);
	uint16_t mario_sh = vga_entry(mario_running ? 20 : 18, BLACK, WHITE);

	vga_set_entry(fx, mario_fh);
	vga_set_entry(sx, mario_sh);

	if (mario_x == 79)
		mario_x = 0;
	else
		mario_x++;

	mario_running = !mario_running;
}

// Shows / updates the program chooser
void show_menu()
{	
	draw_window_with_border(15, 2, 65, 22, BRIGHT_GREY, BLUE, true);

	vga_set_index(vga_get_pos(34, 3));
	printf("${B07}${F00}Program chooser${CLC}");

	show_options();

	vga_set_index(vga_get_pos(21, 21));
	printf(
		" ${B07}${F04}%c ${B07}${F00}- go up, ${CLC}"
		"${B07}${F04}%c ${B07}${F00}- go down, ${CLC}"
		"${B07}${F04}%c${B07}${F04}%c ${CLC}"
		"${B07}${F00} - select${CLC} ",
		24, 25, 27, 217
	);

	show_mouse_cursor();

	if (mario_started)
	{
		mario_x = 0;

		mario_running = false;
		mario_started = true;

		mario_fbkp = vga_get_entry_at(0);
		mario_sbkp = vga_get_entry_at(1);
	}

	while (!chosen)
		HALT;

	vga_set_index(0);

	register_rtc_func(RTC_DATE_WIDGET, NULL);

	if (option == 0 || (option != 11 && BETWEEN(option, 5, OPTION_MAX)))
	{
		register_rtc_func(RTC_CLOCK_WIDGET, NULL);
		register_rtc_func(4, NULL);
	}

	option_func_ptr[option]();

	register_rtc_func(RTC_CLOCK_WIDGET, NULL);
	register_rtc_func(RTC_DATE_WIDGET, NULL);

	init_rtc(13);

	show_desktop(option);
}

void show_desktop(uint8_t option_id)
{
	set_keyboard_handler(desktop_handle_keyboard);
	null_active();

	chosen = false;
	option = option_id;

	textmode_draw_rect(0, 0, VGA_WIDTH, VGA_HEIGHT, BRIGHT_BLUE);
	textmode_draw_rect(0, 0, VGA_WIDTH, 1, BLACK);

	vga_set_index(0);
	printf(
		"${B00}${F12}R${F10}e${F11}x${F14}O${F13}S${F15}!"
	);

	register_rtc_func(RTC_CLOCK_WIDGET, update_clock_widget);
	register_rtc_func(RTC_DATE_WIDGET, update_date_widget);
	register_rtc_func(4, mario_render);

	show_menu();
}

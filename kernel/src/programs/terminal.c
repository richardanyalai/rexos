/* This file is part of RexOS!.
 *
 * RexOS! is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * RexOS! is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with RexOS!.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <drivers/keyboard.h>
#include <drivers/VGA.h>
#include <programs/rexshell.h>
#include <sys/isr.h>
#include <stdint.h>
#include <stdio.h>

extern void clear_command();
extern bool process_command();

int16_t terminal_index, command_index;
char *terminal_buffer, *command, *line;
bool terminal_running;

// Moves cursor to the given position.
void move_cursor()
{ vga_cursor_update(vga_get_x(vga_get_index()), vga_get_y(vga_get_index())); }

// Shows the prompt string.
void show_ps1()
{
	printf("${F11}RexOS${F10}$ ${F15}");
	move_cursor();
}

// Clears the screen.
void clear_screen()
{
	vga_clear();
	vga_set_cursor_y(0);
	show_ps1();
}

// Writes a character.
void write_char(const uint8_t c)
{
	putchar(terminal_buffer[terminal_index++] = command[command_index++] = c);
	move_cursor();
}

// Starts a new line.
void new_line(uint8_t prompt)
{
	if (prompt != 2)
		puts("");

	vga_set_index(vga_get_pos(0, vga_get_y(vga_get_index())));

	if (prompt)
		show_ps1();

	vga_set_cursor_x(0);
	vga_set_cursor_y(vga_get_y(vga_get_index()) + 1);
}

// Listens for key events
void tty_kbrd_handler(active_keys_t *active_keys)
{
	uint8_t ch = active_keys->active_key.char_code;
	uint8_t k = active_keys->active_key.key_code;

	if (active_keys->active_key.pressed)
	{
		switch (k)
		{
			case KEY_ENTER:
				new_line(process_command());
				clear_command();
				break;

			case KEY_BKSPACE:
				if (command_index > 0)
				{
					command[--command_index] = '\0';
					vga_set_index(vga_get_index() - 1);
					putchar(' ');
					vga_set_index(vga_get_index() - 1);
					move_cursor(vga_get_index());
				}
				break;

			case KEY_L:
				if (active_keys->ctrl)
				{
					clear_screen();
					clear_command();
				}
				break;

			case KEY_C:
				if (active_keys->ctrl)
				{
					clear_command();
					new_line(true);
				}
				break;

			default:
				break;
		}

		if (!active_keys->ctrl && ch != '\n' && ch)
			write_char(ch);
	}

	if (ch != 0 || k == KEY_BKSPACE)
		null_active();
}

// Initializes tty
void init_terminal()
{
	null_active();

	set_keyboard_handler(tty_kbrd_handler);

	terminal_running = true;

	vga_set_bgcolor(BLACK);
	vga_set_fgcolor(WHITE);

	command_index = terminal_index = 0;

	vga_cursor_on(14, 15);
	vga_set_cursor_x(7);

	if (vga_get_index() == 0)
		clear_screen();
	else
		new_line(true);
}

void start_terminal_session()
{
	init_terminal();

	enter_shell();

	while (terminal_running)
		HALT;

	exit_shell();
}

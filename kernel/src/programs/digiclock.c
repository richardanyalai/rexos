/* This file is part of RexOS!.
 *
 * RexOS! is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * RexOS! is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with RexOS!.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <drivers/keyboard.h>
#include <drivers/VGA.h>
#include <gui/text_mode.h>
#include <sys/isr.h>
#include <sys/rtc.h>
#include <stdbool.h>
#include <stdio.h>
#include <unistd.h>

const uint16_t digit[27] =
{
		1,   2,   3,   4,   5,
	80,                         86,
	160,                        166,
	240,                        246,
		321, 322, 323, 324, 325,
	400,                        406,
	480,                        486,
	560,                        566,
		641, 642, 643, 644, 645
};
static uint8_t clock_digits[4], color1, color2;
static bool digiclock_running;

// Waits for exit
static void digiclock_handle_keyboard(active_keys_t *active_keys)
{
	uint8_t c = 0;

	if ((c = active_keys->active_key.char_code) != 0)
	{
		if (c == 'q' || c == 'e')
			digiclock_running = 0;

		null_active();
	}
}

static void digiclock_init()
{
	vga_set_bgcolor(BRIGHT_CYAN);
	vga_set_fgcolor(BLACK);

	color2 = BLUE;
	color1 = CYAN;

	digiclock_running = true;

	draw_window_with_title(
		5, 3, 42, 15, 20,
		vga_get_bgcolor(), vga_get_fgcolor(),
		"DigiClock",
		true, true
	);
	vga_set_index(vga_get_pos(39, 3));
	printf("[${F12}Q${CLC}]");

	set_keyboard_handler(digiclock_handle_keyboard);
}

/**
 * @brief		Draws the clock_digits
 * @param pos	is the position on screen
 * @param count	is the order number of the clock_digits (0 - 3)
 */
static void draw_digit(uint16_t pos, uint8_t count)
{
	vga_set_bgcolor(BRIGHT_CYAN);
	uint8_t color = 0;

	for (uint8_t i = 0; i < 27; ++i)
	{
		if (clock_digits[count] == 48)
		{
			if (i >= 11 && i <= 15)
				color = color1;
			else
				color = color2;
		}
		else if (clock_digits[count] == 49)
		{
			if (i == 6 || i == 8 || i == 10 || i == 17 || i == 19 || i == 21)
				color = color2;
			else
				color = color1;
		}
		else if (clock_digits[count] == 50)
		{
			if (i == 5 || i == 7 || i == 9 || i == 17 || i == 19 || i == 21)
				color = color1;
			else
				color = color2;
		}
		else if (clock_digits[count] == 51)
		{
			if (i == 5 || i == 7 || i == 9 || i == 16 || i == 18 || i == 20)
				color = color1;
			else
				color = color2;
		}
		else if (clock_digits[count] == 52)
		{
			if (
				i == 5					||
				i == 6					||
				i == 7					||
				i == 8					||
				i == 9					||
				i == 10					||
				(i >= 11 && i <= 15)	||
				i == 17					||
				i == 19					||
				i == 21

			)
				color = color2;
			else
				color = color1;
		}
		else if (clock_digits[count] == 53)
		{
			if (i == 6 || i == 8 || i == 10 || i == 16 || i == 18 || i == 20)
				color = color1;
			else
				color = color2;
		}
		else if (clock_digits[count] == 54)
		{
			if (i == 6 || i == 8 || i == 10)
				color = color1;
			else
				color = color2;
		}
		else if (clock_digits[count] == 55)
		{
			if (
				i <= 4	||
				i == 6	||
				i == 8	||
				i == 10	||
				i == 17	||
				i == 19	||
				i == 21
			)
				color = color2;
			else
				color = color1;
		}
		else if (clock_digits[count] == 56)
			color = color2;
		else if (clock_digits[count] == 57)
		{
			if (i == 16 || i == 18 || i == 20)
				color = color1;
			else
				color = color2;
		}

		uint8_t bg = color;
		uint8_t ch = ' ';

		if (i <= 4)
		{
			ch = 220;
			bg = vga_get_bgcolor();
		}
		else if (i >= 11 && i <= 15)
			ch = ' ';
		else if (i >= 22 && i <= 27)
		{
			ch = 223;
			bg = vga_get_bgcolor();
		}

		vga_draw_char_at(ch, pos + digit[i], bg, color);
	}
}

// Renders digital clock
static void digiclock_render()
{
	uint16_t startPos = vga_get_pos(7, 5);
	draw_digit(startPos, 0);
	draw_digit(startPos + 8, 1);

	vga_draw_char_at(254, startPos + 256, vga_get_bgcolor(), color2);
	vga_draw_char_at(254, startPos + 416, vga_get_bgcolor(), color2);

	draw_digit(startPos + 18, 2);
	draw_digit(startPos + 26, 3);
}

// Updates the clock_digits
static void digiclock_update()
{
	char digs[6] = {0};
	
	get_text_clock(digs);

	clock_digits[0] = digs[0];
	clock_digits[1] = digs[1];
	clock_digits[2] = digs[3];
	clock_digits[3] = digs[4];

	digiclock_render();
}

void start_digiclock()
{
	digiclock_init();

	register_rtc_func(2, digiclock_update);

	while (digiclock_running)
		HALT;

	register_rtc_func(2, 0);
}

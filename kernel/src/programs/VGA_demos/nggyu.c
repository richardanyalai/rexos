#include <drivers/keyboard.h>
#include <drivers/VGA.h>
#include <gui/graphics_mode.h>
#include <settings.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

extern const uint8_t
	nggyu_0[320*200],
	nggyu_1[320*200],
	nggyu_2[320*200],
	nggyu_3[320*200],
	nggyu_4[320*200],
	nggyu_5[320*200],
	nggyu_6[320*200],
	nggyu_7[320*200],
	nggyu_8[320*200],
	nggyu_9[320*200],
	nggyu_10[320*200],
	nggyu_11[320*200],
	nggyu_12[320*200],
	nggyu_13[320*200],
	nggyu_14[320*200],
	nggyu_15[320*200],
	nggyu_16[320*200],
	nggyu_17[320*200],
	nggyu_18[320*200],
	nggyu_19[320*200],
	nggyu_20[320*200],
	nggyu_21[320*200],
	nggyu_22[320*200],
	nggyu_23[320*200],
	nggyu_24[320*200],
	nggyu_25[320*200],
	nggyu_26[320*200],
	nggyu_27[320*200],
	nggyu_28[320*200],
	nggyu_29[320*200],
	nggyu_30[320*200],
	nggyu_31[320*200];

const uint8_t *nggyu[32] =
{
	nggyu_0,
	nggyu_1,
	nggyu_2,
	nggyu_3,
	nggyu_4,
	nggyu_5,
	nggyu_6,
	nggyu_7,
	nggyu_8,
	nggyu_9,
	nggyu_10,
	nggyu_11,
	nggyu_12,
	nggyu_13,
	nggyu_14,
	nggyu_15,
	nggyu_16,
	nggyu_17,
	nggyu_18,
	nggyu_19,
	nggyu_20,
	nggyu_21,
	nggyu_22,
	nggyu_23,
	nggyu_24,
	nggyu_25,
	nggyu_26,
	nggyu_27,
	nggyu_28,
	nggyu_29,
	nggyu_30,
	nggyu_31,
};

void start_nggyu()
{
	bool nggyu_playing = true;

	set_keyboard_handler(lambda(void, (active_keys_t *active_keys)
	{
		if (active_keys->active_key.char_code == 'q')
			nggyu_playing = false;

		null_active();
	}));

	uint8_t j = 0;

	while (nggyu_playing)
	{
		for (uint8_t i = 0; i < 32; ++i)
		{
			if (!nggyu_playing)
				break;

#ifndef VESA
			memcpy((uint8_t *) GRAPHICS_MODE_ADDRESS, nggyu[i], 320*200);
#else
			for (uint16_t x = 0; x < 320; ++x)
				for (uint8_t y = 0; y < 200; ++y)
					vga_draw_pixel(x, y, nggyu[i][x + 320 * y]);
#endif
			usleep(60);
		}

		j++;
	}
}

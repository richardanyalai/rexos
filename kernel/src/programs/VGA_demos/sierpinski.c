// REF: https://www.geeksforgeeks.org/sierpinski-triangle-using-graphics/

#include <drivers/VGA.h>
#include <gui/graphics_mode.h>
#include <math.h>

#define SIERPINSKI_HEIGHT 190
#define SIERPINSKI_WIDTH 320

void triangle(float x, float y, float h, int color)
{
	for (float delta = 0; delta > -5; delta -= 1)
		vga_draw_triangle(
			x - (h + delta) / sqrt(3),	y - (h + delta) / 3,
			x + (h + delta) / sqrt(3),	y - (h + delta) / 3,
			x,							y + 2 * (h + delta) / 3,
			color % 15 + 1
		);
}

void triangle_v2(float x, float y, float h, int color)
{
	for (float delta = 0; delta > -1 + 5; delta -= 1)
		vga_draw_triangle(
			x - (h + delta) / sqrt(3),	y + (h + delta) / 3,
			x + (h + delta) / sqrt(3),	y + (h + delta) / 3,
			x,							y - 2 * (h + delta) / 3,
			color % 15 + 1
		);
}

int triangles(float x, float y, float h, int color)
{
	if (h < 5)
		return 0;

	if (x > 0 && y > 0 && x < SIERPINSKI_WIDTH && y < SIERPINSKI_HEIGHT)
		triangle(x, y, h, color);

	triangles(x,				y - 2 * h / 3, h / 2,	color + 1);
	triangles(x - h / sqrt(3),	y + h / 3, h / 2,		color + 1);
	triangles(x + h / sqrt(3),	y + h / 3, h / 2,		color + 1);

	return 0;
}

void sierpinski_draw()
{
	vga_clear();

	triangle_v2(
		SIERPINSKI_WIDTH / 2,
		2 * SIERPINSKI_HEIGHT / 3,
		SIERPINSKI_HEIGHT,
		2
	);
	triangles(
		SIERPINSKI_WIDTH / 2,
		2 * SIERPINSKI_HEIGHT / 3,
		SIERPINSKI_HEIGHT / 2,
		0
	);
}

/* This file is part of RexOS!.
 *
 * RexOS! is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * RexOS! is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with RexOS!.  If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * Some useful links:
 * REF: https://en.wikipedia.org/wiki/Perlin_noise
 * REF: https://github.com/sol-prog/Perlin_Noise/blob/master/ppm.cpp
 */

#include <drivers/keyboard.h>
#include <drivers/VGA.h>
#include <gui/graphics_mode.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include <math.h>
#include <unistd.h>

bool perlin_playing;

static int SEED = 0;
float perlin_freq;
int perlin_depth;

const uint8_t HASH[] =
{
	151, 160, 137, 91, 90, 15, 131, 13, 201, 95, 96, 53, 194, 233, 7, 225, 140, 36,
	103, 30, 69, 142, 8, 99, 37, 240, 21, 10, 23, 190, 6, 148, 247, 120, 234, 75, 0,
	26, 197, 62, 94, 252, 219, 203, 117, 35, 11, 32, 57, 177, 33, 88, 237, 149, 56,
	87, 174, 20, 125, 136, 171, 168, 68, 175, 74, 165, 71, 134, 139, 48, 27, 166,
	77, 146, 158, 231, 83, 111, 229, 122, 60, 211, 133, 230, 220, 105, 92, 41, 55,
	46, 245, 40, 244, 102, 143, 54, 65, 25, 63, 161, 1, 216, 80, 73, 209, 76, 132,
	187, 208, 89, 18, 169, 200, 196, 135, 130, 116, 188, 159, 86, 164, 100, 109,
	198, 173, 186, 3, 64, 52, 217, 226, 250, 124, 123, 5, 202, 38, 147, 118, 126,
	255, 82, 85, 212, 207, 206, 59, 227, 47, 16, 58, 17, 182, 189, 28, 42, 223, 183,
	170, 213, 119, 248, 152, 2, 44, 154, 163, 70, 221, 153, 101, 155, 167, 43,
	172, 9, 129, 22, 39, 253, 19, 98, 108, 110, 79, 113, 224, 232, 178, 185, 112,
	104, 218, 246, 97, 228, 251, 34, 242, 193, 238, 210, 144, 12, 191, 179, 162,
	241, 81, 51, 145, 235, 249, 14, 239, 107, 49, 192, 214, 31, 181, 199, 106,
	157, 184, 84, 204, 176, 115, 121, 50, 45, 127, 4, 150, 254, 138, 236, 205,
	93, 222, 114, 67, 29, 24, 72, 243, 141, 128, 195, 78, 66, 215, 61, 156, 180
};

int noise2(int x, int y)
{
	int yindex = (y + SEED) % 200;

	if (yindex < 0)
		yindex += 200;

	int xindex = (HASH[yindex] + x) % 320;

	if (xindex < 0)
		xindex += 320;

	const int  result = HASH[xindex];

	return result;
}

float lin_inter(float x, float y, float s)
{
	return x + s * (y - x);
}

float smooth_inter(float x, float y, float s)
{
	return lin_inter(x, y, s * s * (3 - 2 * s));
}

float noise2d(float x, float y)
{
	int x_int = x;
	int y_int = y;
	float x_frac = x - x_int;
	float y_frac = y - y_int;
	int s = noise2(x_int, y_int);
	int t = noise2(x_int + 1, y_int);
	int u = noise2(x_int, y_int + 1);
	int v = noise2(x_int + 1, y_int + 1);
	float low = smooth_inter(s, t, x_frac);
	float high = smooth_inter(u, v, x_frac);

	return smooth_inter(low, high, y_frac);
}

float perlin2d(float x, float y)
{
	float xa = x * perlin_freq;
	float ya = y * perlin_freq;
	float amp = 1.0;
	float fin = 0;
	float div = 0.0;

	for (uint8_t i = 0; i < perlin_depth; ++i)
	{
		div += 256 * amp;
		fin += noise2d(xa, ya) * amp;
		amp /= 2;
		xa *= 2;
		ya *= 2;
	}

	return fin / div;
}

void perlin_render()
{
	for (uint8_t y = 0; y < 200; ++y)
		for (uint16_t x = 0; x < 320; ++x)
		{
			switch ((256 / ((int) (perlin2d(x, y) * 100) + 90)) % 2)
			{
				case 0:
					vga_draw_pixel(x, y, BLUE);
					break;

				case 1:
					vga_draw_pixel(x, y, GREEN);
					break;
			}
		}
}

void perlin_start()
{
	init_vga(320, 200);
	perlin_playing = true;
	perlin_freq = 0.05;
	perlin_depth = 1;

	set_keyboard_handler(lambda(void, (active_keys_t *active_keys)
	{
		if (active_keys->active_key.pressed)
			switch (active_keys->active_key.key_code)
			{
				case KEY_Q:
					perlin_playing = false;
					break;

				case KEY_SPACE:
					SEED++;
					perlin_render();
					break;

				case KEY_UP:
					if (perlin_depth > 1)
					{
						perlin_depth--;

						if (perlin_freq >= 0.02)
							perlin_freq -= 0.01;
					}

					perlin_render();
					break;

				case KEY_DOWN:
					perlin_freq += 0.01;
					perlin_depth++;
					perlin_render();
					break;

				default:
					break;
			}

		null_active();
	}));

	perlin_render();

	while (perlin_playing)
		asm volatile("hlt");
}

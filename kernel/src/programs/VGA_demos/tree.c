// REF: https://www.rosettacode.org/wiki/Pythagoras_tree#C

#include <drivers/VGA.h>
#include <gui/graphics_mode.h>
#include <math.h>

/*
 * Usage:
 * tree_draw(160, 200, -90.0, 9, 5);
 */

typedef struct { double x, y; } point;

void pythagoras_tree(point a, point b, int times)
{
	point c, d, e;
 
	c.x = b.x - (a.y - b.y);
	c.y = b.y - (b.x - a.x);
 
	d.x = a.x - (a.y -  b.y);
	d.y = a.y - (b.x - a.x);
 
	e.x = d.x +  ( b.x - a.x - (a.y -  b.y) ) / 2;
	e.y = d.y -  ( b.x - a.x + a.y -  b.y ) / 2;
 
	if (times > 0)
	{
		vga_draw_line(a.x, a.y, b.x, b.y, 1 + times);
		vga_draw_line(c.x, c.y, b.x, b.y, 1 + times);
		vga_draw_line(c.x, c.y, d.x, d.y, 1 + times);
		vga_draw_line(a.x, a.y, d.x, d.y, 1 + times);
 
		pythagoras_tree(d, e, times - 1);
		pythagoras_tree(e, c, times - 1);
	}
}

void tree_draw()
{
	point a, b;
	double side = 50;
	int iter = 10;
 
	a.x = 6 * side / 2 - side / 2;
	a.y = 4 * side;
	b.x = 6 * side / 2 + side / 2;
	b.y = 4 * side;
	
	vga_clear();
 
	pythagoras_tree(a, b, iter);
}

/* This file is part of RexOS!.
 *
 * RexOS! is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * RexOS! is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with RexOS!.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <drivers/keyboard.h>
#include <drivers/serial.h>
#include <drivers/VESA.h>
#include <drivers/VGA.h>
#include <gui/graphics_mode.h>
#include <settings.h>
#include <sys/isr.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <unistd.h>

#define WIDTH        320
#define HEIGHT       200
#define SIZE         40
#define FOCAL_LENGTH 70
#define SPEED_X      0.010
#define SPEED_Y      0.020

typedef struct
{
	double x;
	double y;
} point_2d_t;

typedef struct
{
	double x;
	double y;
	double z;
} point_3d_t;

uint8_t cube_faces[6][4] =
{
	{ 0, 1, 2, 3 },
	{ 0, 4, 5, 1 },
	{ 1, 5, 6, 2 },
	{ 3, 2, 6, 7 },
	{ 0, 3, 7, 4 },
	{ 4, 7, 6, 5 }
};

point_3d_t cube_vrt[8];
point_2d_t cube_projection[8];

point_3d_t cube;

uint8_t cube_buffer[200][320];
uint8_t cube_color;

double cube_speed_x, cube_speed_y;

void cube_rotate_x(double radian)
{
	double cosine = cos(radian);
	double sine   = sin(radian);

	for (int16_t i = 7; i > -1; --i)
	{
		point_3d_t *p = &cube_vrt[i];

		double y = (p->y - cube.y) * cosine
			- (p->z - cube.z) * sine;
		double z = (p->y - cube.y) * sine
			+ (p->z - cube.z) * cosine;

		p->y = y + cube.y;
		p->z = z + cube.z;
	}
}

void cube_rotate_y(double radian)
{
	double cosine = cos(radian);
	double sine   = sin(radian);

	for (int16_t i = 7; i > -1; --i)
	{
		point_3d_t *p = &cube_vrt[i];

		double x = (p->z - cube.z) * sine
			+ (p->x - cube.x) * cosine;
		double z = (p->z - cube.z) * cosine
			- (p->x - cube.x) * sine;

		p->x = x + cube.x;
		p->z = z + cube.z;
	}
}

void cube_project()
{
	for (int16_t i = 7; i > -1; --i)
	{
		point_3d_t *p = &cube_vrt[i];

		double x = p->x * (FOCAL_LENGTH / p->z) + WIDTH * 0.5;
		double y = p->y * (FOCAL_LENGTH / p->z) + HEIGHT * 0.5;

		cube_projection[i] = (point_2d_t) { x, y };
	}
}

void cube_draw_line(
	uint16_t x1, uint16_t y1, uint16_t x2, uint16_t y2, uint8_t color
)
{
	int16_t dx = x2 - x1;
	int16_t dy = y2 - y1;
	int16_t steps = 0;

	if (abs(dx) > abs(dy))
		steps = abs(dx);
	else
		steps = abs(dy);

	double x = x1, y = y1, x_inc, y_inc;

	x_inc = dx / (float) steps;
	y_inc = dy / (float) steps;

	for (int16_t v = 0; v < steps; ++v)
	{
		x += x_inc;
		y += y_inc;

		uint16_t _x = (uint16_t) x;
		uint16_t _y = (uint16_t) y;

		cube_buffer[_y][_x] = color;
	}
}

void cube_animate()
{
	memset(cube_buffer, 0, 320 * 200);

	cube_rotate_x(cube_speed_x);
	cube_rotate_y(cube_speed_y);

	cube_project();

	for (int16_t i = 5; i > -1; --i)
	{
		point_3d_t p1 = cube_vrt[cube_faces[i][0]];
		point_3d_t p2 = cube_vrt[cube_faces[i][1]];
		point_3d_t p3 = cube_vrt[cube_faces[i][2]];

		point_3d_t v1 =
		{
			p2.x - p1.x,
			p2.y - p1.y,
			p2.z - p1.z
		};
		point_3d_t v2 =
		{
			p3.x - p1.x,
			p3.y - p1.y,
			p3.z - p1.z
		};
		point_3d_t n =
		{
			v1.y * v2.z - v1.z * v2.y,
			v1.z * v2.x - v1.x * v2.z,
			v1.x * v2.y - v1.y * v2.x
		};

		if (-p1.x * n.x + -p1.y * n.y + -p1.z * n.z <= 0)
		{
			{
				cube_draw_line(
					fabs(cube_projection[cube_faces[i][0]].x),
					fabs(cube_projection[cube_faces[i][0]].y),
					fabs(cube_projection[cube_faces[i][1]].x),
					fabs(cube_projection[cube_faces[i][1]].y),
					WHITE
				);
				cube_draw_line(
					fabs(cube_projection[cube_faces[i][1]].x),
					fabs(cube_projection[cube_faces[i][1]].y),
					fabs(cube_projection[cube_faces[i][2]].x),
					fabs(cube_projection[cube_faces[i][2]].y),
					WHITE
				);
				cube_draw_line(
					fabs(cube_projection[cube_faces[i][2]].x),
					fabs(cube_projection[cube_faces[i][2]].y),
					fabs(cube_projection[cube_faces[i][3]].x),
					fabs(cube_projection[cube_faces[i][3]].y),
					WHITE
				);
				cube_draw_line(
					fabs(cube_projection[cube_faces[i][3]].x),
					fabs(cube_projection[cube_faces[i][3]].y),
					fabs(cube_projection[cube_faces[i][0]].x),
					fabs(cube_projection[cube_faces[i][0]].y),
					WHITE
				);
			}

			uint8_t color = RED;

			switch (i)
			{
				case 1:
					color = BLUE;
					break;

				case 2:
					color = GREEN;
					break;

				case 3:
					color = YELLOW;
					break;

				case 4:
					color = CYAN;
					break;

				case 5:
					color = MAGENTA;
					break;
			}

			for (int16_t y = 0; y < 200; ++y)
			{
				for (int16_t x = 0; x < 320; ++x)
				{
					if (
						(
							(
								cube_buffer[y][x-1] == WHITE &&
								cube_buffer[y][x+1] == WHITE
							) || (
								cube_buffer[y-1][x] == WHITE &&
								cube_buffer[y+1][x] == WHITE
							) || (
								cube_buffer[y][x] == WHITE &&
								cube_buffer[y+1][x] == WHITE
							) || (
								cube_buffer[y - 1][x] == WHITE &&
								cube_buffer[y][x] == WHITE
							)
						) && cube_buffer[y][x] != WHITE
					)
						cube_buffer[y][x] = WHITE;
				}
			}

			for (int16_t y = 0; y < 200; ++y)
			{
				uint8_t counter = 0;

				for (int16_t x = 0; x < 320; ++x)
				{
					bool has_white = false;
					uint8_t white_dist_counter = 0;

					for (int16_t _x = x + 1; _x < 320; ++_x)
						if (cube_buffer[y][_x] != BLACK)
						{
							has_white = true;
							break;
						}
						else
							white_dist_counter++;

					if (!has_white)
						break;

					if (cube_buffer[y][x] == WHITE && white_dist_counter > 1)
					{
						if (cube_buffer[y][x-1] == color)
							break;

						counter++;
						x++;
					}

					if (
						counter == 1 &&
						has_white &&
						cube_buffer[y][x] == BLACK
					)
						cube_buffer[y][x] = color;
				}
			}
		}
	}

#ifndef VESA
	memcpy((uint8_t *) GRAPHICS_MODE_ADDRESS, cube_buffer, 320 * 200);
#else
	for (uint16_t x = 0; x < 320; ++x)
		for (uint8_t y = 0; y < 200; ++y)
			vesa_draw_vga_pixel(
				x, y,
				(uint8_t) cube_buffer[y][x]
			);
#endif
}

void cube_init()
{
	uint8_t x = 0, y = 0, z = 100;

	cube.x = x, cube.y = y, cube.z = z;

	cube_speed_x = SPEED_X;
	cube_speed_y = SPEED_Y;

	cube_vrt[0] = (point_3d_t) { .x = x - SIZE, .y = y - SIZE, .z = z - SIZE };
	cube_vrt[1] = (point_3d_t) { .x = x + SIZE, .y = y - SIZE, .z = z - SIZE };
	cube_vrt[2] = (point_3d_t) { .x = x + SIZE, .y = y + SIZE, .z = z - SIZE };
	cube_vrt[3] = (point_3d_t) { .x = x - SIZE, .y = y + SIZE, .z = z - SIZE };
	cube_vrt[4] = (point_3d_t) { .x = x - SIZE, .y = y - SIZE, .z = z + SIZE };
	cube_vrt[5] = (point_3d_t) { .x = x + SIZE, .y = y - SIZE, .z = z + SIZE };
	cube_vrt[6] = (point_3d_t) { .x = x + SIZE, .y = y + SIZE, .z = z + SIZE };
	cube_vrt[7] = (point_3d_t) { .x = x - SIZE, .y = y + SIZE, .z = z + SIZE };
}

void cube_start()
{
	cube_init();

	bool cube_spinning = true;

	set_keyboard_handler(lambda(void, (active_keys_t * active_keys)
	{
		if (active_keys->active_key.pressed)
			switch (active_keys->active_key.key_code)
			{
				case KEY_Q:
					cube_spinning = false;
					break;

				case KEY_LEFT:
				case KEY_A:
				case KEY_H:
					cube_speed_x = -0.05;
					cube_animate();
					break;

				case KEY_RIGHT:
				case KEY_D:
				case KEY_L:
					cube_speed_x = 0.05;
					cube_animate();
					break;

				case KEY_UP:
				case KEY_W:
				case KEY_K:
					cube_speed_y = - 0.05;
					cube_animate();
					break;

				case KEY_DOWN:
				case KEY_S:
				case KEY_J:
					cube_speed_y = 0.05;
					cube_animate();
					break;
			}
				

		null_active();
	}));

	while (cube_spinning)
	{
		HALT;

		cube_animate();
		usleep(25);
	}
}

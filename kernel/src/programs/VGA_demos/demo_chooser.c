/* This file is part of RexOS!.
 *
 * RexOS! is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * RexOS! is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with RexOS!.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <drivers/keyboard.h>
#include <drivers/VGA.h>
#include <programs/common.h>
#include <gui/graphics_mode.h>
#include <settings.h>
#include <sys/isr.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#define OPTION_COUNT 9

extern const uint8_t doom[320*200];
extern const uint8_t lenna[150*200];

static uint8_t current_index = 0;
static bool demo_running;

const char *menu_options[] =
{
	"Sierpinski triangles",
	"Pythagoras tree",
	"Perlin noise",
	"Game of Life",
	"Can it run Doom?",
	"3D cube",
	"Matrix rain",
	"Never gonna gif you up",
	"Exit"
};

static void demo_exit()
{
	demo_running = false;
	vga_clear();
	init_vga(80, 25);
}

static void but_can_it_run_doom()
{
	for (uint16_t x = 0; x < 320; ++x)
		for (uint8_t y = 0; y < 200; ++y)
			vga_draw_pixel(x, y, doom[x + 320 * y]);
}

static void (*option_ptr[])() =
{
	sierpinski_draw,
	tree_draw,
	perlin_start,
	gol_start,
	but_can_it_run_doom,
	cube_start,
	matrix_start,
	start_nggyu,
	demo_exit
};

static void update_menu(uint8_t index)
{
	current_index = index;
	uint8_t y_offset = 5;

	for (uint8_t i = 0; i < OPTION_COUNT; ++i)
		vga_draw_string(
			1,
			y_offset + i + (i < OPTION_COUNT - 1 ? 0 : 1),
			menu_options[i],
			(i == index) ? 11 : 0,
			(i == index) ? 0 : 15
		);
}

static void init_demo_chooser()
{
	uint8_t xi = 0;
	uint16_t y_offset = 0;

	// List all the colors
	for (uint16_t i = 0; i < 256; ++i)
	{
		xi = i % 16;

		if (i > 0 && xi == 0)
			y_offset += 13;

		vga_draw_fill_rect(
			xi * 20, y_offset + 0, xi * 20 + 20, y_offset + 13, i
		);
	}

	extern uint8_t vga_256_palette[256][3];

	for (uint16_t x = 0; x < 150; ++x)
		for (uint8_t y = 0; y < 200; ++y)
		{
			uint16_t idx = x + y * 150;

			if (
				vga_256_palette[lenna[idx]][1] < 55 &&
				lenna[idx] != 10					&&
				lenna[idx] != 2
			)
				vga_draw_pixel(x + 170, y, lenna[idx]);
		}

	vga_draw_string(
		5, 1, (const char *) "VGA 320x200 demos", YELLOW, BLACK
	);

	set_keyboard_handler(lambda(void, (active_keys_t *active_keys)
	{
		if (active_keys->active_key.pressed)
			switch (active_keys->active_key.key_code)
			{
				case KEY_UP:
					update_menu(
						current_index - 1 >= 0	? current_index - 1
												: OPTION_COUNT - 1
					);
					break;

				case KEY_DOWN:
					update_menu(
						current_index + 1 < OPTION_COUNT ? current_index + 1 : 0
					);
					break;

				case KEY_ENTER:
					if (current_index < 2)
						set_keyboard_handler(0);

					option_ptr[current_index]();

					if (current_index < 2)
						usleep(2000);

					if (current_index != 4 && current_index != OPTION_COUNT - 1)
						init_demo_chooser();

					break;
			}

		if (active_keys->active_key.key_code != 0)
			null_active();
	}));

	update_menu(0);

	while (demo_running)
		HALT;
}

void show_demo_chooser()
{
	demo_running = true;
	
	init_vga(320, 200);
	init_demo_chooser();
}

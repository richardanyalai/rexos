/* This file is part of RexOS!.
 *
 * RexOS! is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * RexOS! is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with RexOS!.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <drivers/keyboard.h>
#include <drivers/serial.h>
#include <drivers/VGA.h>
#include <gui/graphics_mode.h>
#include <sys/isr.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <unistd.h>

#define MATRIX_WIDTH  40
#define MATRIX_HEIGHT 25
#define MATRIX_L      83

typedef struct
{
	uint8_t value;
	uint8_t color;
} matrix_char_t;

matrix_char_t matrix_table[MATRIX_HEIGHT+1][MATRIX_WIDTH];
const char *matrix_chars =
	"1234567890"
	"abcdefghijklmnopqrstuvwxyz"
	"ABCDEFGHIJKLMNOPQRSTUVWXYZ"
	",./';[]!@#$%^&*()-=_+";

bool switches[MATRIX_WIDTH] = { false };
void matrix_render()
{
	for (uint8_t x = 0; x < MATRIX_WIDTH; ++x)
		for (uint8_t y = MATRIX_HEIGHT; y > 0; --y)
			matrix_table[y][x] = matrix_table[y-1][x];

	for (uint8_t x = 0; x < MATRIX_WIDTH; ++x)
		for (uint8_t y = 1; y < MATRIX_HEIGHT + 1; ++y)
		{
			matrix_char_t *matrix_cell = &matrix_table[y][x];
			vga_draw_string(
				x, y - 1,
				(char *) (
					(uint8_t[2]) {
						matrix_cell->color == BRIGHT_GREY
										   ? matrix_chars[rand() % MATRIX_L]
										   : matrix_cell->value,
						'\0' 
					}
				),
				BLACK,
				matrix_cell->color
			);
		}

	for (uint8_t i = 0; i != 5; ++i)
	{
		uint8_t x = rand() % MATRIX_WIDTH;
		switches[x] = !switches[x];
	}

	for (uint8_t i = 0; i < MATRIX_WIDTH; i += 2)
	{
		if (switches[i])
		{
			matrix_table[0][i] = (matrix_char_t){
				.value = matrix_chars[rand() % MATRIX_L],
				.color = matrix_table[1][i].color < 17 ? GREEN : BRIGHT_GREY
			};
			matrix_table[0][i + 1] = (matrix_char_t){
				.value = matrix_chars[rand() % MATRIX_L],
				.color = rand() % 2 == 0 ? 17 : 19
			};
		}
		else
		{
			matrix_table[0][i] = (matrix_char_t){
				.value = matrix_chars[rand() % MATRIX_L],
				.color = rand() % 2 == 0 ? 17 : 18
			};
			matrix_table[0][i + 1] = (matrix_char_t){
				.value = matrix_chars[rand() % MATRIX_L],
				.color = rand() % 2 == 0 ? 17 : 18
			};
		}
	}
}

void matrix_start()
{
	bool matrix_falling = true;

	srand(time(NULL));

	set_keyboard_handler(lambda(void, (active_keys_t *active_keys)
	{
		if (active_keys->active_key.char_code == 'q')
			matrix_falling = false;

		null_active();
	}));

	while (matrix_falling)
	{
		HALT;
		matrix_render();
		usleep(60);
	}
}

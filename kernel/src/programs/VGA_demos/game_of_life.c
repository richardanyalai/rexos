/* This file is part of RexOS!.
 *
 * RexOS! is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * RexOS! is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURgol_posE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with RexOS!.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <stdint.h>
#include <drivers/keyboard.h>
#include <drivers/VGA.h>
#include <games/common.h>
#include <sys/isr.h>
#include <math.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>

#define GOL_WIDTH  64
#define GOL_HEIGHT 40

#define gol_get_pos(x, y)	(x + y * GOL_WIDTH)
#define gol_get_y(pos)		(pos / GOL_WIDTH)
#define gol_get_x(pos)		(pos - gol_get_y(pos) * GOL_WIDTH)

#define for_x for (uint8_t x = 0; x < GOL_WIDTH; ++x)
#define for_y for (uint8_t y = 0; y < GOL_HEIGHT; ++y)
#define for_yx for_y for_x
#define for_xy for_x for_y

uint32_t gol_timer;
uint16_t gol_pos, gol_old_pos;
uint8_t gol_x, gol_y, gol_old_status;
bool gol_running, gol_paused, *gol_playground;

void gol_init(void), gol_render(void);

void gol_draw_cell(uint8_t x, uint8_t y, uint8_t color)
{ vga_draw_fill_rect(x * 5, y * 5, x * 5 + 5, y * 5 + 5, color); }

void gol_handle_keyboard(active_keys_t *active_keys)
{
	key_t active_key = active_keys->active_key;

	if (gol_paused && active_key.pressed)
	{
		switch (active_key.key_code)
		{
			case KEY_UP:
			case KEY_W:
			case KEY_K:
				if (gol_y > 0)
					gol_y--;
				break;

			case KEY_LEFT:
			case KEY_A:
			case KEY_H:
				if (gol_x > 0)
					gol_x--;
				break;

			case KEY_DOWN:
			case KEY_S:
			case KEY_J:
				if (gol_y < GOL_HEIGHT - 1)
					gol_y++;
				break;

			case KEY_RIGHT:
			case KEY_D:
			case KEY_L:
				if (gol_x < GOL_WIDTH - 1)
					gol_x++;
				break;

			case KEY_DOT:
				gol_playground[gol_pos] = !gol_playground[gol_pos];
				break;

			case KEY_R:
				gol_init();
				gol_render();
				break;
		}

		gol_pos = gol_get_pos(gol_x, gol_y);

		gol_draw_cell(
			gol_get_x(gol_old_pos),
			gol_get_y(gol_old_pos),
			gol_playground[gol_old_pos] ? BRIGHT_GREEN : BLACK
		);
		gol_draw_cell(
			gol_get_x(gol_pos),
			gol_get_y(gol_pos),
			gol_playground[gol_pos] ? BRIGHT_GREEN : BLACK
		);
		vga_draw_fill_rect(
			gol_get_x(gol_pos) * 5 + 1,
			gol_get_y(gol_pos) * 5 + 1,
			gol_get_x(gol_pos) * 5 + 4,
			gol_get_y(gol_pos) * 5 + 4,
			BRIGHT_RED
		);

		gol_old_pos = gol_pos;
	}

	if (active_key.pressed)
	{
		if (active_key.key_code == KEY_SPACE)
			gol_paused = !gol_paused;
		else if (active_key.key_code == KEY_Q)
			gol_running = false;
	}
		
	null_active();
}

void gol_init()
{
	set_keyboard_handler(gol_handle_keyboard);

	srand(time(NULL));

	gol_playground = malloc(sizeof(bool) * GOL_HEIGHT * GOL_WIDTH);

	for_yx
		gol_playground[gol_get_pos(x, y)] =
			(((rand() * rand()) / 100) % 3 == 1);

	gol_running = true;
	gol_paused  = true;

	gol_timer = 0;

	gol_x = 32;
	gol_y = 20;

	gol_pos     = gol_get_pos(gol_x, gol_y);
	gol_old_pos = gol_pos;

	gol_render();
}

void gol_render()
{
	for_yx
		gol_draw_cell(
			x, y, gol_playground[gol_get_pos(x, y)] ? BRIGHT_GREEN : BLACK
		);
}

void gol_update()
{
	bool *newgen = malloc(sizeof(bool) * GOL_HEIGHT * GOL_WIDTH);

	memset(newgen, false, GOL_HEIGHT * GOL_WIDTH);

	uint8_t (*count_alive)(uint8_t, uint8_t) =
	lambda (uint8_t, (uint8_t i, uint8_t j)
	{
		uint8_t a = 0;

		for (uint8_t x = i - 1; x <= i + 1; ++x)
			for (uint8_t y = j - 1; y <= j + 1; ++y)
			{
				if (x == i && y == j)
					continue;

				if (y < GOL_HEIGHT && x < GOL_WIDTH)
					a += gol_playground[gol_get_pos(x, y)];
			}

		return a;
	});

	for_yx
	{
		uint16_t cur_gol_pos = gol_get_pos(x, y);

		bool cur = gol_playground[cur_gol_pos];

		uint8_t alive_neighbours = count_alive(x, y);

		if (cur && alive_neighbours < 2)
			newgen[cur_gol_pos] = 0;
		else if (cur && alive_neighbours > 3)
			newgen[cur_gol_pos] = 0;
		else if (!cur && alive_neighbours == 3)
			newgen[cur_gol_pos] = 1;
		else
			newgen[cur_gol_pos] = cur;
	}

	free(gol_playground);
	gol_playground = newgen;
}

void gol_start()
{
	gol_init();

	while (gol_running)
	{
		if (!gol_paused && gol_timer++ == 120)
		{
			gol_timer = 0;

			gol_update();
			gol_render();
		}
		HALT;
	}

	free(gol_playground);
}

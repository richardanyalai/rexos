/* This file is part of RexOS!.
 *
 * RexOS! is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * RexOS! is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with RexOS!.  If not, see <https://www.gnu.org/licenses/>.
 */

// REF: https://www.youtube.com/watch?v=5gllcRuq8Mw

#include <drivers/am79c973.h>
#include <net/IPv4.h>
#include <net/UDP.hpp>
#include <utils.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>

static UDP_provider udp_provider;
static UDP_socket  *udp_socket;
static UDP_handler udp_handler;

void *(*udp_h)(char *data, uint16_t size);

UDP_handler::UDP_handler() {}
UDP_handler::~UDP_handler() {}

void UDP_handler::handle_UDP_message(uint8_t *data, uint16_t size)
{
	if (udp_h != nullptr)
		udp_h((char *) data, size);
}

UDP_socket::UDP_socket(UDP_provider *backend)
{
	this->backend   = backend;
	this->handler   = 0;
	this->listening = false;
}

UDP_socket::~UDP_socket() {}

void UDP_socket::handle_UDP_message(uint8_t *data, uint16_t size)
{
	if (handler != 0)
		handler->handle_UDP_message(data, size);
}

void UDP_socket::send(uint8_t *data, uint16_t size)
{ backend->send(this, data, size); }

void UDP_socket::disconnect()
{ backend->disconnect(this); }

UDP_provider::UDP_provider()
{
	for (uint16_t i = 0; i < 65535; ++i)
		sockets[i] = 0;

	num_sockets = 0;
	free_port   = 1024;
}

UDP_provider::~UDP_provider() {}

bool UDP_provider::on_IP_received(
	uint32_t src_IP_BE, uint32_t dst_IP_BE, uint8_t *payload, uint32_t size
)
{
	if (size < sizeof(UDP_header_t))
		return false;

	UDP_header_t *msg  = (UDP_header_t *) payload;
	UDP_socket *socket = 0;

	for (uint16_t i = 0; i < num_sockets && socket == 0; ++i)
	{
		if (
			sockets[i]->local_port == msg->dst_port		&&
			sockets[i]->local_IP   == dst_IP_BE			&&
			sockets[i]->listening
		)
		{
			socket = sockets[i];

			socket->listening = false;
			socket->remote_port = msg->src_port;
			socket->remote_IP = src_IP_BE;
		}
		else if (
			sockets[i]->local_port  == msg->dst_port	&&
			sockets[i]->local_IP    == dst_IP_BE		&&
			sockets[i]->remote_port == msg->src_port	&&
			sockets[i]->remote_IP   == src_IP_BE
		)
			socket = sockets[i];
	}

	if (socket != 0)
		socket->handle_UDP_message(
			payload + sizeof(UDP_header_t), size - sizeof(UDP_header_t)
		);

	return false;
}

UDP_socket *UDP_provider::connect(uint32_t ip, uint16_t port)
{
	UDP_socket *socket = (UDP_socket *) malloc(sizeof(UDP_socket));
	
	if (socket != 0)
	{
		socket					= new UDP_socket(this);
		
		socket->remote_port		= BIGENDIAN(port);
		socket->remote_IP		= ip;

		uint16_t rport			= free_port++;
		
		socket->local_port		= BIGENDIAN(rport);
		socket->local_IP		= am79c973_get_IP_address();
		
		sockets[num_sockets++]	= socket;
	}
	
	return socket;
}

UDP_socket *UDP_provider::listen(uint16_t port)
{
	UDP_socket *socket = (UDP_socket *) malloc(sizeof(UDP_socket));
	
	if (socket != 0)
	{
		socket					= new UDP_socket(this);
		
		socket->listening		= true;
		socket->local_port		= BIGENDIAN(port);
		socket->local_IP		= am79c973_get_IP_address();

		sockets[num_sockets++] = socket;
	}
	
	return socket;
}

UDP_socket *UDP_provider::new_socket(uint32_t ip, uint16_t port)
{
	UDP_socket *socket			= (UDP_socket *) malloc(sizeof(UDP_socket));

	if (socket != 0)
	{
		socket					= new UDP_socket(this);

		socket->listening		= true;
		socket->remote_port		= BIGENDIAN(port);
		socket->remote_IP		= ip;

		uint16_t rport			= free_port++;
		
		socket->local_port		= BIGENDIAN(rport);
		socket->local_IP		= am79c973_get_IP_address();
		
		sockets[num_sockets++]	= socket;
	}

	return socket;
}

void UDP_provider::disconnect(UDP_socket *socket)
{
	for (uint16_t i = 0; i < num_sockets && socket == 0; ++i)
		if (sockets[i] == socket)
		{
			sockets[i] = sockets[--num_sockets];
			free(socket);
			break;
		}
}

void UDP_provider::send(UDP_socket *socket, uint8_t *data, uint16_t size)
{
	uint16_t total_length	= size + sizeof(UDP_header_t);
	uint8_t  *buffer		= (uint8_t *) malloc(total_length);
	uint8_t	 *buffer2		= buffer + sizeof(UDP_header_t);
	
	UDP_header_t *msg		= (UDP_header_t *) buffer;
	
	msg->src_port			= socket->local_port;
	msg->dst_port			= socket->remote_port;
	msg->length				= BIGENDIAN(total_length);
	
	for (uint32_t i = 0; i < size; ++i)
		buffer2[i] = data[i];
	
	msg -> checksum = 0;
	ip_send(socket->remote_IP, PROTOCOL_TYPE_UDP, buffer, total_length);

	free(buffer);
}

void UDP_provider::bind(UDP_socket *socket, UDP_handler *handler)
{ socket->handler = handler; }

bool UDP_on_IP_received(
	uint32_t src_IP_BE, uint32_t dst_IP_BE, uint8_t *payload, uint32_t size
)
{ return udp_provider.on_IP_received(src_IP_BE, dst_IP_BE, payload, size); }

void UDP_connect(uint32_t ip, uint16_t port)
{
	udp_socket = udp_provider.new_socket(ip, port);
	udp_provider.bind(udp_socket, &udp_handler);
	UDP_send((uint8_t *) "\0", 1);
}

void UDP_listen(uint16_t port)
{
	udp_socket = udp_provider.listen(port);
	udp_provider.bind(udp_socket, &udp_handler);
}

void UDP_send(uint8_t *message, uint16_t size)
{ udp_socket->send(message, size); }

void UDP_disconnect()
{
	UDP_set_handler(nullptr);
	UDP_send(0, 0);
	udp_socket->disconnect();
}

void UDP_set_handler(void *h)
{ udp_h = (void *(*)(char *, uint16_t)) h; }

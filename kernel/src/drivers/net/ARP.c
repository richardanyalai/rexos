/* This file is part of RexOS!.
 *
 * RexOS! is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * RexOS! is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with RexOS!.  If not, see <https://www.gnu.org/licenses/>.
 */

// REF: https://www.youtube.com/watch?v=5Fw77uSewWI&list=PLHh55M_Kq4OApWScZyPl5HhgsTJS9MZ6M&index=23

#include <drivers/am79c973.h>
#include <net/ARP.h>
#include <net/etherframe.h>
#include <utils.h>
#include <stdint.h>
#include <unistd.h>

uint32_t IP_cache[128];
uint64_t MAC_cache[128];

uint8_t num_cache_entries = 0;

void arp_send(uint64_t dst_MAC_BE, uint8_t *buffer)
{ efp_send(dst_MAC_BE, ETHER_FRAME_TYPE_ARP, buffer, sizeof(arp_msg_t)); }

bool arp_on_ether_frame_received(uint8_t *payload, uint32_t size)
{
	if (size < sizeof(arp_msg_t))
		return false;

	arp_msg_t *arp = (arp_msg_t *) payload;

	if (arp->hardware_type == 0x0100)
	{
		if (
			arp->protocol			== 0x0008						&&
			arp->hardware_addr_size	== 6							&&
			arp->protocol_addr_size	== 4							&&
			arp->dst_IP				== am79c973_get_IP_address()
		)
		{
			switch (arp->command)
			{
				case 0x0100:
					arp->command = 0x0200;
					arp->dst_IP  = arp->src_IP;
					arp->dst_MAC = arp->src_MAC;
					arp->src_IP  = am79c973_get_IP_address();
					arp->src_MAC = am79c973_get_MAC_address();
					return true;

				case 0x0200:
					if (num_cache_entries < 128)
					{
						IP_cache[num_cache_entries]  = arp->src_IP;
						MAC_cache[num_cache_entries] = arp->src_MAC;
						num_cache_entries++;
					}
					break;
				
				default: return false;
			}
		}
	}

	return false;
}

void arp_broadcast_MAC_address(uint32_t IP_BE)
{
	arp_msg_t arp =
	{
		.hardware_type		= 0x0100,	// ethernet
		.protocol			= 0x0008,	// IPv4
		.hardware_addr_size	= 6,		// MAC
		.protocol_addr_size	= 4,		// IPv4
		.command			= 0x0200,	// request

		.src_MAC			= am79c973_get_MAC_address(),
		.src_IP				= am79c973_get_IP_address(),
		.dst_MAC			= arp_resolve(IP_BE),
		.dst_IP				= IP_BE
	};

	arp_send(arp.dst_MAC, (uint8_t *) &arp);
}

void arp_request_MAC_address(uint32_t IP_BE)
{
	arp_msg_t arp =
	{
		.hardware_type		= 0x0100,	// ethernet
		.protocol			= 0x0008,	// IPv4
		.hardware_addr_size	= 6,		// MAC
		.protocol_addr_size	= 4,		// IPv4
		.command			= 0x0100,	// request
		.src_MAC			= am79c973_get_MAC_address(),
		.src_IP				= am79c973_get_IP_address(),
		.dst_MAC			= 0xFFFFFFFFFFFF,
		.dst_IP				= IP_BE
	};

	arp_send(arp.dst_MAC, (uint8_t *) &arp);
}

uint64_t arp_get_MAC_from_cache(uint32_t IP_BE)
{
	for (uint8_t i = 0; i < num_cache_entries; ++i)
		if (IP_cache[i] == IP_BE)
			return MAC_cache[i];

	return 0xFFFFFFFFFFFF;

}

uint64_t arp_resolve(uint32_t IP_BE)
{
	uint64_t result = arp_get_MAC_from_cache(IP_BE);

	if (result == 0xFFFFFFFFFFFF)
		arp_request_MAC_address(IP_BE);

	while (result == 0xFFFFFFFFFFFF)
		result = arp_get_MAC_from_cache(IP_BE);

	return result;
}

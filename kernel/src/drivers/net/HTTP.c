/* This file is part of RexOS!.
 *
 * RexOS! is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * RexOS! is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with RexOS!.  If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * Useful links:
 * REF: https://www.tutorialspoint.com/http/http_requests.htm
 */

#include <stdbool.h>
#include <net/TCP.hpp>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef enum
{
	TYPE_GET,
	TYPE_HEAD,
	TYPE_POST,
	TYPE_PUT,
	TYPE_DELETE,
	TYPE_CONNECT,
	TYPE_OPTIONS,
	TYPE_TRACE,
	TYPE_PATCH
} method_type_t;

char *method_types[] =
{
	"GET",
	"HEAD",
	"POST",
	"PUT",
	"DELETE",
	"CONNECT",
	"OPTIONS",
	"TRACE",
	"PATCH"
};

uint32_t IP_from_string(char *);

void send_request(method_type_t type, const char *url, const char *data)
{
	char ip[16] = { 0 }, port[6] = { 0 };

	if (strstr(url, "https://"))
	{
		port[0] = '4';
		port[1] = '4';
		port[2] = '3';

		url += 8;
	}
	else
	{
		port[0] = '8';
		port[1] = '0';

		url += 7;
	}

	if (strstr(url, "localhost"))
	{
		url += 9;

		ip[0] = '1';
		ip[1] = '2';
		ip[2] = '7';
		ip[3] = '.';
		ip[4] = '0';
		ip[5] = '.';
		ip[6] = '0';
		ip[7] = '.';
		ip[8] = '1';
	}

	uint8_t idx = 0, whole_idx = 0;
	bool was_delim = false;

	for (uint8_t i = 0; url[i] != '\0'; ++i)
	{
		if (url[i] == '/')
			break;

		whole_idx++;

		if (url[i] == ':')
		{
			was_delim = true;
			idx = 0;

			continue;
		}

		if (was_delim)
			port[idx++] = url[i];
		else
			ip[idx++] = url[i];
	}

	char *endpoint = (char *) malloc(sizeof(char) * (strlen(url) - whole_idx));

	strcpy(endpoint, url + whole_idx);

	uint32_t IP = IP_from_string(ip);
	uint16_t PORT = atoi((char *) port);

	TCP_connect(IP, PORT);

	char *message = (char *) malloc(sizeof(char) * 700);
	memset(message, 0, 700);

	char *data_content = (char *) malloc(sizeof(char) * 500);
	memset(data_content, 0, 500);

	if (type != TYPE_GET)
		sprintf(
			data_content,
			"Content-Type: application/json\r\n"
			"Content-Length: %d\r\n\r\n%s\r\n",
			strlen(data),
			data
		);

	sprintf(
		message,
		"%s %s HTTP/2.0\r\n"
		"Host: %s:%s\r\n"
		"User-Agent: RexOS/2.0\r\n"
		"Connection: Keep-Alive\r\n"
		"Accept: */*\r\n"
		"%s\r\n",
		method_types[type],
		endpoint,
		ip,
		port,
		type != TYPE_GET ? "" : data_content
	);

	size_t size = strlen(message);

	TCP_send((uint8_t *) message, size);

	free(message);
	free(data_content);
}

void GET(const char *url)
{ send_request(TYPE_GET, url, NULL); }

void POST(const char *url, const char *data)
{ send_request(TYPE_POST, url, data); }

void PUT(const char *url, const char *data)
{ send_request(TYPE_PUT, url, data); }

void PATCH(const char *url, const char *data)
{ send_request(TYPE_PATCH, url, data); }

void DELETE(const char *url, const char *data)
{ send_request(TYPE_DELETE, url, data); }

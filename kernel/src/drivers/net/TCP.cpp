/* This file is part of RexOS!.
 *
 * RexOS! is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * RexOS! is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with RexOS!.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <drivers/am79c973.h>
#include <drivers/fs/fs.h>
#include <net/HTTP.h>
#include <net/IPv4.h>
#include <net/TCP.hpp>
#include <sys/isr.h>
#include <utils.h>
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define LOGGING false

static TCP_provider tcp_provider;
static TCP_socket  *tcp_socket;
static TCP_handler tcp_handler;

TCP_handler::TCP_handler() {}
TCP_handler::~TCP_handler() {}

bool TCP_handler::handle_TCP_message(
	TCP_socket *socket, uint8_t *data, uint16_t size
)
{
	if (
		size > 9
		&& data[0] == 'G'
		&& data[1] == 'E'
		&& data[2] == 'T'
		&& data[3] == ' '
		&& data[4] == '/'
		&& data[5] == ' '
		&& data[6] == 'H'
		&& data[7] == 'T'
		&& data[8] == 'T'
		&& data[9] == 'P'
	)
	{
		uint32_t i = 0;
		struct dirent *node = 0;

		while ((node = readdir_fs(fs_root, i)) != 0)
		{
			if (strcmp(node->name, "index.html") == 0)
			{
				fs_node_t *fsnode = finddir_fs(fs_root, node->name);

				char *buf  = new char[10000];
				char *buf2 = new char[10000];

				for (uint16_t i = 0; i < 10000; ++i)
					buf[i] = buf2[i] = '\0';

				strcpy(
					buf,
					"HTTP/1.1 200 OK\r\nServer: "
					"MyOS\r\nContent-Type: text/html\r\n\r\n"
				);

				uint32_t sz = read_fs(fsnode, 0, 10000, buf2);

				buf2[sz]   = '\r';
				buf2[sz+1] = '\n';
				buf2[sz+2] = '\0';

				strcat(buf, buf2);

				socket->send((uint8_t *) buf, strlen(buf) - 1);

#pragma GCC diagnostic ignored "-Wmismatched-new-delete"
				delete buf;
				delete buf2;
#pragma GCC diagnostic pop

				break;
			}
			i++;
		}
		socket->disconnect();
	} else {
		if (LOGGING)
			for (uint16_t i = 0; i < size; ++i)
				printf("%c", data[i]);

		http_code code = HTTP_OK;
		char http_code_str[4] = { 0 };

		http_code_str[0] = data[9];
		http_code_str[1] = data[10];
		http_code_str[2] = data[11];

		code = (http_code) atoi(http_code_str);

		char res_code[20] = { 0 };

		sprintf(
			res_code,
			"%s%d${F15}",
			code < 400 ? "${F10}" : "${F04}",
			code
		);

		printf(res_code);

		size_t content_length = 0;
		char *length_o = strstr((char *) data, "Content-Length: ");

		if (length_o)
		{
			length_o += 16;

			char length[4];
			uint8_t idx = 0;

			memset(length, 0 , 4);

			for (uint8_t i = 0; i < 5; ++i)
			{
				if (length_o[i] == '\n')
					break;

				if (isdigit(length_o[i]))
					length[idx++] = length_o[i];
			}

			content_length = atoi(length);
		}

		char *body = (char *) malloc(sizeof(char) * content_length);
		char *alive_o = strstr((char *) data, "Keep-Alive: timeout=5\r\n\r\n");
		
		alive_o += 25;

		for (uint16_t i = 0; i < content_length; ++i)
			body[i] = alive_o[i];

		for (uint16_t i = 0; i < content_length; ++i)
		{
			if (i % 80 == 0)
				printf("\n");

			printf("%c", body[i]);
		}

		free(body);
	}

	return true;
}

TCP_socket::TCP_socket(TCP_provider *backend)
{
	this->backend	= backend;
	handler			= 0;
	state			= CLOSED;
}

TCP_socket::~TCP_socket() {}

bool TCP_socket::handle_TCP_message(uint8_t *data, uint16_t size)
{
	if (handler != 0)
		return handler->handle_TCP_message(this, data, size);

	return false;
}

void TCP_socket::send(uint8_t *data, uint16_t size)
{
	while (state != ESTABLISHED)
		HALT;

	backend->send(this, data, size, PSH | ACK);
}

void TCP_socket::disconnect()
{ backend->disconnect(this); }

TCP_provider::TCP_provider()
{
	for (uint16_t i = 0; i < 65535; ++i)
		sockets[i] = 0;

	num_sockets = 0;
	free_port = 1024;
}

TCP_provider::~TCP_provider() {}

uint32_t BIGENDIAN32(uint32_t x)
{
	return ((x & 0xFF000000) >> 24)
		 | ((x & 0x00FF0000) >>  8)
		 | ((x & 0x0000FF00) <<  8)
		 | ((x & 0x000000FF) << 24);
}

bool TCP_provider::on_IP_received(
	uint32_t src_IP_BE, uint32_t dst_IP_BE, uint8_t *payload, uint32_t size
)
{
	if (size < 20)
		return false;

	TCP_header_t *msg  = (TCP_header_t *) payload;
	TCP_socket *socket = 0;

	for (uint16_t i = 0; i < num_sockets && socket == 0; ++i)
	{
		if (
			sockets[i]->local_port	== msg->dst_port	&&
			sockets[i]->local_IP	== dst_IP_BE		&&
			sockets[i]->state		== LISTEN			&&
			(((msg -> flags) & (SYN | ACK)) == SYN)
		)
			socket = sockets[i];
		else if (
			sockets[i]->local_port	== msg->dst_port	&&
			sockets[i]->local_IP	== dst_IP_BE		&&
			sockets[i]->remote_port	== msg->src_port	&&
			sockets[i]->remote_IP	== src_IP_BE
		)
			socket = sockets[i];
	}

	bool reset = false;
	
	if (socket != 0 && msg->flags & RST)
		socket->state = CLOSED;

	if (socket != 0 && socket->state != CLOSED)
	{
		switch ((msg->flags) & (SYN | ACK | FIN))
		{
			case SYN:
				if (socket -> state == LISTEN)
				{
					socket->state		= SYN_RECEIVED;
					socket->remote_port	= msg->src_port;
					socket->remote_IP	= src_IP_BE;
					socket->ack_num		= BIGENDIAN32( msg->seq_num ) + 1;
					socket->seq_num		= 0xBEEFCAFE;
					send(socket, 0, 0, SYN|ACK);
					socket->seq_num++;
				}
				else
					reset = true;
				break;

			case SYN | ACK:
				if (socket->state == SYN_SENT)
				{
					socket->state   = ESTABLISHED;
					socket->ack_num = BIGENDIAN32(msg->seq_num) + 1;
					socket->seq_num++;
					send(socket, 0, 0, ACK);
				}
				else
					reset = true;
				break;

			case SYN | FIN:
			case SYN | FIN | ACK:
				reset = true;
				break;

			case FIN:
			case FIN|ACK:
				if (socket->state == ESTABLISHED)
				{
					socket->state = CLOSE_WAIT;
					socket->ack_num++;
					send(socket, 0, 0, ACK);
					send(socket, 0, 0, FIN|ACK);
				}
				else if (socket->state == CLOSE_WAIT)
					socket->state = CLOSED;
				else if (
					socket->state == FIN_WAIT_1 || socket->state == FIN_WAIT_2
				)
				{
					socket->state = CLOSED;
					socket->ack_num++;
					send(socket, 0, 0, ACK);
				}
				else
					reset = true;

				break;

			case ACK:
				if (socket->state == SYN_RECEIVED)
				{
					socket->state = ESTABLISHED;
					return false;
				}
				else if (socket->state == FIN_WAIT_1)
				{
					socket->state = FIN_WAIT_2;
					return false;
				}
				else if (socket->state == CLOSE_WAIT)
				{
					socket->state = CLOSED;
					break;
				}

				if (msg->flags == ACK)
					break;

				goto TCP_DEFAULT;

				break;

			default:
TCP_DEFAULT:
				if (BIGENDIAN32(msg->seq_num) == socket->ack_num)
				{
					if (
						(socket->handle_TCP_message(
							payload + msg->header_size_32 * 4,
							size    - msg->header_size_32 * 4
						))
					)
					{
						uint16_t x = 0;

						for (
							uint16_t i = msg->header_size_32 * 4;
							i < size;
							++i
						)
							if (payload[i] != 0)
								x = i;

						socket->ack_num += x - msg->header_size_32 * 4 + 1;
						send(socket, 0, 0, ACK);
					}
				}
				else
					reset = true;	// Data is in the wrong order.
		}
	}

	if (reset)
	{
		if (socket != 0)
			send(socket, 0, 0, RST);
		else
		{
			TCP_socket socket(this);
			socket.remote_port	= msg->src_port;
			socket.remote_IP	= src_IP_BE;
			socket.local_port	= msg->dst_port;
			socket.local_IP		= dst_IP_BE;
			socket.seq_num		= BIGENDIAN32(msg->ack_num);
			socket.ack_num		= BIGENDIAN32(msg->seq_num) + 1;
			send(&socket, 0, 0, RST);
		}
	}

	if (socket != 0 && socket->state == CLOSED)
		for (uint16_t i = 0; i < num_sockets && socket == 0; ++i)
			if (sockets[i] == socket)
			{
				sockets[i] = sockets[--num_sockets];
				free(socket);
				break;
			}

	return false;
}

void TCP_provider::send(
	TCP_socket *socket, uint8_t *data, uint16_t size, uint16_t flags
)
{
	uint16_t total_length		= size + sizeof(TCP_header_t);
	uint16_t length_incl_phdr	= total_length + sizeof(TCP_pseudo_header_t);
	
	uint8_t *buffer				= (uint8_t *) malloc(length_incl_phdr);
	
	TCP_pseudo_header_t *phdr	= (TCP_pseudo_header_t *) buffer;
	TCP_header_t *msg			=
	(TCP_header_t *)(buffer + sizeof(TCP_pseudo_header_t));
	uint8_t *buffer2			= buffer + sizeof(TCP_header_t)
								+ sizeof(TCP_pseudo_header_t);
	
	msg->header_size_32			= sizeof(TCP_header_t) / 4;
	msg->src_port				= socket->local_port;
	msg->dst_port				= socket->remote_port;
	
	msg->ack_num				= BIGENDIAN32(socket->ack_num);
	msg->seq_num				= BIGENDIAN32(socket->seq_num);
	msg->reserved				= 0;
	msg->flags					= flags;
	msg->window_size			= 0xFFFF;
	msg->urgent_ptr				= 0;

	msg->options				= ((flags & SYN) != 0) ? 0xB4050402 : 0;

	socket->seq_num				+= size;

	for (uint16_t i = 0; i < size; ++i)
		buffer2[i] = data[i];

	phdr->src_IP				= socket->local_IP;
	phdr->dst_IP				= socket->remote_IP;
	phdr->protocol				= 0x0600;
	phdr->total_length			= BIGENDIAN(total_length);

	msg -> checksum				= 0;
	msg -> checksum				= ip_checksum(
		(uint16_t *) buffer, length_incl_phdr
	);

	ip_send(socket->remote_IP, 0x06, (uint8_t *) msg, total_length);
	free(buffer);
}

TCP_socket *TCP_provider::connect(uint32_t ip, uint16_t port)
{
	TCP_socket *socket = (TCP_socket *) malloc(sizeof(TCP_socket));

	if (socket != 0)
	{
		socket					= new TCP_socket(this);

		socket->remote_port		= BIGENDIAN(port);
		socket->remote_IP		= ip;
		socket->local_port		= BIGENDIAN(free_port);
		free_port++;
		socket->local_IP		= am79c973_get_IP_address();

		sockets[num_sockets++]	= socket;
		socket->state			= SYN_SENT;

		socket->seq_num			= 0xBEEFCAFE;

		send(socket, 0, 0, SYN);
	}

	return socket;
}

void TCP_provider::disconnect(TCP_socket *socket)
{
	socket->state = FIN_WAIT_1;
	send(socket, 0, 0, FIN + ACK);
	socket->seq_num++;
}

TCP_socket *TCP_provider::listen(uint16_t port)
{
	TCP_socket *socket = (TCP_socket *) malloc(sizeof(TCP_socket));

	if (socket != 0)
	{
		socket					= new TCP_socket(this);

		socket->state			= LISTEN;
		socket->local_IP		= am79c973_get_IP_address();
		socket->local_port		= BIGENDIAN(port);

		sockets[num_sockets++]	= socket;
	}

	return socket;
}

void TCP_provider::bind(TCP_socket *socket, TCP_handler *handler)
{ socket->handler = handler; }

bool TCP_on_IP_received(
	uint32_t src_IP_BE, uint32_t dst_IP_BE, uint8_t *payload, uint32_t size
)
{ return tcp_provider.on_IP_received(src_IP_BE, dst_IP_BE, payload, size); }

void TCP_connect(uint32_t ip, uint16_t port)
{
	tcp_socket = tcp_provider.connect(ip, port);
	tcp_provider.bind(tcp_socket, &tcp_handler);
}

void TCP_listen(uint16_t port)
{
	tcp_socket = tcp_provider.listen(port);
	tcp_provider.bind(tcp_socket, &tcp_handler);
}

void TCP_send(uint8_t *message, uint16_t size)
{ tcp_socket->send(message, size); }

void TCP_disconnect()
{ tcp_socket->disconnect(); }

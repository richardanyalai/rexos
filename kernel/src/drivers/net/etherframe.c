#include <drivers/am79c973.h>
#include <net/ARP.h>
#include <net/etherframe.h>
#include <net/ICMP.h>
#include <net/IPv4.h>
#include <utils.h>
#include <stdbool.h>
#include <stdlib.h>

// REF: https://www.youtube.com/watch?v=fU0tMBxEXoA&list=PLHh55M_Kq4OApWScZyPl5HhgsTJS9MZ6M&index=22

bool (*ef_handlers[65535])();

void init_ether_frame_handlers()
{
	for (uint32_t i = 0; i < 65535; ef_handlers[i++] = false);

	ef_handlers[ETHER_FRAME_TYPE_ARP]  = &arp_on_ether_frame_received;
	ef_handlers[ETHER_FRAME_TYPE_IPv4] = &ip_on_ether_frame_received;

	init_IPv4_handlers();
}

bool efp_on_raw_data_received(uint8_t *buffer, uint32_t size)
{
	if (size < sizeof(ether_frame_header_t))
		return false;

	ether_frame_header_t *frame = (ether_frame_header_t *) buffer;

	bool send_back = false;

	if (
		frame->dst_MAC_BE == 0xFFFFFFFFFFFF ||
		frame->dst_MAC_BE == am79c973_get_MAC_address()
	)
		send_back = ef_handlers[frame->ether_type_BE](
		   buffer + sizeof(ether_frame_header_t),
		   size   - sizeof(ether_frame_header_t)
		);

	if (send_back)
	{
		frame->dst_MAC_BE = frame->src_MAC_BE;
		frame->src_MAC_BE = am79c973_get_MAC_address();
	}

	return send_back;
}

void efp_send(
	uint64_t dst_MAC_BE, uint16_t ether_type_BE, uint8_t *buffer, uint32_t size
)
{
	uint8_t *buffer2			=
		(uint8_t *) malloc(sizeof(ether_frame_header_t) + size);
	ether_frame_header_t *frame	= (ether_frame_header_t *) buffer2;
			
	frame->dst_MAC_BE    = dst_MAC_BE;
	frame->src_MAC_BE    = am79c973_get_MAC_address();
	frame->ether_type_BE = ether_type_BE;
			
	uint8_t *src = buffer;
	uint8_t *dst = buffer2 + sizeof(ether_frame_header_t);

	for (uint32_t i = 0; i < size; ++i)
		dst[i] = src[i];

	am79c973_send(buffer2, size + sizeof(ether_frame_header_t));

	free(buffer2);
}

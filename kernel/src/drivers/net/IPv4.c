/* This file is part of RexOS!.
 *
 * RexOS! is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * RexOS! is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with RexOS!.  If not, see <https://www.gnu.org/licenses/>.
 */

// REF: https://www.youtube.com/watch?v=swHIUtgdo5U

#include <drivers/am79c973.h>
#include <net/ARP.h>
#include <net/etherframe.h>
#include <net/ICMP.h>
#include <net/IPv4.h>
#include <net/TCP.hpp>
#include <net/UDP.hpp>
#include <utils.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>

extern uint32_t gateway_IP, subnet_mask;

bool (*ip_handlers[255])();

void init_IPv4_handlers()
{
	for (uint16_t i = 0; i < 255; ip_handlers[i++] = false);

	ip_handlers[PROTOCOL_TYPE_ICMP] = &ICMP_on_IP_received;
	ip_handlers[PROTOCOL_TYPE_UDP]  = &UDP_on_IP_received;
	ip_handlers[PROTOCOL_TYPE_TCP]  = &TCP_on_IP_received;
}

uint16_t ip_checksum(uint16_t *data, uint32_t length_in_bytes)
{
	uint32_t temp = 0;

	for (uint32_t i = 0; i < length_in_bytes / 2; ++i)
		temp += BIGENDIAN(data[i]);

	if (length_in_bytes % 2)
		temp += ((uint16_t)((char *) data)[length_in_bytes-1]) << 8;

	while (temp & 0xFFFF0000)
		temp = (temp & 0xFFFF) + (temp >> 16);

	return ((~temp & 0xFF00) >> 8) | ((~temp & 0x00FF) << 8);
}

void ip_send(
	uint32_t dst_IP_BE, uint8_t protocol, uint8_t *data, uint32_t size
)
{
	uint8_t *buffer = (uint8_t *) malloc(sizeof(IPv4_msg_t) + size);

	IPv4_msg_t *message = (IPv4_msg_t *) buffer;

	message->version			= 4;
	message->header_length		= sizeof(IPv4_msg_t) / 4;
	message->tos				= 0;
	message->total_length		= size + sizeof(IPv4_msg_t);
	message->total_length		= ((message->total_length & 0xFF00) >> 8)
								| ((message->total_length & 0x00FF) << 8);
	message->ident				= 0x0100;
	message->flags_and_offset	= 0x0040;
	message->time_to_live		= 0x40;
	message->protocol			= protocol;
	
	message->dst_IP				= dst_IP_BE;
	message->src_IP				= am79c973_get_IP_address();
	message->checksum			= 0;
	message->checksum			= ip_checksum(
		(uint16_t *) message, sizeof(IPv4_msg_t)
	);

	uint8_t *databuffer = buffer + sizeof(IPv4_msg_t);

	for (uint32_t i = 0; i < size; ++i)
		databuffer[i] = data[i];
	
	uint32_t route = dst_IP_BE;

	if ((dst_IP_BE & subnet_mask) != (message->src_IP & subnet_mask))
		route = gateway_IP;

	efp_send(
		arp_resolve(route),
		ETHER_FRAME_TYPE_IPv4,
		buffer,
		sizeof(IPv4_msg_t) + size
	);
	
	free(buffer);
}

bool ip_on_ether_frame_received(uint8_t *payload, uint32_t size)
{
	if (size < sizeof(IPv4_msg_t))
		return false;
	
	IPv4_msg_t *ip_message = (IPv4_msg_t *) payload;

	bool send_back = false;
	
	if (ip_message->dst_IP == am79c973_get_IP_address())
	{
		int length = ip_message->total_length;

		if (length > (int) size)
			length = size;

		send_back = ip_handlers[ip_message->protocol](
			ip_message->src_IP,
			ip_message->dst_IP,
			payload + 4 * ip_message->header_length,
			length  - 4 * ip_message->header_length
		);
	}
	
	if (send_back)
	{
		uint32_t temp = ip_message->dst_IP;

		ip_message->dst_IP = ip_message->src_IP;
		ip_message->src_IP = temp;
		
		ip_message->time_to_live = 0x40;
		ip_message->checksum     = 0;
		ip_message->checksum     = ip_checksum(
			(uint16_t *) ip_message, 4 * ip_message->header_length
		);
	}
	
	return send_back;
}

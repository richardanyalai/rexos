/* This file is part of RexOS!.
 *
 * RexOS! is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * RexOS! is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with RexOS!.  If not, see <https://www.gnu.org/licenses/>.
 */

// REF: https://www.youtube.com/watch?v=HDhG5Vzzxjk

#include <net/ICMP.h>
#include <net/IPv4.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>

extern bool am79c973_ready;

void *(*ICMP_ping_handler)(uint8_t data[4]);

bool ICMP_on_IP_received(
	uint32_t src_IP_BE, uint32_t dst_IP_BE, uint8_t *payload, uint32_t size
)
{
	(void) dst_IP_BE;

	if (am79c973_ready)
	{
		if (size < sizeof(icmp_msg_t))
			return false;

		icmp_msg_t *msg = (icmp_msg_t *) payload;

		uint8_t IP[4];

		switch (msg->type)
		{
			// Answer to a ping
			case 0:
				IP[0] = (src_IP_BE)			& 0xFF;
				IP[1] = (src_IP_BE >>  8)	& 0xFF;
				IP[2] = (src_IP_BE >> 16)	& 0xFF;
				IP[3] = (src_IP_BE >> 24)	& 0xFF;

				if (ICMP_ping_handler)
					ICMP_ping_handler(IP);

				break;

			// Ping
			case 8:
				msg->type		= 0;
				msg->checksum	= 0;
				msg->checksum	= ip_checksum(
					(uint16_t *) &msg, sizeof(icmp_msg_t)
				);
				return true;
			
			default:
				break;
		}
	}

	return false;
}

void request_echo_reply(uint32_t IP_BE)
{
	icmp_msg_t msg	=
	{
		.type		= 8,		// Ping
		.code		= 0,
		.data		= 0x3713,	// 1337
		.checksum	= 0
	};

	msg.checksum = ip_checksum((uint16_t *) &msg, sizeof(icmp_msg_t));

	ip_send(IP_BE, PROTOCOL_TYPE_ICMP, (uint8_t *) &msg, sizeof(icmp_msg_t));
}

void ICMP_set_ping_handler(void *handler)
{ ICMP_ping_handler = handler; }

/* This file is part of RexOS!.
 *
 * RexOS! is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * RexOS! is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with RexOS!.  If not, see <https://www.gnu.org/licenses/>.
 */

// code from REF: https://wiki.osdev.org/Parallel_port

#include <io/ports.h>
#include <stdint.h>
#include <unistd.h>

void parallel_write_byte(uint8_t data)
{
	uint8_t ctrl;

	// Wait for the printer to be receptive.
	while ((!inb(0x379)) & 0x80)
		usleep(10);

	// Now put the data onto the data lines.
	outb(0x378, data);

	// Now pulse the strobe line to tell the printer to read the data.
	ctrl = inb(0x37A);
	outb(0x37A, ctrl | 1);
	usleep(10);
	outb(0x37A, ctrl);

	// Now wait for the printer to finish processing.
	while ((!inb(0x379)) & 0x80)
		usleep(10);
}

void parallel_write(const char *str)
{ for (uint16_t i = 0; str[i] != '\0'; parallel_write_byte(str[i++])); }

/* This file is part of RexOS!.
 *
 * RexOS! is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * RexOS! is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with RexOS!.  If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * Useful links:
 * REF: https://wiki.osdev.org/PCI
 * REF: https://en.wikipedia.org/wiki/PCI_configuration_space
 * 
 * PCI vendor & device lists:
 * REF: https://devicehunt.com
 * REF: https://pci-ids.ucw.cz/read/PC/
 * 
 * PCI classes and subclasses can be found here:
 * REF: http://www.lowlevel.eu/wiki/Peripheral_Component_Interconnect

 * I found this repository very useful to understand how to detect PCI devices:
 * REF: https://github.com/levex/osdev/blob/master/drivers/pci/pci.c
 * 
 * The video that helped me the most:
 * REF: https://www.youtube.com/watch?v=GE7iO2vlLD4
 * 
 * Base Address Registers:
 * REF: https://www.youtube.com/watch?v=yqjDYF4NCXg&t=1932s
 * 
 * Big thanks to Levente Kurusa <levex@linux.com> and Viktor Engelmann
 */

#include <drivers/am79c973.h>
#include <drivers/ATA.h>
#include <drivers/PCI.h>
#include <io/ports.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

pci_device_t **pci_devices = 0;
pci_driver_t **pci_drivers = 0;

uint32_t devs, drivs;

char *classes[] =
{
/* 00 */	"Unclassified device",
/* 01 */	"Mass storage controller",
/* 02 */	"Network controller",
/* 03 */	"Display controller",
/* 04 */	"Multimedia controller",
/* 05 */	"Memory controller",
/* 06 */	"Bridge",
/* 07 */	"Communication controller",
/* 08 */	"Generic system peripheral",
/* 09 */	"Input device controller",
/* 0A */	"Docking station",
/* 0B */	"Processor",
/* 0C */	"Serial bus controller",
/* 0D */	"Wireless controller",
/* 0E */	"Intelligent controller",
/* 0F */	"Satellite communications controller",
/* 10 */	"Encryption controller",
/* 11 */	"Signal processing controller",
/* 12 */	"Processing accelerators",
/* 13 */	"Non-Essential Instrumentation",
/* 14 */	"",
/* 15 */	"",
/* 40 */	"Coprocessor",
/* 64 */	"",
/* FF */	"Unassigned class"
};

char *vendors[] =
{
/* ???? */	"Unknown",
/* 1022 */	"AMD",
/* 106B */	"Apple, Inc.",
/* 1234 */	"QEMU",
/* 1AF4 */	"Red Hat, Inc.",
/* 80EE */	"VirtualBox",
/* 8086 */	"Intel Corporation"
};

char *devices[] =
{
/* UNKNOWN */	"UNKNOWN",

/* 1022:2000 */	"am79c973/Am79C975 PCnet-FAST III",

/////////////////////////////////////
///          Apple, Inc.          ///
/////////////////////////////////////

/* 106B:003F */	"KeyLargo/Intrepid USB",

/////////////////////////////////////
///              QEMU             ///
/////////////////////////////////////

/* 1234:1111 */	"QEMU Virtual Video Controller",

/////////////////////////////////////
///         Red Hat, Inc.         ///
/////////////////////////////////////

/* 1AF4:1002 */	"Virtio memory balloon",
/* 1AF4:1003 */	"Virtio console",
/* 1B36:0100 */	"QXL paravirtual graphic card",

/////////////////////////////////////
///           VirtualBox          ///
/////////////////////////////////////

/* 80EE:BEEF */	"VirtualBox Graphics Adapter",
/* 80EE:CAFE */	"VirtualBox Guest Service",

/////////////////////////////////////
///          Intel Corp.          ///
/////////////////////////////////////

/* 8086:100E */	"82540EM Gigabit Ethernet Controller",
/* 8086:100F */	"82545EM Gigabit Ethernet Controller",
/* 8086:1237 */	"440FX - 82441FX PMC [Natoma]",
/* 8086:2415 */	"82801AA AC'97 Audio Controller",
/* 8086:2668 */	"82801FB/FBM/FR/FW/FRW (ICH6 Family) High Definition Audio"
				" Controller",
/* 8086:2934 */	"82801I (ICH9 Family) USB UHCI Controller #1",
/* 8086:2935 */	"82801I (ICH9 Family) USB UHCI Controller #2",
/* 8086:2936 */	"82801I (ICH9 Family) USB UHCI Controller #3",
/* 8086:293A */	"82801I (ICH9 Family) USB2 EHCI Controller #1",
/* 8086:7000 */	"82371SB PIIX3 ISA [Natoma/Triton II]",
/* 8086:7010 */	"82371SB PIIX3 IDE [Natoma/Triton II]",
/* 8086:7111 */	"82371AB/EB/MB PIIX4 IDE",
/* 8086:7113 */	"82371AB/EB/MB PIIX4 ACPI",
};

void add_pci_device(pci_device_t *dev)
{ pci_devices[devs++] = dev; }

uint16_t pci_read_word(
	uint16_t bus, uint16_t device, uint16_t function, uint32_t offset
)
{
	uint32_t address = 0x1					<< 31
					| ((bus			& 0xFF) << 16)
					| ((device		& 0x1F) << 11)
					| ((function	& 0x07) <<  8)
					| ((offset		& 0xFC) <<  0);
	
	outl(PCI_COMMAND_PORT, address);
	
	return inl(PCI_DATA_PORT) >> (8 * (offset % 4));
}

void pci_write_word(
	uint16_t bus,
	uint16_t device,
	uint16_t function,
	uint32_t offset,
	uint32_t value
)
{
	uint32_t id	= 0x1					<< 31
				| ((bus			& 0xFF)	<< 16)
				| ((device		& 0x1F)	<< 11)
				| ((function	& 0x07)	<<  8)
				|  (offset		& 0xFC);
	outw(PCI_COMMAND_PORT, id);
	outw(PCI_DATA_PORT, value);
}

bar_t get_bar(uint16_t bus, uint16_t device, uint16_t function, uint16_t bar)
{
	bar_t result = {};

	uint32_t header_type = pci_read_word(bus, device, function, 0x0E) & 0x7F;

	uint8_t max_bars = 6 - (4 * header_type);

	if (bar >= max_bars)
		return result;

	uint32_t bar_value = pci_read_word(bus, device, function, 0x10 + 4 * bar);

	result.type = (bar_value & 0x1) ? IO : MEM;

	if (result.type == MEM)
	{
		switch ((bar_value >> 1) & 0x3)
		{
			case 0: // 32-bit mode
			case 1: // 20-bit mode
			case 2: // 64-bit mode
				break;
		}
	}
	else
	{
		result.address = (uint8_t *)(bar_value & ~0x3);
		result.prefetchable = false;
	}

	return result;
}

void pci_probe()
{
	for (uint16_t bus = 0; bus < 256; ++bus)
		for (uint16_t device = 0; device < 32; ++device)
			for (uint16_t function = 0; function < 8; ++function)
			{
				uint16_t vendor = pci_read_word(bus, device, function, 0x00);

				if (vendor == 0xffff)
					continue;

				pci_device_t *dev =
				(pci_device_t *) malloc(sizeof(pci_device_t));

				for (uint8_t bar_num = 0; bar_num < 6; ++bar_num)
				{
					bar_t bar = get_bar(bus, device, function, bar_num);

					if (bar.address && (bar.type == IO))
						dev->port_base = (uint32_t) bar.address;
				}

				dev->bus         = bus;
				dev->device_num  = device;
				dev->class_id    = pci_read_word(bus, device, function, 0x0B);
				dev->subclass_id = pci_read_word(bus, device, function, 0x0A);
				dev->vendor_id   = vendor;
				dev->device_id   = pci_read_word(bus, device, function, 0x02);
				dev->interface   = pci_read_word(bus, device, function, 0x09);
				dev->function    = function;
				dev->revision    = pci_read_word(bus, device, function, 0x08);
				dev->interrupt   =
				(uint8_t) pci_read_word(bus, device, function, 0x3C) + 0x20;
				dev->driver      = 0;

				switch (dev->vendor_id)
				{
					case 0x1022:
						switch (dev->device_id)
						{
							case 0x2000:
								am79c973_init(dev);
								break;

							default: break;
						}
						break;

					case 0x8086:
						switch (dev->device_id)
						{
							case 0x7010:
							case 0x7111:
								init_ata();
								break;
							
							default: break;
						}
						break;
					
					default: break;
				}

				add_pci_device(dev);
			}
}

void pci_list()
{
	for (uint8_t i = 0; i < devs; ++i)
	{
		pci_device_t *dev = pci_devices[i];

		uint32_t class_id	= 0;
		uint32_t vendor_id	= 0;
		uint32_t device_id	= 0;

		switch (dev->class_id)
		{
			case 0x00: class_id = 0;  break;
			case 0x01: class_id = 1;  break;
			case 0x02: class_id = 2;  break;
			case 0x03: class_id = 3;  break;
			case 0x04: class_id = 4;  break;
			case 0x05: class_id = 5;  break;
			case 0x06: class_id = 6;  break;
			case 0x07: class_id = 7;  break;
			case 0x08: class_id = 8;  break;
			case 0x09: class_id = 9;  break;
			case 0x0A: class_id = 10; break;
			case 0x0B: class_id = 11; break;
			case 0x0C: class_id = 12; break;
			case 0x0D: class_id = 13; break;
			case 0x0E: class_id = 14; break;
			case 0x0F: class_id = 15; break;
			case 0x10: class_id = 16; break;
			case 0x11: class_id = 17; break;
			case 0x12: class_id = 18; break;
			case 0x13: class_id = 19; break;
			case 0x14: class_id = 20; break;
			case 0x15: class_id = 21; break;
			case 0x40: class_id = 22; break;
			case 0x64: class_id = 23; break;
			case 0xFF: class_id = 24; break;
		}

		switch (dev->vendor_id)
		{
			case 0x1022:
				vendor_id = 1;
				switch (dev->device_id)
				{
					case 0x2000: device_id = 1; break;
				}
				break;

			case 0x106B:
				vendor_id = 2;
				switch (dev->device_id)
				{
					case 0x003F: device_id = 2; break;
				}
				break;

			case 0x1234:
				vendor_id = 3;
				switch (dev->device_id)
				{
					case 0x1111: device_id = 3; break;
				}
				break;

			case 0x1AF4:
				vendor_id = 4;
				switch (dev->device_id)
				{
					case 0x1002: device_id = 4; break;
					case 0x1003: device_id = 5; break;
				}
				break;

			case 0x1B36:
				vendor_id = 4;
				switch (dev->device_id)
				{
					case 0x0100: device_id = 6; break;
				}
				break;

			case 0x80EE:
				vendor_id = 5;
				switch (dev->device_id)
				{
					case 0xBEEF: device_id = 7; break;
					case 0xCAFE: device_id = 8; break;
				}
				break;

			case 0x8086:
				vendor_id = 6;
				switch (dev->device_id)
				{
					case 0x100E: device_id = 9;  break;
					case 0x100F: device_id = 10; break;
					case 0x1237: device_id = 11; break;
					case 0x2415: device_id = 12; break;
					case 0x2668: device_id = 13; break;
					case 0x2934: device_id = 14; break;
					case 0x2935: device_id = 15; break;
					case 0x2936: device_id = 16; break;
					case 0x293A: device_id = 17; break;
					case 0x7000: device_id = 18; break;
					case 0x7010: device_id = 19; break;
					case 0x7111: device_id = 20; break;
					case 0x7113: device_id = 21; break;
				}
				break;
		}

		printf(
			"%s, %s, %s",
			classes[class_id],
			vendors[vendor_id],
			devices[device_id]
		);

		if (i < devs - 1)
			putchar('\n');
	}
}

void pci_register_driver(pci_driver_t *driv)
{
	pci_drivers[drivs++] = driv;
	return;
}

void pci_proc_dump()
{
	for (int i = 0; i < (int) devs; ++i)
	{
		pci_device_t *pci_dev = pci_devices[i];

		if (pci_dev->driver)
			printf(
				"[%.4x:%.4x:%.2x] => %s",
				pci_dev->vendor_id,
				pci_dev->device_id,
				pci_dev->function,
				pci_dev->driver->name
			);
		else
			printf(
				"%.2x.%.2x.%x %.2x%.2x: %.4x:%.4x",
				pci_dev->bus,
				pci_dev->device_num,
				pci_dev->function,
				pci_dev->class_id,
				pci_dev->subclass_id,
				pci_dev->vendor_id,
				pci_dev->device_id
			);

		if (pci_dev->revision)
			printf(" (rev %.2x)", pci_dev->revision);

		if (i < (int) devs - 1)
			putchar('\n');
	}
}

void init_pci()
{
	devs = drivs = 0;

	pci_devices = (pci_device_t **) malloc(32 * sizeof(pci_device_t));
	pci_drivers = (pci_driver_t **) malloc(32 * sizeof(pci_driver_t));

	pci_probe();
}

/* This file is part of RexOS!.
 *
 * RexOS! is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * RexOS! is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with RexOS!.  If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * Useful links:
 * REF: https://wiki.osdev.org/ATA
 * REF: https://wiki.osdev.org/ATA_PIO_Mode
 * REF: http://learnitonweb.com/2020/05/22/12-developing-an-operating-system-tutorial-episode-6-ata-pio-driver-osdev/
 * REF: https://github.com/szhou42/osdev/blob/master/src/kernel/drivers/ata.c
 * REF: https://www.youtube.com/watch?v=uS02rOvLgak&list=PLHh55M_Kq4OApWScZyPl5HhgsTJS9MZ6M&index=20
 */

#include <drivers/ATA.h>
#include <drivers/fs/MSDOS.h>
#include <io/ports.h>
#include <io/registers.h>
#include <sys/isr.h>
#include <utils.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

bool init_device(ata_device_t *device)
{
	if (device->number != 1 && device->number != 2)
		return false;

	uint16_t ATA_PORT_BASE	= (device->number == 2)
							? ATA_PORT_BASE_SEC
							: ATA_PORT_BASE_PRI;

	device->port_data				= ATA_PORT_BASE;
	device->port_error				= ATA_PORT_BASE + 0x001;
	device->port_sector_count		= ATA_PORT_BASE + 0x002;
	device->port_lba_low			= ATA_PORT_BASE + 0x003;
	device->port_lba_mid			= ATA_PORT_BASE + 0x004;
	device->port_lba_hi				= ATA_PORT_BASE + 0x005;
	device->port_device				= ATA_PORT_BASE + 0x006;
	device->port_command			= ATA_PORT_BASE + 0x007;
	device->port_control			= ATA_PORT_BASE + 0x206;

	outb(device->port_device,		device->master ? 0xA0 : 0xB0);
	outb(device->port_control,		0x00);
	outb(device->port_device,		0xA0);

	uint8_t status = inb(device->port_command);

	if (status == 0xFF)
		return false;

	outb(device->port_device,		device->master ? 0xA0 : 0xB0);
	outb(device->port_sector_count,	0x00);
	outb(device->port_lba_low,		0x00);
	outb(device->port_lba_mid,		0x00);
	outb(device->port_lba_hi,		0x00);
	outb(device->port_command,		0xEC);

	status = inb(device->port_command);

	if (status == 0x00)
		return false;

	while (((status & 0x80) == 0x80) && ((status & 0x01) != 0x01))
		status = inb(device->port_command);

	if (status & 0x01)
		return false;

	for (int i = 0; i < 256; ++i)
	{
		uint16_t data = inw(device->port_data);
		char *text = "  \0";
		text[0] = (data >> 8) & 0xFF;
		text[1] = data & 0xFF;
	}

	KINF(
		"ATA %s %s",
		device->number == 1 ? "Primary" : "Secondary",
		device->master ? "Master" : "Slave"
	);

	return true;
}

void ata_read28(
	ata_device_t *device, uint32_t sector, uint8_t *data, uint32_t c
)
{
	if (sector > 0x0FFFFFFF)
		return;
	
	outb(device->port_device,		(device->master ? 0xE0 : 0xF0)
									| ((sector & 0x0F000000) >> 24));
	outb(device->port_error,		0x00);
	outb(device->port_sector_count,	0x01);
	outb(device->port_lba_low,		(sector & 0x000000FF));
	outb(device->port_lba_mid,		(sector & 0x0000FF00) >> 8);
	outb(device->port_lba_hi,		(sector & 0x00FF0000) >> 16);
	outb(device->port_command,		0x20);
	
	uint8_t status = inb(device->port_command);

	while (((status & 0x80) == 0x80) && ((status & 0x01) != 0x01))
		status = inb(device->port_command);
		
	if (status & 0x01)
	{
		puts("${F04}ATA READING ERROR${CLC}");
		return;
	}
	
	for (uint16_t i = 0; i < c; i += 2)
	{
		uint16_t wdata = inw(device->port_data);
		
		data[i] = wdata & 0x00FF;

		if (i + 1 < (int) c)
			data[i+1] = (wdata >> 8) & 0x00FF;
	}
	
	for (uint16_t i = c + (c % 2); i < BYTES_PER_SECTOR; i += 2)
		inw(device->port_data);
}

void ata_write28(
	ata_device_t *device, uint32_t sector, uint8_t *data, uint32_t c
)
{
	if (sector > 0x0FFFFFFF)
		return;

	if (c > BYTES_PER_SECTOR)
		return;

	outb(device->port_device,		(device->master ? 0xE0 : 0xF0)
									| ((sector & 0x0F000000) >> 24));
	outb(device->port_error,		0x00);
	outb(device->port_sector_count, 0x01);
	outb(device->port_lba_low,		(sector & 0x000000FF));
	outb(device->port_lba_mid,		(sector & 0x0000FF00) >> 8);
	outb(device->port_lba_hi,		(sector & 0x00FF0000) >> 16);
	outb(device->port_command,		0x30);
	
	printf("Writing: ");

	for (uint16_t i = 0; i < c; i += 2)
	{
		uint16_t wdata = data[i];

		if (i + 1 < (int) c)
			wdata |= ((uint16_t) data[i+1]) << 8;

		outw(device->port_data, wdata);
		
		char *text = "  \0";

		text[0] = wdata & 0xFF;
		text[1] = (wdata >> 8) & 0xFF;

		printf(text);
	}
	
	for (uint16_t i = c + (c % 2); i < BYTES_PER_SECTOR; i += 2)
		outw(device->port_data,	0x0000);
}

void ata_flush(ata_device_t *device)
{
	outb(device->port_device,	device->master ? 0xE0 : 0xF0 );
	outb(device->port_command,	0xE7);

	uint8_t status = inb(device->port_command);

	if (status == 0x00)
		return;
	
	while (((status & 0x80) == 0x80) && ((status & 0x01) != 0x01))
		status = inb(device->port_command);
		
	if (status & 0x01)
	{
		puts("${F04}ATA FLUSHING ERROR${CLC}");
		return;
	}
}

void init_ata()
{
	bool found_ATA_device = false;

	ata_device_t primary_master		= { .number = 1, .master = true  };
	ata_device_t primary_slave		= { .number = 1, .master = false };
	ata_device_t secondary_master	= { .number = 2, .master = true  };
	ata_device_t secondary_slave	= { .number = 2, .master = false };

	// EIDE disk controller’s first chain
	register_interrupt_handler(IRQ14, lambda(void, () {}));
	// EIDE disk controller’s second chain
	register_interrupt_handler(IRQ15, lambda(void, () {}));

	found_ATA_device |= init_device(&primary_master);
	found_ATA_device |= init_device(&primary_slave);
	found_ATA_device |= init_device(&secondary_master);
	found_ATA_device |= init_device(&secondary_slave);

	//MSDOS_read_partitions(&primary_master);

	if (found_ATA_device)
		KOK("Initialized ATA driver");
}

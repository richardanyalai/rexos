/* This file is part of RexOS!.
 *
 * RexOS! is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * RexOS! is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with RexOS!.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <drivers/serial.h>
#include <io/ports.h>
#include <io/registers.h>
#include <sys/isr.h>
#include <sys/pic.h>
#include <utils.h>
#include <stdarg.h>
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>

/**
 * Useful links:
 * REF: https://wiki.osdev.org/Serial_Ports
 * REF: https://wiki.osdev.org/Serial_Port
 * REF: https://wiki.osdev.org/UART
 * REF: https://forum.osdev.org/viewtopic.php?f=1&t=31374
 */

uint32_t serial_received(uint32_t device)
{ return inb(device + 5) & 1; }

char serial_read(uint32_t device)
{
	while (serial_received(device) == 0);

	return inb(device);
}

char serial_read_async(uint32_t device)
{ return inb(device); }

uint32_t serial_is_transmit_empty(uint32_t device)
{ return inb(device + 5) & 0x20; }

void serial_send(uint32_t device, char out)
{
	while (serial_is_transmit_empty(device) == 0);
	outb(device, out);
}

void serial_print(uint32_t device, char *str)
{
	for (uint32_t i = 0; i < strlen(str); ++i)
		serial_send(device, str[i]);
}

int serial_printf(const char *__restrict format, ...)
{
	va_list parameters;
	va_start(parameters, format);

	char str[100];

	int written = vsprintf(str, format, parameters);

	va_end(parameters);

	serial_print(COM1, str);

	return written;
}

void COM1_handler()
{
	char serial = serial_read(COM1);

	send_eoi(SERIAL_IRQ);

	if (serial == 13)
		serial = '\n';

	printf("%c", serial);

	serial_send(COM1, serial);
}

void COM2_handler()
{
	char serial = serial_read(COM1);

	send_eoi(SERIAL_IRQ - 1);

	serial_send(COM1, serial);
}

static uint32_t init_serial_port(uint32_t device)
{
	outb(device + 1, 0x00);	// Disable all interrupts
	outb(device + 3, 0x80);	// Enable DLAB (set baud rate divisor)
	outb(device + 0, 0x03);	// Set divisor to 3 (lo byte) 38400 baud
	outb(device + 1, 0x00);	//                  (hi byte)
	outb(device + 3, 0x03);	// 8 bits, no parity, one stop bit
	outb(device + 2, 0xC7);	// Enable FIFO, clear them, with 14-byte threshold
	outb(device + 4, 0x0B);	// IRQs enabled, RTS/DSR set
	outb(device + 4, 0x1E);	// Set in loopback mode, test the serial chip
	outb(device + 0, 0xAE);	// Test serial chip (send byte 0xAE and check if
							// serial returns same byte)
	
	// Check if serial is faulty (i.e: not same byte as sent)
	if (inb(device + 0) != 0xAE)
		return 1;
	
	// If serial is not faulty set it in normal operation mode
	// (not-loopback with IRQs enabled and OUT#1 and OUT#2 bits enabled)
	outb(device + 4, 0x0F);

	return 0;
}

void init_serial_ports()
{
	init_serial_port(COM1);
	init_serial_port(COM2);

	// Install the serial input handler
	register_interrupt_handler(SERIAL_IRQ,		COM1_handler);
	register_interrupt_handler(SERIAL_IRQ - 1,	COM2_handler);

	outb(COM1 + 1, 0x01);
	outb(COM2 + 1, 0x01);
	outb(COM3 + 1, 0x01);
	outb(COM4 + 1, 0x01);

	KOK("Initialized serial communication driver");
}

/* This file is part of RexOS!.
 *
 * RexOS! is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * RexOS! is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with RexOS!.  If not, see <https://www.gnu.org/licenses/>.
 */

// REF: https://www.youtube.com/watch?v=IGpUdIX-Z5A

#include <drivers/ATA.h>
#include <drivers/fs/FAT32.h>
#include <drivers/fs/MSDOS.h>

void MSDOS_read_partitions(ata_device_t *hd)
{
	MBR_t mbr;

	ata_read28(hd, 0, (uint8_t *) &mbr, sizeof(MBR_t));

	for (uint32_t i = 0; i < 4; ++i)
	{
		if (mbr.primary_partition[i].partition_id == 0x00)
			continue;

		FAT32_read_bios_block(hd, mbr.primary_partition[i].start_lba);
	}
}

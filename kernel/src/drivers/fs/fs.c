/* This file is part of RexOS!.
 *
 * RexOS! is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * RexOS! is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with RexOS!.  If not, see <https://www.gnu.org/licenses/>.
 */

/* Implementation for a simple file system.
 * Written for JamesM's kernel development tutorials,
 * rewritten by Richard Anyalai for the RexOS! project.
 */

#include <drivers/fs/fs.h>
#include <stdbool.h>
#include <stdint.h>

fs_node_t *fs_root = 0;	// The root of the filesystem.

bool is_dir(fs_node_t *node)
{ return (node->flags & 0x7) == FS_DIRECTORY; }

uint32_t read_fs(fs_node_t *node, uint32_t offset, uint32_t size, char *buffer)
{ return (node->read != 0) ? node->read(node, offset, size, buffer) : 0; }

uint32_t write_fs(fs_node_t *node, uint32_t offset, uint32_t size, char *buffer)
{ return (node->write != 0) ? node->write(node, offset, size, buffer) : 0; }

void open_fs(fs_node_t *node, bool read, bool write)
{
	// Has the node got an open callback?
	if ((read || write) && node->open != 0)
		return node->open(node);
}

void close_fs(fs_node_t *node)
{
	// Has the node got a close callback?
	if (node->close != 0)
		return node->close(node);
}

struct dirent *readdir_fs(fs_node_t *node, uint32_t index)
{
	// Is the node a directory, and does it have a callback?
	if (is_dir(node) &&	node->readdir != 0)
		return node->readdir(node, index);
	else
		return 0;
}

fs_node_t *finddir_fs(fs_node_t *node, char *name)
{
	// Is the node a directory, and does it have a callback?
	if (is_dir(node) &&	node->finddir != 0)
		return node->finddir(node, name);
	else
		return 0;
}

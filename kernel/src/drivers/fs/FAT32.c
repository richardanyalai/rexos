/* This file is part of RexOS!.
 *
 * RexOS! is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * RexOS! is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with RexOS!.  If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * Ported from the code based on these 2 videos from Viktor Engelmann's
 * "Write your own operating system" tutorial:
 * REF: https://www.youtube.com/watch?v=tEYgVwN1nRk
 * REF: https://www.youtube.com/watch?v=G8Jqs_UZ1oU
 */

#include <drivers/fs/FAT32.h>
#include <stdio.h>

void FAT32_read_bios_block(ata_device_t *hd, uint32_t partition_offset)
{
	bios_parameter_block_32_t bpb;
	ata_read28(
		hd,
		partition_offset,
		(uint8_t *) &bpb,
		sizeof(bios_parameter_block_32_t)
	);

	uint32_t fat_start	= partition_offset + bpb.reserved_sectors;
	uint32_t fat_size	= bpb.table_size;
	uint32_t data_start	= fat_start + fat_size * bpb.fat_copies;
	uint32_t root_start	= data_start + bpb.sectors_per_cluster
						* (bpb.root_cluster - 2);

	dirent_FAT32_t dirent[16];
	ata_read28(
		hd,
		root_start,
		(uint8_t *) &dirent[0],
		16 * sizeof(dirent_FAT32_t)
	);

	for (uint8_t i = 0; i < 16; ++i)
	{
		if (dirent[i].name[0] == 0x00)
			break;

		if ((dirent[i].attributes & 0x0F) == 0x0F)
			continue;

		char *foo = "        \n";
		for (uint8_t j = 0; j < 8; ++j)
			foo[j] = dirent[i].name[j];

		printf(foo);

		if ((dirent[i].attributes & 0x10) == 0x10) // Directory
			continue;

		uint32_t first_file_cluster	= ((uint32_t) dirent[i].first_cluster_hi)
									<< 16
									| ((uint32_t) dirent[i].first_cluster_lo);

		int		SIZE				= dirent[i].size;
		int		next_file_cluster	= first_file_cluster;
		uint8_t	buffer[513];
		uint8_t	fat_buffer[513];

		while (SIZE > 0)
		{
			uint32_t file_sector	= data_start + bpb.sectors_per_cluster
									* (next_file_cluster - 2);
			uint32_t sector_offset	= 0;

			for (; SIZE > 0; SIZE -= 512)
			{
				ata_read28(hd, file_sector + sector_offset, buffer, 512);

				buffer[SIZE > 512 ? 512 : SIZE] = '\0';

				printf((char *) buffer);

				if (++sector_offset > bpb.sectors_per_cluster)
					break;
			}

			uint32_t fat_sector_for_current_cluster				=
			next_file_cluster / (512 / sizeof(uint32_t));
			ata_read28(
				hd, fat_start + fat_sector_for_current_cluster, fat_buffer, 512
			);
			uint32_t fat_offset	= next_file_cluster % (512 / sizeof(uint32_t));
			next_file_cluster	= ((uint32_t *) &fat_buffer)[fat_offset]
								& 0x0FFFFFFF;
		}
	}
}

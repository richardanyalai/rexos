/* This file is part of RexOS!.
 *
 * RexOS! is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * RexOS! is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with RexOS!.  If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * This is the implementation / port of Viktor Engelmann's AMD am79c973
 * network card driver in C, the original driver can be found here:
 * REF: https://github.com/AlgorithMan-de/wyoos/blob/master/src/drivers/amd_am79c973.cpp
 * 
 * The video where he explains how it works can be found here:
 * REF:https://www.youtube.com/watch?v=9xpKXEZ9Sxo&list=PLHh55M_Kq4OApWScZyPl5HhgsTJS9MZ6M&index=18
 * 
 * Also extremely useful:
 * REF: http://www.lowlevel.eu/wiki/AMD_PCnet
 * REF: https://wiki.osdev.org/AMD_PCNET
 */

#include <drivers/am79c973.h>
#include <drivers/PCI.h>
#include <io/ports.h>
#include <io/registers.h>
#include <net/ARP.h>
#include <net/etherframe.h>
#include <net/ICMP.h>
#include <net/IPv4.h>
#include <sys/isr.h>
#include <utils.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>

pci_device_t *am79c973;

buffer_descriptor_t
	*send_buffer_descr,
	*recv_buffer_descr;
uint8_t
	send_buffers_desc_mem[2*1024+15],
	send_buffers[2*1024+15][8],
	current_send_buffer,
	recv_buffers_desc_mem[2*1024+15],
	recv_buffers[2*1024+15][8],
	current_recv_buffer;

init_block_t init_block;

uint32_t gateway_IP, subnet_mask;

bool am79c973_ready;

uint64_t am79c973_get_MAC_address()
{ return init_block.physical_address; }

uint32_t am79c973_get_IP_address()
{ return init_block.logical_address; }

void am79c973_set_IP_address(uint32_t ip)
{ init_block.logical_address = ip; }

void am79c973_send(uint8_t *buffer, uint32_t size)
{
	uint32_t send_descr = current_send_buffer;
	current_send_buffer = (current_send_buffer + 1) % 8;

	size = (size > 1518) ? 1518 : size;

	memcpy((uint8_t *) send_buffer_descr[send_descr].address, buffer, size);

	send_buffer_descr[send_descr].avail  = 0;	// Device is in use.
	send_buffer_descr[send_descr].flags2 = 0;	// Clearing error messages.
	send_buffer_descr[send_descr].flags  = 0x8300F000
										 | ((uint16_t)((-size) & 0xFFF));

	outw(REG_ADDR_PORT, 0x00);
	outw(REG_DATA_PORT, 0x48);	// Send data.
}

void am79c973_receive()
{
	for (
		; !(recv_buffer_descr[current_recv_buffer].flags & 0x80000000)
		; current_recv_buffer = (current_recv_buffer + 1) % 8
	)
	{
		if (
			!(recv_buffer_descr[current_recv_buffer].flags & 0x40000000) &&
			(
				(recv_buffer_descr[current_recv_buffer].flags & 0x03000000) ==
				0x03000000
			)
		)
		{
			uint32_t size	= recv_buffer_descr[current_recv_buffer].flags
							& 0xFFF;

			if (size >= 64)	// Remove checksum
				size -= 4;

			uint8_t *buffer =
				(uint8_t *) recv_buffer_descr[current_recv_buffer].address;

			if (efp_on_raw_data_received(buffer, size))
				am79c973_send(buffer, size);
		}

		recv_buffer_descr[current_recv_buffer].flags2 = 0;
		recv_buffer_descr[current_recv_buffer].flags  = 0x8000F7FF;
	}
}

void am79c973_handler()
{
	outw(REG_ADDR_PORT, 0x0);

	uint32_t temp = inw(REG_DATA_PORT);

	if ((temp & 0x8000) == 0x8000)
		puts("${F12}am79c973 ERROR${CLC}");
	if ((temp & 0x2000) == 0x2000)
		puts("${F12}am79c973 COLLISION ERROR${CLC}");
	if ((temp & 0x1000) == 0x1000)
		puts("${F12}am79c973 MISSED FRAME${CLC}");
	if ((temp & 0x0800) == 0x0800)
		puts("${F12}am79c973 MEMORY ERROR${CLC}");
	if ((temp & 0x0400) == 0x0400)
		am79c973_receive();
	if ((temp & 0x0200) == 0x0200)
	{ /* puts("${F10}am79c973 DATA SENT${CLC}"); */ }

	// Ack
	outw(REG_ADDR_PORT, 0x0);
	outw(REG_DATA_PORT, temp);

	if ((temp & 0x0100) == 0x0100)
	{
		KOK("Initialized network card driver");
		am79c973_ready = true;
	}
}

uint8_t am79c973_reset()
{
	inl(RESET_PORT + 0x4);	// Resets the card if it is in 32-bit mode.
	inw(RESET_PORT);		// If device is now in 16-bit mode it will
							// reset again, otherwise
							// it will reset for the first time.
	outw(RESET_PORT, 0x0);
	return 10;
}

void UDP_send(uint32_t ip, uint16_t port, uint8_t *message, uint16_t size);

void am79c973_init(pci_device_t *device)
{
	am79c973 = device;

	am79c973_reset();

	init_ether_frame_handlers();

	current_send_buffer = 0;
	current_recv_buffer = 0;

	uint64_t MAC0 = inw(MAC_ADDR_0_PORT) % 256;
	uint64_t MAC1 = inw(MAC_ADDR_0_PORT) / 256;
	uint64_t MAC2 = inw(MAC_ADDR_2_PORT) % 256;
	uint64_t MAC3 = inw(MAC_ADDR_2_PORT) / 256;
	uint64_t MAC4 = inw(MAC_ADDR_4_PORT) % 256;
	uint64_t MAC5 = inw(MAC_ADDR_4_PORT) / 256;

	// Mac address
	uint64_t MAC = MAC5 << 40
				 | MAC4 << 32
				 | MAC3 << 24
				 | MAC2 << 16
				 | MAC1 << 8
				 | MAC0;

	// IP address
	uint8_t  IP[4] = { 10, 0, 2, 15 };
	uint32_t IP_BE = ARRAY_TO_IPv4(IP);

	// Gateway IP
	uint8_t GIP[4] = { 10, 0, 2, 2 };
	gateway_IP     = ARRAY_TO_IPv4(GIP);

	// Subnet mask
	uint8_t MASK[4] = { 255, 255, 255, 0 };
	subnet_mask     = ARRAY_TO_IPv4(MASK);

	// 32 bit mode
	outw(REG_ADDR_PORT,				0x14);
	outw(BUS_CTRL_REG_DATA_PORT,	0x102);

	// STOP reset
	outw(REG_ADDR_PORT, 0x00);
	outw(REG_DATA_PORT, 0x04);

	// InitBlock
	init_block.mode             = 0x0000;
	init_block.reserved1        = 0;
	init_block.num_send_buffers = 3;
	init_block.reserved2        = 0;
	init_block.num_recv_buffers = 3;
	init_block.physical_address = MAC;
	init_block.reserved3        = 0;
	init_block.logical_address  = IP_BE;

	send_buffer_descr = (buffer_descriptor_t *) (
		(((uint32_t) &send_buffers_desc_mem[0]) + 15) & ~((uint32_t) 0xF)
	);
	init_block.send_buffer_desc_addr = (uint32_t) send_buffer_descr;

	recv_buffer_descr = (buffer_descriptor_t *) (
		(((uint32_t) &recv_buffers_desc_mem[0]) + 15) & ~((uint32_t) 0xF)
	);
	init_block.recv_buffer_desc_addr = (uint32_t) recv_buffer_descr;

	for (uint8_t i = 0; i < 8; ++i)
	{
		send_buffer_descr[i].address = (((uint32_t) &send_buffers[i]) + 15)
									 & ~((uint32_t) 0xF);
		send_buffer_descr[i].flags   = 0x77F | 0xF000;
		send_buffer_descr[i].flags2  = 0;
		send_buffer_descr[i].avail   = 0;

		recv_buffer_descr[i].address = (((uint32_t) &recv_buffers[i]) + 15)
										& ~((uint32_t) 0xF);
		recv_buffer_descr[i].flags   = 0xF7FF | 0x80000000;
		recv_buffer_descr[i].flags2  = 0;
		recv_buffer_descr[i].avail   = 0;
	}

	outw(REG_ADDR_PORT, 0x01);
	outw(REG_DATA_PORT, ((uint32_t) &init_block) & 0xFFFF);

	outw(REG_ADDR_PORT, 0x02);
	outw(REG_DATA_PORT, ((uint32_t) &init_block >> 16) & 0xFFFF);

	// Registering the interrupt handler
	register_interrupt_handler(device->interrupt, am79c973_handler);

	// Activation
	outw(REG_ADDR_PORT, 0x00);
	outw(REG_DATA_PORT, 0x41);

	outw(REG_ADDR_PORT, 0x04);

	uint32_t temp = inw(REG_DATA_PORT);

	outw(REG_ADDR_PORT, 0x04);
	outw(REG_DATA_PORT, (temp | 0xC00));

	outw(REG_ADDR_PORT, 0x00);
	outw(REG_DATA_PORT, 0x42);

	arp_broadcast_MAC_address(gateway_IP);
}

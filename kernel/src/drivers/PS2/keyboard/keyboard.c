/* This file is part of RexOS!.
 *
 * RexOS! is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * RexOS! is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with RexOS!.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <drivers/keyboard.h>
#include <io/ports.h>
#include <io/registers.h>
#include <settings.h>
#include <sys/isr.h>
#include <sys/pit.h>
#include <utils.h>
#include <math.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

#define KEYS_COUNT		66
#define MODIFIERS_COUNT	5

extern key_t keys[KEYS_COUNT];
extern modifier_t modifiers[MODIFIERS_COUNT];

static active_keys_t active_keys;
static bool CAPS_LOCK;

void *(*keyboard_handler)(active_keys_t *active_keys);

bool super()
{ return modifiers[0].pressed; }

bool ctrl()
{ return modifiers[1].pressed; }

bool alt()
{ return modifiers[2].pressed; }

bool shift()
{ return modifiers[3].pressed || modifiers[4].pressed; }

void kbrd_driver()
{
	uint8_t code = inb(0x60);
	uint8_t key = code;

	if (key > 0x80)
		key -= 0x80;

	outb(PORT_MCP, 0x20);

	register int i = inb(0x61);

	outb(0x61, i | 0x80);
	outb(0x61, i);
	
	if (!handle_keyboard(key, code < 0x80) && code < 0x80 && code != 0x41)
		printf("KBRD_INTERRUPT 0x%.2x ", key);
}

bool handle_keyboard(uint8_t key_code, bool pressed)
{
	bool matches = false;

	if (pressed && key_code == KEY_CAPS_L)
	{
		CAPS_LOCK = !CAPS_LOCK;
		return true;
	}

	for (uint8_t i = 0; i < MODIFIERS_COUNT; ++i)
	{
		modifier_t *modifier = &modifiers[i];

		if (key_code == modifier->key_code)
		{
			modifier->pressed = pressed;
			matches = true;
			break;
		}
	}

	for (uint16_t i = 0; i < KEYS_COUNT; ++i)
	{
		key_t *key = &keys[i];

		if (key->key_code == 0)
			break;

		if (key_code == key->key_code)
		{
			uint8_t ch = key->char_code;

			if (pressed)
			{
				if ((shift() || CAPS_LOCK) && !alt())
					ch = key->shift_char_code;
				else if (alt())
				{
					if (shift() || CAPS_LOCK)
						ch = key->alt_shift_char_code;
					else
						ch = key->alt_char_code;
				}
			}

			active_keys.active_key.key_code		= key_code;
			active_keys.active_key.char_code	= ch;

			matches = true;
			break;
		}
	}

	active_keys.active_key.pressed	= pressed;
	active_keys.super				= super();
	active_keys.ctrl				= ctrl();
	active_keys.alt					= alt();
	active_keys.shift				= shift();

	if (keyboard_handler)
		keyboard_handler(&active_keys);
	
	return matches;
}

active_keys_t *get_active_keys()
{ return &active_keys; }

void set_keyboard_handler(void *handler)
{
	keyboard_handler = handler;
	IRQ_RES;
}

void *get_keyboard_handler()
{ return keyboard_handler; }

void init_keyboard()
{
	register_interrupt_handler(IRQ1, &kbrd_driver);

	outb(0x21, 0x00); // Enable all IRQs on PICM
	outb(0xA1, 0x00); // Enable all IRQs on PICS

	KOK("Initialized keyboard driver");
}

void null_active()
{
	active_keys.active_key.key_code  = 0;
	active_keys.active_key.char_code = 0;
	active_keys.active_key.pressed   = false;
}

/* This file is part of RexOS!.
 *
 * RexOS! is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * RexOS! is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with RexOS!.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <drivers/keyboard.h>

 // An array to keep information about every useful key.
key_t keys[] =
{
	{ KEY_A,		'a',  'A',	'~',	  0, false },
	{ KEY_B,		'b',  'B',	'{',	  0, false },
	{ KEY_C,		'c',  'C',	'&',	  0, false },
	{ KEY_D,		'd',  'D',	146,	  0, false }, // Æ
	{ KEY_E,		'e',  'E',	238,	  0, false }, // ε
	{ KEY_F,		'f',  'F',	'[',	  0, false },
	{ KEY_G,		'g',  'G',	']',	  0, false },
	{ KEY_H,		'h',  'H',	'`',	  0, false },
	{ KEY_I,		'i',  'I',	239,	  0, false }, // ∩
	{ KEY_J,		'j',  'J',	'\'',	  0, false },
	{ KEY_K,		'k',  'K',	240,	  0, false }, // ≡
	{ KEY_L,		'l',  'L',	236,	  0, false }, // ∞
	{ KEY_M,		'm',  'M',	'^',	  0, false },
	{ KEY_N,		'n',  'N',	'}',	  0, false },
	{ KEY_O,		'o',  'O',	248,	  0, false }, // ø
	{ KEY_P,		'p',  'P',	227,	  0, false }, // π
	{ KEY_Q,		'q',  'Q',	'\\',	  0, false },
	{ KEY_R,		'r',  'R',	168,	  0, false }, // ¿
	{ KEY_S,		's',  'S',	228,	  0, false }, // Σ
	{ KEY_T,		't',  'T',	249,	  0, false }, // ∙
	{ KEY_U,		'u',  'U',	224,	  0, false }, // α
	{ KEY_V,		'v',  'V',	'@',	  0, false },
	{ KEY_W,		'w',  'W',	'|',	  0, false },
	{ KEY_X,		'x',  'X',	'#',	  0, false },
	{ KEY_Y,		'y',  'Y',	247,	  0, false }, // ≈
	{ KEY_Z,		'z',  'Z',	225,	  0, false }, // ß

	{ KEY_1,		'+',  '1',	'!',	  0, false },
	{ KEY_2,		'@',  '2',	253,	  0, false }, // ²
	{ KEY_3,		'#',  '3',	254,	  0, false }, // ■
	{ KEY_4,		'$',  '4',	148,	153, false }, // ö
	{ KEY_5,		'%',  '5',	129,	154, false }, // ü
	{ KEY_6,		'^',  '6',	162,	'O', false }, // ó
	{ KEY_7,		'&',  '7',	163,	'U', false }, // ú
	{ KEY_8,		'*',  '8',	160,	'A', false }, // á
	{ KEY_9,		'(',  '9',	161,	'I', false }, // í
	{ KEY_0,		')',  '0',	130,	144, false }, // é

	{ KEY_NUM_1,	'1',	0,	  0,	  0, false },
	{ KEY_NUM_2,	'2',	0,	  0,	  0, false },
	{ KEY_NUM_3,	'3',	0,	  0,	  0, false },
	{ KEY_NUM_4,	'4',	0,	  0,	  0, false },
	{ KEY_NUM_5,	'5',	0,	  0,	  0, false },
	{ KEY_NUM_6,	'6',	0,	  0,	  0, false },
	{ KEY_NUM_7,	'7',	0,	  0,	  0, false },
	{ KEY_NUM_8,	'8',	0,	  0,	  0, false },
	{ KEY_NUM_9,	'9',	0,	  0,	  0, false },
	{ KEY_NUM_0,	'0',	0,	  0,	  0, false },

	{ KEY_SPACE,	' ',	0,	  0,	  0, false },

	{ KEY_DOT,		'.',  ':',	'>',	  0, false },
	{ KEY_COMMA,	',',  '?',	'<',	  0, false },
	{ KEY_SEMICOL,	';',  248,	  0,	  0, false }, // °
	{ KEY_PARENTH,	'"',  '\'', '`',	  0, false },

	{ KEY_MINUS_1,	'-',  '_',	  0,	  0, false },
	{ KEY_MINUS_2,  '-',	0,	  0,	  0, false },
	{ KEY_PLUS,		'+',	0,	  0,	  0, false },
	{ KEY_DIV,		'/',	0,	  0,	  0, false },
	{ KEY_MUL,		'*',	0,	  0,	  0, false },
	{ KEY_EQUALS,	'=',  '%',	  0,	  0, false },

	{ KEY_ENTER,   '\n',	0,	  0,	  0, false },
	{ KEY_BKSPACE,	  0,	0,	  0,	  0, false },

	{ KEY_NUML,		  0,	0,	  0,	  0, false },

	{ KEY_LEFT,		  0,	0,	  0,	  0, false },
	{ KEY_RIGHT,	  0,	0,	  0,	  0, false },
	{ KEY_UP,		  0,	0,	  0,	  0, false },
	{ KEY_DOWN,		  0,	0,	  0,	  0, false },

	{ KEY_SUPER,	  0,	0,	  0,	  0, false }
};

// Stores information about the modifier keys.
volatile modifier_t modifiers[] =
{
	{ KEY_SUPER,	false },	// Super key
	{ KEY_CTRL,		false },	// Control
	{ KEY_ALT,		false },	// Alt
	{ KEY_LSHIFT,	false },	// Left Shift
	{ KEY_RSHIFT,	false }		// Right Shift
};

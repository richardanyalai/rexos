/* This file is part of RexOS!.
 *
 * RexOS! is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * RexOS! is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with RexOS!.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <drivers/mouse.h>
#include <drivers/VGA.h>
#include <io/ports.h>
#include <settings.h>
#include <sys/isr.h>
#include <utils.h>
#include <stdint.h>
#include <stdio.h>

#define COMMP 0x64
#define DATAP 0x60

uint16_t	bfrbkp;
uint8_t		buffer[3],
			offset,
			buttons,
			xold = 40,
			yold = 12;
bool		mouse_enabled;

void *(*mouse_left_click)(uint8_t x, uint8_t y);
void *(*mouse_right_click)(uint8_t x, uint8_t y);

void draw_mouse_cursor(uint8_t x, uint8_t y)
{
	if (mouse_enabled)
	{
		uint16_t pos = vga_get_pos(x, y);
		vga_set_entry(pos, vga_invert_entry(pos));
	}
}

void mouse_new_line()
{
	if (mouse_enabled)
	{
		uint16_t new_pos = vga_get_pos(xold, yold);
		uint16_t old_pos = vga_get_pos(xold, (yold - 1));
		vga_set_entry(new_pos, vga_invert_entry(new_pos));
		vga_set_entry(old_pos, vga_invert_entry(old_pos));
	}
}

void show_mouse_cursor()
{
	bfrbkp = vga_get_entry_at(vga_get_pos(xold, yold));
	draw_mouse_cursor(xold, yold);
}

void hide_mouse_cursor()
{ vga_set_entry(vga_get_pos(xold, yold), bfrbkp); }

/**
 * This video helped me a lot:
 * REF: https://www.youtube.com/watch?v=PgA05QaIu6Y&list=PLHh55M_Kq4OApWScZyPl5HhgsTJS9MZ6M&index=8
 * These links can also be useful:
 * REF: https://wiki.osdev.org/Mouse_Input
 * REF: https://wiki.osdev.org/PS/2_Mouse
 * REF: https://github.com/stevej/osdev/blob/master/kernel/devices/mouse.c
 * REF: https://github.com/szhou42/osdev/blob/master/src/kernel/drivers/mouse.c
 */
void mouse_driver()
{
	uint8_t status = inb(COMMP);

	if (!(status & 0x20))
		return;
		
	static char x = 40, y = 12;

	buffer[offset] = inb(DATAP);
	offset = (offset + 1) % 3;

	if (offset == 0)
	{
		if (mouse_enabled)
		{
			draw_mouse_cursor(x, y);

			x += buffer[1];

			if (x <= 0)
				x = 0;

			if (x >= 79)
				x = 79;

			y -= buffer[2];

			if (y <= 0)
				y = 0;

			if (y >= 24)
				y = 24;

			bfrbkp = vga_get_entry_at(vga_get_pos(x, y));

			xold = x;
			yold = y;

			draw_mouse_cursor(x, y);
		}

		if (mouse_enabled && buffer[0] & 0x01 && mouse_left_click)
			mouse_left_click(x, y);
		else if (mouse_enabled && buffer[0] & 0x02 && mouse_left_click)
			mouse_right_click(x, y);
		else if (buffer[0] & 0x04)
		{
			vga_set_entry(vga_get_pos(x,    y   ), bfrbkp);
			vga_set_entry(vga_get_pos(xold, yold), bfrbkp);

			mouse_enabled = !mouse_enabled;

			draw_mouse_cursor(x, y);
		}
	}
}

void init_mouse()
{
	offset = 0;
	buttons = 0;
	mouse_enabled = false;

	outb(COMMP, 0xA8);
	outb(COMMP, 0x20);

	uint8_t status = inb(DATAP) | 2;

	outb(COMMP, 0x60);
	outb(DATAP, status);

	outb(COMMP, 0xD4);
	outb(DATAP, 0xF4);
	inb(DATAP);
	
	register_interrupt_handler(IRQ12, &mouse_driver);

	KOK("Initializing mouse driver");
}

void set_left_click_handler(void *handler)
{
	mouse_left_click = handler;
	IRQ_RES;
}

void set_right_click_handler(void *handler)
{
	mouse_right_click = handler;
	IRQ_RES;
}

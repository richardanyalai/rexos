/* This file is part of RexOS!.
 *
 * RexOS! is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * RexOS! is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with RexOS!.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <drivers/VESA.h>
#include <drivers/VGA.h>
#include <settings.h>
#include <stdbool.h>

extern const uint8_t vga_256_palette[256][3];
extern const uint8_t vga_textmode_8x16_font[2048], vesa_8x16_font[4096];

multiboot_info_t *mbi;

uint32_t vesa_get_address()
{ return mbi->framebuffer_addr; }

uint16_t vesa_get_width()
{ return mbi->framebuffer_width; }

uint16_t vesa_get_height()
{ return mbi->framebuffer_height; }

uint16_t vesa_get_pitch()
{ return mbi->framebuffer_pitch; }

uint16_t vesa_get_bpp()
{ return mbi->framebuffer_bpp; }

void vesa_clear()
{ memset((uint8_t *) VIDEO_ADDRESS, 0, VESA_WIDTH * VESA_HEIGHT * 4); }

inline void vesa_draw_pixel(uint16_t x, uint16_t y, uint32_t color)
{
	uint32_t where = y * VESA_PITCH + x * VESA_BPP / 8;

	uint8_t *screen = (uint8_t *) VIDEO_ADDRESS;

	screen[where  ] = (color >>  0) & 255; // BLUE
	screen[where+1] = (color >>  8) & 255; // GREEN
	screen[where+2] = (color >> 16) & 255; // RED
}

uint32_t color_vga_to_vesa(uint8_t color)
{
	uint8_t
		r = vga_256_palette[color][0],
		g = vga_256_palette[color][1],
		b = vga_256_palette[color][2];

	return (((uint8_t) ((0 * 255) / 63)) << 24)
		 | (((uint8_t) ((r * 255) / 63)) << 16)
		 | (((uint8_t) ((g * 255) / 63)) <<  8)
		 | (((uint8_t) ((b * 255) / 63)) <<  0);
}

void vesa_draw_vga_pixel(uint16_t x, uint16_t y, uint8_t color)
{
	if (x < VGA_WIDTH && y < VGA_HEIGHT)
	{
		uint16_t offset_x = (VESA_WIDTH  - 2 * VGA_WIDTH)  / 2;
		uint16_t offset_y = (VESA_HEIGHT - 2 * VGA_HEIGHT) / 2;

		uint16_t _x       = x * 2 + offset_x;
		uint16_t _y       = y * 2 + offset_y;

		for (uint16_t i = _x; i <= _x + 1; ++i)
			for (uint16_t j = _y; j <= _y + 1; ++j)
				vesa_draw_pixel(i, j, color_vga_to_vesa(color));
	}
}

void vesa_draw_fill_rect(
	uint16_t x1, uint16_t y1, uint16_t x2, uint16_t y2, uint32_t color
)
{
	for (uint16_t y = y1; y < y2; ++y)
		for (uint16_t x = x1; x < x2; ++x)
			vesa_draw_pixel(x, y, color);
}

void vesa_draw_char(
	uint16_t   x,
	uint16_t   y,
	const uint8_t c,
	bool       draw_bg,
	uint32_t   bgcolor,
	uint32_t   fgcolor
)
{
	for (uint8_t _y = 0; _y < 16; ++_y)
		for (uint8_t _x = 0; _x < 8; ++_x)
			if (
#ifdef USE_FONT2
					(vesa_8x16_font[c * 16 + _y] >> (7 - _x) & 1) == 1
#else
					(vga_textmode_8x16_font[c * 16 + _y] >> (7 - _x) & 1) == 1
#endif
			)
				vesa_draw_pixel(x + _x, y + _y, fgcolor);
			else if (draw_bg)
				vesa_draw_pixel(x + _x, y + _y, bgcolor);
}

void vesa_draw_tty_bg()
{
	uint16_t offset_x =
		(VESA_WIDTH - VGA_WIDTH * (VGA_WIDTH < 320 ? 8 : 1)) / 2;
	uint16_t offset_y =
		(VESA_HEIGHT - VGA_HEIGHT * (VGA_HEIGHT < 200 ? 16 : 1)) / 2;

	vesa_draw_fill_rect(
		offset_x,
		offset_y,
		offset_x + (VGA_WIDTH < 320 ? 80 * 8 : VGA_WIDTH),
		offset_y + (VGA_HEIGHT < 200 ? 25 * 16 : VGA_HEIGHT),
		0
	);
}

/* This file is part of RexOS!.
 *
 * RexOS! is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * RexOS! is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with RexOS!.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <stdint.h>

uint8_t mario[4][16] =
{
	{
		0b00000000,
		0b00000011,
		0b00000100,
		0b00001000,
		0b00001110,
		0b00010011,
		0b00010011,
		0b00001100,
		0b00000110,
		0b00001001,
		0b00010000,
		0b00010000,
		0b00001000,
		0b00001000,
		0b00000100,
		0b00000011,
	},
	{
		0b00000000,
		0b11110000,
		0b00001100,
		0b00000010,
		0b01011100,
		0b01000010,
		0b00100010,
		0b01111100,
		0b00001000,
		0b10010000,
		0b11001000,
		0b11111000,
		0b11011000,
		0b00111000,
		0b00010000,
		0b11100000,
	},
	{
		0b00000011,
		0b00000100,
		0b00001000,
		0b00001110,
		0b00010011,
		0b00010011,
		0b00001100,
		0b00001110,
		0b00110001,
		0b01000000,
		0b01000001,
		0b00100111,
		0b00011111,
		0b00100111,
		0b00100010,
		0b00011100,
	},
	{
		0b11100000,
		0b00011100,
		0b00000010,
		0b01011100,
		0b01000010,
		0b00100010,
		0b01111100,
		0b00001000,
		0b10011100,
		0b11001010,
		0b11110001,
		0b10110101,
		0b11110010,
		0b11100010,
		0b01000100,
		0b00111000,
	}
};

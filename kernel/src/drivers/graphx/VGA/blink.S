/* This file is part of RexOS!.
 *
 * RexOS! is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * RexOS! is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with RexOS!.  If not, see <https://www.gnu.org/licenses/>.
 */

.intel_syntax noprefix

.section .text
.global vga_blink_disable
.global vga_blink_enable
.global vga_blink_toggle

// Disable blink
vga_blink_disable:
	// Read I/O Address 0x03DA to reset index/data flip-flop
	mov dx, 0x03DA
	in al, dx

	// Write index 0x30 to 0x03C0 to set register index to 0x30
	mov dx, 0x03C0
	mov al, 0x30
	out dx, al

	// Read from 0x03C1 to get register contents
	inc dx
	in al, dx

	// Unset Bit 3 to disable Blink
	and al, 0xF7

	// Write to 0x03C0 to update register with changed value
	dec dx
	out dx, al
	ret

// Enable blink
vga_blink_enable:
	// Read I/O Address 0x03DA to reset index/data flip-flop
	mov dx, 0x03DA
	in al, dx

	// Write index 0x30 to 0x03C0 to set register index to 0x30
	mov dx, 0x03C0
	mov al, 0x30
	out dx, al

	// Read from 0x03C1 to get register contents
	inc dx
	in al, dx

	// Set Bit 3 to enable Blink
	or al, 0x08

	// Write to 0x03C0 to update register with changed value
	dec dx
	out dx, al
	ret

// Toggle blink
vga_blink_toggle:
	// Read I/O Address 0x03DA to reset index/data flip-flop
	mov dx, 0x03DA
	in al, dx

	// Write index 0x30 to 0x03C0 to set register index to 0x30
	mov dx, 0x03C0
	mov al, 0x30
	out dx, al

	// Read from 0x03C1 to get register contents
	inc dx
	in al, dx

	// Flip Bit 3 to toggle Blink
	xor al, 0x08

	// Write to 0x03C0 to update register with changed value
	dec dx
	out dx, al
	ret

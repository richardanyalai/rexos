/* This file is part of RexOS!.
 *
 * RexOS! is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * RexOS! is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with RexOS!.  If not, see <https://www.gnu.org/licenses/>.
 */

/*
 * REF: https://wiki.osdev.org/VGA_Hardware
 * REF: https://wiki.osdev.org/Drawing_In_Protected_Mode
 * REF: https://www.youtube.com/watch?v=N68cYNWZgy8
 * REF: https://wiki.osdev.org/VGA_Fonts
 * REF: https://files.osdev.org/mirrors/geezer/osd/graphics/modes.c
 * REF: https://forum.osdev.org/viewtopic.php?f=1&t=10534
 * REF: http://xkr47.outerspace.dyndns.org/progs/mode%2013h%20without%20using%20bios.htm
 * REF: https://jonasjacek.github.io/colors/
 */

#include <drivers/VESA.h>
#include <drivers/VGA.h>
#include <io/ports.h>
#include <settings.h>
#include <utils.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct
{
	uint8_t ch;
	uint8_t bg;
	uint8_t fg;
} vga_entry_t;

////////////////////////////////////////
/// External variables / functions  ////
////////////////////////////////////////

extern uint8_t
	vga_256_palette[256][3],
	vga_textmode_8x8_font[2048],
	vga_textmode_8x16_font[4096],
	vga_graphics_mode_font[2048],
	vesa_8x16_font[4096];

extern void
	vga_setpal(uint8_t, uint8_t, uint8_t, uint8_t),
	vga_blink_disable(),
	mouse_new_line();

////////////////////////////////////////
/// VGA registers for various modes ////
////////////////////////////////////////

uint8_t vga_80x25_text[] =
{
	/* MISC */
	0x67,
	/* SEQ */
	0x03, 0x00, 0x03, 0x00, 0x02,
	/* CRTC */
	0x5F, 0x4F, 0x50, 0x82, 0x55, 0x81, 0xBF, 0x1F,
	0x00, 0x4F, 0x0D, 0x0E, 0x00, 0x00, 0x00, 0x50,
	0x9C, 0x0E, 0x8F, 0x28, 0x1F, 0x96, 0xB9, 0xA3,
	0xFF,
	/* GC */
	0x00, 0x00, 0x00, 0x00, 0x00, 0x10, 0x0E, 0x00,
	0xFF,
	/* AC */
	0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x14, 0x07,
	0x38, 0x39, 0x3A, 0x3B, 0x3C, 0x3D, 0x3E, 0x3F,
	0x0C, 0x00, 0x0F, 0x08, 0x00
};

uint8_t vga_90x60_text[] =
{
	/* MISC */
	0xE7,
	/* SEQ */
	0x03, 0x01, 0x03, 0x00, 0x02,
	/* CRTC */
	0x6B, 0x59, 0x5A, 0x82, 0x60, 0x8D, 0x0B, 0x3E,
	0x00, 0x47, 0x06, 0x07, 0x00, 0x00, 0x00, 0x00,
	0xEA, 0x0C, 0xDF, 0x2D, 0x08, 0xE8, 0x05, 0xA3,
	0xFF,
	/* GC */
	0x00, 0x00, 0x00, 0x00, 0x00, 0x10, 0x0E, 0x00,
	0xFF,
	/* AC */
	0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x14, 0x07,
	0x38, 0x39, 0x3A, 0x3B, 0x3C, 0x3D, 0x3E, 0x3F,
	0x0C, 0x00, 0x0F, 0x08, 0x00,
};

static uint8_t vga_720x480x16[] =
{
	/* MISC */
	0xE7,
	/* SEQ */
	0x03, 0x01, 0x08, 0x00, 0x06,
	/* CRTC */
	0x6B, 0x59, 0x5A, 0x82, 0x60, 0x8D, 0x0B, 0x3E,
	0x00, 0x40, 0x06, 0x07, 0x00, 0x00, 0x00, 0x00,
	0xEA, 0x0C, 0xDF, 0x2D, 0x08, 0xE8, 0x05, 0xE3,
	0xFF,
	/* GC */
	0x00, 0x00, 0x00, 0x00, 0x03, 0x00, 0x05, 0x0F,
	0xFF,
	/* AC */
	0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07,
	0x08, 0x09, 0x0A, 0x0B, 0x0C, 0x0D, 0x0E, 0x0F,
	0x01, 0x00, 0x0F, 0x00, 0x00,
};

static uint8_t vga_320x200x256[] =
{
	/* MISC */
	0x63,
	/* SEQ */
	0x03, 0x01, 0x0F, 0x00, 0x0E,
	/* CRTC */
	0x5F, 0x4F, 0x50, 0x82, 0x54, 0x80, 0xBF, 0x1F,
	0x00, 0x41, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x9C, 0x0E, 0x8F, 0x28,	0x40, 0x96, 0xB9, 0xA3,
	0xFF,
	/* GC */
	0x00, 0x00, 0x00, 0x00, 0x00, 0x40, 0x05, 0x0F,
	0xFF,
	/* AC */
	0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07,
	0x08, 0x09, 0x0A, 0x0B, 0x0C, 0x0D, 0x0E, 0x0F,
	0x41, 0x00, 0x0F, 0x00,	0x00
};

////////////////////////////////////////
///          Common stuff           ////
////////////////////////////////////////

static uint16_t
	__vga_width,				// VGA width
	__vga_height,				// VGA height
	__vga_index,				// Current VGA buffer index
	*__vga_buffer,				// Pointer to video memory
	*vga_textmode_back_buffer;	// Pointer to VGA textmode 

static uint8_t
	__vga_bgcolor,				// Font background color
	__vga_fgcolor;				// Font foreground color

bool __initialized;

/* Getters */

uint16_t vga_get_width()
{ return __vga_width; }

uint16_t vga_get_height()
{ return __vga_height; }

uint16_t vga_get_index()
{ return __vga_index; }

uint8_t vga_get_bgcolor()
{ return __vga_bgcolor; }

uint8_t vga_get_fgcolor()
{ return __vga_fgcolor; }

/* Setters */

uint16_t vga_set_index(uint16_t index)
{ return __vga_index = index; }

uint8_t vga_set_bgcolor(uint8_t bgcolor)
{ return __vga_bgcolor = bgcolor; }

uint8_t vga_set_fgcolor(uint8_t fgcolor)
{ return __vga_fgcolor = fgcolor; }

/* Other */

void vga_write_registers(uint8_t *regs)
{
	uint16_t i;

	// Write MISCELLANEOUS reg
	outb(VGA_MISC_WRITE, *regs++);

	// Write SEQUENCER regs */
	for (i = 0; i < VGA_NUM_SEQ_REGS; ++i)
	{
		outb(VGA_SEQ_INDEX, i);
		outb(VGA_SEQ_DATA, *regs++);
	}
	// Unlock CRTC registers
	outb(VGA_CRTC_INDEX, 0x03);
	outb(VGA_CRTC_DATA, inb(VGA_CRTC_DATA) | 0x80);
	outb(VGA_CRTC_INDEX, 0x11);
	outb(VGA_CRTC_DATA, inb(VGA_CRTC_DATA) & ~0x80);

	// Make sure they remain unlocked
	regs[0x03] |= 0x80;
	regs[0x11] &= ~0x80;

	// Write CRTC regs
	for (i = 0; i < VGA_NUM_CRTC_REGS; ++i)
	{
		outb(VGA_CRTC_INDEX, i);
		outb(VGA_CRTC_DATA, *regs++);
	}

	// Write GRAPHICS CONTROLLER regs
	for (i = 0; i < VGA_NUM_GC_REGS; ++i)
	{
		outb(VGA_GC_INDEX, i);
		outb(VGA_GC_DATA, *regs++);
	}

	// Write ATTRIBUTE CONTROLLER regs
	for (i = 0; i < VGA_NUM_AC_REGS; ++i)
	{
		(void) inb(VGA_INSTAT_READ);
		outb(VGA_AC_INDEX, i);
		outb(VGA_AC_WRITE, *regs++);
	}

	// Lock 16-color palette and unblank display
	(void) inb(VGA_INSTAT_READ);
	outb(VGA_AC_INDEX, 0x20);
}

static void vga_set_plane(uint16_t p)
{
	p &= 3;
	uint8_t pmask = 1 << p;

	// Set read plane
	outb(VGA_GC_INDEX, 4);
	outb(VGA_GC_DATA, p);

	// Set write plane
	outb(VGA_SEQ_INDEX, 2);
	outb(VGA_SEQ_DATA, pmask);
}

static uint16_t vga_get_fb_segment()
{
	outb(VGA_GC_INDEX, 6);

	uint16_t seg = inb(VGA_GC_DATA);
	seg >>= 2;
	seg &= 3;

	switch (seg)
	{
		case 0:
		case 1:
			seg = 0xA000;
			break;

		case 2:
			seg = 0xB000;
			break;

		case 3:
			seg = 0xB800;
			break;
	}

	return seg;
}

uint16_t vga_entry(uint8_t ch, uint8_t bgcolor, uint8_t fgcolor);

// Clears the screen.
void vga_clear()
{
	if (__vga_width == 80)
		for (
			uint16_t i = 0;
			i < 80 * 25;
			vga_textmode_back_buffer[i++] = vga_entry(0, BLACK, WHITE)
		);

	vga_set_next_line_index(1);
	vga_set_index(0);

#ifndef VESA
	if (__vga_width > 90)
		memset((uint8_t *) GRAPHICS_MODE_ADDRESS, 0, VGA_WIDTH * VGA_HEIGHT);
	else
		for (uint16_t i = 0; i < __vga_width * __vga_height; ++i)
			__vga_buffer[i]	= vga_textmode_back_buffer[i] =
				vga_entry(0, BLACK, WHITE);
#else
	void vesa_draw_fill_rect(
		uint16_t x1, uint16_t y1, uint16_t x2, uint16_t y2, uint32_t color
	);

	uint16_t offset_x =
		(VESA_WIDTH - VGA_WIDTH * (VGA_WIDTH < 320 ? 8 : 2)) / 2;
	uint16_t offset_y =
		(VESA_HEIGHT - VGA_HEIGHT * (VGA_HEIGHT < 200 ? 16 : 2)) / 2;

	vesa_draw_fill_rect(
		offset_x,
		offset_y,
		offset_x + (VGA_WIDTH  < 320 ? 80 *  8 : VGA_WIDTH  * 2),
		offset_y + (VGA_HEIGHT < 200 ? 25 * 16 : VGA_HEIGHT * 2),
		0
	);
#endif
}

////////////////////////////////////////
///    Textmode-specific content    ////
////////////////////////////////////////

uint16_t
	__vga_textmode_back_buffer[25][80];	// VGA textmode back bufferback buffer
static uint8_t
	__vga_next_line_index,				// Row index of the next line
	__vga_cursor_x,						// X coordinate of the cursor
	__vga_cursor_y;						// Y coordinate of the cursor
static bool __mario = false;

/* Getters */

uint16_t vga_get_next_line_index()
{ return __vga_next_line_index; }

uint8_t vga_get_cursor_x()
{ return __vga_cursor_x; }

uint8_t vga_get_cursor_y()
{ return __vga_cursor_y; }

/* Setters */

uint16_t vga_set_next_line_index(uint16_t index)
{ return __vga_next_line_index = index; }

uint8_t vga_set_cursor_x(uint8_t x)
{ return __vga_cursor_x = x; }

uint8_t vga_set_cursor_y(uint8_t y)
{ return __vga_cursor_y = y; }

/* VGA textmode entries */

uint16_t vga_entry(uint8_t ch, uint8_t bgcolor, uint8_t fgcolor)
{ return (ch | (((bgcolor << 4) | (fgcolor & 0x0F)) << 8)); }

uint16_t vga_invert_entry(uint16_t pos)
{
	return
		((vga_textmode_back_buffer[pos] & 0xF000) >> 4) |
		((vga_textmode_back_buffer[pos] & 0x0F00) << 4) |
		((vga_textmode_back_buffer[pos] & 0x00FF) << 0);
}

uint16_t vga_get_entry_at(uint16_t pos)
{ return vga_textmode_back_buffer[pos]; }

vga_entry_t vga_get_entry(uint16_t entry)
{
	vga_entry_t res;

	uint8_t attrs = (entry & 0xFF00) >> 8;

	res.ch = (entry & 0x00FF);
	res.fg = (attrs & 0x0F);
	res.bg = (attrs & 0xF0) >> 4;

	return res;
}

void vga_set_entry(uint16_t pos, uint16_t entry)
{
#ifndef VESA
	__vga_buffer[pos] = entry;
#else
	if (VGA_WIDTH < 320)
	{
		vga_entry_t e = vga_get_entry(entry);

		void vesa_draw_char_at(uint8_t, uint16_t, char, uint8_t);

		vesa_draw_char_at(e.ch, pos, e.bg, e.fg);
	}
#endif
	vga_textmode_back_buffer[pos] = entry;
}

/* Printing to display */

void vesa_draw_char_at(uint8_t ch, uint16_t pos, char bgcolor, uint8_t fgcolor)
{
	uint32_t
		bg = color_vga_to_vesa(bgcolor),
		fg = color_vga_to_vesa(fgcolor);

	uint16_t offset_x = (VESA_WIDTH  - VGA_WIDTH  *  8) / 2;
	uint16_t offset_y = (VESA_HEIGHT - VGA_HEIGHT * 16) / 2;

	void vesa_draw_char(uint16_t, uint16_t, uint8_t, bool, uint32_t, uint32_t);
	vesa_draw_char(
		vga_get_x(pos) * 8 + offset_x,
		vga_get_y(pos) * 16 + offset_y,
		ch,
		bgcolor > -1,
		bg,
		fg
	);
}

void vga_textmode_print_new_line()
{
	if (vga_get_y(vga_get_index()) < VGA_HEIGHT)
		__vga_index = vga_get_pos(0, __vga_next_line_index++);

	if (vga_get_y(vga_get_index()) >= VGA_HEIGHT)
	{
		uint16_t i;
		// Move the current text chunk that makes up the screen
		// back in the buffer by a line
		for (i = 0; i < (VGA_HEIGHT - 1) * VGA_WIDTH; ++i)
		{
			vga_textmode_back_buffer[i] = vga_textmode_back_buffer[i+VGA_WIDTH];
			__vga_buffer[i] = vga_textmode_back_buffer[i];
		}

		// The last line should now be blank. Do this by writing
		// 80 spaces to it.
		for (i = (VGA_HEIGHT - 1) * VGA_WIDTH; i < VGA_HEIGHT * VGA_WIDTH; ++i)
			__vga_buffer[i] = vga_textmode_back_buffer[i] =
				vga_entry(0, BLACK, WHITE);

#ifdef VESA
		vesa_draw_tty_bg();

		for (i = 0; i < VGA_HEIGHT * VGA_WIDTH; ++i)
		{
			vga_entry_t entry = vga_get_entry(vga_textmode_back_buffer[i]);

			vesa_draw_char_at(entry.ch, i, entry.bg, entry.fg);
		}
#endif

		// The cursor should now be on the last line.
		vga_set_cursor_y(24);
		vga_set_index(vga_get_pos(0, 24));

		mouse_new_line();
	}
}

void vga_draw_char_at(
	uint8_t ch, uint16_t pos, uint8_t bgcolor, uint8_t fgcolor
)
{
	if (__vga_width >= 320)
	{
		char str[2] = { ch, '\0' };
		vga_draw_string(
			vga_get_x(pos), vga_get_y(pos), str, bgcolor, fgcolor
		);
	}
	else
		vga_set_entry(pos, vga_entry(ch, bgcolor, fgcolor));
}

static void vga_draw_colored_char(
	uint8_t ch, uint8_t bgcolor, uint8_t fgcolor
)
{
	uint8_t
		curr_x = vga_get_x(__vga_index),
		curr_y = vga_get_y(__vga_index);

	switch (ch)
	{
		case '\n':
			vga_textmode_print_new_line();
			break;

		case '\r':
			vga_set_index(vga_get_pos(0, curr_y));
			break;

		case '\t':
			vga_set_index(vga_get_pos(((curr_x + 4) & ~(4 - 1)), curr_y));
			break;

		default:
			if (__vga_index >= 90 * 60)
				vga_textmode_print_new_line();

			vga_draw_char_at(ch, __vga_index++, bgcolor, fgcolor);
	}
}

void vga_draw_char(uint8_t ch)
{ vga_draw_colored_char(ch, __vga_bgcolor, __vga_fgcolor); }

/* Other */

// Sets VGA font.
void write_font(uint8_t *buf, uint8_t font_height)
{
	uint8_t seq2, seq4, gc4, gc5, gc6;
	uint32_t i;

	// Save registers
	// vga_set_plane() modifies GC 4 and SEQ 2, so save them as well
	outb(VGA_SEQ_INDEX, 2);
	seq2 = inb(VGA_SEQ_DATA);

	outb(VGA_SEQ_INDEX, 4);
	seq4 = inb(VGA_SEQ_DATA);

	// Turn off even-odd addressing (set flat addressing)
	// assume: chain-4 addressing already off
	outb(VGA_SEQ_DATA, seq4 | 0x04);

	outb(VGA_GC_INDEX, 4);
	gc4 = inb(VGA_GC_DATA);

	outb(VGA_GC_INDEX, 5);
	gc5 = inb(VGA_GC_DATA);

	// Turn off even-odd addressing
	outb(VGA_GC_DATA, gc5 & ~0x10);

	outb(VGA_GC_INDEX, 6);
	gc6 = inb(VGA_GC_DATA);

	// Turn off even-odd addressing
	outb(VGA_GC_DATA, gc6 & ~0x02);

	// Wite font to plane P4
	vga_set_plane(2);

	// Write font 0
	for (i = 0; i < 256; ++i)
	{
		vmemwr(vga_get_fb_segment(), 16384u * 0 + i * 32, buf, font_height);
		buf += font_height;
	}

	// Restore registers
	outb(VGA_SEQ_INDEX, 2);
	outb(VGA_SEQ_DATA, seq2);
	outb(VGA_SEQ_INDEX, 4);
	outb(VGA_SEQ_DATA, seq4);
	outb(VGA_GC_INDEX, 4);
	outb(VGA_GC_DATA, gc4);
	outb(VGA_GC_INDEX, 5);
	outb(VGA_GC_DATA, gc5);
	outb(VGA_GC_INDEX, 6);
	outb(VGA_GC_DATA, gc6);
}

// Sets 16 color colorpalette for textmode.
void vga_set_color_palette_textmode()
{
	vga_setpal( 0, 0x00, 0x00, 0x00); // Black
	vga_setpal( 1, 0x00, 0x00, 0xAA); // Blue
	vga_setpal( 2, 0x00, 0xAA, 0x00); // Green
	vga_setpal( 3, 0x00, 0xAA, 0xAA); // Cyan
	vga_setpal( 4, 0xAA, 0x00, 0x00); // Red
	vga_setpal( 5, 0xAA, 0x00, 0xAA); // Magenta
	vga_setpal(20, 0xAA, 0x55, 0x00); // Brown
	vga_setpal(55, 0xAA, 0xAA, 0xAA); // Bright grey
	vga_setpal(56, 0x55, 0x55, 0x55); // Grey
	vga_setpal(57, 0x55, 0x55, 0xFF); // Bright Blue
	vga_setpal(58, 0x55, 0xFF, 0x55); // Bright Green
	vga_setpal(59, 0x55, 0xFF, 0xFF); // Bright Cyan
	vga_setpal(60, 0xFF, 0x55, 0x55); // Bright Red
	vga_setpal(61, 0xFF, 0x55, 0xFF); // Bright Magenta
	vga_setpal(62, 0xFF, 0xFF, 0x55); // Yellow
	vga_setpal(63, 0xFF, 0xFF, 0xFF); // White
}

void set_vga_char(uint8_t c, uint8_t *ch)
{
	for (uint8_t _y = 0; _y < 16; ++_y)
#ifdef USE_FONT2
		vesa_8x16_font[c * 16 + _y] = ch[_y];
#else
		vga_textmode_8x16_font[c * 16 + _y] = ch[_y];
#endif
};

// Initializes 80x25 or 90x60 textmode.
void init_vga_textmode(bool hi_res)
{
#ifndef VESA
	uint8_t rows, cols, height;

	if (hi_res)
	{
		vga_write_registers(vga_90x60_text);
		cols   = 90;
		rows   = 60;
		height = 8;
	}
	else
	{
		vga_write_registers(vga_80x25_text);
		cols   = 80;
		rows   = 25;
		height = 16;
	}

	// Set the font
	if (height >= 16)
	{
#else
	UNUSED(hi_res);
#endif
		if (!__mario)
		{
			extern uint8_t mario[4][16];

			set_vga_char(17, mario[0]);
			set_vga_char(18, mario[1]);
			set_vga_char(19, mario[2]);
			set_vga_char(20, mario[3]);

			__mario = true;
		}
#ifndef VESA
		write_font(vga_textmode_8x16_font, 16);
	}
	else
		write_font(vga_textmode_8x8_font, 8);

	// Tell the BIOS what we've done, so BIOS text output works OK
#pragma GCC diagnostic ignored "-Warray-bounds"
	pokew(0x40, 0x4A, cols);			// Columns on screen
	pokew(0x40, 0x4C, cols * rows * 2); // Framebuffer size
	pokew(0x40, 0x50, 0);				// Cursor pos'n
#pragma GCC diagnostic ignored "-Wstringop-overflow"
	pokeb(0x40, 0x60, height - 1);		// Cursor shape
	pokeb(0x40, 0x61, height - 2);
	pokeb(0x40, 0x84, rows - 1);		//* Rows on screen - 1
	pokeb(0x40, 0x85, height);			// Char height
#pragma GCC diagnostic pop
#pragma GCC diagnostic pop

	vga_cursor_off();
	vga_blink_disable();

	vga_set_color_palette_textmode();

	__vga_buffer = (uint16_t *) TEXTMODE_ADDRESS;
	vga_textmode_back_buffer = (uint16_t *) &__vga_textmode_back_buffer;
	vga_clear();

	vga_set_bgcolor(BLACK);
	vga_set_fgcolor(WHITE);

	if (!__initialized)
	{
		for (uint8_t i = 0; i < 16; vga_draw_colored_char(' ', i++, 0));
		puts("");
		KOK("Initialized VGA textmode");

		__initialized = true;
	}
#endif
}

////////////////////////////////////////
/// Graphics mode-specific content  ////
////////////////////////////////////////

static void (*__vga_draw_pixel)(uint16_t, uint16_t, uint8_t);

static inline void vga_draw_pixel_4p(uint16_t x, uint16_t y, uint16_t color)
{
	uint16_t wd_in_bytes = __vga_width / 8;
	uint16_t off = wd_in_bytes * y + x / 8;

	x = (x & 7) * 1;

	uint16_t mask = 0x80 >> x;
	uint16_t pmask = 1;

	for (uint16_t p = 0; p < 4; ++p)
	{
		vga_set_plane(p);

		if (pmask & color)
			pokeb(vga_get_fb_segment(), off, peekb(off) | mask);
		else
			pokeb(vga_get_fb_segment(), off, peekb(off) & ~mask);

		pmask <<= 1;
	}
}

static inline void vga_draw_pixel_8(uint16_t x, uint16_t y, uint16_t color)
{ pokeb(GRAPHICS_MODE_SEGMENT, x + y * __vga_width, color); }

void vga_draw_pixel(uint16_t x, uint16_t y, uint8_t color)
{ __vga_draw_pixel(x, y, color); }

// Sets 256 color colorpalette for 320x200 mode.
void vga_set_color_palette_320x200()
{
	for (uint16_t i = 0; i < 255; ++i)
	{
		vga_setpal(
			i,						// Color index
			vga_256_palette[i][0],	// Red
			vga_256_palette[i][1],	// Green
			vga_256_palette[i][2]	// Blue
		);
	}
}

void vga_draw_fill_rect(
	uint16_t x1, uint16_t y1, uint16_t x2, uint16_t y2, uint8_t color
)
{
	for (uint16_t y = y1; y < y2; ++y)
		for (uint16_t x = x1; x < x2; ++x)
			__vga_draw_pixel(x, y, color);
}

////////////////////////////////////////
///         Main initializer        ////
////////////////////////////////////////

void init_vga(uint16_t width, uint16_t height)
{
	if (
		(width ==  80 && height ==  25) ||
		(width ==  90 && height ==  60) ||
		(width == 320 && height == 200) ||
		(width == 720 && height == 480)
	)
	{
		__vga_width           = width;
		__vga_height          = height;
		__vga_index           = 0;
		__vga_next_line_index = 1;
		__vga_bgcolor         = BLACK;
		__vga_fgcolor         = WHITE;

#ifndef VESA
		switch (width)
		{
			case 80:
#endif
				init_vga_textmode(0);
#ifndef VESA
				return;

			case 90:
				init_vga_textmode(1);
				return;

			case 320:
				__vga_draw_pixel = (void *) vga_draw_pixel_8;
				vga_write_registers(vga_320x200x256);
				vga_set_color_palette_320x200();
				vga_clear();
				return;

			case 720:
				__vga_draw_pixel = (void *) vga_draw_pixel_4p;
				vga_write_registers(vga_720x480x16);
				vga_clear();
				return;
		}
#else
		(void) vga_320x200x256;
		(void) vga_720x480x16;

		__vga_draw_pixel = (void *) vesa_draw_vga_pixel;

		vga_clear();
#endif
	}
}

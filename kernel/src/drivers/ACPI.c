/**
 * Source:
 * REF: https://forum.osdev.org/viewtopic.php?t=16990
 */

#include <io/ports.h>
#include <utils.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <unistd.h>

uint8_t  ACPI_ENABLE, ACPI_DISABLE, PM1_CNT_LEN;
uint16_t SLP_TYPa, SLP_TYPb, SLP_EN, SCI_EN;
uint32_t *SMI_CMD, *PM1a_CNT, *PM1b_CNT;

typedef struct
{
	uint8_t  signature[8];
	uint8_t  checksum;
	uint8_t  OEM_ID[6];
	uint8_t  revision;
	uint32_t *RSDT_address;
} RSDPtr_t;

typedef struct
{
	uint8_t  signature[4];
	uint32_t length;
	uint8_t  unneded1[32];
	uint32_t *DSDT;
	uint8_t  unneded2[4];
	uint32_t *SMI_CMD;
	uint8_t  ACPI_ENABLE;
	uint8_t  ACPI_DISABLE;
	uint8_t  unneded3[10];
	uint32_t *PM1a_CNT_BLK;
	uint32_t *PM1b_CNT_BLK;
	uint8_t  unneded4[17];
	uint8_t  PM1_CNT_LEN;
} FACP_t;

// check if the given address has a valid header
uint32_t *ACPI_check_RSD_ptr(uint32_t *ptr)
{
	char *sig = "RSD PTR ";
	RSDPtr_t *rsdp = (RSDPtr_t *) ptr;
	uint8_t *bptr;
	uint8_t check = 0;

	if (memcmp(sig, rsdp, 8) == 0)
	{
		// check checksum rsdpd
		bptr = (uint8_t *) ptr;

		for (uint8_t i = 0; i < sizeof(RSDPtr_t); ++i)
			check += *(bptr + i);

		// found valid rsdpd
		if (check == 0)
			return (uint32_t *) rsdp->RSDT_address;
	}

	return NULL;
}

// finds the acpi header and returns the address of the rsdt
uint32_t *ACPI_get_RSD_ptr()
{
	uint32_t *addr = (uint32_t *) 0x000E0000;
	uint32_t *rsdp;

	// search below the 1mb mark for RSDP signature
	while ((uint32_t) addr < 0x00100000)
	{
		rsdp = ACPI_check_RSD_ptr(addr);

		if (rsdp != NULL)
			return rsdp;

		addr += 0x10 / sizeof(addr);
	}

	// at address 0x40:0x0E is the RM segment of the ebda
#pragma GCC diagnostic ignored "-Warray-bounds"
	uint32_t ebda = *((short *) 0x40E) * 0x10 & 0x000FFFFF;
#pragma GCC diagnostic pop

	// search Extended BIOS Data Area for the Root System Description
	// Pointer signature
	addr = (uint32_t *) ebda;

	while ((uint32_t) addr < ebda + 1024)
	{
		rsdp = ACPI_check_RSD_ptr(addr);

		if (rsdp != NULL)
			return rsdp;

		addr += 0x10 / sizeof(addr);
	}

	return NULL;
}

// checks for a given header and validates checksum
int ACPI_check_header(uint32_t *ptr, char *sig)
{
	if (memcmp(ptr, sig, 4) == 0)
	{
		char *check_ptr	= (char *) ptr;
		int len			= *(ptr + 1);
		char check		= 0;

		for (uint16_t i = 0; i < len; ++i)
			check += *(check_ptr + i);

		if (check == 0)
			return 0;
	}

	return -1;
}

int ACPI_enable()
{
	// check if acpi is enabled
	if ((inw((uint32_t) PM1a_CNT) & SCI_EN) == 0)
	{
		// check if acpi can be enabled
		if (SMI_CMD != 0 && ACPI_ENABLE != 0)
		{
			outb((uint32_t) SMI_CMD, ACPI_ENABLE); // send acpi enable command
			// give 3 seconds time to enable acpi
			int i;

			for (i = 0; i < 300; ++i)
			{
				if ((inw((uint32_t) PM1a_CNT) & SCI_EN) == 1)
					break;

				usleep(10);
			}

			if (PM1b_CNT != 0)
				for (; i < 300; ++i)
				{
					if ((inw((uint32_t) PM1b_CNT) & SCI_EN) == 1)
						break;

					usleep(10);
				}

			if (i < 300)
			{
				KOK("Enabled ACPI");
				return 0;
			}
			else
			{
				KERR("Couldn't enable ACPI");
				return -1;
			}
		}
		else
		{
			KERR("No known way to enable acpi");
			return -1;
		}
	}
	else // already enabled
		return 0;
}

int ACPI_init()
{
	uint32_t *ptr = ACPI_get_RSD_ptr();

	// check if address is correct  ( if acpi is available on this pc )
	if (ptr != NULL && ACPI_check_header(ptr, "RSDT") == 0)
	{
		// the RSDT contains an unknown number of pointers to acpi tables
		int16_t entries = (*(ptr + 1) - 36) / 4;
		ptr += 9;	// skip header information

		while (0 < entries--)
		{
			// check if the desired table is reached
			if (ACPI_check_header((uint32_t *) *ptr, "FACP") == 0)
			{
				entries			= -2;
				FACP_t *facp	= (FACP_t *) *ptr;

				if (ACPI_check_header((uint32_t *) facp->DSDT, "DSDT") == 0)
				{
					// search the \_S5 package in the DSDT
					char *S5Addr	= (char *) facp->DSDT + 36; // skip header
					int dsdtlength	= *(facp->DSDT + 1) - 36;

					while (0 < dsdtlength--)
					{
						if (memcmp(S5Addr, "_S5_", 4) == 0)
							break;

						S5Addr++;
					}

					// check if \_S5 was found
					if (dsdtlength > 0)
					{
						// check for valid AML structure
						if (
							(
								*(S5Addr - 1) == 0x08 ||
								(
									*(S5Addr - 2) == 0x08 &&
									*(S5Addr - 1) == '\\'
								)
							) &&
							*(S5Addr + 4) == 0x12
						)
						{
							S5Addr	+= 5;
							// calculate Pkglength size
							S5Addr	+= ((*S5Addr & 0xC0) >> 6) + 2;

							if (*S5Addr == 0x0A)
								S5Addr++;	// skip uint8_tprefix

							SLP_TYPa = *(S5Addr) << 10;
							S5Addr++;

							if (*S5Addr == 0x0A)
								S5Addr++;	// skip uint8_tprefix

							SLP_TYPb		= *(S5Addr) << 10;

							SMI_CMD			= facp->SMI_CMD;

							ACPI_ENABLE		= facp->ACPI_ENABLE;
							ACPI_DISABLE	= facp->ACPI_DISABLE;

							PM1a_CNT		= facp->PM1a_CNT_BLK;
							PM1b_CNT		= facp->PM1b_CNT_BLK;
							
							PM1_CNT_LEN		= facp->PM1_CNT_LEN;

							SLP_EN			= 1 << 13;
							SCI_EN			= 1;

							KOK("Initialized ACPI");

							return 0;
						}
						else
							KERR("\\_S5 parse error");
					}
					else
						KERR("\\_S5 not present");
				}
				else
					KERR("DSDT invalid");
			}
			ptr++;
		}
		KERR("No valid FACP present");
	}
	else
		KERR("No acpi");

	return -1;
}

void ACPI_power_off()
{
	// SCI_EN is set to 1 if acpi shutdown is possible
	if (SCI_EN == 0)
		return;

	// send the shutdown command
	outw((uint32_t) PM1a_CNT, SLP_TYPa | SLP_EN );

	if (PM1b_CNT != 0)
		outw((uint32_t) PM1b_CNT, SLP_TYPb | SLP_EN );

	KERR("Acpi poweroff failed.\n");
}

/* This file is part of RexOS!.
 *
 * RexOS! is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * RexOS! is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with RexOS!.  If not, see <https://www.gnu.org/licenses/>.
 */

// REF: https://wiki.osdev.org/PC_Speaker

#include <io/ports.h>
#include <stdint.h>
#include <unistd.h>

static void play_sound(uint32_t freq)
{
	uint32_t div;
	uint8_t tmp;

	div = 1193180 / freq;
	outb(0x43, 0xb6);
	outb(0x42, (uint8_t) (div));
	outb(0x42, (uint8_t) (div >> 8));

	tmp = inb(0x61);
	if (tmp != (tmp | 3))
		outb(0x61, tmp | 3);
}

static void nosound()
{ outb(0x61, (inb(0x61) & 0xFC)); }

void beep(uint32_t freq, uint8_t time)
{
	play_sound(freq);
	usleep(time);
	nosound();
}

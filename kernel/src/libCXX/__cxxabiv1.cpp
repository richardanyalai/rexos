/* This file is part of RexOS!.
 *
 * RexOS! is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * RexOS! is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with RexOS!.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <stdint.h>

namespace __cxxabiv1
{
	// The ABI requires a 64-bit type
	__extension__ typedef int __guard __attribute__((mode(__DI__)));

	extern "C" int	__cxa_guard_acquire	(__guard *);
	extern "C" void	__cxa_guard_release	(__guard *);
	extern "C" void	__cxa_guard_abort	(__guard *);

	extern "C" int	__cxa_guard_acquire	(__guard *g)
	{ return !*(char *) (g); }

	extern "C" void	__cxa_guard_release	(__guard *g)
	{ *(char *) g = 1; }

	extern "C" void	__cxa_guard_abort	(__guard *) {}
}

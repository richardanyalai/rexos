/* This file is part of RexOS!.
 *
 * RexOS! is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * RexOS! is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with RexOS!.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <string.hpp>
#include <stdint.h>
#include <string.h>
#include <stdlib.h>

namespace std
{
	string::string() : buf(nullptr), size(0)
	{}

	string::string(const char *cstr)
	{
		size = strlen(cstr);
		buf = new char[size+1];
		strncpy(buf, cstr, size);
	}

	string::string(const string& str)
	{
		if (str.buf != nullptr)
		{
			size = str.size;
			buf = new char[size+1];
			strncpy(buf, str.buf, size);
		}
	}

	string::string(string&& dying)
	{
		// Cleanup any existing data.
		__cleanup__();

		// Copy data from the incoming strect.
		size = dying.size;
		
		// Transfer ownership of underlying char buffer
		// from incoming strect to this strect.
		buf = dying.buf;
		dying.buf = nullptr;
	}

	string::~string()
	{ __cleanup__(); }

	char string::operator [] (uint16_t index)
	{ return string::c_str()[index]; }

	string& string::operator = (const string& str)
	{ return string::operator = (str.buf); }

	string& string::operator = (string&& dying)
	{
		// Cleanup any existing data.
		__cleanup__();

		// Copy data from the incoming strect.
		size = dying.size;
		
		// Transfer ownership of underlying char buffer
		// from incoming strect to this strect.
		buf = dying.buf;
		dying.buf = nullptr;

		return *this;
	}

	string& string::operator = (const char *cstr)
	{
		if (cstr && size != strlen(cstr) && !strstr(buf, cstr))
		{
			__cleanup__();

			size = strlen(cstr);
			buf = new char[size+1];
			strncpy(buf, cstr, size);
		}

		return *this;
	}

	string& string::operator += (const string& str)
	{ return string::operator += (str.buf); }

	string& string::operator += (const char *cstr)
	{
		if (cstr)
		{
			size += strlen(cstr);
			char *temp = new char[size+1];

			if (buf)
			{
				strncpy(temp, buf, size - strlen(cstr));
				delete[] buf;
			}

			strcat(temp, cstr);
			buf = temp;
		}

		return *this;
	}

	string& string::operator + (const string& str)
	{ return string::operator + (str.buf); }

	string& string::operator + (const char *cstr)
	{
		uint16_t newSize = strlen(buf) + strlen(cstr);

		char *temp = new char[newSize+1];

		if (buf)
			strncpy(temp, buf, size);

		strcat(temp, cstr);

		string *result = new std::string(temp);

		delete[] temp;

		return *result;
	}

	bool string::operator == (const string& str)
	{ return string::operator == (str.buf); }

	bool string::operator == (const char *cstr)
	{
		char *tmp = new char[strlen(cstr)];

		strcpy(tmp, cstr);

		bool matches = !strcmp(buf, cstr);

		delete [] tmp;

		return matches;
	}

	bool string::operator != (const string& str)
	{ return !(string::operator == (str.buf)); }

	bool string::operator != (const char *cstr)
	{ return !(string::operator == (cstr)); }

	void string::__cleanup__()
	{
		if (buf)
			delete[] buf;

		size = 0;
	}

	uint16_t string::length()
	{ return size; }

	char *string::c_str() const
	{ return buf; }

	const char *endl = "\n\0";
}

/* *This file is part of RexOS!.
 *
 * RexOS! is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * RexOS! is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with RexOS!.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <iostream.hpp>
#include <stdint.h>
#include <stdio.h>
#include <string.hpp>

namespace std
{
	ostream::ostream()
	{}

	ostream::~ostream()
	{}

	ostream& ostream::operator << (const string& str)
	{
		printf("%s", str.c_str());
		return *this;
	}

	ostream& ostream::operator << (const char *cstr)
	{
		printf("%s", cstr);
		return *this;
	}

	ostream& ostream::operator << (int num)
	{
		printf("%d", num);
		return *this;
	}

	ostream& ostream::operator << (double num)
	{
		printf("%.2f", num);
		return *this;
	}

	ostream& ostream::operator << (char c)
	{
		putchar(c);
		return *this;
	}
}

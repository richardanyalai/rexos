/* This file is part of RexOS!.
 *
 * RexOS! is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * RexOS! is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with RexOS!.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <stdint.h>
#include <string.hpp>

namespace std
{
	template <typename T>
	class vector
	{
	private:
		T *buffer; 			// A buffer to store all the elements
		uint32_t length;	// Lenngth of the vector
		uint32_t capacity;	// Lenngth of the vector

	public:
		// Returns the length (item count) of the vector
		uint32_t get_size();

		// Returns an iterator (head)
		T *begin();

		// Returns an iterator (tail)
		T *end();

		// Appends the given item at the end of the list
		void push_back(T item);

		// Returns the item at the given position
		T get(uint32_t index);

		// Removes and returns the last element
		T pop_back();

		// Clears the vector
		void clear();

		// Increase the capacity
		void reserve(uint32_t capacity);

		// Shortcut to add an item
		void operator += (T item);

		// Shortcut to get the item at the given position
		T operator [] (uint32_t index);

		vector<T> &operator=(const T items[]);

		// Parameterless constructor
		vector();

		// Constructor with capacity
		vector(uint32_t capacity);

		// Destructor
		~vector();
	};

	template class vector<uint8_t>;
	template class vector<uint16_t>;
	template class vector<uint32_t>;
	template class vector<int>;
	template class vector<float>;
	template class vector<std::string>;
}

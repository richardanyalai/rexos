/* This file is part of RexOS!.
 *
 * RexOS! is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * RexOS! is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with RexOS!.  If not, see <https://www.gnu.org/licenses/>.
 */

// REF: https://medium.com/swlh/write-your-own-c-stl-string-class-e20113a8de79
// REF: https://gist.github.com/philipheimboeck/099e540d800063e3e6ec

#ifndef __STRING_HPP
#define __STRING_HPP

#include <stdint.h>

namespace std
{
	class string
	{
		private:
			char *buf;		// String buffer
			uint16_t size;	// Buffer size

			void __cleanup__();

		public:
			// Returns the length of the char array
			uint16_t length();

			// Returns the buffer
			char *c_str() const;

			// Returns the character at the specified index
			char operator [] (uint16_t index);

			// Assignment operator with object
			string& operator = (const string& str);

			// Assignment operator with a dying object
			string& operator = (string&& dying);

			// Assignment operator with char *
			string& operator = (const char *cstr);

			// Increments the buffer of the string object 	with the buffer of
			// another string object
			string& operator += (const string& str);

			// Increments the buffer of the string object with a char *
			string& operator += (const char *cstr);

			// Concatenates the buffer of 2 string objects
			string& operator + (const string& str);

			// Concatenates the buffer of a string object with a char array
			string& operator + (const char *cstr);

			// Checks if this string object is equal to another string object
			bool operator == (const string& str);

			// Checks if the buffer is equal to a char array
			bool operator == (const char *cstr);

			// Checks if this string object is not equal to a string object
			bool operator != (const string& str);

			// Checks if the buffer is not equal to a  char array
			bool operator != (const char *cstr);

			// Default constructor
			string();

			// Constructor with char array
			string(const char *str);

			// Constructor with object
			string(const string& obj);

			// Constructor with a dying object
			string(string&& dying);

			// Destructor
			~string();
	};
}

#endif

/* This file is part of RexOS!.
 *
 * RexOS! is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * RexOS! is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with RexOS!.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef __IOSTREAM_HPP
#define __IOSTREAM_HPP

#include <stdint.h>
#include <string.hpp>

namespace std
{
	class ostream
	{
		public:
			// String to output
			ostream& operator << (const string& str);

			// C-string to output
			ostream& operator << (const char *cstr);

			// Integer to output
			ostream& operator << (int num);

			// Double to output
			ostream& operator << (double num);

			// Char to output
			ostream& operator << (char c);

			// Parameterless onstructor
			ostream();
	
			// Destructor
			~ostream();
	};

	// Static object for handling output stream
	static ostream cout;

	// End of the line (with line break character)
	extern const char *endl;
}

#endif

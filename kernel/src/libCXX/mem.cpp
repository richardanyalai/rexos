/* This file is part of RexOS!.
 *
 * RexOS! is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * RexOS! is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with RexOS!.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <stddef.h>
#include <stdint.h>
#include <stdlib.h>

void *operator new(size_t size)
{ return malloc(size); }

void *operator new[](size_t size) { return malloc(size); }

// Default placement versions of operator new.
inline void *operator new    (size_t, void *p) throw() { return p; }
inline void *operator new [] (size_t, void *p) throw() { return p; }

void operator delete(void *p)
{ free(p); }

void operator delete[](void *p) { free(p); }

// Default placement versions of operator delete.
inline void operator	delete    (void *, void *)   throw() {}
void operator			delete    (void *, uint32_t) throw() {}
inline void operator	delete [] (void *, void *)   throw() {}
void operator			delete [] (void *, uint32_t) throw() {}

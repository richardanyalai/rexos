/* This file is part of RexOS!.
 *
 * RexOS! is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * RexOS! is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with RexOS!.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <vector.hpp>

namespace std
{
	template <typename T>
	vector<T>::vector()
	{
		buffer   = 0;
		capacity = 0;
		length   = 0;
	}

	template <typename T>
	vector<T>::vector(uint32_t capacity)
	{
		buffer = new T[capacity];
		this->capacity = capacity;
		length = 0;
	}

	template <typename T>
	vector<T>::~vector()
	{ delete [] buffer; }

	template <typename T>
	uint32_t vector<T>::get_size()
	{ return length; }

	template <typename T>
	T *vector<T>::begin()
	{ return buffer; }

	template <typename T>
	T *vector<T>::end()
	{ return buffer + length; }

	template <typename T>
	void vector<T>::push_back(T item)
	{
		if (length >= capacity)
			reserve(capacity + 15);

		buffer[length++] = item;
	}

	template <typename T>
	T vector<T>::get(uint32_t index)
	{ return buffer[index]; }

	template <typename T>
	T vector<T>::pop_back()
	{ return buffer[--length]; }

	template <typename T>
	void vector<T>::clear()
	{
		length   = 0;
		capacity = 0;
		buffer   = 0;
	}

	template<typename T>
	void vector<T>::reserve(uint32_t capacity)
	{
		if (buffer == 0)
		{
			length   = 0;
			this->capacity = 0;
		}

		T *new_buffer = new T[capacity];
		
		uint32_t l = capacity < length ? capacity : length;

		for (uint32_t i = 0; i < l; ++i)
			new_buffer[i] = buffer[i];

		this->capacity = capacity;
		delete[] buffer;
		buffer = new_buffer;
	}

	template <typename T>
	void vector<T>::operator += (T item)
	{ vector<T>::push_back(item); }

	template <typename T>
	vector<T> &vector<T>::operator=(const T items[])
	{
		this->clear();

		for (uint16_t i = 0; i < 6; ++i)
			this->push_back(items[i]);

		return *this;
	}

	template <typename T>
	T vector<T>::operator [] (uint32_t index)
	{ return vector<T>::get(index); }
}

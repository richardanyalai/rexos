# RexOS terminal commands

```bash
help
```

Shows list of commands.

```bash
clear
```

Clears terminal buffer.

```bash
echo <text>
```

Prints the text to the screen.

```bash
ls
```

Lists the content of the directory.

```bash
cat <filename>
```

Prints the content of the file to the screen.

```bash
ogrep <switch> <expression> <filename>
```

Ogrep is a [grep](https://en.wikipedia.org/wiki/Grep) clone for RexOS with
limited abilities. You can only search for substrings of textfiles yet.

There are two switches:

- `-i` - Tells ogrep that it should perform a case insensitive search.
- `-c` - Enables color mode, so if ogrep finds the substring in the text file,
  it gets highlighted by a bright red foreground color.

```bash
sleep <millis>
```

Blocks the OS for the given time.

```bash
exit
```

Exits the terminal.

```bash
shutdown
```

Shuts the computer down.

```bash
restart
```

Restarts the computer.

```bash
uptime
```

Prints the time elapsed since booting.

```bash
clock
```

Prints current time.

```bash
date
```

Prints current date.

```bash
lspci <switch>
```

Lists PCI devices.

```bash
hexdump <input>
```

Prints the hexdump of the input. The input can be a file or a memory address.

```bash
bf <input>
```

Interprets _BrainFuck_ programs.

> WARNING: to use Ping, WebPussy and WebHost, you
need to follow [these instructions](./virtualbox.md#setting-up-network-device)
to configure networking in VirtualBox.

```bash
ping <IPv4 address>
```

Sends a ping message.

```bash
webpussy <switch> <port> <address>
```

WebPussy is a _netcat_ clone for RexOS. It supports both UDP and TCP protocols.

If you want to use WebPussy, you have to open a terminal tab before booting
RexOS in VirtualBox and start a `ncat` listener in that terminal session,
otherwise RexOS won't be able to communicate with the computer.

In WebPussy, the default protocol is UDP and the default IP address + port
combination is `127.0.0.1 1234`.

Available switches:

- `-l` - listen
- `-p` - port, you **MUST** also specify the IP address.
- `-u` - UDP
- `-t` - TCP

```bash
webhost
```

You have to boot RexOS, open up the RexOS terminal, launch WebHost by executing
`webhost`. It doesn't require any parameters and it will host the website
on [localhost:1234](http://127.0.0.1:1234).

After launching WebHost, open your web browser and navigate to the address
above.

```bash
license
```

Shows legal information.

```bash
tick
```

Prints current timer tick count.

```bash
random
```

Prints a random number.

```bash
randint <start> <end>
```

Prints a random number from the given interval.

```bash
beep
```

Makes a beep sound.

```bash
hascii <filename without extension>
```

Prints ascii art.

```bash
rexfetch
```

Prints information about the computer and OS.

```bash
cowjonas <text>
```

Makes a cow say the input text.

```bash
brot
```

Prints the Mandelbrot set using ASCII characters.

```bash
donut
```

Starts the donut.c program.

```bash
hangman
```

Starts the hangman game

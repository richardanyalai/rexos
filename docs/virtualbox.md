# VirtualBox

Setting up VirtualBox is only necessary if you want to use the networking
features of RexOS. It can also run RexOS with better performane.

## Table of contents

1. [Creating the virtual machine](#creating-the-virtual-machine)
1. [Setting up hard drive](#setting-up-hard-drive)
1. [Setting up network device](#setting-up-network-device)
1. [Running the virtual machine](#running-the-virtual-machine)

---

## Creating the virtual machine

- Open VirtualBox and hit the "New" button to create a new virtual machine.

- To make things easier, lets call it "RexOS" (some commands in the MakeFile
  rely on this)

- Set Type and Version to "Other", "Other/Unknown".

- 32MBs of RAM is more than enough.

- Lets create a hard disk with the size of 4MBs if you want to check whether the
  ATA driver recognizes it. You can also skip this step for now and add a hard
  disk image later.

---

## Setting up hard drive

- Open the settings for the RexOS virtual machine, and go to the `Storage`
  section.

- On the "Controller: IDE" panel click on the hard drive icon then choose your
  hard disk image.

- If you don't have any, just create a new VDI image, leave it as it is (dynamic
  size up to 2GB).

- It will be set as the `IDE Primary Device 0` (if this is the first hard disk
  you attached to the virtual machine).

For setting up a filesystem on that disk image, follow
[this tutorial](https://youtu.be/IGpUdIX-Z5A?t=577).

---

## Setting up network device

- Click on the settings 'gear' and choose `Network`.

- Make sure it says "Attached to: NAT".

- Click on `Advanced` and set "Adapter Type" to `PCnet-FAST III (Am79C973)` as
  the network device.

- Click on `Port Forwarding` and add a new rule:

- At the "Protocol field choose `UDP` and set `127.0.0.1` as "Host IP", `1234`
  as "Host Port" & "Guest Port", finally `10.0.2.15` as "Guest IP".

- After setting the UDP rule, right-click on this rule and choose "Copy Selected
  Rule".

- Set the protocol of the new rule to TCP.

For more details read the [WebPussy](./commands.md#webpussy),
[WebHost](./commands.md#webhost) and [Ping](./commands.md#ping) sections.

---

## Running the virtual machine

You can start the RexOS virtual machine by selecting RexOS in VirtualBox then
clicking on the green arrow button.

There is also a way to start it from your terminal emulator:

```bash
VBoxManage startvm "RexOS"
```

or by simply executing

```bash
make vbox
```

This will also kill all te currently running virtual machines.
